`include "nuplus_define.sv"
`include "user_define.sv"
`include "system_defines.sv"
`include "load_store_unit_defines.sv"

module tb_nuplus_system_mont256( );

	logic clk            = 1'b0;
	logic reset          = 1'b1;
	logic job_terminated;

	logic [1 : 0] mux_selection;

	logic             uart_tx;
	logic             uart_rx;
	logic [16 - 1 : 0]synch_tmp;
    localparam                     CLK_RATE              = 100000000;
    localparam                     CLK_PERIOD_NS         = 10;
    localparam                     DEFAULT_UART_BAUD     = 5000000;//921600;
    localparam                     CLK_PER_BIT           = ((CLK_RATE / DEFAULT_UART_BAUD) - 1);
    localparam                     BIT_PERIOD            = CLK_PER_BIT * CLK_PERIOD_NS;

	nuplus_system uut (
		.clk           ( clk            ),
		.reset         ( reset          ),
		.end_of_kernel ( job_terminated ),
		.mux_selection ( mux_selection  ),
		.uart_tx       ( uart_tx        ),
		.uart_rx       ( uart_rx        )
	);
	
	integer i, j, h, k;
    integer testvec_f, ret;
    integer output_file;
    
    logic   [`V_REGISTER_SIZE*2 - 1 : 0]                a[3:0];
    logic   [`V_REGISTER_SIZE*2 - 1 : 0]                b[3:0];
    logic   [`V_REGISTER_SIZE*2 - 1 : 0]                m[3:0];
    logic   [`V_REGISTER_SIZE*2 - 1 : 0]                r[3:0];
    logic   [`V_REGISTER_SIZE*2 - 1 : 0]                rp[3:0];
    logic   [`REGISTER_SIZE - 1     : 0]                mp[3:0];
    logic   [`V_REGISTER_SIZE*2     : 0]                tmp[3:0];
    logic   [`V_REGISTER_SIZE*2 - 1 : 0]                q[3:0];
    logic   [`V_REGISTER_SIZE*2 - 1 : 0]                q2[3:0];

	// Takes in input byte and serializes it
	task UART_WRITE_BYTE;
		input [7 : 0] data_in;
		integer i;
		begin
			// Send Start Bit
			uart_rx <= 1'b0;
			#( BIT_PERIOD );
			// Send Data Byte
			for ( i = 0; i < 8; i = i + 1 )
			begin
				uart_rx <= data_in[i];
				#( BIT_PERIOD );
			end
			// Send Stop Bit
			uart_rx <= 1'b1;
			#( BIT_PERIOD );
		end
	endtask // UART_WRITE_BYTE


	always #( CLK_PERIOD_NS/2 ) clk = ~clk;

	logic rx = 1'b0;
	initial begin
        
        testvec_f = $fopen({`PROJECT_PATH, "testvectors/mont256_vectors"}, "r");
		
		#100
		reset = 0;
		
		
                 
       
           
           #10 
           
     
       while(!$feof(testvec_f)) begin
                   
               for(j = 0; j < 256; j++) begin
                   uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_1.RAM[j] = 0;
                   uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_2.RAM[j] = 0;
                   uut.nuplus_core.operand_fetch1.scalar_reg_file.memory_bank_1r1w_1.RAM[j] = 0;
                   uut.nuplus_core.operand_fetch1.scalar_reg_file.memory_bank_1r1w_2.RAM[j] = 0;
               end
                           
               for(i = 0; i < 4; i++) begin
                   ret = $fscanf(testvec_f, "%h\n", a[i]);
                   ret = $fscanf(testvec_f, "%h\n", b[i]);
                   ret = $fscanf(testvec_f, "%h\n", m[i]);
                   ret = $fscanf(testvec_f, "%h\n", mp[i]);
                   ret = $fscanf(testvec_f, "%h\n", q[i]);

                   q2[i] = q[i] + m[i];
                   uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_1.RAM[0+64*i] = a[i][`V_REGISTER_SIZE*2 - 1 : `V_REGISTER_SIZE];
                   uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_1.RAM[1+64*i] = a[i][`V_REGISTER_SIZE - 1 : 0];
                   uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_1.RAM[2+64*i] = b[i][`V_REGISTER_SIZE*2 - 1 : `V_REGISTER_SIZE];
                   uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_1.RAM[3+64*i] = b[i][`V_REGISTER_SIZE - 1 : 0];
                   uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_1.RAM[12+64*i] = m[i][`V_REGISTER_SIZE*2 - 1 : `V_REGISTER_SIZE];
                   uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_1.RAM[13+64*i] = m[i][`V_REGISTER_SIZE - 1 : 0];
                       
                   uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_2.RAM[0+64*i] =  uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_1.RAM[0+64*i];
                   uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_2.RAM[1+64*i] =  uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_1.RAM[1+64*i];
                   uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_2.RAM[2+64*i] =  uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_1.RAM[2+64*i];
                   uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_2.RAM[3+64*i] =  uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_1.RAM[3+64*i];
                   uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_2.RAM[12+64*i] =  uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_1.RAM[12+64*i];
                   uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_2.RAM[13+64*i] =  uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_1.RAM[13+64*i];
                   
                   uut.nuplus_core.operand_fetch1.scalar_reg_file.memory_bank_1r1w_1.RAM[14+64*i] = mp[i][`REGISTER_SIZE-1 : 0];
                   uut.nuplus_core.operand_fetch1.scalar_reg_file.memory_bank_1r1w_2.RAM[14+64*i] = uut.nuplus_core.operand_fetch1.scalar_reg_file.memory_bank_1r1w_1.RAM[14+64*i];
               end 
                

		@ ( posedge nuplus_system.uart_control_unit.divisor_set );

		UART_WRITE_BYTE( `BOOT_CORE_COMMAND );
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Destination
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Thread_id
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Program counter
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Program counter
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Program counter
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Program counter
		#100 rx = ~rx;
		UART_WRITE_BYTE( `BOOT_CORE_COMMAND );
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Destination
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h01 ); //Thread_id
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Program counter
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Program counter
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Program counter
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Program counter
		#100 rx = ~rx;
		UART_WRITE_BYTE( `BOOT_CORE_COMMAND );
        #100 rx = ~rx;
        UART_WRITE_BYTE( 8'h00 ); //Destination
        #100 rx = ~rx;
        UART_WRITE_BYTE( 8'h02 ); //Thread_id
        #100 rx = ~rx;
        UART_WRITE_BYTE( 8'h00 ); //Program counter
        #100 rx = ~rx;
        UART_WRITE_BYTE( 8'h00 ); //Program counter
        #100 rx = ~rx;
        UART_WRITE_BYTE( 8'h00 ); //Program counter
        #100 rx = ~rx;
        UART_WRITE_BYTE( 8'h00 ); //Program counter
        #100 rx = ~rx;
        UART_WRITE_BYTE( `BOOT_CORE_COMMAND );
        #100 rx = ~rx;
        UART_WRITE_BYTE( 8'h00 ); //Destination
        #100 rx = ~rx;
        UART_WRITE_BYTE( 8'h03 ); //Thread_id
        #100 rx = ~rx;
        UART_WRITE_BYTE( 8'h00 ); //Program counter
        #100 rx = ~rx;
        UART_WRITE_BYTE( 8'h00 ); //Program counter
        #100 rx = ~rx;
        UART_WRITE_BYTE( 8'h00 ); //Program counter
        #100 rx = ~rx;
        UART_WRITE_BYTE( 8'h00 ); //Program counter
        #100 rx = ~rx;

		//#100 rx = ~rx;
		UART_WRITE_BYTE( `ENABLE_THREAD_COMMAND );
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Destination
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'b0000_1111 ); //Thread mask
		#100 rx = ~rx;



		#10000;
		
		UART_WRITE_BYTE( `ENABLE_THREAD_COMMAND );
        #100 rx = ~rx;
        UART_WRITE_BYTE( 8'h00 ); //Destination
        #100 rx = ~rx;
        UART_WRITE_BYTE( 8'b0000_0000 ); //Thread mask
        #100 rx = ~rx;
		
		 for(i = 0; i < 4; i++) begin
           if(uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_1.RAM[4+64*i][31:0] === 32'hFFFFFFFF) begin
               if((uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_1.RAM[10+64*i] ) !== 
               (q[i][127:0] ) || 
                   (uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_1.RAM[11+64*i]) !== 
                   (q[i][255:128] )) begin
                   $display("Assertion error at ", $time, " Thread: ", i);
                   $stop;
                   end
            end 
            else begin
              if((uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_1.RAM[8+64*i] ) !== 
              (q[i][127:0] ) || 
                          (uut.nuplus_core.operand_fetch1.vector_reg_file.memory_bank_1r1w_1.RAM[9+64*i] ) !== 
                          (q[i][255:128] )) begin
                          $display("Assertion error at ", $time, " Thread: ", i);
                          $stop;
                          end
           end
       end
          #1000;
        end
         
         $fclose(testvec_f);
         #10 $stop;    
	end


endmodule
