
## Clock signal
create_clock -period 5.000 -name clk -waveform {0.000 2.500} -add [get_ports clk]

## Reset false path
set_false_path -through [get_ports reset]

