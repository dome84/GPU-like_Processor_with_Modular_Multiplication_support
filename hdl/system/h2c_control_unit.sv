`include "system_defines.sv"
`include "load_store_unit_defines.sv"
`include "user_define.sv"
`include "nuplus_define.sv"
`include "synchronization_defines.sv"

module h2c_control_unit # (
		parameter ITEM_w     = 32
	)
	(
		input                                 clk,
		input                                 reset,
		//output logic         [7 : 0]          LED,
		
		// Interface to UART Control Unit
		input                [ITEM_w - 1 : 0] item_data_i,          // Input: items from outside
		input                                 item_valid_i,         // Input: valid signal associated with item_data_i port
		output logic                          item_avail_o,         // Output: avail signal to input port item_data_i
		output logic         [ITEM_w - 1 : 0] item_data_o,          // Output: items to outside
		output logic                          item_valid_o,         // Output: valid signal associated with item_data_o port
		input                                 item_avail_i,         // Input: avail signal to ouput port item_data_o
		input 								  job_terminated,
		
		// Interface To Nuplus for boot
		output logic                 [`THREAD_NUMB - 1 : 0] hi_thread_en,
		output logic                                        hi_job_valid,
		output address_t                                    hi_job_pc,
		output thread_id_t                                  hi_job_thread_id,
		
		//Sync
		output	sync_setup_t   					ni_setup_mess,
		output  logic          					ni_setup_mess_valid,
		input   logic          					setup_available,	
		//To DSU
		output  logic 							 ext_freeze,
		output  logic							 resume,
		output  logic      						 dsu_enable, 
		output  logic 							 dsu_single_step,
		output  address_t  [7 : 0]      		 dsu_breakpoint,
		output  logic 	  [7 : 0] 				 dsu_breakpoint_enable,
		output  logic                            dsu_thread_selection,
		output  thread_id_t						 dsu_thread_id,
		output  logic							 dsu_en_vector,
		output  logic							 dsu_en_scalar,
		output  logic							 dsu_load_shift_reg,
		output  logic							 dsu_start_shift, 
		output  logic [`REGISTER_ADDRESS - 1 : 0] dsu_reg_addr,
		
		output  logic							 dsu_write_scalar,
		output  logic							 dsu_write_vector,
		output  logic							 dsu_serial_reg_in,
		
		//From DSU	
		input address_t  [`THREAD_NUMB - 1 : 0] dsu_bp_instruction, 
		input thread_id_t                       dsu_bp_thread_id,
		input logic							 	dsu_serial_reg_out,
		input logic							 	dsu_stop_shift,
		input logic							 	dsu_hit_breakpoint
		
		) ;
	
	localparam 				RECEIVE_WORD_NUMB = 5;
	localparam				SEND_WORD_NUMB	= 16;
	localparam				TOTAL_RECEIVE_BIT = RECEIVE_WORD_NUMB*ITEM_w;
	localparam				TOTAL_SEND_BIT = SEND_WORD_NUMB*ITEM_w;
	
	assign ext_freeze = 1'b0;
	
	h2c_cu_state_t							  				state_control, next_state_control, dsu_state_control_supp;
	logic                  [4:0]             				word_uart_receive, word_uart_sent; 
	host_messages_t                           				cmd_host_in, dsu_ack; 
	logic                  [TOTAL_RECEIVE_BIT - 1 : 0]      uart_buffer_in;
	logic				   [TOTAL_SEND_BIT - 1 : 0]			uart_buffer_out; 	
	breakpoint_info_t 							    		bp_info;
	logic 													is_vector_read, is_scalar_read;
	logic													end_detected;
//	logic													hit_breakpoint;
	
	assign cmd_host_in                         = host_messages_t'( item_data_i );
	assign dsu_reg_addr						   = uart_buffer_in[`REGISTER_ADDRESS - 1 : 0];
	
	always_ff @ ( posedge clk, posedge reset ) begin
		if ( reset ) begin
			end_detected <= 1'b0;
		end else begin
			if ( job_terminated ) begin
				end_detected <= 1'b1;
			end else if (hi_job_valid) begin
				end_detected <= 1'b0;
			end
		end
	end
	
//	always_ff @ ( posedge clk, posedge reset ) begin
//		if ( reset ) begin
//			hit_breakpoint <= 1'b0;
//		end else begin
//			if ( dsu_hit_breakpoint ) begin
//				hit_breakpoint <= 1'b1;
//			end else if (reset_interrupt) begin
//				hit_breakpoint <= 1'b0;
//			end
//		end
//	end
	
	always_ff @ ( posedge clk, posedge reset ) begin 
		if ( reset ) begin
			state_control <= STATE_WAIT_COMMAND_UART;
			next_state_control <= STATE_WAIT_COMMAND_UART;
			dsu_state_control_supp <= STATE_WAIT_COMMAND_UART;
			
			item_avail_o       <= 1'b1;
			item_valid_o       <= 1'b0;
			
			hi_thread_en <= {`THREAD_NUMB{1'b0}};
			hi_job_valid <= 1'b0;
			hi_job_thread_id <= thread_id_t'(0);
			hi_job_pc <= address_t'(0);
			
			dsu_enable <= 1'b0;
			dsu_single_step <= 1'b0;
			dsu_thread_selection <= 1'b0;
			dsu_breakpoint <= {32'hffffffff,32'hffffffff,32'hffffffff,32'hffffffff,32'hffffffff,32'hffffffff,32'hffffffff,32'hffffffff};
			dsu_breakpoint_enable<=8'h00;
			resume <= 1'b0;
			dsu_en_vector <= 1'b0;
			dsu_en_scalar <= 1'b0;
			dsu_load_shift_reg <= 1'b0;
			dsu_start_shift <= 1'b0;
			
			is_scalar_read <= 1'b0;
			is_vector_read <= 1'b0;
			
			
			word_uart_receive <= 5'b00000;
			word_uart_sent <= 5'b00000;
			
		end else begin
			
			item_valid_o       <= 1'b0;
			resume			   <= 1'b0;
			dsu_en_vector <= 1'b0;
			dsu_en_scalar <= 1'b0;
			dsu_load_shift_reg <= 1'b0;
			//dsu_start_shift <= 1'b0;
			hi_job_valid <= 1'b0;
			hi_job_thread_id <= thread_id_t'(0);
			hi_job_pc <= address_t'(0);
			
			//sync setup
			ni_setup_mess_valid = 1'b0;
			
			unique case ( state_control )
				
				STATE_WAIT_COMMAND_UART : begin 
					item_avail_o = 1'b1;
					
					if ( item_valid_i) begin
						unique case ( cmd_host_in ) 
							BOOT_COMMAND : begin
								word_uart_receive  <= 3;
								state_control <= STATE_RECEIVE_UART_DATA;	
								next_state_control <= STATE_MAKE_BOOT_CORE;
							end
							ENABLE_THREAD : begin
								word_uart_receive  <= 2;
								state_control <= STATE_RECEIVE_UART_DATA;	
								next_state_control <= STATE_MAKE_ENABLE_THREAD;
							end
							SYNC_COMMAND : begin
								word_uart_receive  <= 1;
								state_control <= STATE_RECEIVE_UART_DATA;	
								next_state_control <= STATE_BARRIER_COUNTER;
							end
							GET_END_OF_KERNEL : begin
								word_uart_receive  <= 1;
								word_uart_sent	   <= 1;
								state_control <= STATE_CATCH_INTERRUPT;	
								//next_state_control <= STATE_CATCH_INTERRUPT;
								dsu_state_control_supp <= STATE_WAIT_COMMAND_UART;
							end
							DSU_ENABLE_CMD : begin
								dsu_enable <= 1'b1;
								word_uart_receive  <= 1;	//la word ricevuta non è di interesse essendo la destinazione
								state_control <= STATE_RECEIVE_UART_DATA;	
								next_state_control <= STATE_DSU_TO_UART_ACK;
								dsu_state_control_supp <= STATE_WAIT_COMMAND_UART;
								dsu_ack <= DSU_ACK_RSP;
							end
							DSU_DISABLE_CMD : begin
								dsu_enable <= 1'b0;
								word_uart_receive  <= 1;	//la word ricevuta non è di interesse essendo la destinazione
								state_control <= STATE_RECEIVE_UART_DATA;	
								next_state_control <= STATE_DSU_TO_UART_ACK;
								dsu_state_control_supp <= STATE_WAIT_COMMAND_UART;
								dsu_ack <= DSU_ACK_RSP;
							end				
							DSU_ENABLE_SS_CMD : begin 
								dsu_single_step <= 1'b1;
								word_uart_receive  <= 0;
								state_control <= STATE_DSU_TO_UART_ACK;
								dsu_state_control_supp <= STATE_WAIT_COMMAND_UART;
								dsu_ack <= DSU_ACK_RSP;
							end
							DSU_DISABLE_SS_CMD : begin 
								dsu_single_step <= 1'b0;
								word_uart_receive  <= 0;
								state_control <= STATE_DSU_TO_UART_ACK;
								dsu_state_control_supp <= STATE_WAIT_COMMAND_UART;
								dsu_ack <= DSU_ACK_RSP;
							end
							DSU_ENABLE_SELECTION_TH_CMD : begin 
								word_uart_receive  <= 1;	
								state_control <= STATE_RECEIVE_UART_DATA;	
								next_state_control <= STATE_ENABLE_THREAD_DSU;
								dsu_state_control_supp <= STATE_WAIT_COMMAND_UART;
								dsu_ack <= DSU_ACK_RSP;
							end
							DSU_DISABLE_SELECTION_TH_CMD : begin 
								dsu_thread_selection <= 1'b0;
								word_uart_receive  <= 0;
								state_control <= STATE_DSU_TO_UART_ACK;
								dsu_state_control_supp <= STATE_WAIT_COMMAND_UART;
								dsu_ack <= DSU_ACK_RSP;
							end
							DSU_SET_BP_CMD : begin
								word_uart_receive  <= 1;	
								state_control <= STATE_RECEIVE_UART_DATA;	
								next_state_control <= STATE_DSU_TO_UART_ACK;
								dsu_state_control_supp <= STATE_SET_BP_DSU;
								dsu_ack <= DSU_ACK_RSP;
							end 
							DSU_SET_BP_MASK_CMD : begin
								word_uart_receive  <= 1;
								state_control <= STATE_RECEIVE_UART_DATA;	
								next_state_control <= STATE_DSU_TO_UART_ACK;
								dsu_state_control_supp <= STATE_SET_BP_MASK_DSU;
								dsu_ack <= DSU_ACK_RSP;
							end 
							DSU_READ_BP_INFO_CMD : begin
								word_uart_receive  <= 0;
								word_uart_sent <= 2;
								uart_buffer_out[`THREAD_NUMB + `ADDRESS_SIZE - 1 : 0] <= bp_info.thread_id;
								uart_buffer_out[`ADDRESS_SIZE - 1 : 0] <= bp_info.bp_address;
								state_control <= STATE_SEND_DATA_HOST;	
								dsu_state_control_supp <= STATE_WAIT_COMMAND_UART;
								dsu_ack <= DSU_BP_VALUE_RSP;
							end
							DSU_RESUME_CMD : begin
								resume <= 1'b1;
								word_uart_receive  <= 0;
								state_control <= STATE_DSU_TO_UART_ACK;
								dsu_state_control_supp <= STATE_WAIT_COMMAND_UART;
								dsu_ack <= DSU_ACK_RSP;
							end
							DSU_READ_S_REG_CMD : begin
								is_scalar_read <= 1'b1;
								word_uart_receive <= 1;
								word_uart_sent <= 1;
								state_control <= STATE_RECEIVE_UART_DATA;
								next_state_control <= STATE_READ_REG_DSU;
								dsu_state_control_supp <= STATE_WAIT_COMMAND_UART;
								dsu_ack <= DSU_REG_VALUE_RSP;
							end
							DSU_READ_V_REG_CMD : begin
								is_vector_read <= 1'b1;
								//dsu_en_vector <= 1'b1;
								word_uart_receive  <= 1;
								word_uart_sent <= 16;
								state_control <= STATE_RECEIVE_UART_DATA;
								next_state_control <= STATE_READ_REG_DSU;
								dsu_state_control_supp <= STATE_WAIT_COMMAND_UART;
								dsu_ack <= DSU_REG_VALUE_RSP;
							end
//							DSU_WRITE_S_REG_CMD : begin
//	
//							end
//							DSU_WRITE_V_REG_CMD : begin
//	
//							end
						endcase 
					end 
//					else if ( dsu_hit_breakpoint ) begin
//						state_control <= STATE_CATCH_BP_INFO_DSU;
//					end else if( job_terminated ) begin
//					    hi_thread_en <= {`THREAD_NUMB{1'b0}};
//						state_control <= STATE_CATCH_TERMINATION;
//					end
				end
				
				STATE_RECEIVE_UART_DATA : begin
					if(word_uart_receive == 5'b00000) begin
						state_control <= next_state_control;
					end else begin
						if (item_valid_i) begin 
							word_uart_receive = word_uart_receive - 5'b00001;
							uart_buffer_in <= {uart_buffer_in[TOTAL_RECEIVE_BIT - ITEM_w - 1 : 0], item_data_i};
						end
					end
				end
				
				STATE_MAKE_BOOT_CORE : begin
					hi_job_valid <= 1'b1;
					hi_job_thread_id <= uart_buffer_in[ITEM_w + $bits(thread_id_t) - 1 : ITEM_w];
					hi_job_pc <= uart_buffer_in[ITEM_w - 1 : 0];
					state_control <= STATE_WAIT_COMMAND_UART;
				end
				
				STATE_MAKE_ENABLE_THREAD : begin
					hi_thread_en <= uart_buffer_in[`THREAD_NUMB - 1 : 0];
					state_control <= STATE_WAIT_COMMAND_UART;
				end
				
				STATE_BARRIER_COUNTER : begin
					ni_setup_mess.is_master = 1'b1;
					ni_setup_mess.id_barrier = item_data_i[$clog2( `BARRIER_NUMB )+$clog2( `TILE_COUNT*`THREAD_NUMB ):$clog2( `TILE_COUNT*`THREAD_NUMB )+1];; 
					ni_setup_mess.cnt_setup = item_data_i[$clog2( `TILE_COUNT*`THREAD_NUMB ):1]; 
					if(setup_available) begin
						ni_setup_mess_valid = 1'b1;
						state_control <= STATE_WAIT_COMMAND_UART;
					end else 
						state_control <= STATE_BARRIER_COUNTER;
				end
				
				STATE_ENABLE_THREAD_DSU : begin
					dsu_thread_selection <= 1'b1;
					dsu_thread_id <= uart_buffer_in[$bits (thread_id_t) - 1 : 0]; 
					state_control <= STATE_DSU_TO_UART_ACK;
				end
				
				STATE_DSU_TO_UART_ACK : begin
					if (item_avail_i) begin
						item_valid_o <= 1'b1;
						item_data_o <= dsu_ack;
						state_control <= dsu_state_control_supp;
					end
				end
				
				STATE_SET_BP_DSU : begin
					dsu_breakpoint <= {dsu_breakpoint[6 : 0] , uart_buffer_in[`ADDRESS_SIZE - 1 : 0]};
					state_control <= STATE_DSU_TO_UART_ACK;
					dsu_state_control_supp <= STATE_WAIT_COMMAND_UART;
				end
				
				STATE_SET_BP_MASK_DSU : begin
					dsu_breakpoint_enable <= uart_buffer_in[7 : 0];
					state_control <= STATE_DSU_TO_UART_ACK;
					dsu_state_control_supp <= STATE_WAIT_COMMAND_UART;
				end
				
				STATE_CATCH_BP_INFO_DSU : begin
					bp_info.bp_address <= dsu_bp_instruction[dsu_bp_thread_id];
					bp_info.thread_id <= dsu_bp_thread_id;
					state_control <= STATE_WAIT_COMMAND_UART;
				end
				
				//Manda i dati dalla DSU all'host
				STATE_SEND_DATA_HOST : begin
					if (item_avail_i) begin
						if(word_uart_sent == 5'b00000) begin
							state_control <= dsu_state_control_supp;
						end else begin
							word_uart_sent <= word_uart_sent - 5'b00001;
							item_valid_o <= 1'b1;
							item_data_o <= uart_buffer_out[ITEM_w - 1 : 0];
							uart_buffer_out[TOTAL_SEND_BIT - 1 : 0] <= {{ITEM_w{1'b0}}, uart_buffer_out[TOTAL_SEND_BIT - 1 : ITEM_w]}; 
						end
					end
				end
				
				STATE_READ_REG_DSU : begin
					if(is_scalar_read) 
						dsu_en_scalar <= 1'b1;
					else if (is_vector_read)
						dsu_en_vector <= 1'b1;
					//dsu_load_shift_reg <= 1'b1;
					state_control <= STATE_LOAD_SHIFT_REGISTER;
				end
				
				STATE_LOAD_SHIFT_REGISTER : begin
					dsu_load_shift_reg <= 1'b1;
					dsu_start_shift           <= 1'b1;
					state_control <= STATE_WAIT_DSU_REGISTER;
				end
				
				STATE_WAIT_DSU_REGISTER : begin
					dsu_start_shift           <= 1'b1;
					if (~dsu_stop_shift)
						uart_buffer_out <= {uart_buffer_out[510 : 0], dsu_serial_reg_out}; 
					else begin
						dsu_start_shift <= 1'b0;
						state_control <= STATE_SEND_DATA_HOST;
					end
				end
				
				STATE_CATCH_INTERRUPT : begin
					if(end_detected) begin
						hi_thread_en <= {`THREAD_NUMB{1'b0}};
						uart_buffer_out[ITEM_w - 1 : 0] <= `KERNEL_ENDED;
						state_control <= STATE_SEND_DATA_HOST;
					end else if (dsu_hit_breakpoint) begin 
						uart_buffer_out[ITEM_w - 1 : 0] <= `BREAKPOINT_CATCHED;
						state_control <= STATE_SEND_DATA_HOST;
					end
				end
				
			endcase
		end
	end
	
endmodule
