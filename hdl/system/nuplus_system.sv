`include "nuplus_define.sv"
`include "user_define.sv"
`include "system_defines.sv"
`include "synchronization_defines.sv"
`include "load_store_unit_defines.sv"

module nuplus_system (
		input                clk,
		input                reset,
		output logic         end_of_kernel,
		output logic [1 : 0] mux_selection,
		output logic [3 : 0] hi_thread_en,
		output logic [5 : 0] LED,

`ifndef SIMULATION
		output logic [6 : 0]  SEG,
		output logic          DP,
		output logic [7 : 0]  AN,
		output logic          init_calib_complete,
		// DDR2
		output logic [12 : 0] ddr2_addr,
		output logic [2 : 0]  ddr2_ba,
		output logic          ddr2_cas_n,
		output logic [0 : 0]  ddr2_ck_n,
		output logic [0 : 0]  ddr2_ck_p,
		output logic [0 : 0]  ddr2_cke,
		output logic [0 : 0]  ddr2_cs_n,
		output logic [1 : 0]  ddr2_dm,
		inout  logic [15 : 0] ddr2_dq,
		inout  logic [1 : 0]  ddr2_dqs_n,
		inout  logic [1 : 0]  ddr2_dqs_p,
		output logic [0 : 0]  ddr2_odt,
		output logic          ddr2_ras_n,
		output logic          ddr2_we_n,
`endif
		output logic uart_tx,
		input  logic uart_rx
	);

    localparam                                   NUM_MASTER              = `BUS_MASTER;
    localparam                                   ITEM_w                  = `UART_HM_WIDTH;

	logic ui_clk;
	logic mux_switch;


	//Host interface
	logic [ITEM_w - 1 : 0] item_data_i;
	logic                  item_valid_i;
	logic                  item_avail_o;
	logic [ITEM_w - 1 : 0] item_data_o;
	logic                  item_valid_o;
	logic                  item_avail_i;

	// Mux interface

	address_t     [NUM_MASTER - 1 : 0] m_n2m_request_address;
	dcache_line_t [NUM_MASTER - 1 : 0] m_n2m_request_data;
	logic         [NUM_MASTER - 1 : 0] m_n2m_request_read;
	logic         [NUM_MASTER - 1 : 0] m_n2m_request_write;
	logic         [NUM_MASTER - 1 : 0] m_mc_avail_o;

	logic         [NUM_MASTER - 1 : 0] m_m2n_request_available;
	logic         [NUM_MASTER - 1 : 0] m_m2n_response_valid;
	address_t     [NUM_MASTER - 1 : 0] m_m2n_response_address;
	dcache_line_t [NUM_MASTER - 1 : 0] m_m2n_response_data;

	// Core interface memory controller
	address_t     n2m_request_address;
	dcache_line_t n2m_request_data;
	logic         n2m_request_read;
	logic         n2m_request_write;
	logic         mc_avail_o;

	logic         m2n_request_available;
	logic         m2n_response_valid;
	address_t     m2n_response_address;
	dcache_line_t m2n_response_data;

	// Interface To Nuplus
	//logic         [`THREAD_NUMB - 1 : 0]         hi_thread_en;
	logic       hi_job_valid;
	address_t   hi_job_pc;
	thread_id_t hi_job_thread_id;

	//thread_mask_t                              job_terminated_mask;
	logic job_terminated;

	logic                                   ext_freeze;
	logic                                   resume;
	logic                                   dsu_enable;
	logic                                   dsu_single_step;
	address_t   [7 : 0]                     dsu_breakpoint;
	logic       [7 : 0]                     dsu_breakpoint_enable;
	logic                                   dsu_thread_selection;
	thread_id_t                             dsu_thread_id;
	logic                                   dsu_en_vector;
	logic                                   dsu_en_scalar;
	logic                                   dsu_load_shift_reg;
	logic                                   dsu_start_shift;
	logic       [`REGISTER_ADDRESS - 1 : 0] dsu_reg_addr;
	logic                                   dsu_write_scalar;
	logic                                   dsu_write_vector;
	logic                                   dsu_serial_reg_in;
	address_t   [`THREAD_NUMB - 1 : 0]      dsu_bp_instruction;
	thread_id_t                             dsu_bp_thread_id;
	logic                                   dsu_serial_reg_out;
	logic                                   dsu_stop_shift;
	logic                                   dsu_hit_breakpoint;
	//For debug not for DSU
	address_t                               program_counter;

	logic                 c2n_account_valid;
	service_c2n_message_t c2n_account_message;
	logic                 network_available;
	//From Network Interface
	service_c2n_message_t n2c_release_message;
	logic                 n2c_release_valid;
	logic                 account_available;

	sync_setup_t   ni_setup_mess;
	logic          ni_setup_mess_valid;
	logic          setup_available;
	//Account
	sync_account_t ni_account_mess;
	logic          ni_account_mess_valid;
	//Release
	sync_release_t ss3_release_mess;
	logic          ss3_release_valid;

	assign network_available                                    = 1'b1;
	assign n2c_release_message.data[$bits( barrier_t ) + 1 : 2] = ss3_release_mess.id_barrier;
	assign n2c_release_valid                                    =ss3_release_valid;
	//assign ni_account_mess.id_barrier = c2n_account_message.data[$bits( barrier_t )+$clog2( `TILE_COUNT )+1:$clog2( `TILE_COUNT )+2];
	assign ni_account_mess.id_barrier                           = c2n_account_message.data[$bits( barrier_t ) + $clog2( `TILE_COUNT ) : $clog2( `TILE_COUNT ) + 1];
	assign ni_account_mess.tile_id_source                       = 0;
	assign ni_account_mess_valid                                = c2n_account_valid;


	always_ff @ ( posedge clk, posedge reset ) begin
		if ( reset ) begin
			end_of_kernel <= 1'b0;
		end else begin

			if ( job_terminated ) begin
				end_of_kernel <= 1'b1;
			end else if ( hi_job_valid ) begin
				end_of_kernel <= 1'b0;
			end

		end
	end

`ifndef SIMULATION
//  address_t                      instruction_bp;
	logic [31 : 0] x;

//      always_ff @ ( posedge clk, posedge reset ) begin
//      if ( reset ) begin
//          instruction_bp <= 0;
//      end else begin
//        if (hi_job_valid) begin
//          instruction_bp <= dsu_bp_instruction;
//        end
//      end
//      end


	assign x = program_counter;
	seg7decimal seg7decimal (
		.clk   ( ui_clk ) ,
		.reset ( reset  ) ,
		.an    ( AN     ) ,
		.dp    ( DP     ) ,
		.seg   ( SEG    ) ,
		.x     ( x      )
	) ;
`endif

	uart_control_unit # (
		.ITEM_w     ( ITEM_w          ) ,
		.CLOCK_RATE ( `CLOCK_RATE     ) ,
		.UART_BAUD  ( `UART_BAUD_RATE )
	)
	uart_control_unit (
		`ifndef SIMULATION
		.clk ( ui_clk ) ,
		`else
		.clk ( clk ) ,
		`endif
		.reset                 ( reset                      ) ,
		.LED                   ( LED                        ) ,
		.mux_switch            ( mux_switch                 ) ,
		//.job_terminated        (job_terminated       ) ,
		//Interface To Axi
		.n2m_request_address   ( m_n2m_request_address[0]   ) ,
		.n2m_request_data      ( m_n2m_request_data[0]      ) ,
		.n2m_request_read      ( m_n2m_request_read[0]      ) ,
		.n2m_request_write     ( m_n2m_request_write[0]     ) ,
		.mc_avail_o            ( m_mc_avail_o[0]            ) , //non usato -> [Mirko] questo deve essere usato per impedire che la risposta dal MC non vada persa
		.m2n_request_available ( m_m2n_request_available[0] ) ,
		.m2n_response_valid    ( m_m2n_response_valid[0]    ) ,
		.m2n_response_address  ( m_m2n_response_address[0]  ) ,
		.m2n_response_data     ( m_m2n_response_data[0]     ) ,
		//Interface to Host Request Manager
		.item_data_i           ( item_data_o                ) , //Input: items from outside
		.item_valid_i          ( item_valid_o               ) , //Input: valid signal associated with item_data_i port
		.item_avail_o          ( item_avail_i               ) , //Output: avail signal to input port item_data_i
		.item_data_o           ( item_data_i                ) , //Output: items to outside
		.item_valid_o          ( item_valid_i               ) , //Output: valid signal associated with item_data_o port
		.item_avail_i          ( item_avail_o               ) , //Input: avail signal to ouput port item_data_o
		//UART interface
		.uart_tx               ( uart_tx                    ) ,
		.uart_rx               ( uart_rx                    )
	) ;



	h2c_control_unit # (
		.ITEM_w ( ITEM_w )
	)
	h2c_control_unit (
		`ifndef SIMULATION
		.clk ( ui_clk ) ,
		`else
		.clk ( clk ) ,
		`endif
		.reset                 ( reset                 ) ,
		.ext_freeze            ( ext_freeze            ) ,
		.resume                ( resume                ) ,
		.job_terminated        ( job_terminated        ) ,
		//Interface to UART Control Unit
		.item_data_i           ( item_data_i           ) , //Input: items from outside
		.item_valid_i          ( item_valid_i          ) , //Input: valid signal associated with item_data_i port
		.item_avail_o          ( item_avail_o          ) , //Output: avail signal to input port item_data_i
		.item_data_o           ( item_data_o           ) , //Output: items to outside
		.item_valid_o          ( item_valid_o          ) , //Output: valid signal associated with item_data_o port
		.item_avail_i          ( item_avail_i          ) , //Input: avail signal to ouput port item_data_o
		//Interface To Nuplus for boot
		.hi_thread_en          ( hi_thread_en          ) ,
		.hi_job_valid          ( hi_job_valid          ) ,
		.hi_job_pc             ( hi_job_pc             ) ,
		.hi_job_thread_id      ( hi_job_thread_id      ) ,
		//sybc setup
		.ni_setup_mess         ( ni_setup_mess         ) ,
		.ni_setup_mess_valid   ( ni_setup_mess_valid   ) ,
		.setup_available       ( setup_available       ) ,
		//From Host Debug
		.dsu_enable            ( dsu_enable            ) ,
		.dsu_single_step       ( dsu_single_step       ) ,
		.dsu_breakpoint        ( dsu_breakpoint        ) ,
		.dsu_breakpoint_enable ( dsu_breakpoint_enable ) ,
		.dsu_thread_selection  ( dsu_thread_selection  ) ,
		.dsu_thread_id         ( dsu_thread_id         ) ,
		.dsu_en_vector         ( dsu_en_vector         ) ,
		.dsu_en_scalar         ( dsu_en_scalar         ) ,
		.dsu_load_shift_reg    ( dsu_load_shift_reg    ) ,
		.dsu_start_shift       ( dsu_start_shift       ) ,
		.dsu_reg_addr          ( dsu_reg_addr          ) ,
		.dsu_write_scalar      ( dsu_write_scalar      ) ,
		.dsu_write_vector      ( dsu_write_vector      ) ,
		.dsu_serial_reg_in     ( dsu_serial_reg_in     ) ,
		//To Host Debug
		.dsu_bp_instruction    ( dsu_bp_instruction    ) ,
		.dsu_bp_thread_id      ( dsu_bp_thread_id      ) ,
		.dsu_serial_reg_out    ( dsu_serial_reg_out    ) ,
		.dsu_stop_shift        ( dsu_stop_shift        ) ,
		.dsu_hit_breakpoint    ( dsu_hit_breakpoint    )
	) ;

	nuplus_core # (
		.TILE_ID ( 0 ) ,
		.CORE_ID ( 0 )
	)
	nuplus_core (
		`ifndef SIMULATION
		.clk ( ui_clk ) ,
		`else
		.clk ( clk ) ,
		`endif
		.reset                 ( reset                 ) ,
		.ext_freeze            ( ext_freeze            ) ,
		.resume                ( resume                ) ,
		.thread_en             ( hi_thread_en          ) ,
		.program_counter       ( program_counter       ) ,
		//Host Interface
		.hi_job_valid          ( hi_job_valid          ) ,
		.hi_job_pc             ( hi_job_pc             ) ,
		.hi_job_thread_id      ( hi_job_thread_id      ) ,
		//From DSU
		.dsu_enable            ( dsu_enable            ) ,
		.dsu_single_step       ( dsu_single_step       ) ,
		.dsu_breakpoint        ( dsu_breakpoint        ) ,
		.dsu_breakpoint_enable ( dsu_breakpoint_enable ) ,
		.dsu_thread_selection  ( dsu_thread_selection  ) ,
		.dsu_thread_id         ( dsu_thread_id         ) ,
		.dsu_en_vector         ( dsu_en_vector         ) ,
		.dsu_en_scalar         ( dsu_en_scalar         ) ,
		.dsu_load_shift_reg    ( dsu_load_shift_reg    ) ,
		.dsu_start_shift       ( dsu_start_shift       ) ,
		.dsu_reg_addr          ( dsu_reg_addr          ) ,
		.dsu_write_scalar      ( dsu_write_scalar      ) ,
		.dsu_write_vector      ( dsu_write_vector      ) ,
		.dsu_serial_reg_in     ( dsu_serial_reg_in     ) ,

		//To DSU Host
		.dsu_bp_instruction ( dsu_bp_instruction ) ,
		.dsu_bp_thread_id   ( dsu_bp_thread_id   ) ,
		.dsu_serial_reg_out ( dsu_serial_reg_out ) ,
		.dsu_stop_shift     ( dsu_stop_shift     ) ,
		.dsu_hit_breakpoint ( dsu_hit_breakpoint ) ,

		.n2m_request_address ( m_n2m_request_address[1] ) ,
		.n2m_request_data    ( m_n2m_request_data[1]    ) ,
		.n2m_request_read    ( m_n2m_request_read[1]    ) ,
		.n2m_request_write   ( m_n2m_request_write[1]   ) ,
		.mc_avail_o          ( m_mc_avail_o[1]          ) ,

		.m2n_request_available ( m_m2n_request_available[1] ) ,
		.m2n_response_valid    ( m_m2n_response_valid[1]    ) ,
		.m2n_response_address  ( m_m2n_response_address[1]  ) ,
		.m2n_response_data     ( m_m2n_response_data[1]     ) ,

		.bc2n_account_valid   ( c2n_account_valid   ) ,
		.bc2n_account_message ( c2n_account_message ) ,
		`ifndef SINGLE_CORE
		.bc2n_account_destination_valid ( bc2n_account_destination_valid ) ,
		`endif
		.n2bc_network_available   ( account_available   ) ,
		//From Network Interface
		.n2bc_release_message     ( n2c_release_message ) ,
		.n2bc_release_valid       ( n2c_release_valid   ) ,
		.n2bc_mes_service_consumed(                     )
	) ;


	mux_n2m # (
		.NUM_MASTER ( NUM_MASTER )
	)
	u_mux_n2m (
		.selection ( mux_selection ) ,
		`ifndef SIMULATION
		.clk ( ui_clk ) ,
		`else
		.clk ( clk ) ,
		`endif
		.reset                   ( reset                   ),
		.mux_switch              ( mux_switch              ),
		.job_terminated          ( job_terminated          ),
		.m_n2m_request_address   ( m_n2m_request_address   ),
		.m_n2m_request_data      ( m_n2m_request_data      ),
		.m_n2m_request_read      ( m_n2m_request_read      ),
		.m_n2m_request_write     ( m_n2m_request_write     ),
		.m_mc_avail_o            ( m_mc_avail_o            ),
		.m_m2n_request_available ( m_m2n_request_available ),
		.m_m2n_response_valid    ( m_m2n_response_valid    ),
		.m_m2n_response_address  ( m_m2n_response_address  ),
		.m_m2n_response_data     ( m_m2n_response_data     ),
		.s_n2m_request_address   ( n2m_request_address     ),
		.s_n2m_request_data      ( n2m_request_data        ),
		.s_n2m_request_read      ( n2m_request_read        ),
		.s_n2m_request_write     ( n2m_request_write       ),
		.s_mc_avail_o            ( mc_avail_o              ),
		.s_m2n_request_available ( m2n_request_available   ),
		.s_m2n_response_valid    ( m2n_response_valid      ),
		.s_m2n_response_address  ( m2n_response_address    ),
		.s_m2n_response_data     ( m2n_response_data       )
	) ;

	/*
	 end_detector u_end_detector (
	 `ifndef SIMULATION
	 .clk                   (ui_clk ) ,
	 `else
	 .clk                   (clk) ,
	 `endif
	 .job_terminated      (job_terminated      ) ,
	 .n2m_request_address (n2m_request_address ) ,
	 .n2m_request_write   (n2m_request_write   ) ,
	 //.job_terminated_mask (job_terminated_mask ) ,
	 .reset               (reset               )
	 ) ;
	 */


	synchronization_core synchronization_core (
		.clk                   ( clk                   ) ,
		.reset                 ( reset                 ) ,
		.job_terminated        ( job_terminated        ) ,
		//Setup
		.ni_setup_mess         ( ni_setup_mess         ) ,
		.ni_setup_mess_valid   ( ni_setup_mess_valid   ) ,
		.setup_available       ( setup_available       ) ,
		.ss1_setup_consumed    (                       ) ,
		//Account
		.ni_account_mess       ( ni_account_mess       ) ,
		.ni_account_mess_valid ( ni_account_mess_valid ) ,
		.ss1_account_consumed  (                       ) ,
		.account_available     ( account_available     ) ,
		//Release
		.ss3_release_mess      ( ss3_release_mess      ) ,
		.ss3_release_valid     ( ss3_release_valid     ) ,
		.ni_available          ( network_available     )  //controllare questo segnale con MIRKO
	) ;

`ifndef SIMULATION
	logic [31 : 0]                       axi_awaddr;
	logic [7 : 0]                        axi_awlen;
	logic [2 : 0]                        axi_awsize;
	logic [1 : 0]                        axi_awburst;
	logic [3 : 0]                        axi_awcache;
	logic                                axi_awvalid;
	logic                                axi_awready;
	logic [`AXI_DATA_WIDTH - 1 : 0]      axi_wdata;
	logic [`AXI_DATA_BYTE_WIDTH - 1 : 0] axi_wstrb;
	logic                                axi_wlast;
	logic                                axi_wvalid;
	logic                                axi_wready;
	logic                                axi_bvalid;
	logic                                axi_bready;
	logic [31 : 0]                       axi_araddr;
	logic [7 : 0]                        axi_arlen;
	logic [2 : 0]                        axi_arsize;
	logic [1 : 0]                        axi_arburst;
	logic [3 : 0]                        axi_arcache;
	logic                                axi_arvalid;
	logic                                axi_arready;
	logic [`AXI_DATA_WIDTH - 1 : 0]      axi_rdata;
	logic                                axi_rvalid;
	logic                                axi_rready;

	axi_interface u_axi_interface (
		.clk                   ( ui_clk                ) ,
		.reset                 ( reset                 ) ,
		//Address write data channel
		.m_axi_awaddr          ( axi_awaddr            ) ,
		.m_axi_awlen           ( axi_awlen             ) ,
		.m_axi_awsize          ( axi_awsize            ) ,
		.m_axi_awburst         ( axi_awburst           ) ,
		.m_axi_awcache         ( axi_awcache           ) ,
		.m_axi_awvalid         ( axi_awvalid           ) ,
		.m_axi_awready         ( axi_awready           ) ,
		//Write data channel
		.m_axi_wdata           ( axi_wdata             ) ,
		.m_axi_wstrb           ( axi_wstrb             ) ,
		.m_axi_wlast           ( axi_wlast             ) ,
		.m_axi_wvalid          ( axi_wvalid            ) ,
		.m_axi_wready          ( axi_wready            ) ,
		//Write response channel
		.m_axi_bvalid          ( axi_bvalid            ) ,
		.m_axi_bready          ( axi_bready            ) ,
		//Read address channel
		.m_axi_araddr          ( axi_araddr            ) ,
		.m_axi_arlen           ( axi_arlen             ) ,
		.m_axi_arsize          ( axi_arsize            ) ,
		.m_axi_arburst         ( axi_arburst           ) ,
		.m_axi_arcache         ( axi_arcache           ) ,
		.m_axi_arvalid         ( axi_arvalid           ) ,
		.m_axi_arready         ( axi_arready           ) ,
		//Read data channel
		.m_axi_rdata           ( axi_rdata             ) ,
		.m_axi_rvalid          ( axi_rvalid            ) ,
		.m_axi_rready          ( axi_rready            ) ,
		//interfaccia core
		.n2m_request_address   ( n2m_request_address   ) ,
		.n2m_request_data      ( n2m_request_data      ) ,
		.n2m_request_read      ( n2m_request_read      ) ,
		.n2m_request_write     ( n2m_request_write     ) ,
		.mc_avail_o            ( mc_avail_o            ) , //non usato -> [Mirko] questo deve essere usato per impedire che la risposta dal MC non vada persa
		.m2n_request_available ( m2n_request_available ) ,
		.m2n_response_valid    ( m2n_response_valid    ) ,
		.m2n_response_address  ( m2n_response_address  ) ,
		.m2n_response_data     ( m2n_response_data     )
	) ;


	memory_controller u_memory_controller (
		.clk100              ( clk                 ) ,
		.reset               ( reset               ) ,
		.ui_clk              ( ui_clk              ) , // from MIG
		.init_calib_complete ( init_calib_complete ) , // from MIG
		//Interface to DDR2
		.ddr2_addr           ( ddr2_addr           ) ,
		.ddr2_ba             ( ddr2_ba             ) ,
		.ddr2_cas_n          ( ddr2_cas_n          ) ,
		.ddr2_ck_n           ( ddr2_ck_n           ) ,
		.ddr2_ck_p           ( ddr2_ck_p           ) ,
		.ddr2_cke            ( ddr2_cke            ) ,
		.ddr2_cs_n           ( ddr2_cs_n           ) ,
		.ddr2_dm             ( ddr2_dm             ) ,
		.ddr2_dq             ( ddr2_dq             ) ,
		.ddr2_dqs_n          ( ddr2_dqs_n          ) ,
		.ddr2_dqs_p          ( ddr2_dqs_p          ) ,
		.ddr2_odt            ( ddr2_odt            ) ,
		.ddr2_ras_n          ( ddr2_ras_n          ) ,
		.ddr2_we_n           ( ddr2_we_n           ) ,
		//AXI interface
		//Write address channel
		.s_axi_awaddr        ( axi_awaddr          ) ,
		.s_axi_awlen         ( axi_awlen           ) ,
		.s_axi_awsize        ( axi_awsize          ) ,
		.s_axi_awburst       ( axi_awburst         ) ,
		.s_axi_awcache       ( axi_awcache         ) ,
		.s_axi_awvalid       ( axi_awvalid         ) ,
		.s_axi_awready       ( axi_awready         ) ,
		//Write data channel
		.s_axi_wdata         ( axi_wdata           ) ,
		.s_axi_wstrb         ( axi_wstrb           ) ,
		.s_axi_wlast         ( axi_wlast           ) ,
		.s_axi_wvalid        ( axi_wvalid          ) ,
		.s_axi_wready        ( axi_wready          ) ,
		//Write response channel
		.s_axi_bvalid        ( axi_bvalid          ) ,
		.s_axi_bready        ( axi_bready          ) ,
		//Read address channel
		.s_axi_araddr        ( axi_araddr          ) ,
		.s_axi_arlen         ( axi_arlen           ) ,
		.s_axi_arsize        ( axi_arsize          ) ,
		.s_axi_arburst       ( axi_arburst         ) ,
		.s_axi_arcache       ( axi_arcache         ) ,
		.s_axi_arvalid       ( axi_arvalid         ) ,
		.s_axi_arready       ( axi_arready         ) ,
		//Read data channel
		.s_axi_rdata         ( axi_rdata           ) ,
		.s_axi_rvalid        ( axi_rvalid          ) ,
		.s_axi_rready        ( axi_rready          )
	) ;



`else

	mango_dummy # (
		.OFF_WIDTH      ( `ICACHE_OFFSET_LENGTH ) ,
		.FILENAME_INSTR ( `MEMORY_BIN           )
	)
	u_mango_dummy (
		.clk                   ( clk                   ) ,
		.reset                 ( reset                 ) ,
		//From MC
		//To MANGO NI
		.n2m_request_address   ( n2m_request_address   ) ,
		.n2m_request_data      ( n2m_request_data      ) ,
		.n2m_request_read      ( n2m_request_read      ) ,
		.n2m_request_write     ( n2m_request_write     ) ,
		.mc_avail_o            ( mc_avail_o            ) ,
		//From MANGO NI
		.m2n_request_available ( m2n_request_available ) ,
		.m2n_response_valid    ( m2n_response_valid    ) ,
		.m2n_response_address  ( m2n_response_address  ) ,
		.m2n_response_data     ( m2n_response_data     )
	) ;
`endif

`ifdef DISPLAY_CORE
	int core_file;

	initial core_file = $fopen ( `DISPLAY_CORE_FILE, "wb" ) ;

	final $fclose ( core_file ) ;
`endif

`ifdef DISPLAY_MEMORY
	int memory_file;

	initial memory_file = $fopen ( `DISPLAY_MEMORY_FILE, "wb" ) ;

`endif


endmodule 