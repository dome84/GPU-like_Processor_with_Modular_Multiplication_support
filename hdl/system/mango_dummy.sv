`include "nuplus_define.sv"
`include "user_define.sv"

module mango_dummy #(
		parameter ADDRESS_WIDTH  = 32,
		parameter DATA_WIDTH     = 512,
		parameter MAX_WIDTH      = 32, // 0x41000 * 64 byte = 0x01004000
		parameter OFF_WIDTH      = 6,
		parameter FILENAME_INSTR = "")
	(
		input                                clk,
		input                                reset,

		// From MC
		// To MANGO NI
		input  logic [ADDRESS_WIDTH - 1 : 0] n2m_request_address,
		input  logic [DATA_WIDTH - 1 : 0]    n2m_request_data,
		input  logic                         n2m_request_read,
		input  logic                         n2m_request_write,
		//input  logic                         n2m_request_is_instr,
		input  logic                         mc_avail_o,

		// From MANGO NI
		output logic                         m2n_request_available,
		output logic                         m2n_response_valid,
		output logic [ADDRESS_WIDTH - 1 : 0] m2n_response_address,
		output logic [DATA_WIDTH - 1 : 0]    m2n_response_data

	);

	logic   [DATA_WIDTH - 1 : 0]    mem_dummy_instr [MAX_WIDTH];
	logic   [ADDRESS_WIDTH - 1 : 0] address_in;
	typedef enum logic[1 : 0] {IDLE, WAITING} state_t;

	integer                         delay                       = 0;
	state_t                         state                       = IDLE;

	initial begin
		$readmemh( FILENAME_INSTR, mem_dummy_instr );
	end

	always_ff @( posedge clk, posedge reset ) begin
		if ( reset ) begin
			state                 <= IDLE;
			m2n_request_available <= 1'b0;
			m2n_response_data     <= 0;
			m2n_response_valid    <= 1'b0;
			delay                 <= 0;
		end else begin

			m2n_request_available <= 1'b0;
			m2n_response_valid    <= 1'b0;

			case( state )
				IDLE    : begin
					state <= IDLE;
					delay <= 0;
					if ( n2m_request_read ) begin
						state                                                 <= WAITING;
						address_in                                            <= n2m_request_address >> OFF_WIDTH;
						delay                                                 <= ( ( $urandom ) % 100 ) + 10;
						m2n_request_available                                 <= 1'b0;
						//is_instr                                              <= n2m_request_is_instr;
					end else if ( n2m_request_write ) begin
						state                                                 <= IDLE;
						address_in                                            <= n2m_request_address >> OFF_WIDTH;
						mem_dummy_instr[( n2m_request_address >> OFF_WIDTH )] <= n2m_request_data;
						m2n_request_available                                 <= 1'b0;
					end else
						m2n_request_available                                 <= 1'b1;
				end

				WAITING : begin
					if ( delay <= 0 ) begin
						if( mc_avail_o ) begin
							state              <= IDLE;
							m2n_response_valid <= 1'b1;
						end else
							state              <= WAITING;

						m2n_response_data                                     <= mem_dummy_instr[address_in[$clog2( MAX_WIDTH ) - 1 : 0]];
						m2n_response_address                                  <= address_in << OFF_WIDTH;
					end else
						delay                                                 <= delay - 1;
				end

			endcase
		end
	end

`ifdef DISPLAY_MEMORY

	final begin
		for ( int i = 0; i < MAX_WIDTH; i++ ) begin
			$fdisplay( `DISPLAY_MEMORY_VAR, "%h \t %h", ( i * 64 ), mem_dummy_instr[i]);
		end
		$fclose( `DISPLAY_MEMORY_VAR );
	end
	
`endif

endmodule