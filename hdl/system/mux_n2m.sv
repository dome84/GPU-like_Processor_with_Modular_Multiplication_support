`include "user_define.sv"
`include "nuplus_define.sv"
`include "load_store_unit_defines.sv"

module mux_n2m #(
		NUM_MASTER = 2 )
	(
		output logic         [NUM_MASTER - 1 : 0] selection,

		input                                     clk,
		input                                     reset,
		input  logic                              mux_switch,
		input                                     job_terminated,

		input  address_t     [NUM_MASTER - 1 : 0] m_n2m_request_address,
		input  dcache_line_t [NUM_MASTER - 1 : 0] m_n2m_request_data,
		input  logic         [NUM_MASTER - 1 : 0] m_n2m_request_read,
		input  logic         [NUM_MASTER - 1 : 0] m_n2m_request_write,
		input  logic         [NUM_MASTER - 1 : 0] m_mc_avail_o,

		output logic         [NUM_MASTER - 1 : 0] m_m2n_request_available,
		output logic         [NUM_MASTER - 1 : 0] m_m2n_response_valid,
		output address_t     [NUM_MASTER - 1 : 0] m_m2n_response_address,
		output dcache_line_t [NUM_MASTER - 1 : 0] m_m2n_response_data,

		output address_t                          s_n2m_request_address,
		output dcache_line_t                      s_n2m_request_data,
		output logic                              s_n2m_request_read,
		output logic                              s_n2m_request_write,
		output logic                              s_mc_avail_o,

		input  logic                              s_m2n_request_available,
		input  logic                              s_m2n_response_valid,
		input  address_t                          s_m2n_response_address,
		input  dcache_line_t                      s_m2n_response_data
	);


	logic nuplus_activated;
	
	assign nuplus_activated = mux_switch;

	typedef enum logic [1 : 0] {IDLE, WAITING, WAITING_DEACTIVATION} state_t;
	state_t state;

	always_ff @( posedge clk, posedge reset ) begin
		if ( reset )begin
			selection <= 2'b01;
			state     <= IDLE;
		end else begin
			case ( state )
				IDLE    :
				begin
					selection <= 2'b01; // boot o risposta
					if ( nuplus_activated ) begin
						selection <= 2'b10;
						state     <= WAITING;
					end else 
						state     <= IDLE;
				end

				WAITING :
				begin
					selection <= 2'b10; // nuplus
					if ( job_terminated ) begin
						//selection <= 2'b01;
						state     <= WAITING_DEACTIVATION;
					end else
						state     <= WAITING;
				end
				
				WAITING_DEACTIVATION :
				begin
					selection <= 2'b01; 
					state     <= IDLE;

				end
				
			endcase
		end
	end

	always_comb begin
		m_m2n_request_available = '{default : '0};
		m_m2n_response_valid    = '{default : '0};
		m_m2n_response_address  = '{default : '0};
		m_m2n_response_data     = '{default : '0};
		s_n2m_request_address   = '{default : '0};
		s_n2m_request_data      = '{default : '0};
		s_n2m_request_read      = 1'b0;
		s_n2m_request_write     = 1'b0;
		s_mc_avail_o            = 1'b0;
		if ( selection == 2'b10 ) begin
			m_m2n_request_available[1] = s_m2n_request_available;
			m_m2n_response_valid[1]    = s_m2n_response_valid;
			m_m2n_response_address[1]  = s_m2n_response_address;
			m_m2n_response_data[1]     = s_m2n_response_data;
			s_n2m_request_address      = m_n2m_request_address[1];
			s_n2m_request_data         = m_n2m_request_data[1];
			s_n2m_request_read         = m_n2m_request_read[1];
			s_n2m_request_write        = m_n2m_request_write[1];
			s_mc_avail_o               = m_mc_avail_o[1];
		end else begin
			m_m2n_request_available[0] = s_m2n_request_available;
			m_m2n_response_valid[0]    = s_m2n_response_valid;
			m_m2n_response_address[0]  = s_m2n_response_address;
			m_m2n_response_data[0]     = s_m2n_response_data;
			s_n2m_request_address      = m_n2m_request_address[0];
			s_n2m_request_data         = m_n2m_request_data[0];
			s_n2m_request_read         = m_n2m_request_read[0];
			s_n2m_request_write        = m_n2m_request_write[0];
			s_mc_avail_o               = m_mc_avail_o[0];
		end

	end

endmodule