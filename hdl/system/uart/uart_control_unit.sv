`include "system_defines.sv"
`include "load_store_unit_defines.sv"
`include "user_define.sv"
`include "nuplus_define.sv"

`ifndef SINGLE_CORE
`include "message_service_defines.sv"
`endif

// DA MODIFICARE, AGGIUNGI INTERFACCIA MEMORIA

// massimo 8 thread per tile, massimo 16x16

/*
 * all'interno di un vettore si invia  prima il BYTE MENO SIGNIFICATIVO, per� la comunicazione va nell'ordine predefinitio.
 * Quindi, per esempio, per fare il boot si manda in ordine:CMD, poi DESTIN, poi ID, poi PC(0), poi PC(1), poi PC(2), poi PC(3).
 * Questa convenzione ci permette di non fare il reverte dei dati ricevuti.
 */

module uart_control_unit # (
		parameter ITEM_w     = 32,
		parameter CLOCK_RATE = 50000000,
		parameter UART_BAUD  = 9600
	)
	(
		input                                 clk,
		input                                 reset,
		output logic         [5 : 0]          LED,
		output logic                          mux_switch,
//		input		 						  hit_breakpoint,
		//output logic                          job_terminated,
		// Interface To Axi
		output address_t                      n2m_request_address,
		output dcache_line_t                  n2m_request_data,
		output logic                          n2m_request_read,
		output logic                          n2m_request_write,
		output logic                          mc_avail_o,           // non usato -> [Mirko] questo deve essere usato per impedire che la risposta dal MC non vada persa
		
		input  logic                          m2n_request_available,
		input  logic                          m2n_response_valid,
		input  address_t                      m2n_response_address,
		input  dcache_line_t                  m2n_response_data,

		// Interface to Host Request Manager
		input                [ITEM_w - 1 : 0] item_data_i,          // Input: items from outside
		input                                 item_valid_i,         // Input: valid signal associated with item_data_i port
		output logic                          item_avail_o,         // Output: avail signal to input port item_data_i
		output logic         [ITEM_w - 1 : 0] item_data_o,          // Output: items to outside
		output logic                          item_valid_o,         // Output: valid signal associated with item_data_o port
		input                                 item_avail_i,         // Input: avail signal to ouput port item_data_o

		// UART interface
		output                                uart_tx,
		input                                 uart_rx
	) ;

	typedef logic [7 : 0] byte_t;

	logic                  [`ID_DEVICE_LENGTH - 1 : 0] id_device;
	assign id_device         = 32'h206E752B; // nu+

	//UART Interface
	logic                                              divisor_set;
	logic                  [31 : 0]                    divisor_reg;
	logic                  [7 : 0]                     rx_fifo_char_out;
	logic                                              rx_fifo_empty_out;
	logic                                              rx_fifo_frame_error_out;
	logic                                              rx_fifo_overrun_out;
	logic                                              rx_fifo_read_in;
	logic                  [7 : 0]                     tx_char_in;
	logic                                              tx_en_in;
	logic                                              tx_ready_out;

//	assign LED[6]            = rx_fifo_overrun_out;
//	assign LED[7]            = rx_fifo_frame_error_out;

	assign mc_avail_o        = 1'b1;
	assign divisor_reg [31 : 0]      = ( CLOCK_RATE / UART_BAUD ) - 1;

	
	uart # (
		.BASE_ADDRESS ( 0 )
	)
	u_uart (
		.clk                     ( clk                     ) ,
		.reset                   ( reset                   ) ,

		.divisor_reg             ( divisor_reg             ) ,
		.divisor_set             ( divisor_set             ) ,

		.rx_fifo_char_out        ( rx_fifo_char_out        ) ,
		.rx_fifo_empty_out       ( rx_fifo_empty_out       ) ,
		.rx_fifo_frame_error_out ( rx_fifo_frame_error_out ) ,
		.rx_fifo_overrun_out     ( rx_fifo_overrun_out     ) ,
		.rx_fifo_read_in         ( rx_fifo_read_in         ) ,

		.tx_char_in              ( tx_char_in              ) ,
		.tx_en_in                ( tx_en_in                ) ,
		.tx_ready_out            ( tx_ready_out            ) ,

		.uart_rx                 ( uart_rx                 ) ,
		.uart_tx                 ( uart_tx                 )
	) ;


	host_messages_t                                    core_command;
	logic                                              is_memory_write, is_memory_read, memory_op_start;
	logic                                              receiving_address;

	byte_t                 [64 -1 : 0]                 host_uart_buffer;                                                 // 6
	byte_t                 [64 -1 : 0]                 memory_uart_buffer;                                               // 6
	byte_t                 [64 -1 : 0]                 data_to_send;
	byte_t                 [64 -1 : 0]                 vector_register;
	byte_t                                             host_ack, final_dsu_host_ack;

	logic                  [ITEM_w - 1 : 0]            host_uart_command[4];

	address_t                                          memory_address, memory_address_reg;
	
	logic                  [2:0]                       num_command_word;                                                 // 3 bit dipende da lunghezza comando
	logic                  [6:0]                       tx_counter_byte, tx_counter_byte_nxt, tx_counter_word, tx_counter_word_nxt, num_command_byte, byte_to_send; // 6 bit dipende da lunghezza comando


	logic                                              wait_for_memory, data_received;

	logic                                              is_read_id, is_barrier, is_interrupt;

	byte_t                                             interrupt_message;

	byte_t                 [64 -1 : 0]                 data_write, data1;
	logic           signed [`DCACHE_WIDTH -1 : 0 ]     write_mask, a, b, c, d;
	
	logic											   is_dsu_command, is_read_v_reg, is_set_bp_command, is_read_s_reg, is_read_bp_info;
	logic				   [4 : 0]					   word_receive;

	assign data_to_send      = (is_memory_read) ? memory_uart_buffer : (is_read_v_reg) ? vector_register : (is_read_s_reg) ? {0,vector_register[63 : 60]} : (is_read_bp_info) ? {0,vector_register[63 : 56]} : (is_interrupt) ? interrupt_message : id_device;

	assign
		host_uart_command[0] = core_command,
		host_uart_command[1] = (is_barrier)? {16'b0, host_uart_buffer[1],host_uart_buffer[0]} : (is_set_bp_command) ? {host_uart_buffer[0], host_uart_buffer[1], host_uart_buffer[2], host_uart_buffer[3]} : host_uart_buffer[0], // SOLO SE DESTINAZIONE STA IN UN BYTE
		host_uart_command[2] = host_uart_buffer[1], // thread_id o thread_mask, solo se massimo 8 thread
		host_uart_command[3] = {host_uart_buffer[5],host_uart_buffer[4],host_uart_buffer[3],host_uart_buffer[2]}; // pc

	assign
		memory_address       = {host_uart_buffer[3],host_uart_buffer[2],host_uart_buffer[1],host_uart_buffer[0]};
	
	// serve solo per definire cosa scrivere in memoria nel caso di write
	always_comb begin
		a                   = 0;
		a[`DCACHE_WIDTH -1] = 1;
		b                   = a >>> (`DCACHE_WIDTH - (num_command_byte << 3) - 1);
		c                   = ~b;
		data1               = c & host_uart_buffer;
		d                   = c << (memory_address_reg[`DCACHE_OFFSET_LENGTH -1 : 0] *8);
		write_mask          = ~d;
		if (num_command_byte == 64)
			data_write = host_uart_buffer;
		else			
			data_write = write_mask & memory_uart_buffer | (data1 << (memory_address_reg[`DCACHE_OFFSET_LENGTH -1 : 0] *8));
	end

				
	assign		tx_counter_byte = tx_counter_byte_nxt,
				tx_counter_word = tx_counter_word_nxt;

	uart_state_t                                       state_control_next, dsu_state_next;
	always_ff @ ( posedge clk, posedge reset ) begin : Loader_Control_Unit
		if ( reset ) begin

			LED[3 : 0]         <= 4'b0001;
			state_control_next <= STATE_WAIT_COMMAND;
			dsu_state_next	   <= STATE_WAIT_DSU_ACK;

			// Uart interface
			divisor_set        <= 1'b0;
			tx_en_in           <= 1'b0;
			rx_fifo_read_in    <= 1'b0;
			//job_terminated 	   <= 1'b0;

			// HRman
			item_avail_o       <= 1'b1;
			item_valid_o       <= 1'b0;

			// App
			is_read_id         <= 1'b0;
			mux_switch         <= 1'b0;
			is_barrier         <= 1'b0;
			is_interrupt 	   <= 1'b0; 

			// Memory
			is_memory_read     <= 1'b0;
			is_memory_write    <= 1'b0;
			memory_op_start    <= 1'b0;
			receiving_address  <= 1'b0;
			data_received      <= 1'b0;
			
			//DSU
			is_dsu_command	   <= 1'b0;
			is_read_v_reg	   <= 1'b0;
			is_read_s_reg	   <= 1'b0;
			is_set_bp_command  <= 1'b0;
			is_read_bp_info	   <= 1'b0;

		end else begin
			rx_fifo_read_in    <= 1'b0;
			tx_en_in           <= 1'b0;
			divisor_set        <= 1'b0;

			item_valid_o       <= 1'b0;

			memory_op_start    <= 1'b0;

			mux_switch         <= 1'b0;
			
			
			//job_terminated 	   <= 1'b0;

			
			unique case ( state_control_next )

				STATE_WAIT_COMMAND : begin
					LED[3 : 0]         <= 4'b0001;
					divisor_set        <= 1'b1;
					
//					if ( item_valid_i && item_data_i[4:0] == END_OF_KERNEL) begin
//						//hrm_response   <= item_data_i[4:0];
//						state_control_next <= STATE_CHECK_TERMINATION;
//					end else 
//					if(hit_breakpoint) begin
//						state_control_next <= STATE_CATCH_BP;
//					end else
					
					if ( !rx_fifo_empty_out ) begin

						tx_counter_byte_nxt    <= 0;
						tx_counter_word_nxt    <= 0;
						state_control_next <= STATE_WAIT_HOST_ACK;

						rx_fifo_read_in    <= 1'b1; // vedi se va bene farlo qui o un colpo prima

						item_avail_o       <= 1'b0;

						unique case ( rx_fifo_char_out )

							`BOOT_CORE_COMMAND : begin
								host_ack          <= `BOOT_CORE_COMMAND;
								core_command      <= BOOT_COMMAND;

								num_command_byte  <= 6; // destination_tile + thread_id + pc
								num_command_word  <= 4; //
							end

							`ENABLE_THREAD_COMMAND : begin
								host_ack          <= `ENABLE_THREAD_COMMAND;
								core_command      <= ENABLE_THREAD;

								num_command_byte  <= 2; // destination_tile + thread mask
								num_command_word  <= 3; //
							end

							`READ_ID_COMMAND : begin
								host_ack          <= `READ_ID_COMMAND;
								is_read_id        <= 1'b1;

								num_command_byte  <= 0; // NIENTE
							end

							`BARRIER_COMMAND : begin
								host_ack          <= `BARRIER_COMMAND;
								core_command      <= SYNC_COMMAND;
								num_command_word  <= 2; //
								num_command_byte  <= 2; //TODO: ceil($bits(sync_setup_t)/8); 

								is_barrier        <= 1'b1;
							end
							
							`GET_INTERRUPTION_COMMAND : begin
								host_ack          <= `GET_INTERRUPTION_ACK;
								core_command      <= GET_END_OF_KERNEL;
								num_command_byte  <= 0; 
								num_command_word  <= 1; //
								is_interrupt 	  <= 1'b1; 

							end

							`WRITE_MEMORY_COMMAND : begin
								host_ack          <= `WRITE_MEMORY_COMMAND;
								is_memory_write   <= 1'b1;
								receiving_address <= 1'b1;
								num_command_byte  <= 5; // address + num_byte
							end

							`READ_MEMORY_COMMAND : begin
								host_ack          <= `READ_MEMORY_COMMAND;
								is_memory_read    <= 1'b1;
								num_command_byte  <= 4; // address
							end
							
							//DSU COMMAND
							`DSU_ENABLE_COMMAND : begin 
								host_ack          <= `DSU_ENABLE_COMMAND;
								final_dsu_host_ack<= `DSU_ENABLE_ACK;
								core_command	  <= DSU_ENABLE_CMD;
								is_dsu_command	  <= 1'b1;
								dsu_state_next	  <= STATE_FINAL_ACK;	//After STATE_WAIT_DSU_ACK
								num_command_byte  <= 4;	//byte da ricevere dall'host dopo il comando
								num_command_word  <= 2; //tutte le word da inviare eventualmente all'h2c (comando compreso)
							end
							
							`DSU_DISABLE_COMMAND : begin 
								host_ack          <= `DSU_DISABLE_COMMAND;
								final_dsu_host_ack<= `DSU_DISABLE_ACK;
								is_dsu_command	  <= 1'b1;
								core_command	  <= DSU_DISABLE_CMD;
								dsu_state_next	  <= STATE_FINAL_ACK;	//After STATE_WAIT_DSU_ACK
								num_command_byte  <= 4;	//destinazione
								num_command_word  <= 2; 
							end
							
							`DSU_SET_BP_COMMAND : begin
								host_ack          <= `DSU_SET_BP_COMMAND;
								final_dsu_host_ack<= `DSU_SET_BP_ACK;
								core_command	  <= DSU_SET_BP_CMD;
								is_dsu_command	  <= 1'b1;
								is_set_bp_command <= 1'b1;
								dsu_state_next	  <= STATE_FINAL_ACK;	//After STATE_WAIT_DSU_ACK
								num_command_byte  <= 4; 
								num_command_word  <= 2;
							end
							
							`DSU_SET_BP_MASK_COMMAND : begin
								host_ack          <= `DSU_SET_BP_MASK_COMMAND;
								final_dsu_host_ack<= `DSU_SET_BP_MASK_ACK;
								is_dsu_command	  <= 1'b1;
								core_command	  <= DSU_SET_BP_MASK_CMD;
								dsu_state_next	  <= STATE_FINAL_ACK;	//After STATE_WAIT_DSU_ACK
								num_command_byte  <= 1;
								num_command_word  <= 2; //
							end
							
							`DSU_ENABLE_SS_COMMAND : begin 
								host_ack          <= `DSU_ENABLE_SS_COMMAND;
								final_dsu_host_ack<= `DSU_ENABLE_SS_ACK;
								core_command	  <= DSU_ENABLE_SS_CMD;
								is_dsu_command	  <= 1'b1;
								num_command_byte  <= 0;
								dsu_state_next	  <= STATE_FINAL_ACK;
							end
							
							`DSU_DISABLE_SS_COMMAND : begin 
								host_ack          <= `DSU_DISABLE_SS_COMMAND;
								final_dsu_host_ack<= `DSU_DISABLE_SS_ACK;
								core_command	  <= DSU_DISABLE_SS_CMD;
								is_dsu_command	  <= 1'b1;
								num_command_byte  <= 0;
								dsu_state_next	  <= STATE_FINAL_ACK;
							end
							
							`DSU_ENABLE_SELECTION_TH_COMMAND : begin 
								host_ack          <= `DSU_ENABLE_SELECTION_TH_COMMAND;
								final_dsu_host_ack<= `DSU_ENABLE_SELECTION_TH_ACK;
								core_command	  <= DSU_ENABLE_SELECTION_TH_CMD;
								is_dsu_command	  <= 1'b1;
								num_command_byte  <= 1;
								num_command_word  <= 2; //
								dsu_state_next	  <= STATE_FINAL_ACK;
							end
							
							`DSU_DISABLE_SELECTION_TH_COMMAND : begin 
								host_ack          <= `DSU_DISABLE_SELECTION_TH_COMMAND;
								final_dsu_host_ack<= `DSU_DISABLE_SELECTION_TH_ACK;
								core_command	  <= DSU_DISABLE_SELECTION_TH_CMD;
								is_dsu_command	  <= 1'b1;
								num_command_byte  <= 0;
								num_command_word  <= 1;
								dsu_state_next	  <= STATE_FINAL_ACK;
							end
							
							`DSU_READ_S_REG_COMMAND : begin
								host_ack          <= `DSU_READ_S_REG_COMMAND;
								final_dsu_host_ack<= `DSU_READ_S_REG_ACK;
								is_read_s_reg	  <= 1'b1;
								core_command	  <= DSU_READ_S_REG_CMD;
								dsu_state_next	  <= STATE_FINAL_ACK;	//After STATE_WAIT_DSU_ACK
								num_command_byte  <= 1;
								num_command_word  <= 2; //
							end 
							
							`DSU_READ_V_REG_COMMAND : begin
								host_ack          <= `DSU_READ_V_REG_COMMAND;
								final_dsu_host_ack<= `DSU_READ_V_REG_ACK;
								is_read_v_reg	  <= 1'b1;
								core_command	  <= DSU_READ_V_REG_CMD;
								dsu_state_next	  <= STATE_FINAL_ACK;	//After STATE_WAIT_DSU_ACK
								num_command_byte  <= 1;
								num_command_word  <= 2; //
							end 
							
//							`DSU_WRITE_S_REG_COMMAND : begin
//								host_ack          <= `DSU_WRITE_S_REG_COMMAND;
//								final_dsu_host_ack<= `DSU_WRITE_S_REG_ACK;
//								is_dsu_command	  <= 1'b1;
//								core_command	  <= DSU_WRITE_S_REG_CMD;
//								dsu_state_next	  <= STATE_FINAL_ACK;	//After STATE_WAIT_DSU_ACK
//								num_command_byte  <= 1;
//								num_command_word  <= 2; //
//							end 
//							
//							`DSU_WRITE_V_REG_COMMAND : begin
//								host_ack          <= `DSU_WRITE_V_REG_COMMAND;
//								final_dsu_host_ack<= `DSU_WRITE_V_REG_ACK;
//								is_dsu_command	  <= 1'b1;
//								core_command	  <= DSU_WRITE_V_REG_CMD;
//								dsu_state_next	  <= STATE_FINAL_ACK;	//After STATE_WAIT_DSU_ACK
//								num_command_byte  <= 1;
//								num_command_word  <= 2; //
//							end 

							`DSU_READ_BP_INFO_COMMAND : begin
								host_ack          <= `DSU_READ_BP_INFO_COMMAND;
								final_dsu_host_ack<= `DSU_READ_BP_INFO_ACK;
								is_read_bp_info	  <= 1'b1;
								core_command	  <= DSU_READ_BP_INFO_CMD;
								dsu_state_next	  <= STATE_FINAL_ACK;	//After STATE_WAIT_DSU_ACK
								word_receive	  <= 5'b00010;
								byte_to_send	  <= 8; //4 byte + clog(`THREAD_NUMB), ma in questo caso si inviano direttamente 2 word.
								num_command_byte  <= 0;
								num_command_word  <= 1; //
							end 
							
							`DSU_RESUME_COMMAND : begin
								host_ack          <= `DSU_RESUME_COMMAND;
								final_dsu_host_ack<= `DSU_RESUME_ACK;
								core_command	  <= DSU_RESUME_CMD;
								is_dsu_command	  <= 1'b1;
								num_command_byte  <= 0;
								num_command_word  <= 1;
								dsu_state_next	  <= STATE_FINAL_ACK;
							end 

						endcase

					end

				end

				STATE_WAIT_HOST_ACK : begin
					LED[3 : 0]         <= 4'b0010;
					if ( tx_ready_out) begin
						state_control_next <= STATE_RECEIVE_DATA_COMMAND;
						tx_en_in           <= 1'b1;
						tx_char_in         <= host_ack;
					end

				end

				//-----------
				STATE_RECEIVE_DATA_COMMAND : begin
					LED[3 : 0]         <= 4'b0011;
					if ( !(tx_counter_byte == num_command_byte) ) begin

						if ( !rx_fifo_empty_out ) begin
							host_uart_buffer[tx_counter_byte] <= rx_fifo_char_out;
							rx_fifo_read_in                   <= 1'b1;
							state_control_next                <= STATE_WAIT_BUFFER_DEQUEUE;
						end
					end else begin
						if (is_memory_read) begin
							host_ack                          <= `READ_MEMORY_ACK;
							byte_to_send                      <= 64;
							state_control_next                <= STATE_SEND_DATA;
							tx_counter_byte_nxt               <= 0;
							memory_op_start                   <= 1'b1;
							memory_address_reg                <= memory_address;
						end else if (is_memory_write & receiving_address) begin
							receiving_address                 <= 1'b0;
							num_command_byte                  <= host_uart_buffer[4];
							tx_counter_byte_nxt               <= 0;
							memory_op_start                   <= 1'b1;
							memory_address_reg                <= memory_address;
						//state_control_next <= STATE_RECEIVE_DATA_COMMAND;
						end else if (is_memory_write & !receiving_address) begin
							host_ack                          <= `WRITE_MEMORY_ACK;
							state_control_next                <= STATE_FINAL_ACK;
							data_received                     <= 1'b1;
						end else if (is_read_id) begin
							host_ack                          <= `READ_ID_ACK;
							state_control_next                <= STATE_SEND_DATA;
							byte_to_send                      <= 4;
							is_read_id                        <= 1'b0;
						end else if (is_read_v_reg) begin
							state_control_next                <= STATE_SEND_COMMAND;
							byte_to_send                      <= 64;
							word_receive 					  <= 5'b10000;
							tx_counter_byte_nxt               <= 0;
							is_read_id                        <= 1'b0;
						end else if (is_read_s_reg) begin
							state_control_next                <= STATE_SEND_COMMAND;
							byte_to_send                      <= 4;
							word_receive 					  <= 5'b00001;
							tx_counter_byte_nxt               <= 0;
							is_read_id                        <= 1'b0;
						end else if(is_interrupt) begin
							host_ack						  <= `GET_INTERRUPTION_ACK;
							byte_to_send	  		 		  <= 1;
							state_control_next                <= STATE_SEND_COMMAND;
						end else
							state_control_next                <= STATE_SEND_COMMAND;
					end
				end
				
				STATE_WAIT_DSU_ACK : begin
				LED[3 : 0]         <= 4'b0100;
					if(item_valid_i) begin
						if( host_messages_t'(item_data_i) == DSU_ACK_RSP )
							state_control_next                <= dsu_state_next;
					end
				end
				
				STATE_PULL_DSU_DATA : begin 
				LED[3 : 0]         <= 4'b0101;
					if(item_valid_i) begin
						word_receive <= word_receive - 5'b00001;
						vector_register[63 : 0] <= {item_data_i, vector_register[63 : 4]};
					end else if(word_receive == 5'b00000)
						state_control_next				 <= STATE_SEND_DATA;
				end

				STATE_WAIT_BUFFER_DEQUEUE : begin
					LED[3 : 0]          <= 4'b0111;
					tx_counter_byte_nxt <= tx_counter_byte + 1;
					state_control_next  <= STATE_RECEIVE_DATA_COMMAND;

				end

				//-----------
				// Invia un comando
				STATE_SEND_COMMAND : begin
					LED[3 : 0]         <= 4'b1000;
					if ( !(tx_counter_word == num_command_word) ) begin
						if ( item_avail_i ) begin
							item_valid_o                      <= 1'b1;
							item_data_o                       <= host_uart_command[tx_counter_word];
							tx_counter_word_nxt               <= tx_counter_word + 1;
						end
					end else begin
						if (core_command == BOOT_COMMAND) begin
							host_ack                          <= `BOOT_CORE_ACK;
							state_control_next <= STATE_FINAL_ACK;
						end else if (core_command == SYNC_COMMAND) begin
							host_ack                          <= `BARRIER_ACK;
							state_control_next <= STATE_FINAL_ACK;
						end else if (is_interrupt) begin
							state_control_next <= STATE_CATCH_STOP;
						end else if ( is_read_v_reg || is_read_s_reg ) begin
							state_control_next <= STATE_PULL_DSU_DATA;
						end else if ( is_read_bp_info ) begin
							state_control_next <= STATE_PULL_DSU_DATA;
						end else if(is_dsu_command) begin
							state_control_next <= STATE_WAIT_DSU_ACK;
						end else begin
							mux_switch         <= 1'b1;
							host_ack           <= `ENABLE_THREAD_ACK;
							state_control_next <= STATE_FINAL_ACK;
						end
						item_avail_o       <= 1'b1;
					end

				end

				STATE_SEND_DATA : begin
					LED[3 : 0]         <= 4'b1001;
					if ( !(tx_counter_byte == byte_to_send) ) begin
						if ( tx_ready_out & !wait_for_memory & !memory_op_start) begin // dovrebbe fermare quel colpo di clock che basta per innescare wait_for_memory
							tx_en_in                          <= 1'b1;
							tx_char_in                        <= data_to_send[tx_counter_byte];//memory_uart_buffer[tx_counter_byte];
							state_control_next <= STATE_WAIT_TX_UART;
						end
					end else
						state_control_next <= STATE_FINAL_ACK;
				end

				STATE_WAIT_TX_UART : begin
					tx_counter_byte_nxt               <= tx_counter_byte + 1;
					state_control_next <= STATE_SEND_DATA;
				end
				//--------
				STATE_FINAL_ACK : begin
					LED[3 : 0]         <= 4'b1011;
					if ( tx_ready_out & !wait_for_memory ) begin
						state_control_next <= STATE_WAIT_COMMAND;
						data_received      <= 1'b0;
						is_memory_read     <= 1'b0;
						is_memory_write    <= 1'b0;
						is_barrier         <= 1'b0;
						is_dsu_command	   <= 1'b0;
						is_read_v_reg	   <= 1'b0;
						is_read_s_reg	   <= 1'b0;
						is_set_bp_command  <= 1'b0;
						is_read_bp_info    <= 1'b0;
						is_interrupt	   <= 1'b0;
						tx_en_in           <= 1'b1;
						tx_char_in         <= (is_dsu_command || is_read_v_reg || is_read_s_reg || is_read_bp_info) ? final_dsu_host_ack : host_ack;
					end

				end

				STATE_CATCH_STOP : begin
				    LED[3 : 0]         <= 4'b1100;
					if(item_valid_i) begin
						if ( item_data_i == `KERNEL_ENDED) begin
							interrupt_message   <= `KERNEL_ENDED;
							state_control_next <= STATE_SEND_DATA;
						end else if(item_data_i == `BREAKPOINT_CATCHED) begin
							interrupt_message   <= `BREAKPOINT_CATCHED;
							state_control_next <= STATE_SEND_DATA;
						end
					end 
				end
				
//				STATE_CHECK_TERMINATION : begin
//				    LED[3 : 0]         <= 4'b1100;
//						if ( tx_ready_out ) begin
//							tx_en_in       <= 1'b1;
//							tx_char_in     <= `KERNEL_ENDED;
//							//job_terminated <= 1'b1;
//							state_control_next <= STATE_WAIT_COMMAND;
//						end
//				end
				
//				STATE_CATCH_BP : begin
//				    LED[3 : 0]         <= 4'b1101;
//					if ( tx_ready_out ) begin
//						tx_en_in       <= 1'b1;
//						tx_char_in     <= `BREAKPOINT_CATCHED;
//						//job_terminated <= 1'b1;
//						state_control_next <= STATE_WAIT_COMMAND;
//					end
//				end
				
			endcase
		end

	end


	uart_memory_t                                      state_memory_next;

	always_ff @ ( posedge clk, posedge reset ) begin : Memory_control_interface
		if ( reset ) begin
			LED[5 : 4]        <= 2'b00;
			wait_for_memory   <= 1'b0;
			n2m_request_write <= 1'b0;
			n2m_request_read  <= 1'b0;
			//state_memory_next  <= STATE_WAIT_MEMORY_COMMAND;
            
		end else begin

			n2m_request_read  <= 1'b0;
			n2m_request_write <= 1'b0;

			unique case ( state_memory_next )

				STATE_WAIT_MEMORY_COMMAND : begin
					LED[5 : 4] <= 2'b00;
					if ( memory_op_start) begin
						wait_for_memory     <= 1'b1;
						state_memory_next   <= STATE_READ_MEMORY;
					end
				end

				STATE_READ_MEMORY : begin
					LED[5 : 4] <= 2'b01;
					if ( m2n_request_available ) begin
						n2m_request_read    <= 1'b1;
						n2m_request_address <= memory_address_reg;
						state_memory_next   <= STATE_WAIT_DATA;
					end
				end

				STATE_WAIT_DATA : begin
					LED[5 : 4] <= 2'b10;
					if ( m2n_response_valid ) begin
						memory_uart_buffer  <= m2n_response_data;
						if (is_memory_read) begin
							wait_for_memory     <= 1'b0;
							state_memory_next   <= STATE_WAIT_MEMORY_COMMAND;
						end else
							state_memory_next   <= STATE_WRITE_DATA;
					end
				end

				STATE_WRITE_DATA : begin
					LED[5 : 4] <= 2'b11;
					if (data_received) begin
						if ( m2n_request_available ) begin
							n2m_request_data    <= data_write;
							n2m_request_write   <= 1'b1;
							n2m_request_address <= memory_address_reg;
							wait_for_memory     <= 1'b0;
							state_memory_next   <= STATE_WAIT_MEMORY_COMMAND;
						end
					end
				end

			endcase
		end
	end


endmodule
