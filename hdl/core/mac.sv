`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 24.04.2017 19:01:00
// Design Name: 
// Module Name: mac
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "nuplus_define.sv"

module mac(
    input clk,
    input reset,
    input [31:0] a,
    input [31:0] b,
    input ce      ,
    input multiply,
    input loadacc,
    input [$clog2( `THREAD_NUMB ) - 1 : 0] index,
    output [63:0] q
    );
   
   logic [31:0] b_sel;
    
   logic [15:0] a0;
   logic [15:0] a1; 
   logic [15:0] b0;
   logic [15:0] b1; 
   logic [47:0] dsp0_out;
   logic [47:0] dsp0_casc_out;
   
   logic [16:0] preadd_a;
   logic [16:0] preadd_b;
   logic [47:0] dsp1_out;
   logic [47:0] dsp1_casc_out;
   
   logic [47:0] dsp2_out;
   logic [47:0] dsp2_casc_out;
   logic [47:0] dsp2_out_neg;
   
   logic [47:0] dsp3_out;
   logic [47:0] dsp3_casc_out;
   
   logic [47:0] stage1_out;
   logic [16:0] stage0_out_reg0;
   logic [16:0] stage0_out_0;
   logic [15:0] stage0_out_reg1;
   
   logic [47:0] stage2_out_reg0;
   logic        stage2_out_carry;
   logic        stage2_out_carry1;
   logic [31:0] stage2_out_reg1;
   logic [31:0] stage2_out_1;
   logic [15:0] stage1_out_reg1;
   logic [15:0] stage0_out_reg2;
   
   logic multiply0, multiply1, multiply2 ,multiply3, multiply4;
   
   logic [$clog2( `THREAD_NUMB ) - 1 : 0] index0;
   logic [$clog2( `THREAD_NUMB ) - 1 : 0] index1;
   logic [$clog2( `THREAD_NUMB ) - 1 : 0] index2;
   logic [$clog2( `THREAD_NUMB ) - 1 : 0] index3;
   logic [$clog2( `THREAD_NUMB ) - 1 : 0] index4;
   
   logic [15:0] acc0;
   logic [15:0] acc1;
   logic [31:0] acc2;
   
   logic loadacc0;
   logic loadacc1;
   logic loadacc2;
   logic loadacc3;
   
   logic ce0;
   logic ce1;
   logic ce2;
   logic ce3;
   
   logic [31:0] acc_high0;
   logic [31:0] acc_high1;
   logic [31:0] acc_high2;
   logic [31:0] acc_high3;
   
   /*logic [63:0] c_reg0;
   logic [63:0] c_reg1;
   logic [31:0] ch_reg2;
   logic [15:0] clh_reg2;
   logic [31:0] ch_reg3;*/
   
   assign b_sel = ((loadacc == 1'b0)? b : 32'h00000001); 
   
   assign a0 = a[15:0];
   assign a1 = a[31:16];
   assign b0 = b_sel[15:0];
   assign b1 = b_sel[31:16];
   
       DSP48E1 #(
          // Feature Control Attributes: Data Path Selection
          .A_INPUT("DIRECT"),               // Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
          .B_INPUT("DIRECT"),               // Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
          .USE_DPORT("FALSE"),              // Select D port usage (TRUE or FALSE)
          .USE_MULT("MULTIPLY"),            // Select multiplier usage ("MULTIPLY", "DYNAMIC", or "NONE")
          .USE_SIMD("ONE48"),               // SIMD selection ("ONE48", "TWO24", "FOUR12")
          // Pattern Detector Attributes: Pattern Detection Configuration
          .AUTORESET_PATDET("NO_RESET"),    // "NO_RESET", "RESET_MATCH", "RESET_NOT_MATCH" 
          .MASK(48'h3fffffffffff),          // 48-bit mask value for pattern detect (1=ignore)
          .PATTERN(48'h000000000000),       // 48-bit pattern match for pattern detect
          .SEL_MASK("MASK"),                // "C", "MASK", "ROUNDING_MODE1", "ROUNDING_MODE2" 
          .SEL_PATTERN("PATTERN"),          // Select pattern value ("PATTERN" or "C")
          .USE_PATTERN_DETECT("NO_PATDET"), // Enable pattern detect ("PATDET" or "NO_PATDET")
          // Register Control Attributes: Pipeline Register Configuration
          .ACASCREG(0),                     // Number of pipeline stages between A/ACIN and ACOUT (0, 1 or 2)
          .ADREG(0),                        // Number of pipeline stages for pre-adder (0 or 1)
          .ALUMODEREG(1),                   // Number of pipeline stages for ALUMODE (0 or 1)
          .AREG(0),                         // Number of pipeline stages for A (0, 1 or 2)
          .BCASCREG(0),                     // Number of pipeline stages between B/BCIN and BCOUT (0, 1 or 2)
          .BREG(0),                         // Number of pipeline stages for B (0, 1 or 2)
          .CARRYINREG(1),                   // Number of pipeline stages for CARRYIN (0 or 1)
          .CARRYINSELREG(1),                // Number of pipeline stages for CARRYINSEL (0 or 1)
          .CREG(0),                         // Number of pipeline stages for C (0 or 1)
          .DREG(0),                         // Number of pipeline stages for D (0 or 1)
          .INMODEREG(0),                    // Number of pipeline stages for INMODE (0 or 1)
          .MREG(1),                         // Number of multiplier pipeline stages (0 or 1)
          .OPMODEREG(1),                    // Number of pipeline stages for OPMODE (0 or 1)
          .PREG(1)                          // Number of pipeline stages for P (0 or 1)
       )
       dsp_0 (
         
          .PCOUT(dsp0_casc_out),                   // 48-bit output: Cascade output
       
          .P(dsp0_out),                           // 48-bit output: Primary data output
         
          // Control: 4-bit (each) input: Control Inputs/Status Bits
          .ALUMODE(4'h0),               // 4-bit input: ALU control input
          .CARRYINSEL(3'b000),         // 3-bit input: Carry select input
          .CLK(clk),                       // 1-bit input: Clock input
          .INMODE(5'b00000),                 // 5-bit input: INMODE control input
          .OPMODE(7'b0000101),                 // 7-bit input: Operation mode input
          // Data: 30-bit (each) input: Data Ports
          .A({14'd0, a0}),                           // 30-bit input: A data input
          .B({2'b00, b0}),                           // 18-bit input: B data input
          .C(48'd0),                           // 48-bit input: C data input
          .CARRYIN(1'b0),
          // Reset/Clock Enable: 1-bit (each) input: Reset/Clock Enable Inputs
          .CEA1(1'b1),                     // 1-bit input: Clock enable input for 1st stage AREG
          .CEA2(1'b1),                     // 1-bit input: Clock enable input for 2nd stage AREG
          .CEAD(1'b1),                     // 1-bit input: Clock enable input for ADREG
          .CEALUMODE(1'b1),           // 1-bit input: Clock enable input for ALUMODE
          .CEB1(1'b1),                     // 1-bit input: Clock enable input for 1st stage BREG
          .CEB2(1'b1),                     // 1-bit input: Clock enable input for 2nd stage BREG
          .CEC(1'b1),                       // 1-bit input: Clock enable input for CREG
          .CECARRYIN(1'b1),           // 1-bit input: Clock enable input for CARRYINREG
          .CECTRL(1'b1),                 // 1-bit input: Clock enable input for OPMODEREG and CARRYINSELREG
          .CED(1'b1),                       // 1-bit input: Clock enable input for DREG
          .CEINMODE(1'b1),             // 1-bit input: Clock enable input for INMODEREG
          .CEM(1'b1),                       // 1-bit input: Clock enable input for MREG
          .CEP(1'b1),                       // 1-bit input: Clock enable input for PREG
          .RSTA(reset),                     // 1-bit input: Reset input for AREG
          .RSTALLCARRYIN(reset),   // 1-bit input: Reset input for CARRYINREG
          .RSTALUMODE(reset),         // 1-bit input: Reset input for ALUMODEREG
          .RSTB(reset),                     // 1-bit input: Reset input for BREG
          .RSTC(reset),                     // 1-bit input: Reset input for CREG
          .RSTCTRL(reset),               // 1-bit input: Reset input for OPMODEREG and CARRYINSELREG
          .RSTD(reset),                     // 1-bit input: Reset input for DREG and ADREG
          .RSTINMODE(reset),           // 1-bit input: Reset input for INMODEREG
          .RSTM(reset),                     // 1-bit input: Reset input for MREG
          .RSTP(reset)                      // 1-bit input: Reset input for PREG
       );
       
       assign preadd_a = {1'b0, a0} + {1'b0, a1};
       assign preadd_b = {1'b0, b0} + {1'b0, b1};
       
              DSP48E1 #(
          // Feature Control Attributes: Data Path Selection
          .A_INPUT("DIRECT"),               // Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
          .B_INPUT("DIRECT"),               // Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
          .USE_DPORT("FALSE"),              // Select D port usage (TRUE or FALSE)
          .USE_MULT("MULTIPLY"),            // Select multiplier usage ("MULTIPLY", "DYNAMIC", or "NONE")
          .USE_SIMD("ONE48"),               // SIMD selection ("ONE48", "TWO24", "FOUR12")
          // Pattern Detector Attributes: Pattern Detection Configuration
          .AUTORESET_PATDET("NO_RESET"),    // "NO_RESET", "RESET_MATCH", "RESET_NOT_MATCH" 
          .MASK(48'h3fffffffffff),          // 48-bit mask value for pattern detect (1=ignore)
          .PATTERN(48'h000000000000),       // 48-bit pattern match for pattern detect
          .SEL_MASK("MASK"),                // "C", "MASK", "ROUNDING_MODE1", "ROUNDING_MODE2" 
          .SEL_PATTERN("PATTERN"),          // Select pattern value ("PATTERN" or "C")
          .USE_PATTERN_DETECT("NO_PATDET"), // Enable pattern detect ("PATDET" or "NO_PATDET")
          // Register Control Attributes: Pipeline Register Configuration
          .ACASCREG(1),                     // Number of pipeline stages between A/ACIN and ACOUT (0, 1 or 2)
          .ADREG(0),                        // Number of pipeline stages for pre-adder (0 or 1)
          .ALUMODEREG(1),                   // Number of pipeline stages for ALUMODE (0 or 1)
          .AREG(1),                         // Number of pipeline stages for A (0, 1 or 2)
          .BCASCREG(1),                     // Number of pipeline stages between B/BCIN and BCOUT (0, 1 or 2)
          .BREG(1),                         // Number of pipeline stages for B (0, 1 or 2)
          .CARRYINREG(1),                   // Number of pipeline stages for CARRYIN (0 or 1)
          .CARRYINSELREG(1),                // Number of pipeline stages for CARRYINSEL (0 or 1)
          .CREG(0),                         // Number of pipeline stages for C (0 or 1)
          .DREG(0),                         // Number of pipeline stages for D (0 or 1)
          .INMODEREG(0),                    // Number of pipeline stages for INMODE (0 or 1)
          .MREG(1),                         // Number of multiplier pipeline stages (0 or 1)
          .OPMODEREG(1),                    // Number of pipeline stages for OPMODE (0 or 1)
          .PREG(1)                          // Number of pipeline stages for P (0 or 1)
       )
       dsp_1 (
         
          .PCOUT(dsp1_casc_out),                   // 48-bit output: Cascade output
       
          .P(dsp1_out),                           // 48-bit output: Primary data output
         
          .PCIN(dsp0_casc_out),
          // Control: 4-bit (each) input: Control Inputs/Status Bits
          .ALUMODE(4'h1),               // 4-bit input: ALU control input
          .CARRYINSEL(3'b000),         // 3-bit input: Carry select input
          .CLK(clk),                       // 1-bit input: Clock input
          .INMODE(5'b10001),                 // 5-bit input: INMODE control input
          .OPMODE(7'b0010101),                 // 7-bit input: Operation mode input
          // Data: 30-bit (each) input: Data Ports
          .A({13'd0, preadd_a}),                           // 30-bit input: A data input
          .B({1'b0, preadd_b}),                           // 18-bit input: B data input
          .C(48'd0),                           // 48-bit input: C data input
          .CARRYIN(1'b1),
          // Reset/Clock Enable: 1-bit (each) input: Reset/Clock Enable Inputs
          .CEA1(1'b1),                     // 1-bit input: Clock enable input for 1st stage AREG
          .CEA2(1'b1),                     // 1-bit input: Clock enable input for 2nd stage AREG
          .CEAD(1'b1),                     // 1-bit input: Clock enable input for ADREG
          .CEALUMODE(1'b1),           // 1-bit input: Clock enable input for ALUMODE
          .CEB1(1'b1),                     // 1-bit input: Clock enable input for 1st stage BREG
          .CEB2(1'b1),                     // 1-bit input: Clock enable input for 2nd stage BREG
          .CEC(1'b1),                       // 1-bit input: Clock enable input for CREG
          .CECARRYIN(1'b1),           // 1-bit input: Clock enable input for CARRYINREG
          .CECTRL(1'b1),                 // 1-bit input: Clock enable input for OPMODEREG and CARRYINSELREG
          .CED(1'b1),                       // 1-bit input: Clock enable input for DREG
          .CEINMODE(1'b1),             // 1-bit input: Clock enable input for INMODEREG
          .CEM(1'b1),                       // 1-bit input: Clock enable input for MREG
          .CEP(1'b1),                       // 1-bit input: Clock enable input for PREG
          .RSTA(reset),                     // 1-bit input: Reset input for AREG
          .RSTALLCARRYIN(reset),   // 1-bit input: Reset input for CARRYINREG
          .RSTALUMODE(reset),         // 1-bit input: Reset input for ALUMODEREG
          .RSTB(reset),                     // 1-bit input: Reset input for BREG
          .RSTC(reset),                     // 1-bit input: Reset input for CREG
          .RSTCTRL(reset),               // 1-bit input: Reset input for OPMODEREG and CARRYINSELREG
          .RSTD(reset),                     // 1-bit input: Reset input for DREG and ADREG
          .RSTINMODE(reset),           // 1-bit input: Reset input for INMODEREG
          .RSTM(reset),                     // 1-bit input: Reset input for MREG
          .RSTP(reset)                      // 1-bit input: Reset input for PREG
       );
       
        DSP48E1 #(
                // Feature Control Attributes: Data Path Selection
                .A_INPUT("DIRECT"),               // Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
                .B_INPUT("DIRECT"),               // Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
                .USE_DPORT("FALSE"),              // Select D port usage (TRUE or FALSE)
                .USE_MULT("MULTIPLY"),            // Select multiplier usage ("MULTIPLY", "DYNAMIC", or "NONE")
                .USE_SIMD("ONE48"),               // SIMD selection ("ONE48", "TWO24", "FOUR12")
                // Pattern Detector Attributes: Pattern Detection Configuration
                .AUTORESET_PATDET("NO_RESET"),    // "NO_RESET", "RESET_MATCH", "RESET_NOT_MATCH" 
                .MASK(48'h3fffffffffff),          // 48-bit mask value for pattern detect (1=ignore)
                .PATTERN(48'h000000000000),       // 48-bit pattern match for pattern detect
                .SEL_MASK("MASK"),                // "C", "MASK", "ROUNDING_MODE1", "ROUNDING_MODE2" 
                .SEL_PATTERN("PATTERN"),          // Select pattern value ("PATTERN" or "C")
                .USE_PATTERN_DETECT("NO_PATDET"), // Enable pattern detect ("PATDET" or "NO_PATDET")
                // Register Control Attributes: Pipeline Register Configuration
                .ACASCREG(0),                     // Number of pipeline stages between A/ACIN and ACOUT (0, 1 or 2)
                .ADREG(0),                        // Number of pipeline stages for pre-adder (0 or 1)
                .ALUMODEREG(1),                   // Number of pipeline stages for ALUMODE (0 or 1)
                .AREG(0),                         // Number of pipeline stages for A (0, 1 or 2)
                .BCASCREG(0),                     // Number of pipeline stages between B/BCIN and BCOUT (0, 1 or 2)
                .BREG(0),                         // Number of pipeline stages for B (0, 1 or 2)
                .CARRYINREG(1),                   // Number of pipeline stages for CARRYIN (0 or 1)
                .CARRYINSELREG(1),                // Number of pipeline stages for CARRYINSEL (0 or 1)
                .CREG(0),                         // Number of pipeline stages for C (0 or 1)
                .DREG(0),                         // Number of pipeline stages for D (0 or 1)
                .INMODEREG(0),                    // Number of pipeline stages for INMODE (0 or 1)
                .MREG(1),                         // Number of multiplier pipeline stages (0 or 1)
                .OPMODEREG(1),                    // Number of pipeline stages for OPMODE (0 or 1)
                .PREG(1)                          // Number of pipeline stages for P (0 or 1)
             )
             dsp_2 (
               
                .PCOUT(dsp2_casc_out),                   // 48-bit output: Cascade output
             
                .P(dsp2_out),                           // 48-bit output: Primary data output
               
                // Control: 4-bit (each) input: Control Inputs/Status Bits
                .ALUMODE(4'h0),               // 4-bit input: ALU control input
                .CARRYINSEL(3'b000),         // 3-bit input: Carry select input
                .CLK(clk),                       // 1-bit input: Clock input
                .INMODE(5'b00000),                 // 5-bit input: INMODE control input
                .OPMODE(7'b0000101),                 // 7-bit input: Operation mode input
                // Data: 30-bit (each) input: Data Ports
                .A({14'd0, a1}),                           // 30-bit input: A data input
                .B({2'b00, b1}),                           // 18-bit input: B data input
                .C(48'd0),                           // 48-bit input: C data input
                .CARRYIN(1'b0),
                // Reset/Clock Enable: 1-bit (each) input: Reset/Clock Enable Inputs
                .CEA1(1'b1),                     // 1-bit input: Clock enable input for 1st stage AREG
                .CEA2(1'b1),                     // 1-bit input: Clock enable input for 2nd stage AREG
                .CEAD(1'b1),                     // 1-bit input: Clock enable input for ADREG
                .CEALUMODE(1'b1),           // 1-bit input: Clock enable input for ALUMODE
                .CEB1(1'b1),                     // 1-bit input: Clock enable input for 1st stage BREG
                .CEB2(1'b1),                     // 1-bit input: Clock enable input for 2nd stage BREG
                .CEC(1'b1),                       // 1-bit input: Clock enable input for CREG
                .CECARRYIN(1'b1),           // 1-bit input: Clock enable input for CARRYINREG
                .CECTRL(1'b1),                 // 1-bit input: Clock enable input for OPMODEREG and CARRYINSELREG
                .CED(1'b1),                       // 1-bit input: Clock enable input for DREG
                .CEINMODE(1'b1),             // 1-bit input: Clock enable input for INMODEREG
                .CEM(1'b1),                       // 1-bit input: Clock enable input for MREG
                .CEP(1'b1),                       // 1-bit input: Clock enable input for PREG
                .RSTA(reset),                     // 1-bit input: Reset input for AREG
                .RSTALLCARRYIN(reset),   // 1-bit input: Reset input for CARRYINREG
                .RSTALUMODE(reset),         // 1-bit input: Reset input for ALUMODEREG
                .RSTB(reset),                     // 1-bit input: Reset input for BREG
                .RSTC(reset),                     // 1-bit input: Reset input for CREG
                .RSTCTRL(reset),               // 1-bit input: Reset input for OPMODEREG and CARRYINSELREG
                .RSTD(reset),                     // 1-bit input: Reset input for DREG and ADREG
                .RSTINMODE(reset),           // 1-bit input: Reset input for INMODEREG
                .RSTM(reset),                     // 1-bit input: Reset input for MREG
                .RSTP(reset)                      // 1-bit input: Reset input for PREG
             );
        /*always_ff @(posedge clk) begin
            ch_reg2 <= c_reg1[63:32];
            ch_reg3 <= ch_reg2;
            clh_reg2 <= c_reg1[31:16];
        end*/
        
        always_ff @(posedge clk) begin
           if(reset == 1'b1) begin
              multiply0 <= 0;
              multiply1 <= 0;
              multiply2 <= 0;
              multiply3 <= 0;
           end 
           else begin
               multiply0 <= multiply;
               multiply1 <= multiply0;
               multiply2 <= multiply1;
               multiply3 <= multiply2;
           end
        end
        
        always_ff @(posedge clk) begin
           if(reset == 1'b1) begin
              index0 <= 0;
              index1 <= 0;
              index2 <= 0;
              index3 <= 0;
              index4 <= 0;
           end 
           else begin
               index0 <= index;
               index1 <= index0;
               index2 <= index1;
               index3 <= index2;
               index4 <= index3;
           end
        end
        
        always_ff @(posedge clk) begin
           if(reset == 1'b1) begin
              loadacc0 <= 0;
              loadacc1 <= 0;
              loadacc2 <= 0;
              loadacc3 <= 0;
           end 
           else begin
               loadacc0 <= loadacc;
               loadacc1 <= loadacc0;
               loadacc2 <= loadacc1;
               loadacc3 <= loadacc2;
           end
        end
        
        always_ff @(posedge clk) begin
           if(reset == 1'b1) begin
              acc_high0 <= 0;
              acc_high1 <= 0;
              acc_high2 <= 0;
              acc_high3 <= 0;
           end 
           else begin
               acc_high0 <= b;
               acc_high1 <= acc_high0;
               acc_high2 <= acc_high1;
               acc_high3 <= acc_high2;
           end
        end
        
        always_ff @(posedge clk) begin
           if(reset == 1'b1) begin
              ce0 <= 0;
              ce1 <= 0;
              ce2 <= 0;
              ce3 <= 0;
           end 
           else begin
               ce0 <= ce;
               ce1 <= ce0;
               ce2 <= ce1;
               ce3 <= ce2;
           end
        end
          
        
        assign stage0_out_0 = {1'b0, dsp0_out[15:0]} + ((multiply1==1'b1)?17'd0:{1'd0, acc0[15:0]});
       
         
        
        ram_1r1w #(.DATA_WIDTH(16), .SIZE(`THREAD_NUMB)) ram0(clk, index0, index1, 1'b1, 1'b1, ((index0==index1)?ce1:1'b0), ce1, 
                         stage0_out_0[15:0], stage0_out_0[15:0], acc0, stage0_out_reg0[15:0]);
        always_ff @(posedge clk) begin
            stage0_out_reg0[16] <= stage0_out_0[16];
        end
        
         //*********    
//        always_ff @(posedge clk) begin
//            stage1_out <= ({1'b0, dsp1_out[34:0]} + {4'd0, ch_reg2[15:1], stage0_out_reg0[32:16]} - {4'd0, dsp2_out[31:0]});
//        end
        
        DSP48E1 #(
                        // Feature Control Attributes: Data Path Selection
                        .A_INPUT("DIRECT"),               // Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
                        .B_INPUT("DIRECT"),               // Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
                        .USE_DPORT("FALSE"),              // Select D port usage (TRUE or FALSE)
                        .USE_MULT("NONE"),            // Select multiplier usage ("MULTIPLY", "DYNAMIC", or "NONE")
                        .USE_SIMD("ONE48"),               // SIMD selection ("ONE48", "TWO24", "FOUR12")
                        // Pattern Detector Attributes: Pattern Detection Configuration
                        .AUTORESET_PATDET("NO_RESET"),    // "NO_RESET", "RESET_MATCH", "RESET_NOT_MATCH" 
                        .MASK(48'h3fffffffffff),          // 48-bit mask value for pattern detect (1=ignore)
                        .PATTERN(48'h000000000000),       // 48-bit pattern match for pattern detect
                        .SEL_MASK("MASK"),                // "C", "MASK", "ROUNDING_MODE1", "ROUNDING_MODE2" 
                        .SEL_PATTERN("PATTERN"),          // Select pattern value ("PATTERN" or "C")
                        .USE_PATTERN_DETECT("NO_PATDET"), // Enable pattern detect ("PATDET" or "NO_PATDET")
                        // Register Control Attributes: Pipeline Register Configuration
                        .ACASCREG(0),                     // Number of pipeline stages between A/ACIN and ACOUT (0, 1 or 2)
                        .ADREG(0),                        // Number of pipeline stages for pre-adder (0 or 1)
                        .ALUMODEREG(0),                   // Number of pipeline stages for ALUMODE (0 or 1)
                        .AREG(0),                         // Number of pipeline stages for A (0, 1 or 2)
                        .BCASCREG(0),                     // Number of pipeline stages between B/BCIN and BCOUT (0, 1 or 2)
                        .BREG(0),                         // Number of pipeline stages for B (0, 1 or 2)
                        .CARRYINREG(0),                   // Number of pipeline stages for CARRYIN (0 or 1)
                        .CARRYINSELREG(0),                // Number of pipeline stages for CARRYINSEL (0 or 1)
                        .CREG(0),                         // Number of pipeline stages for C (0 or 1)
                        .DREG(0),                         // Number of pipeline stages for D (0 or 1)
                        .INMODEREG(0),                    // Number of pipeline stages for INMODE (0 or 1)
                        .MREG(0),                         // Number of multiplier pipeline stages (0 or 1)
                        .OPMODEREG(0),                    // Number of pipeline stages for OPMODE (0 or 1)
                        .PREG(0)                          // Number of pipeline stages for P (0 or 1)
                     )
                     dsp_3 (
                       
                        .PCOUT(dsp3_casc_out),                   // 48-bit output: Cascade output
                     
                        .P(dsp3_out),                           // 48-bit output: Primary data output
                        .PCIN(dsp1_casc_out),
                        // Control: 4-bit (each) input: Control Inputs/Status Bits
                        .ALUMODE(4'h0),               // 4-bit input: ALU control input
                        .CARRYINSEL(3'b000),         // 3-bit input: Carry select input
                        .CLK(clk),                       // 1-bit input: Clock input
                        .INMODE(5'b00000),                 // 5-bit input: INMODE control input
                        .OPMODE({5'b00111, {2{~multiply2}}}),                 // 7-bit input: Operation mode input
                        // Data: 30-bit (each) input: Data Ports
                        .A(/*dsp2_out[31:2]*/{14'd0, 16'd0}),                           // 30-bit input: A data input
                        .B({/*dsp2_out[1]*/2'b0, /*stage0_out_reg0[32:16]*/acc1[15:0]}),                           // 18-bit input: B data input
                        .C(stage2_out_reg0),                           // 48-bit input: C data input
                        .CARRYIN(stage0_out_reg0[16]),
  
                        // Reset/Clock Enable: 1-bit (each) input: Reset/Clock Enable Inputs
                        .CEA1(1'b1),                     // 1-bit input: Clock enable input for 1st stage AREG
                        .CEA2(1'b1),                     // 1-bit input: Clock enable input for 2nd stage AREG
                        .CEAD(1'b1),                     // 1-bit input: Clock enable input for ADREG
                        .CEALUMODE(1'b1),           // 1-bit input: Clock enable input for ALUMODE
                        .CEB1(1'b1),                     // 1-bit input: Clock enable input for 1st stage BREG
                        .CEB2(1'b1),                     // 1-bit input: Clock enable input for 2nd stage BREG
                        .CEC(1'b1),                       // 1-bit input: Clock enable input for CREG
                        .CECARRYIN(1'b1),           // 1-bit input: Clock enable input for CARRYINREG
                        .CECTRL(1'b1),                 // 1-bit input: Clock enable input for OPMODEREG and CARRYINSELREG
                        .CED(1'b1),                       // 1-bit input: Clock enable input for DREG
                        .CEINMODE(1'b1),             // 1-bit input: Clock enable input for INMODEREG
                        .CEM(1'b1),                       // 1-bit input: Clock enable input for MREG
                        .CEP(1'b1),                       // 1-bit input: Clock enable input for PREG
                        .RSTA(reset),                     // 1-bit input: Reset input for AREG
                        .RSTALLCARRYIN(reset),   // 1-bit input: Reset input for CARRYINREG
                        .RSTALUMODE(reset),         // 1-bit input: Reset input for ALUMODEREG
                        .RSTB(reset),                     // 1-bit input: Reset input for BREG
                        .RSTC(reset),                     // 1-bit input: Reset input for CREG
                        .RSTCTRL(reset),               // 1-bit input: Reset input for OPMODEREG and CARRYINSELREG
                        .RSTD(reset),                     // 1-bit input: Reset input for DREG and ADREG
                        .RSTINMODE(reset),           // 1-bit input: Reset input for INMODEREG
                        .RSTM(reset),                     // 1-bit input: Reset input for MREG
                        .RSTP(reset)                      // 1-bit input: Reset input for PREG
                     );
        ram_1r1w #(.DATA_WIDTH(16), .SIZE(`THREAD_NUMB)) ram1(clk, index1, index2, 1'b1, 1'b1, ((index1==index2)?ce2:1'b0), ce2, 
                 dsp3_out[15:0], dsp3_out[15:0], acc1, stage1_out[15:0]);
        
        always_ff @(posedge clk) begin
            stage1_out[47:16] <= dsp3_out[47:16];
        end
        
        always_ff @(posedge clk) begin
            stage0_out_reg1 <= stage0_out_reg0[15:0];
        end
        
         //**************************
        assign dsp2_out_neg = ~{16'd0, dsp2_out[31:0]};
        always_ff @(posedge clk) begin
            stage2_out_reg0[47:16] <=  dsp2_out[31:0] + dsp2_out_neg[47:16];
            {stage2_out_carry, stage2_out_reg0[15:0]} <= {1'b0, dsp0_out[31:16]} + {1'b0, dsp2_out_neg[15:0]} + {16'd0, 1'b1}/*+ {16'd0, 15'd0, ch_reg2[0]}*/;
            stage2_out_carry1 <= stage2_out_carry;
        end
                 
        always_comb begin
            if(loadacc3 == 1'b1)
               stage2_out_1 = acc_high3;
            else
               stage2_out_1 = stage1_out[47:16] + {31'd0, stage2_out_carry1} + ((multiply3==1'b1)?32'd0:acc2[31:0]);
        end
        
        
        ram_1r1w #(.DATA_WIDTH(32), .SIZE(`THREAD_NUMB)) ram2(clk, index2, index3, 1'b1, 1'b1, ((index2==index3)?ce3:1'b0), ce3, 
                                 stage2_out_1, stage2_out_1, acc2, stage2_out_reg1);
        
         always_ff @(posedge clk) begin
            stage0_out_reg2 <= stage0_out_reg1;
         end
         
         always_ff @(posedge clk) begin
           stage1_out_reg1 <= stage1_out[15:0];
         end
        
        
             
        assign q = {stage2_out_reg1, stage1_out_reg1, stage0_out_reg2};            
             
endmodule
