`include "nuplus_define.sv"

/*
 * This is the principal execution pipe. It execute: jumps, arithmetic and logic operations, control register accesses, and moves.
 * In order to understand the correct operation, the kind of operation and the pipe selection fields inside the instruction decoded
 * are used.
 * The vectorial operation are executed in parallel using the concept of lane: each lane performs a scalar operation and the
 * final vectorial result is composed chaining all the scalar intermediate results from the lanes.
 * If an operation is scalar, only the first lane makes sense.
 */
`define SCALAR_GAP     (`REGISTER_SIZE / `HW_LANE)

module int_pipe #(
		parameter TILE_ID = 0,
		parameter CORE_ID = 0 )
	(
		input                        clk,
		input                        reset,
		input 						 enable,

		// From Operand Fetch
		input                        opf_valid,
		input  instruction_decoded_t opf_inst_scheduled,
		input  vec_reg_size_t        opf_fetched_op0,
		input  vec_reg_size_t        opf_fetched_op1,
		input  scal_reg_size_t       opf_fetched_op2,
		input  hw_lane_mask_t        opf_hw_lane_mask,

		// To Writeback
		output logic                 int_valid,
		output instruction_decoded_t int_inst_scheduled,
		output vec_reg_size_t        int_result,
		output hw_lane_mask_t        int_hw_lane_mask,
		output scal_reg_size_t       int_2nd_result,
        output vec_reg_size_t        int_forward,
        output thread_id_t           int_thread_forward
	);

	logic           is_cr_access;
	logic           is_int_instr;
	logic           is_jmpsr;
	scal_reg_size_t cr_result;

	logic           is_compare;
	logic           is_move_imm;
	logic           is_shuffle;
    logic           is_sfu;
    logic           is_mixcol;
	scal_reg_size_t lane_cmp_result;
	scal_reg_size_t cmp_result;
	vec_reg_size_t  vec_result;
	vec_reg_size_t  movei_result;
	vec_reg_size_t  shfl_result;
    vec_reg_size_t  mixcol_result;
    vec_reg_size_t  mixcol_result1;
    
    scal_reg_size_t [2*`HW_LANE - 1 : 0] shfl_source;
        
    logic [`HW_LANE-1 : 0]  cout;
    
    scal_reg_size_t       int_scalar_result;
    
    vec_reg_size_t        op_forward;
    
    vec_reg_size_t        op0_actual;
    vec_reg_size_t        shfl_op2;

	assign is_jmpsr     = opf_inst_scheduled.pipe_sel == PIPE_BRANCH & opf_inst_scheduled.is_int & opf_inst_scheduled.is_branch;
	assign is_int_instr = opf_inst_scheduled.pipe_sel == PIPE_INT;
	assign is_cr_access = opf_inst_scheduled.pipe_sel == PIPE_CR;
	assign is_move_imm  = opf_inst_scheduled.is_movei;
	assign is_shuffle   = opf_inst_scheduled.is_shuffle;
    assign is_sfu       = opf_inst_scheduled.is_sfu;
	assign is_compare   = opf_inst_scheduled.op_code >= CMPEQ && opf_inst_scheduled.op_code <= CMPLE_U ;
	assign cmp_result   = ( opf_inst_scheduled.is_source0_vectorial || opf_inst_scheduled.is_source1_vectorial ) ?
		lane_cmp_result : scal_reg_size_t'( lane_cmp_result[0] );
    assign is_mixcol    = opf_inst_scheduled.is_sfu && (opf_inst_scheduled.op_code == (8'b11100000 | MIXCOL));

		
	genvar          i;
	generate
		for ( i = 0; i < `HW_LANE; i ++ ) begin
			 
           reg cin;
           assign cin = (opf_inst_scheduled.has_3_sources == 1)? opf_fetched_op2[i*`SCALAR_GAP+:1] : 1'b0;
           
           int_single_lane int_single_lane (
                  .op0    ( opf_fetched_op0[i]         ),
                  .op1    ( opf_fetched_op1[i]         ),
                  .op_code( opf_inst_scheduled.op_code ),
                  .cin    (cin),
                  .cout(cout[i]),
                  .result ( vec_result[i]              )
              );
           
           always_comb begin : INTEGER_LANE_2ND_RES
               int_scalar_result[i*`SCALAR_GAP+:`SCALAR_GAP] = {`SCALAR_GAP{1'b0}};
               int_scalar_result[i*`SCALAR_GAP+:1]  =  cout[i] & opf_hw_lane_mask[i];
          
           end
		end

		for ( i = 0; i < `HW_LANE; i++ ) begin
			assign lane_cmp_result[i] = vec_result[i][0];

			//Move Immediate result composer
			always_comb begin : RESULT_COMPOSER_MOVE
				case ( opf_inst_scheduled.op_code )

					MOVEI,
					MOVEI_L : begin
						movei_result[i][`REGISTER_SIZE-1 : `REGISTER_SIZE/2] = 0;
						movei_result[i][(`REGISTER_SIZE/2)-1 : 0 ]           = opf_fetched_op1[i];
					end

					MOVEI_H : begin
						movei_result[i][`REGISTER_SIZE-1 : `REGISTER_SIZE/2] = opf_fetched_op1[i];
						movei_result[i][(`REGISTER_SIZE/2)-1 : 0 ]           = 0;
					end

					default : movei_result[i]                                = 0;

				endcase
			end

		end
		
		for ( i = 0; i < `HW_LANE; i++ ) begin
          
           assign  shfl_source[i+`HW_LANE] = opf_fetched_op1[i];   
           assign  shfl_source[i] = opf_fetched_op0[i];
          
       
           always_comb begin
               case(opf_inst_scheduled.op_code)
                   INTLVL: shfl_op2[i] = 2*i;
                   INTLVH: shfl_op2[i] = 2*i+1;
                   DEILVL: shfl_op2[i] = (i[0]*`HW_LANE) + (i >> 1);
                   DEILVH: shfl_op2[i] = (i[0]*`HW_LANE) + (i >> 1) + (`HW_LANE/2);
                   default:  shfl_op2[i] = opf_fetched_op1[i] & (`HW_LANE - 1);
               endcase
           end
           
           assign shfl_result[i] = (opf_inst_scheduled.op_code == SHUFFLE && opf_fetched_op1[i] == {32{1'b1}})?32'd0:shfl_source[shfl_op2[i]];
       end
	endgenerate
	
	genvar h;
    genvar j;
    generate
       
       for(h = 0; h < `HW_LANE/4; h++) begin
           logic [15:0][7:0] s;
           logic [15:0][7:0] sp;
           
           for(j = 0; j < 4; j++) begin
                assign s[j*4+:4] = {opf_fetched_op0[h*4+3][j*8+:8], opf_fetched_op0[h*4+2][j*8+:8], opf_fetched_op0[h*4+1][j*8+:8], opf_fetched_op0[h*4+0][j*8+:8]};
                assign {mixcol_result1[h*4+3][j*8+:8], mixcol_result1[h*4+2][j*8+:8], mixcol_result1[h*4+1][j*8+:8], mixcol_result1[h*4+0][j*8+:8]} = sp[j*4+:4];
           end
           
           mixcolumns mixcol0(s[3:0], sp[3:0]);
           mixcolumns mixcol1(s[7:4], sp[7:4]);
           mixcolumns mixcol2(s[11:8], sp[11:8]);
           mixcolumns mixcol3(s[15:12], sp[15:12]);
       end
    
    endgenerate
    
    assign mixcol_result = mixcol_result1 ^ opf_fetched_op1;
	
	control_register #(
		.TILE_ID( TILE_ID ),
		.CORE_ID( CORE_ID )
	)
	control_register (
		//From Operand Fetch
		.opf_inst_scheduled( opf_inst_scheduled ),
		.opf_fecthed_op1   ( opf_fetched_op1    ),
		//To Writeback
		.cr_result         ( cr_result          )
	);

	always_ff @ ( posedge clk, posedge reset ) begin
		if ( reset )
			int_valid <= 1'b0;
		else if ( enable )
			int_valid <= opf_valid & ( is_int_instr | is_cr_access | is_jmpsr );
	end

	always_ff @ ( posedge clk ) begin
		if ( enable ) begin
			int_inst_scheduled <= opf_inst_scheduled;
			int_hw_lane_mask   <= opf_hw_lane_mask;

			int_2nd_result     <= int_scalar_result;
		
		    int_result         <= op_forward;
		end
	end
	
	
    always_comb  begin 
        op_forward = 0;      
        if (is_jmpsr)
            op_forward[0] = opf_fetched_op0[0] + 4; // PC + 4
        else if(is_mixcol)
            op_forward = mixcol_result;
        else if ( is_cr_access )
            op_forward[0] = cr_result;
        else if( is_compare )
            op_forward[0] = cmp_result;
        else if ( is_move_imm )
            op_forward    = movei_result;
        else if (is_shuffle )
            op_forward      = shfl_result;
        else
            op_forward    = vec_result;
    end
    
    assign int_forward = op_forward;
    assign int_thread_forward = opf_inst_scheduled.thread_id;


`ifdef SIMULATION
	always_ff @ (posedge clk )
		if ( int_valid )
			assert( int_inst_scheduled.op_code.alu_opcode <= SEXT32)
			else $error("Opcode error! Time: %t  \t PC: %h", $time(), opf_inst_scheduled.pc);
`endif

endmodule