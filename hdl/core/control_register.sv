`include "nuplus_define.sv"

// TODO: NON � UN CONTROL REGISTER, MA SOLO UN DUMMY
/*
 * This module returns 4 parameter: TILE_ID, CORE_ID, THREAD_ID, GLOBAL_ID.
 * The parameters CORE_ID and GLOBAL_ID are significant only if other nu+ core can be supported
 * in the same tile.
 */

module control_register #(
		parameter TILE_ID = 0,
		parameter CORE_ID = 4 )
	(
		// From Operand Fetch
		input  instruction_decoded_t opf_inst_scheduled,
		input  vec_reg_size_t        opf_fecthed_op1,

		// To Writeback
		output scal_reg_size_t       cr_result //TODO: the size of this signal must depend on the number of cores and threads
	);

	control_register_index_t command;

	assign command = control_register_index_t'( opf_fecthed_op1 );

	always_comb
		case ( command )
			READ_TILE_ID : cr_result   = TILE_ID;
			READ_CORE_ID : cr_result   = CORE_ID;
			READ_THREAD_ID : cr_result = opf_inst_scheduled.thread_id;
			READ_GLOBAL_ID : cr_result = {TILE_ID, CORE_ID, opf_inst_scheduled.thread_id};
			default :
				`ifdef SIMULATION
					cr_result = {`REGISTER_SIZE{1'bx}};
				`else
					cr_result     = {`REGISTER_SIZE{1'b0}};
				`endif

		endcase

endmodule