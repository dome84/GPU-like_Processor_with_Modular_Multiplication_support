// TODO FARE LA DEFINITIVA
// E' SOLO UN DUMMY
`include "nuplus_define.sv"

/*
 * This is one of the lane inside the execution int pipe.
 */
module int_single_lane (
		input  scal_reg_size_t    op0,
		input  scal_reg_size_t    op1,
		input  opcode_t           op_code,
        input  logic              cin,
        
        output logic              cout,
		output scal_reg_size_t    result
	);


	scal_reg_size_t                              op0_signed;
	scal_reg_size_t                              op1_signed;
	logic           [`REGISTER_SIZE * 2 - 1 : 0] aritm_shift;
	logic [`REGISTER_SIZE * 2 - 1 : 0]  		 circ_shift;
    logic [`REGISTER_SIZE * 2 - 1 : 0]           circ_result;
	logic                                        is_greater_uns;
	logic                                        is_equal;
	logic                                        is_greater;
	scal_reg_size_t                    			 add_result, sub_result, sbox_result;
    logic                                        c_out, b_out;


	assign op0_signed     = integer'( op0 );
	assign op1_signed     = integer'( op1 );
	
	assign circ_shift       = {op0, op0};
    assign aritm_shift      = {{`REGISTER_SIZE{op0[`REGISTER_SIZE-1]}}, op0};
    assign is_equal         = op0_signed == op1_signed;     
    assign is_greater       = op0_signed > op1_signed;
    assign is_greater_uns   = op0 > op1;
    
    assign {c_out, add_result} = {1'b0, op0} + {1'b0, op1} + {{`REGISTER_SIZE{1'b0}}, cin};
    assign {b_out, sub_result} = {1'b0, op0} - {1'b0, op1} - {{`REGISTER_SIZE{1'b0}}, cin};
    
    assign circ_result = (circ_shift << op1);
    
    genvar i;
    generate
       for(i = 0; i < `BYTE_PER_REGISTER; i++) begin
           aesbox sbox(
               .addr(  op0[8*i+:8] ),
               .rdata( sbox_result[8*i+:8])
           );
       end
    endgenerate

	logic is_not_null;
	logic [$clog2(`REGISTER_SIZE) - 1 : 0] encode;
	logic [`REGISTER_SIZE - 1 : 0] clz_ctz_in, inverted_op0;
	always_comb begin
		for (int i=0; i < `REGISTER_SIZE; i = i+1)
	    	 inverted_op0[i] = op0[`REGISTER_SIZE - i -1];
		clz_ctz_in = (op_code == CLZ) ? op0 : inverted_op0;
	end
	
	priority_encoder_nuplus #(
		.INPUT_WIDTH (`REGISTER_SIZE ),
		.MAX_PRIORITY("MSB")
	)
	u_priority_encoder_nuplus (
		.decode(clz_ctz_in),
		.encode(encode),
		.valid (is_not_null )
	);

	always_comb begin
	    cout = 1'b0;
	    
		case ( op_code )
			ADD, 
            ADDC  : begin
                        result = add_result;
                        cout = c_out;
                    end
            SUB, 
            SUBB  : begin
                        result = sub_result;
                        cout = b_out;
                    end
			MOVE    : result = op0;
			
			ASHR    : result = ( aritm_shift >> op1 );
			SHR     : result = ( op0 >> op1 );
			SHL     : result = ( op0 << op1 );
			ROTR    : result = (circ_shift >> op1);
            ROTL    : result = circ_result[`REGISTER_SIZE * 2 -1 : `REGISTER_SIZE];
			CLZ ,     
			CTZ     : result = is_not_null ?  `REGISTER_SIZE - encode - 1 : `REGISTER_SIZE;
			OR      : result = op0 | op1;
			AND     : result = op0 & op1;
			XOR     : result = op0 ^ op1;

			CMPEQ   : result = scal_reg_size_t'( is_equal );
			CMPNE   : result = scal_reg_size_t'( ~is_equal );
			CMPGT   : result = scal_reg_size_t'( is_greater );
			CMPGE   : result = scal_reg_size_t'( is_greater | is_equal );
			CMPLE   : result = scal_reg_size_t'( ~is_greater | is_equal );
			CMPLT   : result = scal_reg_size_t'( ~is_greater & ~is_equal );
			CMPGT_U : result = scal_reg_size_t'( is_greater_uns );
			CMPGE_U : result = scal_reg_size_t'( is_greater_uns | is_equal );
			CMPLT_U : result = scal_reg_size_t'( ~is_greater_uns & ~is_equal );
			CMPLE_U : result = scal_reg_size_t'( ~is_greater_uns | is_equal );

			SEXT8   : result = {{24{op0[7]}}, op0[7:0]};
			SEXT16  : result = {{16{op0[15]}}, op0[15:0]};
			SEXT32  : result = op0;
			
            (8'b11100000 | SBOX)    : result = sbox_result;
            (8'b11100000 | MIXCOLLAST): result = op0 ^ op1;

			default : 
			`ifdef SIMULATION
					result = {`REGISTER_SIZE{1'bx}};
				`else
					result     = {`REGISTER_SIZE{1'b0}};
				`endif
		endcase
	end

endmodule