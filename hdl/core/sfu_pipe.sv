`include "nuplus_define.sv"

`define SCALAR_GAP     (`REGISTER_SIZE / `HW_LANE)


module sfu_pipe #(
		parameter TILE_ID = 0,
		parameter CORE_ID = 0 )
	(
		input                        clk,
		input                        reset,

		// From Operand Fetch
		input                        opf_valid,
		input  instruction_decoded_t opf_inst_scheduled,
		input  vec_reg_size_t        opf_fetched_op0,
		input  vec_reg_size_t        opf_fetched_op1,
		input  hw_lane_mask_t        opf_hw_lane_mask,

		// To Writeback
		output logic                 sfu_valid,
		output instruction_decoded_t sfu_inst_scheduled,
		output vec_reg_size_t        sfu_result,
		output hw_lane_mask_t        sfu_hw_lane_mask,
		output vec_reg_size_t        sfu_forward,
		output thread_id_t           sfu_thread_forward,
		output logic                 sfu_valid_forward
	);


	logic           is_sfu;
	logic           is_mixcol;
	
	vec_reg_size_t  vec_result;
	
	vec_reg_size_t  mixcol_result;
	vec_reg_size_t  mixcol_result1;
	
	vec_reg_size_t        op_forward;
	
	vec_reg_size_t        op0_actual;
	
	logic                        stage1_valid;
	vec_reg_size_t               stage1_fetched_op0;
    vec_reg_size_t               stage1_fetched_op1;
    instruction_decoded_t        stage1_inst_scheduled;
    hw_lane_mask_t               stage1_hw_lane_mask;
    
    vec_reg_size_t               stage1_result;


	assign is_mixcol    = opf_inst_scheduled.is_sfu && (opf_inst_scheduled.op_code == MIXCOL);
		

	genvar          i;
	generate
		for ( i = 0; i < `HW_LANE; i ++ ) begin
		    
            sfu_single_lane sfu_single_lane (
                   .op1    ( opf_fetched_op0[i]         ),
                   .op2    ( opf_fetched_op1[i]         ),
                   .op_code( opf_inst_scheduled.op_code ),
                   .result ( vec_result[i]              )
               );
		    
		end
		                        
	endgenerate
	
	always_ff @ ( posedge clk ) begin
	    stage1_valid          <= opf_valid;
        stage1_inst_scheduled <= opf_inst_scheduled;
        stage1_hw_lane_mask   <= opf_hw_lane_mask;
        stage1_fetched_op0    <= opf_fetched_op0;
        stage1_fetched_op1    <= opf_fetched_op1;
        if(opf_inst_scheduled.op_code == ENCXOR)
            stage1_result         <= opf_fetched_op0;
        else
            stage1_result         <= vec_result;
        
    end
    
//    always_comb  begin
//            stage1_valid          = opf_valid;
//            stage1_inst_scheduled = opf_inst_scheduled;
//            stage1_hw_lane_mask   = opf_hw_lane_mask;
//            stage1_fetched_op0    = opf_fetched_op0;
//            stage1_fetched_op1    = opf_fetched_op1;
//            if(opf_inst_scheduled.op_code == ENCXOR)
//                stage1_result         = opf_fetched_op0;
//            else
//                stage1_result         = vec_result;
            
//        end
	
	genvar h;
	genvar j;
	generate
       
	   for(h = 0; h < `HW_LANE/4; h++) begin
	       logic [15:0][7:0] s;
           logic [15:0][7:0] sp;
           
           for(j = 0; j < 4; j++) begin
                assign s[j*4+:4] = {stage1_result[h*4+3][j*8+:8], stage1_result[h*4+2][j*8+:8], stage1_result[h*4+1][j*8+:8], stage1_result[h*4+0][j*8+:8]};
                assign {mixcol_result1[h*4+3][j*8+:8], mixcol_result1[h*4+2][j*8+:8], mixcol_result1[h*4+1][j*8+:8], mixcol_result1[h*4+0][j*8+:8]} = sp[j*4+:4];
	       end
	       
	       mixcolumns mixcol0(s[3:0], sp[3:0]);
	       mixcolumns mixcol1(s[7:4], sp[7:4]);
	       mixcolumns mixcol2(s[11:8], sp[11:8]);
	       mixcolumns mixcol3(s[15:12], sp[15:12]);
	   end
	
	endgenerate
	
	assign mixcol_result = (stage1_inst_scheduled.op_code == AESENCLAST || stage1_inst_scheduled.op_code == ENCXOR)? stage1_result ^ stage1_fetched_op1 : mixcol_result1 ^ stage1_fetched_op1;

	

	always_ff @ ( posedge clk, posedge reset ) begin
		if ( reset )
			sfu_valid <= 1'b0;
		else
			sfu_valid <= stage1_valid & ( stage1_inst_scheduled.is_sfu );
	end

	always_ff @ ( posedge clk ) begin
		sfu_inst_scheduled <= stage1_inst_scheduled;
		sfu_hw_lane_mask   <= stage1_hw_lane_mask;
		sfu_result         <= op_forward;
	
	end
	
	always_comb  begin       
            op_forward    =  mixcol_result;
    end
    
    assign sfu_forward = op_forward;
    assign sfu_thread_forward = stage1_inst_scheduled.thread_id;
    assign sfu_valid_forward  = stage1_valid & ( stage1_inst_scheduled.is_sfu );

endmodule