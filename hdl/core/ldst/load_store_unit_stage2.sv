`include "nuplus_define.sv"
`include "user_define.sv"
`include "load_store_unit_defines.sv"
`include "system_defines.sv"

/*
 * This stage has the main scope to choose a request to serve and fetching the tag&privileges from the tag cache.
 * 
 * It receives from the previous stage the load/store requests plus the recycle request for each thread; both kind of request
 * are dequeued using two different signals (they are treated differently), but assert the same signal. The recycling has the
 * priority over the regular requests. 
 * 
 * The signal cc_update_ldst_valid is important and establishes when the cache controller wants to update the cache and it has the highest priority
 * 
 * Basing on these signals, an arbiter choose a request and it performs the tag&privileges read or update.
 * In parallel, another reading can happen because of a snooping request by the cache controller.
 * 
 *  
 */

module load_store_unit_stage2 (
		input                                               clk,
		input                                               reset,

		// Load Sore Unit Stage 1
		input  thread_mask_t                                ldst1_valid,
		input  instruction_decoded_t [`THREAD_NUMB - 1 : 0] ldst1_instruction,
		input  dcache_address_t      [`THREAD_NUMB - 1 : 0] ldst1_address,
		input  dcache_line_t         [`THREAD_NUMB - 1 : 0] ldst1_store_value,
		input  dcache_store_mask_t   [`THREAD_NUMB - 1 : 0] ldst1_store_mask,
		input  hw_lane_mask_t        [`THREAD_NUMB - 1 : 0] ldst1_hw_lane_mask,

		input  thread_mask_t                                ldst1_recycle_valid,
		input  instruction_decoded_t [`THREAD_NUMB - 1 : 0] ldst1_recycle_instruction,
		input  dcache_address_t      [`THREAD_NUMB - 1 : 0] ldst1_recycle_address,
		input  dcache_line_t         [`THREAD_NUMB - 1 : 0] ldst1_recycle_store_value,
		input  dcache_store_mask_t   [`THREAD_NUMB - 1 : 0] ldst1_recycle_store_mask,
		input  hw_lane_mask_t        [`THREAD_NUMB - 1 : 0] ldst1_recycle_hw_lane_mask,

		output thread_mask_t                                ldst2_dequeue_instruction,
		output thread_mask_t                                ldst2_recycled,

		// Load Sore Unit Stage 3
		input  thread_mask_t                                ldst3_thread_sleep,

		output logic                                        ldst2_valid,
		output instruction_decoded_t                        ldst2_instruction,
		output dcache_address_t                             ldst2_address,
		output dcache_line_t                                ldst2_store_value,
		output dcache_store_mask_t                          ldst2_store_mask,
		output hw_lane_mask_t                               ldst2_hw_lane_mask,
		output dcache_tag_t          [`DCACHE_WAY - 1 : 0]  ldst2_tag_read,
		output dcache_privileges_t   [`DCACHE_WAY - 1 : 0]  ldst2_privileges_read,
		output logic                                        ldst2_is_flush,

		output logic                                        ldst2_update_valid,
		output dcache_way_idx_t                             ldst2_update_way,
		output logic                                        ldst2_evict_valid,
		
		// Synch Core
		output logic			     [`THREAD_NUMB - 1 : 0] s2_no_ls_pending,

		// Cache Controller
		input                                               cc_update_ldst_valid,
		input  cc_command_t                                 cc_update_ldst_command,
		input  dcache_way_idx_t                             cc_update_ldst_way,
		input  dcache_address_t                             cc_update_ldst_address,
		input  dcache_privileges_t                          cc_update_ldst_privileges,
		input  dcache_line_t                                cc_update_ldst_store_value,

		input                                               cc_snoop_tag_valid,
		input  dcache_set_t                                 cc_snoop_tag_set,
		input  logic                                        cc_wakeup,
		input  thread_id_t                                  cc_wakeup_thread_id,
		output dcache_privileges_t   [`DCACHE_WAY - 1 : 0]  ldst2_snoop_privileges,
		output dcache_tag_t          [`DCACHE_WAY - 1 : 0]  ldst2_snoop_tag


	);

//  -----------------------------------------------------------------------
//  -- Load Store Unit 2 - Signals
//  -----------------------------------------------------------------------

	logic                                             ldst1_request_valid;
	logic            [`THREAD_NUMB - 1 : 0]           ldst1_fifo_requestor;
	logic            [`THREAD_NUMB - 1 : 0]           ldst1_fifo_winner;
	logic            [$clog2( `THREAD_NUMB ) - 1 : 0] ldst1_fifo_winner_id;
	logic                                             ldst1_fifo_is_flush;

	logic                                             cc_command_is_evict;
	logic                                             cc_command_is_update_data;

	dcache_request_t                                  ldst1_fifo_request;
	dcache_request_t                                  cc_update_request;
	dcache_request_t                                  next_request;

	logic                                             tag_sram_read1_enable;
	dcache_set_t                                      tag_sram_read1_address;
	dcache_tag_t     [`DCACHE_WAY - 1 : 0]            tag_sram_read1_data;

	logic                                             tag_sram_read2_enable;
	dcache_set_t                                      tag_sram_read2_address;
	dcache_tag_t     [`DCACHE_WAY - 1 : 0]            tag_sram_read2_data;

	logic            [`DCACHE_WAY - 1 : 0]            tag_sram_write_enable;
	dcache_set_t                                      tag_sram_write_address;
	dcache_tag_t                                      tag_sram_write_data;

	thread_mask_t                                     sleeping_thread_mask;
	thread_mask_t                                     sleeping_thread_mask_next;

	thread_mask_t                                     thread_wakeup_oh;
	thread_mask_t                                     thread_wakeup_mask;

//  -----------------------------------------------------------------------
//  -- Load Store Unit 2 - Arbiter
//  -----------------------------------------------------------------------

	// The Cache Controller has the highest priority when it wants to update
	// tag and data caches
	assign ldst1_request_valid       = |ldst1_fifo_requestor;
	assign ldst1_fifo_requestor      = ( ldst1_valid | ldst1_recycle_valid ) & {`THREAD_NUMB{~cc_update_ldst_valid}} & ~sleeping_thread_mask_next;
	assign sleeping_thread_mask_next = ( sleeping_thread_mask | ldst3_thread_sleep ) & ( ~thread_wakeup_mask );
	assign thread_wakeup_mask        = thread_wakeup_oh & {`THREAD_NUMB{cc_wakeup}};

	// The Cache Controller can send four type of command.
	// If a INSTRUCTION command is send by Cache Controller, a pending instruction has to complete.
	// If a UPDATE_INFO occurs, this stage updates infos and nothing is propagated to the next one.
	// If a UPDATE_INFO_DATA command is send by Cache Controller, the current stage has to update infos, using
	// index, tag and privileges send by Cache Controller. Furthermore, This stage must forward those information
	// and the store_value to the data cache stage.
	// If an EVICT occurs the next stage requires the evicted tag and the new index to construct the evicting address.
	assign cc_command_is_update_data = cc_update_ldst_valid & ( cc_update_ldst_command == CC_UPDATE_INFO_DATA | cc_update_ldst_command == CC_REPLACEMENT );
	assign cc_command_is_evict       = cc_update_ldst_valid & ( cc_update_ldst_command == CC_REPLACEMENT );

	// RR arbiter selects which request from Load Store Unit 1 can access the
	// tag and data caches
	rr_arbiter
	#(
		.NUM_REQUESTERS( `THREAD_NUMB )
	)
	rr_arbiter
	(
		.clk       ( clk                  ),
		.grant_oh  ( ldst1_fifo_winner    ),
		.request   ( ldst1_fifo_requestor ),
		.reset     ( reset                ),
		.update_lru( ldst1_request_valid  )
	);


	oh_to_idx
	#(
		.NUM_SIGNALS( `THREAD_NUMB ),
		.DIRECTION  ( "LSB0"       )
	)
	oh_to_idx
	(
		.index  ( ldst1_fifo_winner_id ),
		.one_hot( ldst1_fifo_winner    )
	);


	idx_to_oh
	#(
		.NUM_SIGNALS( `THREAD_NUMB ),
		.DIRECTION  ( "LSB0"       )
	)
	u_idx_to_oh
	(
		.one_hot( thread_wakeup_oh    ),
		.index  ( cc_wakeup_thread_id )
	);

	always_comb begin
		if ( ldst1_recycle_valid[ldst1_fifo_winner_id] ) begin
			ldst1_fifo_request.instruction  = ldst1_recycle_instruction[ldst1_fifo_winner_id];
			ldst1_fifo_request.address      = ldst1_recycle_address[ldst1_fifo_winner_id];
			ldst1_fifo_request.store_value  = ldst1_recycle_store_value[ldst1_fifo_winner_id];
			ldst1_fifo_request.store_mask   = ldst1_recycle_store_mask[ldst1_fifo_winner_id];
			ldst1_fifo_request.hw_lane_mask = ldst1_recycle_hw_lane_mask[ldst1_fifo_winner_id];
		end else begin
			ldst1_fifo_request.instruction  = ldst1_instruction[ldst1_fifo_winner_id];
			ldst1_fifo_request.address      = ldst1_address[ldst1_fifo_winner_id];
			ldst1_fifo_request.store_value  = ldst1_store_value[ldst1_fifo_winner_id];
			ldst1_fifo_request.store_mask   = ldst1_store_mask[ldst1_fifo_winner_id];
			ldst1_fifo_request.hw_lane_mask = ldst1_hw_lane_mask[ldst1_fifo_winner_id];
		end
		
		// Gli altri valori di update request mancano perch� non servono allo stage successivo
		cc_update_request.address     = cc_update_ldst_address;
		cc_update_request.store_value = cc_update_ldst_store_value;

	end

	// If the memory instruction has a register destination it is a load, otherwise the
	// memory instruction is a store
	assign ldst1_fifo_is_flush       = ldst1_request_valid & ldst1_fifo_request.instruction.is_control & ldst1_fifo_request.instruction.op_code.contr_opcode == FLUSH;

//  -----------------------------------------------------------------------
//  -- Load Store Unit 2 - Tag and Privileges
//  -----------------------------------------------------------------------

	// In case of Cache Control Update signal, the next stage needs the updated tag in case of EVICT,
	// hence it is forwarded reading it on the first port.
	//assign tag_sram_read1_enable     = ( ldst1_request_valid & ldst1_fifo_is_read ) | cc_update_ldst_valid;
	assign tag_sram_read1_enable     = ldst1_request_valid | cc_update_ldst_valid;
	assign tag_sram_read1_address    = ( cc_update_ldst_valid ) ? cc_update_ldst_address.index : ldst1_fifo_request.address.index;

	// Cache controller request valid enables the dedicated read port. If the cache
	// controller update flag is high, the CC accesses to the write port
	assign tag_sram_read2_enable     = cc_snoop_tag_valid & ~cc_update_ldst_valid;
	assign tag_sram_read2_address    = cc_snoop_tag_set;
	assign ldst2_snoop_tag           = tag_sram_read2_data;

	assign tag_sram_write_data       = cc_update_ldst_address.tag;
	assign tag_sram_write_address    = cc_update_ldst_address.index;

	genvar                                            dcache_way;
	generate
		for ( dcache_way = 0; dcache_way < `DCACHE_WAY; dcache_way++ ) begin : WAY_ALLOCATOR
			/*   TAG SRAM    */
			memory_bank_2r1w #
			(
				.COL_WIDTH    ( `DCACHE_TAG_LENGTH ),
				.NB_COL       ( 1                  ),
				.SIZE         ( `DCACHE_SET        ),
				.WRITE_FIRST1 ( "FALSE"            ),
				.WRITE_FIRST2 ( "FALSE"            )
			)
			tag_sram (
				.clock        ( clk                               ),
				.read1_address( tag_sram_read1_address            ),
				.read1_data   ( tag_sram_read1_data[dcache_way]   ),
				.read1_enable ( tag_sram_read1_enable             ),
				.read2_address( tag_sram_read2_address            ),
				.read2_data   ( tag_sram_read2_data[dcache_way]   ),
				.read2_enable ( tag_sram_read2_enable             ),
				.write_address( tag_sram_write_address            ),
				.write_data   ( tag_sram_write_data               ),
				.write_enable ( tag_sram_write_enable[dcache_way] )
			);

			assign tag_sram_write_enable[dcache_way] = cc_update_ldst_valid & cc_update_ldst_way == dcache_way_idx_t'( dcache_way );

			// In case of update from CC, the new tag is stored in SRAM and bypassed to the next stage.
			// The module memory_bank_2r1w has a builtin bypass mechanism.
			assign ldst2_tag_read[dcache_way]        = tag_sram_read1_data[dcache_way];

			/*   Privileges SRAM    */
			// Only the cache controller (or Protocol FSM can change this status bits)
			dcache_privileges_t [`DCACHE_SET - 1 : 0] dcache_privileges;

			always_ff @ ( posedge clk, posedge reset )
				if ( reset ) begin
					dcache_privileges <= 0;
				end else
					// In case of update from Cache Controller, the new privileges are stored and
					// bypassed to the next stage
					if ( tag_sram_write_enable[dcache_way] ) begin
						dcache_privileges[tag_sram_write_address] <= cc_update_ldst_privileges;
						ldst2_privileges_read[dcache_way]         <= cc_update_ldst_privileges;
						ldst2_snoop_privileges[dcache_way]        <= cc_update_ldst_privileges;
					end else begin //FIXME: Seprarate gli always_ff per letture e scritture, eventualmente con BYPASS nel caso WRITEFIRST
						// In case of update from CC, privileges must be read and passed to the next stage.
						// The next stage will propagate the result to Writeback in case of laod or will
						// store the data in data cache in case of store.
						if ( tag_sram_read1_enable | cc_update_ldst_valid )
							ldst2_privileges_read[dcache_way]  <= dcache_privileges[tag_sram_read1_address];
						if ( tag_sram_read2_enable )
							ldst2_snoop_privileges[dcache_way] <= dcache_privileges[tag_sram_read2_address];
					end
		end
	endgenerate

	// A request coming from Cache Controller has the highest priority. If cc_update_ldst_valid is high, the
	// request is served and bypassed to the next stage
	assign next_request              = ( cc_update_ldst_valid ) ? cc_update_request            : ldst1_fifo_request;

	always_comb begin
		ldst2_dequeue_instruction = ldst1_fifo_winner & {`THREAD_NUMB{~cc_update_ldst_valid}} & ~ldst1_recycle_valid;
		ldst2_recycled            = ldst1_fifo_winner & {`THREAD_NUMB{~cc_update_ldst_valid}} & ldst1_recycle_valid;
	end

	always_ff @ ( posedge clk, posedge reset )
		if ( reset ) begin
			ldst2_valid          <= 1'b0;
			ldst2_evict_valid    <= 1'b0;
			ldst2_update_valid   <= 1'b0;
			sleeping_thread_mask <= {`THREAD_NUMB{1'b0}};
		end else begin
			ldst2_valid          <= ldst1_request_valid;
			ldst2_evict_valid    <= cc_command_is_evict;
			ldst2_update_valid   <= cc_command_is_update_data;
			sleeping_thread_mask <= sleeping_thread_mask_next;
		end

	always_ff @ ( posedge clk ) begin
		ldst2_instruction  <= next_request.instruction;
		ldst2_address      <= next_request.address;
		ldst2_store_value  <= next_request.store_value;
		ldst2_store_mask   <= next_request.store_mask;
		ldst2_hw_lane_mask <= next_request.hw_lane_mask;
		ldst2_update_way   <= cc_update_ldst_way;
		ldst2_is_flush     <= ldst1_fifo_is_flush;
	end
	
	// checking if there are no load/store pending running in the 2nd stage
	assign s2_no_ls_pending = ~ldst1_fifo_requestor;

`ifdef SIMULATION
	always_ff @( posedge clk )
		if ( !reset )
			assert( |( ldst3_thread_sleep & thread_wakeup_mask ) == 1'b0 );
`endif

endmodule