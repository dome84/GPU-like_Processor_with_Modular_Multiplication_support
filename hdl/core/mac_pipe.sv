`include "nuplus_define.sv"

`define SCALAR_GAP     (`REGISTER_SIZE / `HW_LANE)


module mac_pipe #(
		parameter TILE_ID = 0,
		parameter CORE_ID = 0 )
	(
		input                        clk,
		input                        reset,

		// From Operand Fetch
		input                        opf_valid,
		input  instruction_decoded_t opf_inst_scheduled,
		input  vec_reg_size_t        opf_fetched_op0,
		input  vec_reg_size_t        opf_fetched_op1,
		input  scal_reg_size_t       opf_fetched_op2,
		input  hw_lane_mask_t        opf_hw_lane_mask,

		// To Writeback
		output logic                 mac_valid,
		output instruction_decoded_t mac_inst_scheduled,
		output vec_reg_size_t        mac_result,
		output hw_lane_mask_t        mac_hw_lane_mask,
		output scal_reg_size_t       mac_2nd_result
	);


	logic           is_mac_instr;
	logic           is_mont1_op, is_mont1_op1, is_mont1_op15, is_mont1_op2, is_mont1_op25, is_mont1_op04;
	logic           is_mont2_op;
	logic           is_mulhi_op;
	logic           is_mullo_op;
	
	/*vec_reg_size_t*/ logic  [`HW_LANE-1:0][64:0]  vec_result;
	logic  [`HW_LANE-1:0][31:0]  vec_result1;
	
	logic           tmp_valid;
	instruction_decoded_t tmp_inst_scheduled;
    hw_lane_mask_t        tmp_hw_lane_mask;
    
    scal_reg_size_t [`THREAD_NUMB - 1 : 0]    q;
    scal_reg_size_t [`THREAD_NUMB - 1 : 0]    q0;
    
	vec_reg_size_t  [`THREAD_NUMB-1:0]        m;
	scal_reg_size_t [`THREAD_NUMB-1:0]     mp;
    logic           [`THREAD_NUMB-1:0][`HW_LANE-1:0][32:0]     h1;
    vec_reg_size_t  [`THREAD_NUMB-1:0]     h2;
    
    logic [$clog2( `THREAD_NUMB ) - 1 : 0] index0;
    logic [$clog2( `THREAD_NUMB ) - 1 : 0] index1;
    logic [$clog2( `THREAD_NUMB ) - 1 : 0] index15;
    logic [$clog2( `THREAD_NUMB ) - 1 : 0] index2;
    logic [$clog2( `THREAD_NUMB ) - 1 : 0] index25;
    logic [$clog2( `THREAD_NUMB ) - 1 : 0] index04;
    logic [$clog2( `THREAD_NUMB ) - 1 : 0] index21;
    
    logic                                  ce0, ce1, ce1_0, ce2, /*ce05,*/ ce25;
    logic                                  multiply0, multiply1, multiply1_0, multiply2;
    logic                                  loadacc0, loadacc1, loadacc2;
    logic                                  loadmb0, loadmb1, loadmb2;
    logic                                  loadmp0, loadmp1, loadmp2;
    logic                                  loadh0, loadh1, loadh2, loadh3;
    
    vec_reg_size_t                         op1_1, op1_2, op1_3;
    vec_reg_size_t                         op0_1, op0_2, op0_3;
    scal_reg_size_t                        op2_1, op2_2, op2_3;
    
    opcode_t                               op_code0, op_code1, op_code2;
    
    vec_reg_size_t                         stage0_res_l;
    logic  [`HW_LANE-1:0][32:0]            stage0_res_h;
    
    vec_reg_size_t                         stage1_res_l;
    logic  [`HW_LANE-1:0][32:0]           stage1_res_h, stage1_res_h_d;
    
     logic  [`HW_LANE-1:0]                  l_cout_tmp, cout_stage0;
     logic  [`THREAD_NUMB-1:0][`HW_LANE-1:0]     l_cout;
     logic  [`THREAD_NUMB-1:0][`HW_LANE-1:0]     l_cout_i, l_cout_o;
     
     vec_reg_size_t  [`THREAD_NUMB-1:0]     h1_z;
     vec_reg_size_t  [`THREAD_NUMB-1:0]     h2_z;
     scal_reg_size_t [`THREAD_NUMB-1:0]     l_z;
     vec_reg_size_t                         h2_z_tmp;
     scal_reg_size_t                        l_z_tmp;
     logic                                  loadh04;
    
    //int z;
    
	assign is_mac_instr = opf_inst_scheduled.pipe_sel == PIPE_MAC;
	assign is_mont1_op = (opf_inst_scheduled.op_code == MONT1 && opf_valid == 1'b1 && is_mac_instr == 1'b1)? 1'b1 : 1'b0;
	assign is_mont2_op = (opf_inst_scheduled.op_code == MONT2 && opf_valid == 1'b1 && is_mac_instr == 1'b1)? 1'b1 : 1'b0;
	assign is_mulhi_op = (opf_inst_scheduled.op_code == MULHI && opf_valid == 1'b1 && is_mac_instr == 1'b1)? 1'b1 : 1'b0;
	assign is_mullo_op = (opf_inst_scheduled.op_code == MULLO && opf_valid == 1'b1 && is_mac_instr == 1'b1)? 1'b1 : 1'b0;
	
	assign index0       = opf_inst_scheduled.thread_id;

     dummypipe 
           #(.WIDTH($bits(index2)), .DEPTH(2))
               thread_index2_pipe
           (.clk(clk), .en(1'b1), .datain(index1), .q(index2));
    
    dummypipe 
            #(.WIDTH($bits(index0)), .DEPTH(5))
                thread_index_pipe
            (.clk(clk), .en(1'b1), .datain(index0), .q(index1));
            
   dummypipe 
            #(.WIDTH($bits(index1)), .DEPTH(5))
                thread_index_bis_pipe
            (.clk(clk), .en(1'b1), .datain(index2), .q(index25));
            
  dummypipe 
        #(.WIDTH($bits(index0)), .DEPTH(4))
            thread_index04_pipe
        (.clk(clk), .en(1'b1), .datain(index0), .q(index04));

    
    always_comb begin
      case(opf_inst_scheduled.op_code)
          GETACCL,
              GETACCH,
              LOADM,
             // LOADB,
              LOADMP,
              LOADH,
              INCBIND,
              STOH1,
              STOH2 : ce0 = 1'b0;
          default: ce0 = opf_valid &  is_mac_instr;
      endcase
    end
    
    
    dummypipe 
            #(.WIDTH(1), .DEPTH(5))
                ce_pipe
            (.clk(clk), .en(1'b1), .datain(ce0), .q(ce1));
    

   dummypipe
            #(.WIDTH(1), .DEPTH(5))
                ce_bis_pipe
            (.clk(clk), .en(1'b1), .datain(ce2), .q(ce25));

    
    assign multiply0 = is_mullo_op | is_mulhi_op; 
    assign loadacc0   =  (opf_inst_scheduled.op_code == LOADACC)? 1'b1 : 1'b0;
    assign loadmb0   =  (opf_inst_scheduled.op_code == LOADM && opf_valid == 1'b1 && is_mac_instr == 1'b1)? 1'b1 : 1'b0;
    assign loadmp0   =  (opf_inst_scheduled.op_code == LOADMP && opf_valid == 1'b1 && is_mac_instr == 1'b1)? 1'b1 : 1'b0;
    assign loadh0   =  (opf_inst_scheduled.op_code == LOADH && opf_valid == 1'b1 && is_mac_instr == 1'b1)? 1'b1 : 1'b0;

    
    dummypipe 
        #(.WIDTH(7), .DEPTH(2))
            signals_pipe
        (.clk(clk), .en(1'b1), .datain({loadacc1,multiply1,loadmb1,loadmp1,ce1,is_mont1_op1, loadh1}), 
                .q({loadacc2,multiply2,loadmb2,loadmp2,ce2,is_mont1_op2, loadh2}));
    
    dummypipe 
            #(.WIDTH(1), .DEPTH(5))
                load_pipe
            (.clk(clk), .en(1'b1), .datain(loadacc0), .q(loadacc1));
    dummypipe 
            #(.WIDTH(1), .DEPTH(5))
                multiply_pipe
            (.clk(clk), .en(1'b1), .datain(multiply0), .q(multiply1));
    dummypipe 
            #(.WIDTH(1), .DEPTH(5))
                loadmb_pipe
            (.clk(clk), .en(1'b1), .datain(loadmb0), .q(loadmb1));
                    
    dummypipe 
            #(.WIDTH(1), .DEPTH(5))
                loadmp_pipe
            (.clk(clk), .en(1'b1), .datain(loadmp0), .q(loadmp1));
            
    dummypipe 
            #(.WIDTH(1), .DEPTH(5))
                    loadh_pipe
                (.clk(clk), .en(1'b1), .datain(loadh0), .q(loadh1));

    
    dummypipe 
            #(.WIDTH($bits(op0_2)), .DEPTH(2))
                op0_2_pipe
            (.clk(clk), .en(1'b1), .datain(op0_1), .q(op0_2));
    dummypipe 
        #(.WIDTH($bits(op1_2)), .DEPTH(2))
            op1_2_pipe
        (.clk(clk), .en(1'b1), .datain(op1_1), .q(op1_2));
    
    dummypipe 
        #(.WIDTH($bits(opf_fetched_op1)), .DEPTH(5))
            op1_pipe
        (.clk(clk), .en(1'b1), .datain(opf_fetched_op1), .q(op1_1));
        
    dummypipe 
        #(.WIDTH($bits(opf_fetched_op0)), .DEPTH(5))
            op0_pipe
        (.clk(clk), .en(1'b1), .datain(opf_fetched_op0), .q(op0_1));
        
    dummypipe 
        #(.WIDTH($bits(opf_fetched_op2)), .DEPTH(12))
            op2_pipe
        (.clk(clk), .en(1'b1), .datain(opf_fetched_op2), .q(op2_3));
        
        
     dummypipe 
           #(.WIDTH($bits(opf_fetched_op2)), .DEPTH(4))
               op24_pipe
           (.clk(clk), .en(1'b1), .datain(opf_fetched_op2), .q(l_z_tmp));
           
      dummypipe 
            #(.WIDTH($bits(opf_fetched_op1)), .DEPTH(4))
                op14_pipe
            (.clk(clk), .en(1'b1), .datain(opf_fetched_op1), .q(h2_z_tmp));
            
      dummypipe 
           #(.WIDTH(1), .DEPTH(4))
               loadh04_pipe
           (.clk(clk), .en(1'b1), .datain(loadh0), .q(loadh04));
           
     dummypipe 
          #(.WIDTH(1), .DEPTH(4))
              ismacop04_pipe
          (.clk(clk), .en(1'b1), .datain(is_mont1_op), .q(is_mont1_op04));
                       
            
    always_ff @ (posedge clk) begin
        if(reset == 1'b1) begin
            h1_z <= 0;
            h2_z <= 0;
            l_z  <= 0;
        end
        else begin
            if(is_mont1_op == 1'b1)
                h1_z[index0]    <= {`HW_LANE*32{1'b0}};
            else if(loadh0 == 1'b1)
                h1_z[index0]    <=  opf_fetched_op0;
                
            if(is_mont1_op04 == 1'b1) begin
                h2_z[index04]    <= {`HW_LANE*32{1'b0}};
                l_z[index04]     <= {32{1'b0}};
            end
            if(loadh04 == 1'b1) begin
                h2_z[index04]    <=  h2_z_tmp;
                l_z[index04]     <=  l_z_tmp;
            end
        end
    end
    
    assign op_code0 = opf_inst_scheduled.op_code;  

    
    dummypipe 
        #(.WIDTH($bits(op_code2)), .DEPTH(2))
            opcode2_pipe
        (.clk(clk), .en(1'b1), .datain(op_code1), .q(op_code2));
    
    dummypipe 
            #(.WIDTH($bits(op_code0)), .DEPTH(5))
                opcode_pipe
            (.clk(clk), .en(1'b1), .datain(op_code0), .q(op_code1));
            
   dummypipe 
            #(.WIDTH(1), .DEPTH(5))
                ismac_pipe
            (.clk(clk), .en(1'b1), .datain(is_mont1_op), .q(is_mont1_op1));


    // First stage
	genvar          i;
	generate
		for ( i = 0; i < `HW_LANE; i ++ ) begin: mac_stage0_others
		    logic [64:0] tmp_result0;
		   
            mac1 mac_lane_0_others(
                .clk        ( clk            ),
                .ce         ( /*ce0*/1'b1           ),
                .reset      ( reset          ),
                .multiply   ( 1'b0      ),
                .loadacc    ( loadacc0       ),
                .a  ( opf_fetched_op0[i]    ),
                .b  ( (ce0 == 1'b1)?(((is_mullo_op|is_mulhi_op) == 1'b1)?opf_fetched_op1[i]:opf_fetched_op2):32'd0 ),
                //.b  ( b[opf_inst_scheduled.thread_id][b_index[index0][$clog2(`HW_LANE)]][b_index[index0][$clog2(`HW_LANE)-1:0]]),
                .c  ( 64'd0 ),
                .d  ( (ce0==1'b1 && (is_mont1_op | is_mont2_op))?opf_fetched_op1[i]:32'd0 ),
                .e  ( (is_mont1_op==1'b1)?h1_z[index0][i]:32'd0 /*h2[opf_inst_scheduled.thread_id][h_index0][i]*/  ),
                .q  ( /*tmp_result0*/ {stage0_res_h[i], stage0_res_l[i]} ),
                .index  ( 0 ),
                .l(h2_z[index04][i]),
                .l_cin(l_z[index04][i*`SCALAR_GAP+:1]),
                .l_cout(cout_stage0[i])
            );

            
               always_ff @ (posedge clk) begin
                   //if(reset == 1'b1)
                       //l_cout_i <= 0;
                   //else 
                   if(reset == 1'b0) begin
                       if(is_mont1_op1 == 1'b1)
                           l_cout_i[index1][i] <= cout_stage0[i];
                   end
 
               end          
		end

	endgenerate

      always_ff @ (posedge clk) begin         
         index21 <= index2;
      end

    //SECOND Stage
    generate //Lane 0
        mult32 q_mult32(clk, mp[index1], stage0_res_l[0], q0[0]);
        always_ff @ (posedge clk) begin
           if(is_mont1_op15)
                q[index15] <= q0[0];

           index15 <= index1;
           is_mont1_op15 <= is_mont1_op1;
        end 
        
   
        
        dummypipe 
               #(.WIDTH($bits(stage1_res_l[0])), .DEPTH(2))
                   stage1_res_l0_pipe
               (.clk(clk), .en(1'b1), .datain(stage0_res_l[0]), .q(stage1_res_l[0]));    
               
        dummypipe 
              #(.WIDTH($bits(stage1_res_h[0])), .DEPTH(2))
                  stage0_res_h0_pipe
              (.clk(clk), .en(1'b1), .datain(stage0_res_h[0]), .q(stage1_res_h[0]));                     
    endgenerate
    
    genvar          j;
    generate //Other Lanes
        for ( j = 1; j < `HW_LANE; j ++ ) begin  
   
            
            dummypipe 
               #(.WIDTH($bits(stage1_res_l[j])), .DEPTH(2))
                   stage1_res_l_j_pipe
               (.clk(clk), .en(1'b1), .datain(stage0_res_l[j]), .q(stage1_res_l[j]));  
               
            dummypipe 
                 #(.WIDTH($bits(stage1_res_h[j])), .DEPTH(2))
                     stage1_res_h_j_pipe
                 (.clk(clk), .en(1'b1), .datain(stage0_res_h[j]), .q(stage1_res_h[j]));        
        end

    endgenerate
    
    always_ff @ (posedge clk) begin
       if(loadmp1 == 1'b1) begin
            mp[index1] <= op1_1; 
       end
   end  
    
    
    
    always_ff @ (posedge clk) begin
        if(reset == 1'b1)
            h1 <= 0;
        else begin
          for (int mj = 0; mj < `HW_LANE; mj ++ ) begin  

               if(loadh2 == 1'b1) begin
                   h1[index2][mj] <= 0; //op0_2[j];
               end
               else if(ce2 == 1'b1) begin
                   h1[index2][mj] <= stage1_res_h[mj];
               end
  
          end 
      end
   
   end
   
  
   
    dummypipe 
          #(.WIDTH($bits(l_cout_i)), .DEPTH(6))
             long_carry_pipe
          (.clk(clk), .en(1'b1), .datain(l_cout_i), .q(l_cout_o));  
          
   dummypipe 
        #(.WIDTH(1), .DEPTH(5))
           is_mont1_op25_pipe
        (.clk(clk), .en(1'b1), .datain(is_mont1_op2), .q(is_mont1_op25));  
                      
    //THIRD Stage    
    genvar          h;
    generate 
        for ( h = 0; h < `HW_LANE; h ++ ) begin
            logic [63:0] tmp_result2;
           
            mac1 mac_lane_2_others(
                .clk        ( clk            ),
                .ce         ( /*ce2 & */(~reset) ),
                .reset      ( reset          ),
                .multiply   ( 1'b0      ),
                .loadacc    ( loadacc2       ),
                .a  ( (multiply2 == 1'b1)? 32'b0 : q[index2]                 ),
                .b  ( m[index2][h]  ),
                .c  ( (multiply2 == 1'b1)? {stage1_res_h[h], stage1_res_l[h]} : {31'd0, h1[index2][h] }   ),
                .d  ( (multiply2 == 1'b1)? 32'b0 : stage1_res_l[h]/*op1_2[h]*/              ),
                .e  ( 0  ),
                .q  ( /*tmp_result2*/  vec_result[h] ),
                .index ( 0 ),
                .l(32'd0),
                .l_cin(1'b0)
            );
            
            always_comb begin
                {l_cout_tmp[h], vec_result1[h][31:0]} = {1'b0, vec_result[h][31:0]} + {1'b0, h2[index25][h]} + {32'd0, 
                (is_mont1_op25==1'b1)?/*l_cout_o[index25][h]*/1'b0:l_cout[index25][h]};
            end
                    
           
            
       end

    endgenerate

    
     always_ff @ (posedge clk) begin
           if(reset == 1'b1) begin
               h2 <= 0;
               //h1 <= 0;
               l_cout <= 0;
           end
           else begin
           for(int mh = 0; mh <`HW_LANE; mh++) begin
            if(loadh3 == 1'b1) begin
               h2[index25][mh] <= 0; //op1_3[h]; //tmp_result2[63:32];
               l_cout[index25][mh]        <= 0; //op2_3[h];
               end
            else if(ce25 == 1'b1) begin
                h2[index25][mh] <= vec_result[mh][63:32]; //tmp_result2[63:32];
                //h1[index25][h_index25][h] <= stage1_res_h_d[h];
                if(is_mont1_op25 == 1'b1)
                l_cout[index25][mh]        <= l_cout_o[index25][mh];
                else
                l_cout[index25][mh]        <= l_cout_tmp[mh];
                     
               end
           //vec_result[h] <= tmp_result2[31:0]; 
           end
           end     
       end
                 
    
    dummypipe 
            #(.WIDTH($bits(/*stage1_res_h*/h1[0])), .DEPTH(/*5*/4))
                stage1_res_h_pipe
            (.clk(clk), .en(1'b1), .datain(/*stage1_res_h*/h1[index21]), .q(stage1_res_h_d));
            
   dummypipe 
            #(.WIDTH($bits(op1_2)), .DEPTH(5))
                op1_25_pipe
            (.clk(clk), .en(1'b1), .datain(op1_2), .q(op1_3));
    
    always_ff @ (posedge clk) begin
        if(reset == 1'b1) begin
            for(int k = 0; k < `THREAD_NUMB; k++)
                m[k] <= {`HW_LANE{0}};
        end            
        else if(loadmb2 == 1'b1) begin
             m[index2] <= op0_2; 
             //m[index2][1] <= op1_2; 
        end
    end  
    
	dummypipe 
    #(.WIDTH(1), .DEPTH(12)) //CHANGED FROM 5
        valid_pipe
    (.clk(clk), .en(1'b1), .datain(opf_valid &  is_mac_instr), .q(tmp_valid));	
	
	dummypipe 
        #(.WIDTH($bits(opf_inst_scheduled)), .DEPTH(12)) //CHANGED FROM 5
            inst_pipe
        (.clk(clk), .en(1'b1), .datain(opf_inst_scheduled), .q(tmp_inst_scheduled));
        
   	dummypipe 
        #(.WIDTH($bits(opf_hw_lane_mask)), .DEPTH(12)) //CHANGED FROM 5
            mask_pipe
        (.clk(clk), .en(1'b1), .datain(opf_hw_lane_mask), .q(tmp_hw_lane_mask));
        
    dummypipe 
        #(.WIDTH(1), .DEPTH(12))
            hxxx_pipe
        (.clk(clk), .en(1'b1), .datain(loadh0), .q(loadh3));


	always_ff @ ( posedge clk ) begin
		mac_inst_scheduled <= tmp_inst_scheduled;
		mac_hw_lane_mask   <= tmp_hw_lane_mask;
		/*if(tmp_inst_scheduled.op_code == STOH2) begin
		     for(z=0; z < `HW_LANE; z++)
               mac_result[z] <= vec_result[z][31:0];
		end
		else begin*/
		
            
       // end
	end
	
	genvar z;
	generate
	for(z=0; z < `HW_LANE; z++) begin
	   always_ff @ (posedge clk) begin
	        if(tmp_inst_scheduled.op_code == STOH2) begin
                mac_result[z] <= h2[tmp_inst_scheduled.thread_id][z];
	        end
	        else if(tmp_inst_scheduled.op_code == STOH1) begin
	            mac_result[z] <= stage1_res_h_d[z]; // Attenzione!
	        end
            else if(tmp_inst_scheduled.op_code == MULHI) begin
               mac_result[z] <= vec_result[z][63:32];
            end
            else if(tmp_inst_scheduled.op_code == MULLO) begin
               mac_result[z] <= vec_result[z][31:0];
            end
	        else begin
                mac_result[z] <= vec_result1[z][31:0]; //da rivedere
            end
        end
     end
    endgenerate 
    
     always_ff @ (posedge clk) begin
           if(tmp_inst_scheduled.op_code == STOH2) begin
               for(int mz = 0; mz < `HW_LANE; mz++) begin
                 mac_2nd_result[mz*`SCALAR_GAP+:1] <= l_cout[tmp_inst_scheduled.thread_id][mz];
                 mac_2nd_result[(mz*`SCALAR_GAP + 1) +:`SCALAR_GAP-1] <= {`SCALAR_GAP-1{1'b0}};
               end
           end
           else begin
               mac_2nd_result    <= 0; 
           end
       end             
                   
	always_ff @ ( posedge clk) begin
        if ( reset )
            mac_valid <= 1'b0;
        else
            mac_valid <= tmp_valid;
    end

endmodule