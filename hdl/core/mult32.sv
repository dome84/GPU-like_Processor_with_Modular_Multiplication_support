`timescale 1ns / 1ps


module mult32(
    input           clk,
    input [31:0]    a,
    input [31:0]    b,
    output [31:0]   q
    
    );
    
logic [15:0]    a_l;
logic [15:0]    a_h;
logic [15:0]    b_l;
logic [15:0]    b_h;
logic [31:0]    res_l;
logic [31:0]    res_h1;
logic [31:0]    res_h2;

assign a_l = a[15:0];
assign a_h = a[31:16];
assign b_l = b[15:0];
assign b_h = b[31:16];

always_ff @ (posedge clk) begin
    res_l <= a_l * b_l;
    res_h1 <= a_l * b_h;
    res_h2 <= a_h * b_l;
 end
 
assign q[15:0] = res_l[15:0];

assign q[31:16] = res_l[31:16] + res_h1[15:0] + res_h2[15:0];
    

endmodule
