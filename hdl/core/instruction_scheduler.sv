`include "nuplus_define.sv"
`include "user_define.sv"

`ifdef DISPLAY_CORE
`include "debug_log.sv"
`endif

module instruction_scheduler (
		input                                                 clk,
		input                                                 reset,
		input                                                 enable,
		//From Instruction Buffer
		input  thread_mask_t                                  ib_instructions_valid,
		input  instruction_decoded_t  [`THREAD_NUMB - 1 : 0]  ib_instructions,

		//From Writeback
		input                                                 wb_valid,
		input  thread_id_t                                    wb_thread_id,
		input  wb_result_t                                    wb_result,
		input  ex_pipe_mask_t                                 wb_fifo_full,

		//From Rollback Handler
		input  thread_mask_t                                  rb_valid,
		input  scoreboard_t           [`THREAD_NUMB - 1 : 0]  rb_destination_mask,

		//From DSU
		input  logic                  [`THREAD_NUMB - 1 : 0]  dsu_stop_issue,

		//To Operand fetch
		output logic                                          is_instruction_valid,
		output thread_id_t                                    is_thread_id,
		output instruction_decoded_t                          is_instruction,
		output scoreboard_t                                   is_destination_mask,

		output logic                  [`THREAD_NUMB - 1 : 0]  scoreboard_empty,
		input  logic                  [`THREAD_NUMB - 1 : 0]  release_val,
		`ifdef PERFORMANCE_SYNC
		output logic                  [`THREAD_NUMB - 1 : 0]  counter_sync_detect,
		`endif
		//To Instruction Buffer
		output thread_mask_t                                  is_thread_scheduled_mask
	) ;

	logic            [`THREAD_NUMB-1:0 ]    sync_detect, sync_detect_cmb;

	thread_mask_t                           can_issue;
	thread_id_t                             thread_scheduled_id;
	scoreboard_t     [`THREAD_NUMB - 1 : 0] scoreboard_set_bitmap;

	genvar                                  thread_id;
	generate
		for ( thread_id = 0; thread_id < `THREAD_NUMB; thread_id++ ) begin

			scoreboard_t                           scoreboard;
			scoreboard_t                           scoreboard_next;
			scoreboard_t                           scoreboard_clear_bitmap;

			scoreboard_t                           source_mask_0;
			scoreboard_t                           source_mask_1;
			scoreboard_t                           destination_mask;
			scoreboard_t                           release_mask;
			logic                                  issue_this_thread;
			logic                                  hazard_raw;
			logic                                  hazard_waw;
			logic [1:0]                            int_lat_cnt = 0;
            logic                                  hazard_of;

			// FP pending instruction tracker
			logic        [`FP_DIV_LATENCY - 1 : 0] fp_pending_queue;
            logic        [11 : 0]                  mac_pending;
			// FP structural hazard signals
			logic                                  can_issue_fp, fp_add_issued, fp_mul_issued, fp_div_issued, fp_cmp_issued, mont_issued, can_issue_mont;
			logic                                  is_fp, fp_is_add, fp_is_sub, fp_is_mul, fp_is_div, fp_is_cmp, is_mont;

			assign issue_this_thread                = is_thread_scheduled_mask[thread_id];

			always_comb begin
				source_mask_0                                                                                                    = scoreboard_t' ( 1'b0 ) ;
				source_mask_1                                                                                                    = scoreboard_t' ( 1'b0 ) ;
				destination_mask                                                                                                 = scoreboard_t' ( 1'b0 ) ;
				release_mask                                                                                                     = scoreboard_t' ( 1'b0 ) ;

				// For each instruction from Instruction Buffer module, a source mask is created. The main purpose is to
				// check eventual data hazards. Each source mask has the same bit number of the scoreboard.
				if ( ib_instructions[thread_id].mask_enable ) begin
					source_mask_0 [reg_addr_t' ( `MASK_REG ) ] = 1'b1;
					source_mask_1 [reg_addr_t' ( `MASK_REG ) ] = 1'b1;
				end
				
                if( ib_instructions[thread_id].is_op0_forward == 1'b0)
				    source_mask_0 [{ib_instructions[thread_id].is_source0_vectorial , ib_instructions[thread_id].source0 }]          = ib_instructions[thread_id].has_source0;
				else source_mask_0 = source_mask_0;
				
				source_mask_1 [{ib_instructions[thread_id].is_source1_vectorial , ib_instructions[thread_id].source1 }]          = ib_instructions[thread_id].has_source1;
				
				if(ib_instructions[thread_id].has_3_sources)
                    source_mask_1 [{1'b0, reg_addr_t'( `CARRY_REG )}]  = 1'b1;
                else source_mask_1 = source_mask_1;
				
				if(ib_instructions[thread_id].is_lookup != 1'b1 || ib_instructions[thread_id].immediate[8] == 1'b1/* || ib_instructions[thread_id].is_op0_forward == 1'b0 */)
				    destination_mask[{ib_instructions[thread_id].is_destination_vectorial , ib_instructions[thread_id].destination}] = ib_instructions[thread_id].has_destination;
				else destination_mask = destination_mask;
				
				if(ib_instructions[thread_id].has_2_results)
                    destination_mask [{1'b0, reg_addr_t'( `CARRY_REG )}]  = 1'b1;
                 else destination_mask = destination_mask;
                                     
                if(wb_result.no_scoreboard == 1'b0)                                
				    release_mask [{~wb_result.wb_result_is_scalar , wb_result.wb_result_register }]       = wb_valid && ( wb_thread_id == thread_id_t' ( thread_id ) ) ;
                else release_mask = release_mask; 
                
                if(wb_result.has_2_results)
                    release_mask [{1'b0, reg_addr_t'( `CARRY_REG )}]                             = wb_valid && ( wb_thread_id == thread_id_t'( thread_id ) );
                else release_mask = release_mask;    
			end

			// When the AND between the source mask and the scoreboard is not a vector of 0, the current instruction requires
			// a busy register, and a RAW hazard arises. In the same way we check eventual WAW hazard.
			assign hazard_raw                       = | ( ( source_mask_0 | source_mask_1 ) & scoreboard ) ;
			assign hazard_waw                       = | ( destination_mask & scoreboard ) ;
			
			always_comb begin
                if(ib_instructions[thread_id].pipe_sel == PIPE_SFU && ib_instructions[thread_id].is_op0_forward == 1'b1 &&
                     int_lat_cnt > 2'b00) hazard_of = 1'b1;
                else hazard_of = 1'b0;
            end
            
			// If the instruction is valid, there are no data hazards, the Writeback fifo are not full, no rollback, and the instruction
			// does not raise a structural hazard in the FP pipeline, the thread can be issued.
			assign can_issue[thread_id]             = ib_instructions_valid[thread_id] && ! ( hazard_raw || hazard_waw || hazard_of) && ( ~ ( |wb_fifo_full ) ) &&
				~rb_valid[thread_id] && can_issue_fp && can_issue_mont && release_val[thread_id] && sync_detect[thread_id] && sync_detect_cmb[thread_id]
				&& ~dsu_stop_issue[thread_id];


			// Scoreboard update logic. When the thread is scheduled, the scoreboard_set_bitmap will update the scoreboard with the destination
			// register, it will set a bit in order to track the destination register used by the current operation. From this moment onward, this
			// register results busy and an instruction which wants to use it will raise a data hazard.
			// The scoreboard_clear_bitmap tracks all registers released by the Writeback stage. An operation in Writeback releases its destination
			// register
			assign scoreboard_set_bitmap[thread_id] = destination_mask & {`SCOREBOARD_LENGTH{issue_this_thread}};
			assign scoreboard_clear_bitmap          = release_mask | ( rb_destination_mask[thread_id] & {`SCOREBOARD_LENGTH{rb_valid[thread_id]}} ) ;
			assign scoreboard_next                  = ( scoreboard | scoreboard_set_bitmap[thread_id] ) & ( ~scoreboard_clear_bitmap ) ;

			always_ff @ ( posedge clk, posedge reset ) begin
				if ( reset ) begin
					scoreboard <= scoreboard_t' ( 1'b0 ) ;
					int_lat_cnt <= 1'b0;
				end else begin
					scoreboard <= scoreboard_next;
					
					if(/*ottimizz.*/ib_instructions[thread_scheduled_id].thread_id == thread_id_t'( thread_id ) && ib_instructions[thread_id].pipe_sel == PIPE_SFU &&
                           can_issue[thread_scheduled_id] == 1'b1 && thread_scheduled_id ==  thread_id) begin
                       int_lat_cnt <= 2'b10;
                    end else if(int_lat_cnt > 2'b00)
                       int_lat_cnt <= int_lat_cnt - 2'b01;

				end
			end

			assign scoreboard_empty[thread_id]      = |scoreboard;
			//assign sync_detect[thread_id]           = 1'b1;
			always_comb begin
				sync_detect_cmb[thread_id] = 1'b1;
				if (release_val[thread_id] == 1'b1 && is_instruction.pipe_sel==PIPE_SYNC && is_instruction.thread_id==thread_id_t' ( thread_id ) && is_instruction_valid== 1'b1)
					sync_detect_cmb[thread_id] = 1'b0;
			end

			always_ff @ ( posedge clk,posedge reset) begin
				if (reset)
					sync_detect[thread_id] <= 1'b1;
				else if (enable) begin
					if (release_val[thread_id] == 1'b1 && is_instruction.pipe_sel==PIPE_SYNC && is_instruction.thread_id==thread_id_t' ( thread_id ) && is_instruction_valid== 1'b1)
						sync_detect[thread_id] <= 1'b0;
					else if (release_val[thread_id] == 1'b0 )
						sync_detect[thread_id] <= 1'b1;
					else
						sync_detect[thread_id] <= sync_detect[thread_id];
				end
			end

			// FP structural hazard checking. The FP pipe has one output demux to the Writeback unit. There is no conflict control
			// inside the FP module, two different operation can terminate at the same time and collide in the output propagation.
			// FP operation decoding.
			assign is_fp                            = ib_instructions[thread_id].is_fp;
			assign fp_is_add                        = is_fp & ib_instructions[thread_id].op_code.alu_opcode == ADD_FP;
			assign fp_is_sub                        = is_fp & ib_instructions[thread_id].op_code.alu_opcode == SUB_FP;
			assign fp_is_mul                        = is_fp & ib_instructions[thread_id].op_code.alu_opcode == MUL_FP;
			assign fp_is_div                        = is_fp & ib_instructions[thread_id].op_code.alu_opcode == DIV_FP;
			assign fp_is_cmp                        = is_fp & ( ib_instructions[thread_id].op_code.alu_opcode >= CMPGT_FP
					| ib_instructions[thread_id].op_code.alu_opcode <= CMPNE_FP ) ;
					
		    assign is_mont                          = ib_instructions[thread_id].pipe_sel == PIPE_MAC && ib_instructions[thread_id].op_code.alu_opcode == MONT1 &&
		                  ib_instructions[thread_id].op_code.alu_opcode == MONT2;

			// FP checking for collisions. If the element is 1 the FP operation cannot be scheduled
			always_comb begin
				fp_add_issued <= 1'b0;
				fp_mul_issued <= 1'b0;
				fp_div_issued <= 1'b0;
				fp_cmp_issued <= 1'b0;
				if ( fp_is_add | fp_is_sub ) begin
					fp_add_issued <= ~fp_pending_queue[9];
				end
				else if ( fp_is_mul ) begin
					fp_mul_issued <= ~fp_pending_queue[3];
				end
				else if ( fp_is_div ) begin
					fp_div_issued <= ~fp_pending_queue[16];
				end
				else if ( fp_is_cmp ) begin
					fp_cmp_issued <= ~fp_pending_queue[0];
				end
			end
			
			always_comb begin
                mont_issued = 1'b0;

                if ( is_mont ) begin
                    mont_issued <= ~mac_pending[11];
                end
            end

			// When a FP instruction is scheduled (is_fp high), this signal states whether the current instruction causes an
			// instruction hazard or not. When a scheduled FP instruction causes an hazard, this signal disable the thread
			// can issue.
			assign can_issue_fp = ( is_fp ) ? ( fp_add_issued | fp_mul_issued | fp_div_issued | fp_cmp_issued ) : 1'b1;
			assign can_issue_mont = ( is_mont ) ? ( mont_issued ) : 1'b1;

			// FP pending shifting queue update. This queue keeps track of all FP operation already issued by all threads. When
			// the current thread is scheduled by the arbiter (thread_scheduled_id == thread_id) and it issues a floating point
			// instruction, the queue is updated according to the scheduled operation latency.
			always_ff @ ( posedge clk, posedge reset )
				if ( reset )
					fp_pending_queue <= 0;
				else
					if ( thread_scheduled_id == thread_id & |can_issue) begin
						if ( fp_add_issued )
							fp_pending_queue <= {1'b0, fp_pending_queue[16 : 11], 1'b1, fp_pending_queue[9 : 1]};
						else if ( fp_mul_issued )
							fp_pending_queue <= {1'b0, fp_pending_queue[16 : 5], 1'b1, fp_pending_queue[3 : 1]};
						else if ( fp_div_issued )
							fp_pending_queue <= {1'b1, fp_pending_queue[16 : 1]};
						else if ( fp_cmp_issued )
							fp_pending_queue <= {1'b0, fp_pending_queue[16 : 2], 1'b1};
						else
							fp_pending_queue <= {1'b0, fp_pending_queue[16 : 1]};
					end
					else
						fp_pending_queue <= {1'b0, fp_pending_queue[16 : 1]};
						
			always_ff @ ( posedge clk, posedge reset )
                if ( reset )
                    mac_pending <= 0;
                else
                    if ( thread_scheduled_id == thread_id & |can_issue) begin
                        if ( mont_issued )
                            mac_pending <= {1'b1, mac_pending[11 : 1]};
                        else
                            mac_pending <= {1'b0, mac_pending[11 : 1]};
                    end
                    else
                        mac_pending <= {1'b0, mac_pending[11 : 1]};
		end
	endgenerate

	// The arbiter selects one eligible thread from the pool. A thread is eligible if its can_issue
	// bit is high. The arbiter evaluates every can_issue bit and in a round robin fashion selects
	// one thread from the eligible pool.
	rr_arbiter # (
		.NUM_REQUESTERS ( `THREAD_NUMB )
	)
	thread_arbiter (
		.clk        ( clk                      ) ,
		.reset      ( reset                    ) ,
		.request    ( can_issue                ) ,
		.update_lru ( |can_issue               ) ,
		.grant_oh   ( is_thread_scheduled_mask )
	) ;

	oh_to_idx # (
		.NUM_SIGNALS ( `THREAD_NUMB          ) ,
		.DIRECTION   ( "LSB0"                ) ,
		.INDEX_WIDTH ( $bits ( thread_id_t ) )
	)
	oh_to_idx (
		.one_hot ( is_thread_scheduled_mask ) ,
		.index   ( thread_scheduled_id      )
	) ;

	// If at least one thread is eligible (at least one can_issue bit is high), the output is valid
	// and the selected instruction is propagated to the Operand Fetch module.
	always_ff @ ( posedge clk, posedge reset )
		if ( reset )
			is_instruction_valid <= 1'b0;
		else if ( enable ) begin
			is_instruction_valid <= ( |can_issue ) & ( ~rb_valid[thread_scheduled_id] ) ;
		end

	always_ff @ ( posedge clk ) begin
		if ( enable ) begin
			is_instruction      <= ib_instructions[thread_scheduled_id];
			is_thread_id        <= thread_scheduled_id;
			is_destination_mask <= scoreboard_set_bitmap[thread_scheduled_id];
		end
	end

	// starts the counter if a synch is detected and stopped when a release signal is asserted
	`ifdef PERFORMANCE_SYNC
	assign counter_sync_detect = ~ (release_val & sync_detect & sync_detect_cmb) ;
	`endif
`ifdef DISPLAY_CORE
	always_ff @ ( posedge clk ) begin
		if ( is_instruction_valid & ~reset )
			print_core_issue ( 0, is_instruction ) ;
	end
`endif

endmodule