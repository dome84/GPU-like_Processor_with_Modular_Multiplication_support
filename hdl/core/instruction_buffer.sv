`include "nuplus_define.sv"
`include "user_define.sv"

/*
 * This module takes an instruction decoded at puts it in a buffer. There are as many buffers as the number of threads are.
 * These buffers permit the dequeue only when the buffer is not empty or the load/store unit is not full of request.
 * The enqueue is allowed only when the buffer are not full.
 * The output instruction goes to the instruction scheduler, which controls the dequeue signal.
 * 
 * The goal of this module is to decouple the high speed of instruction entrance with the lower speed of instruction issuing
 * due to hazard in the pipe. The second goal in to flush the instruction fetched when a rollback is performed,
 * avoiding inaccurate handling of exceptional event that change the normal flow of fetching. 
 * 
 * Note that the buffer full signal has a threshold reduced of 3, equals to the numbers of pipe stages between the first one.
 */
module instruction_buffer #(
		parameter THREAD_FIFO_LENGTH = 8 )
	(
		input                                               clk,
		input                                               reset,
		input												enable,

		//From Decode
		input                                               dec_valid,
		input  instruction_decoded_t                        dec_instr,

		//From Dynamic Scheduler
		input  thread_mask_t                                is_thread_scheduled_mask,

		// From L1D
		input  thread_mask_t                                l1d_full,

		//From Rollback Handler
		input  thread_mask_t                                rb_valid,
		
		//To Instruction Fetch
		output thread_mask_t                                ib_fifo_full,

		//To Dynamic Scheduler
		output thread_mask_t                                ib_instructions_valid,
		output instruction_decoded_t [`THREAD_NUMB - 1 : 0] ib_instructions
	);


	genvar thread_id;
	generate
		for ( thread_id = 0; thread_id < `THREAD_NUMB; thread_id++ ) begin

			logic instruction_valid;
			logic fifo_empty;
																																			
			assign instruction_valid = dec_valid && ( dec_instr.thread_id == thread_id_t'( thread_id ) ) && ( !rb_valid[thread_id] );
			
			sync_fifo #(
				.WIDTH                ( $bits( instruction_decoded_t ) ),
				.SIZE                 ( THREAD_FIFO_LENGTH             ),
				.ALMOST_FULL_THRESHOLD( THREAD_FIFO_LENGTH - 3         ) // XXX PRIMA ERA -5, CHECK
			) instruction_fifo (
				.clk          ( clk                                 ),
				.reset        ( reset                               ),
				.flush_en     ( rb_valid[thread_id]					),
				.full         (                                     ),
				.almost_full  ( ib_fifo_full[thread_id]             ),
				.enqueue_en   ( instruction_valid 					),
				.value_i      ( dec_instr                           ),
				.empty        ( fifo_empty                          ),
				.almost_empty (                                     ),
				.dequeue_en   ( is_thread_scheduled_mask[thread_id]	),
				.value_o      ( ib_instructions[thread_id]        	)
			);
			
			assign ib_instructions_valid[thread_id] = ~fifo_empty & ~(l1d_full[thread_id] & ib_instructions[thread_id].pipe_sel == PIPE_MEM); 

		end
	endgenerate

endmodule