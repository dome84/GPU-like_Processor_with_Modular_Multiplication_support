`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.05.2017 23:03:40
// Design Name: 
// Module Name: mixcolumns
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mixcolumns(
        input   [3:0][7:0] s,
        output reg [3:0][7:0] sp
    );
    
    logic   [3:0][7:0] xtimes;
    logic   [3:0][7:0] s2;
    logic   [3:0][7:0] s3;
    logic   [3:0][8:0] tmp;
    
    genvar      i;
    generate
        for(i = 0; i < 4; i++) begin
            always_comb begin
              tmp[i] = {1'b0, s[i]} << 1;
              if(tmp[i][8] == 1'b1)
                xtimes[i] = tmp[i][7:0] ^ 8'h1B; // GF8 modulus
              else
                xtimes[i] = tmp[i][7:0];
            end
            
            assign s2[i] = xtimes[i];
            
            assign s3[i] = s2[i] ^ s[i];
             
        end
    endgenerate
    
    always_comb begin
        sp[0] = s2[0] ^ s3[1] ^ s[2] ^ s[3];
        sp[1] = s[0] ^ s2[1] ^ s3[2] ^ s[3];
        sp[2] = s[0] ^ s[1] ^ s2[2] ^ s3[3];
        sp[3] = s3[0] ^ s[1] ^ s[2] ^ s2[3];
    end
endmodule
