`include "nuplus_define.sv"
`include "user_define.sv"
`include "load_store_unit_defines.sv"

/*
 * Writeback stage detects on-the-fly structural hazard on register writeback and extends load signed operations.
 * Some operators, such as Scratchpad Memory, have non predictable latency at issue time, hence Writeback must
 * checks eventually structural hazard on register files. Furthermore, this module handles 8-bit/16-bit
 * and 32-bit load sign extentions.
 */

module writeback (
		input                                               clk,
		input                                               reset,
		input												enable,

		// From CRP Ex Pipe
		input                                               crp_valid,
		input  instruction_decoded_t                        crp_inst_scheduled,
		input  vec_reg_size_t                               crp_result,
		input  hw_lane_mask_t                               crp_hw_lane_mask,

		// From FP Ex Pipe
		input                                               fp_valid,
		input  instruction_decoded_t                        fp_inst_scheduled,
		input  vec_reg_size_t                               fp_result,
		input  scal_reg_size_t                              fp_mask_reg,

		// From INT Ex Pipe
		input                                               int_valid,
		input  instruction_decoded_t                        int_inst_scheduled,
		input  vec_reg_size_t                               int_result,
		input  hw_lane_mask_t                               int_hw_lane_mask,
		input   scal_reg_size_t                             int_2nd_result,
               
        // From SFU Ex Pipe
        input                                                sfu_valid,
        input    instruction_decoded_t                       sfu_inst_scheduled,
        input    vec_reg_size_t                              sfu_result,
        input    hw_lane_mask_t                              sfu_hw_lane_mask,
       
        // From MAC Ex Pipe
        input                                                mac_valid,
        input    instruction_decoded_t                       mac_inst_scheduled,
        input    vec_reg_size_t                              mac_result,
        input    hw_lane_mask_t                              mac_hw_lane_mask,
        input    scal_reg_size_t                             mac_2nd_result,


		// From Scrathpad Memory Pipe
		input                                               spm_valid,
		input  instruction_decoded_t                        spm_inst_scheduled,
		input  vec_reg_size_t                               spm_result,
		input  hw_lane_mask_t                               spm_hw_lane_mask,

		// From Cache L1 Pipe
		input                                               ldst_valid,
		input  instruction_decoded_t                        ldst_inst_scheduled,
		input  vec_reg_size_t                               ldst_result,
		input  hw_lane_mask_t                               ldst_hw_lane_mask,
		input  dcache_address_t                             ldst_address,

		// To Operand Fetch
		output logic                                        wb_valid,
		output thread_id_t                                  wb_thread_id,
		output wb_result_t                                  wb_result,

		// TO Dynamic Scheduler
		output                       [`NUM_EX_PIPE - 1 : 0] wb_fifo_full
	);

	//  -----------------------------------------------------------------------
	//  -- Writeback Parameter and Signals
	//  -----------------------------------------------------------------------
	typedef struct packed {
		scal_reg_size_t pc;
		logic writeback_valid;
		thread_id_t thread_id;

		vec_reg_size_t writeback_result;
		scal_reg_size_t writeback_result1;
		hw_lane_mask_t writeback_hw_lane_mask;
		dcache_address_t result_address;

		reg_addr_t destination;
		logic has_destination;
		logic is_destination_vectorial;
		logic has_2_results;

        logic [2:0] byte_en_shift;

		opcode_t op_code;
		pipeline_disp_t pipe_sel;
		logic is_memory_access;
		logic is_branch;
		logic is_control;
		logic is_movei;
		
		logic no_scoreboard;
	} writeback_request_t;

	localparam                                               PIPE_FP_ID                       = 0; //Indice della pipeline floating point
	localparam                                               PIPE_INT_ID                      = 1; //Indice della pipeline intera
	localparam                                               PIPE_SPM_ID                      = 2; //Indice della pipeline di memora
	localparam                                               PIPE_MEM_ID                      = 3; //Indice della pipeline di memora
	localparam                                               PIPE_MAC_ID                      = 4;
	localparam                                               PIPE_SFU_ID					  = 5; //Indice della pipeline SFU

	logic               [`NUM_EX_PIPE - 1 : 0]               writeback_fifo_empty;                 //Bitmap: L'i-esimo bit è alto se l'i-esima coda è vuota
	logic               [`NUM_EX_PIPE - 1 : 0]               writeback_fifo_full;                  //Bitmap: L'i-esimo bit è alto se l'i-esima coda è vuota
	logic               [`NUM_EX_PIPE - 1 : 0]               pending_requests;                     //Bitmap delle richieste pendenti: l'i-esimo bit è alto se l'i-esima coda non è vuota
	logic               [`NUM_EX_PIPE - 1 : 0]               selected_request_oh;                  //Maschera one-hot: l'i-esimo bit è alto se, nel ciclo corrente, viene servita la richiesta in testa all'i-esima coda
	logic               [$clog2( `NUM_MAX_EX_PIPE ) - 1 : 0] selected_pipe_max;
	logic               [$clog2( `NUM_EX_PIPE ) - 1 : 0]     selected_pipe;

	//Segnali di I/O delle code
	writeback_request_t                                      input_wb_request [`NUM_EX_PIPE];
	writeback_request_t                                      output_wb_request [`NUM_EX_PIPE];

	//  -----------------------------------------------------------------------
	//  -- Writeback Request FIFOs
	//  -----------------------------------------------------------------------
	assign input_wb_request[PIPE_FP_ID].pc                        = fp_inst_scheduled.pc;
	assign input_wb_request[PIPE_FP_ID].writeback_valid           = fp_valid;
	assign input_wb_request[PIPE_FP_ID].thread_id                 = fp_inst_scheduled.thread_id;
	assign input_wb_request[PIPE_FP_ID].writeback_result          = fp_result;
	assign input_wb_request[PIPE_FP_ID].writeback_result1		  = {`REGISTER_SIZE{1'b0}};
    assign input_wb_request[PIPE_FP_ID].has_2_results              = 1'b0;
	assign input_wb_request[PIPE_FP_ID].writeback_hw_lane_mask    = fp_mask_reg;
	assign input_wb_request[PIPE_FP_ID].destination               = fp_inst_scheduled.destination;
	assign input_wb_request[PIPE_FP_ID].is_destination_vectorial  = fp_inst_scheduled.is_destination_vectorial;
	assign input_wb_request[PIPE_FP_ID].op_code                   = fp_inst_scheduled.op_code;
	assign input_wb_request[PIPE_FP_ID].pipe_sel                  = fp_inst_scheduled.pipe_sel;
	assign input_wb_request[PIPE_FP_ID].is_memory_access          = fp_inst_scheduled.is_memory_access;
	assign input_wb_request[PIPE_FP_ID].has_destination           = fp_inst_scheduled.has_destination;
	assign input_wb_request[PIPE_FP_ID].is_memory_access          = fp_inst_scheduled.is_memory_access;
	assign input_wb_request[PIPE_FP_ID].is_branch                 = fp_inst_scheduled.is_branch;
	assign input_wb_request[PIPE_FP_ID].is_control                = fp_inst_scheduled.is_control;
	assign input_wb_request[PIPE_FP_ID].is_movei                  = fp_inst_scheduled.is_movei;
	assign input_wb_request[PIPE_FP_ID].result_address            = 0;
	assign input_wb_request[PIPE_FP_ID].no_scoreboard             = 0;

	assign input_wb_request[PIPE_INT_ID].pc                       = int_inst_scheduled.pc;
	assign input_wb_request[PIPE_INT_ID].writeback_valid          = int_valid;
	assign input_wb_request[PIPE_INT_ID].thread_id                = int_inst_scheduled.thread_id;
	assign input_wb_request[PIPE_INT_ID].writeback_result         = int_result;
	assign input_wb_request[PIPE_INT_ID].writeback_result1		  = int_2nd_result;
    assign input_wb_request[PIPE_INT_ID].has_2_results            = int_inst_scheduled.has_2_results;
	assign input_wb_request[PIPE_INT_ID].writeback_hw_lane_mask   = int_hw_lane_mask;
	assign input_wb_request[PIPE_INT_ID].destination              = int_inst_scheduled.destination;
	assign input_wb_request[PIPE_INT_ID].is_destination_vectorial = int_inst_scheduled.is_destination_vectorial;
	assign input_wb_request[PIPE_INT_ID].op_code                  = int_inst_scheduled.op_code;
	assign input_wb_request[PIPE_INT_ID].pipe_sel                 = ( int_inst_scheduled.pipe_sel == PIPE_BRANCH & int_inst_scheduled.is_int ) ? PIPE_INT : int_inst_scheduled.pipe_sel;
	assign input_wb_request[PIPE_INT_ID].is_memory_access         = int_inst_scheduled.is_memory_access;
	assign input_wb_request[PIPE_INT_ID].has_destination          = int_inst_scheduled.has_destination;
	assign input_wb_request[PIPE_INT_ID].is_memory_access         = int_inst_scheduled.is_memory_access;
	assign input_wb_request[PIPE_INT_ID].is_branch                = int_inst_scheduled.is_branch;
	assign input_wb_request[PIPE_INT_ID].is_control               = int_inst_scheduled.is_control;
	assign input_wb_request[PIPE_INT_ID].is_movei                 = int_inst_scheduled.is_movei;
	assign input_wb_request[PIPE_INT_ID].result_address           = 0;
	assign input_wb_request[PIPE_INT_ID].no_scoreboard            = 0;

	assign input_wb_request[PIPE_SPM_ID].pc                       = spm_inst_scheduled.pc;
	assign input_wb_request[PIPE_SPM_ID].writeback_valid          = spm_valid;
	assign input_wb_request[PIPE_SPM_ID].thread_id                = spm_inst_scheduled.thread_id;
	assign input_wb_request[PIPE_SPM_ID].writeback_result         = spm_result;
	assign input_wb_request[PIPE_SPM_ID].writeback_result1		  = {`REGISTER_SIZE{1'b0}};
    assign input_wb_request[PIPE_SPM_ID].has_2_results            = 1'b0;
	assign input_wb_request[PIPE_SPM_ID].writeback_hw_lane_mask   = spm_hw_lane_mask;
	assign input_wb_request[PIPE_SPM_ID].destination              = spm_inst_scheduled.destination;
	assign input_wb_request[PIPE_SPM_ID].is_destination_vectorial = spm_inst_scheduled.is_destination_vectorial;
	assign input_wb_request[PIPE_SPM_ID].op_code                  = spm_inst_scheduled.op_code;
	assign input_wb_request[PIPE_SPM_ID].pipe_sel                 = spm_inst_scheduled.pipe_sel;
	assign input_wb_request[PIPE_SPM_ID].is_memory_access         = spm_inst_scheduled.is_memory_access;
	assign input_wb_request[PIPE_SPM_ID].has_destination          = spm_inst_scheduled.has_destination;
	assign input_wb_request[PIPE_SPM_ID].is_memory_access         = spm_inst_scheduled.is_memory_access;
	assign input_wb_request[PIPE_SPM_ID].is_branch                = spm_inst_scheduled.is_branch;
	assign input_wb_request[PIPE_SPM_ID].is_control               = spm_inst_scheduled.is_control;
	assign input_wb_request[PIPE_SPM_ID].is_movei                 = spm_inst_scheduled.is_movei;
	assign input_wb_request[PIPE_SPM_ID].result_address           = 0;
	assign input_wb_request[PIPE_SPM_ID].byte_en_shift            = spm_inst_scheduled.immediate[2:0];
    assign input_wb_request[PIPE_SPM_ID].no_scoreboard            = spm_inst_scheduled.is_lookup & (spm_inst_scheduled.immediate[8] == 1'b0);

	assign input_wb_request[PIPE_MEM_ID].pc                       = ldst_inst_scheduled.pc;
	assign input_wb_request[PIPE_MEM_ID].writeback_valid          = ldst_valid;
	assign input_wb_request[PIPE_MEM_ID].thread_id                = ldst_inst_scheduled.thread_id;
	assign input_wb_request[PIPE_MEM_ID].writeback_result         = ldst_result;
	assign input_wb_request[PIPE_MEM_ID].writeback_result1		  = {`REGISTER_SIZE{1'b0}};
    assign input_wb_request[PIPE_MEM_ID].has_2_results              = 1'b0;
	assign input_wb_request[PIPE_MEM_ID].writeback_hw_lane_mask   = ldst_hw_lane_mask;
	assign input_wb_request[PIPE_MEM_ID].destination              = ldst_inst_scheduled.destination;
	assign input_wb_request[PIPE_MEM_ID].is_destination_vectorial = ldst_inst_scheduled.is_destination_vectorial;
	assign input_wb_request[PIPE_MEM_ID].op_code                  = ldst_inst_scheduled.op_code;
	assign input_wb_request[PIPE_MEM_ID].pipe_sel                 = ldst_inst_scheduled.pipe_sel;
	assign input_wb_request[PIPE_MEM_ID].is_memory_access         = ldst_inst_scheduled.is_memory_access;
	assign input_wb_request[PIPE_MEM_ID].has_destination          = ldst_inst_scheduled.has_destination;
	assign input_wb_request[PIPE_MEM_ID].is_memory_access         = ldst_inst_scheduled.is_memory_access;
	assign input_wb_request[PIPE_MEM_ID].is_branch                = ldst_inst_scheduled.is_branch;
	assign input_wb_request[PIPE_MEM_ID].is_control               = ldst_inst_scheduled.is_control;
	assign input_wb_request[PIPE_MEM_ID].is_movei                 = ldst_inst_scheduled.is_movei;
	assign input_wb_request[PIPE_MEM_ID].result_address           = ldst_address;
	assign input_wb_request[PIPE_MEM_ID].no_scoreboard            = 0;
	
	assign input_wb_request[PIPE_MAC_ID].pc                          = mac_inst_scheduled.pc;
    assign input_wb_request[PIPE_MAC_ID].writeback_valid             = mac_valid;
    assign input_wb_request[PIPE_MAC_ID].thread_id                   = mac_inst_scheduled.thread_id;
    assign input_wb_request[PIPE_MAC_ID].writeback_result            = mac_result;
    assign input_wb_request[PIPE_MAC_ID].writeback_result1           = mac_2nd_result;
    assign input_wb_request[PIPE_MAC_ID].has_2_results               = mac_inst_scheduled.has_2_results;
    assign input_wb_request[PIPE_MAC_ID].writeback_hw_lane_mask      = mac_hw_lane_mask;
    assign input_wb_request[PIPE_MAC_ID].destination                 = mac_inst_scheduled.destination;
    assign input_wb_request[PIPE_MAC_ID].is_destination_vectorial    = mac_inst_scheduled.is_destination_vectorial;
    assign input_wb_request[PIPE_MAC_ID].op_code                     = mac_inst_scheduled.op_code;
    assign input_wb_request[PIPE_MAC_ID].pipe_sel                    = mac_inst_scheduled.pipe_sel;
    assign input_wb_request[PIPE_MAC_ID].is_memory_access            = mac_inst_scheduled.is_memory_access;
    assign input_wb_request[PIPE_MAC_ID].has_destination             = mac_inst_scheduled.has_destination;
    assign input_wb_request[PIPE_MAC_ID].is_branch                   = mac_inst_scheduled.is_branch;
    assign input_wb_request[PIPE_MAC_ID].is_control                  = mac_inst_scheduled.is_control;
    assign input_wb_request[PIPE_MAC_ID].is_movei                    = mac_inst_scheduled.is_movei;
    assign input_wb_request[PIPE_MAC_ID].result_address              = 0;
    assign input_wb_request[PIPE_MAC_ID].no_scoreboard               = 0;
    
    assign input_wb_request[PIPE_SFU_ID].pc                          = sfu_inst_scheduled.pc;
    assign input_wb_request[PIPE_SFU_ID].writeback_valid             = sfu_valid;
    assign input_wb_request[PIPE_SFU_ID].thread_id                   = sfu_inst_scheduled.thread_id;
    assign input_wb_request[PIPE_SFU_ID].writeback_result            = sfu_result;
    assign input_wb_request[PIPE_SFU_ID].writeback_result1           = {`REGISTER_SIZE{1'b0}};
    assign input_wb_request[PIPE_SFU_ID].has_2_results               = 1'b0;
    assign input_wb_request[PIPE_SFU_ID].writeback_hw_lane_mask      = sfu_hw_lane_mask;
    assign input_wb_request[PIPE_SFU_ID].destination                 = sfu_inst_scheduled.destination;
    assign input_wb_request[PIPE_SFU_ID].is_destination_vectorial    = sfu_inst_scheduled.is_destination_vectorial;
    assign input_wb_request[PIPE_SFU_ID].op_code                     = sfu_inst_scheduled.op_code;
    assign input_wb_request[PIPE_SFU_ID].pipe_sel                    = sfu_inst_scheduled.pipe_sel;
    assign input_wb_request[PIPE_SFU_ID].is_memory_access            = sfu_inst_scheduled.is_memory_access;
    assign input_wb_request[PIPE_SFU_ID].has_destination             = sfu_inst_scheduled.has_destination;
    assign input_wb_request[PIPE_SFU_ID].is_branch                   = sfu_inst_scheduled.is_branch;
    assign input_wb_request[PIPE_SFU_ID].is_control                  = sfu_inst_scheduled.is_control;
    assign input_wb_request[PIPE_SFU_ID].is_movei                    = sfu_inst_scheduled.is_movei;
    assign input_wb_request[PIPE_SFU_ID].result_address              = 0;
    assign input_wb_request[PIPE_SFU_ID].no_scoreboard               = 0;

	genvar                                                   i;
	generate
		for ( i = 0; i < `NUM_EX_PIPE; i++ ) begin : WB_FIFOS
			sync_fifo #(
				.WIDTH                ( $bits( writeback_request_t ) ),
				.SIZE                 ( `WB_FIFO_SIZE                ),
				// 4 equals to the distance from the first stage of the operand fetch stage
				.ALMOST_FULL_THRESHOLD( `WB_FIFO_SIZE - 4            )
			) writeback_request_fifo (
				.clk          ( clk                                 ),
				.reset        ( reset                               ),
				.flush_en     (                                     ),
				.full         (                                     ),
				.almost_full  ( writeback_fifo_full[i]              ),
				.enqueue_en   ( input_wb_request[i].writeback_valid ),
				.value_i      ( input_wb_request[i]                 ),
				.empty        ( writeback_fifo_empty[i]             ),
				.almost_empty (                                     ),
				.dequeue_en   ( selected_request_oh[i]              ),
				.value_o      ( output_wb_request[i]                )
			);
		end
	endgenerate

	// Combinatorialmente si segnala al dynamic scheduler che le fifo delle wb sono piene
	assign wb_fifo_full                                           = writeback_fifo_full;

	//  -----------------------------------------------------------------------
	//  -- Request Dispatcher and Result Composer
	//  -----------------------------------------------------------------------
	vec_reg_size_t                                           byte_data_mem;
	vec_reg_size_t                                           half_data_mem;
	vec_reg_size_t                                           word_data_mem;
	vec_reg_size_t                                           byte_data_mem_s;
	vec_reg_size_t                                           half_data_mem_s;

	vec_reg_size_t                                           byte_data_spm;
	vec_reg_size_t                                           half_data_spm;
	vec_reg_size_t                                           word_data_spm;
	vec_reg_size_t                                           byte_data_spm_s;
	vec_reg_size_t                                           half_data_spm_s;

	vec_reg_size_t                                           result_data_mem;
	vec_reg_size_t                                           result_data_spm;
	wb_result_t                                              wb_next;
	thread_id_t                                              wb_thread_id_next;

	reg_byte_enable_t                                        reg_byte_en;
	reg_byte_enable_t                                        mem_byte_en;
	logic                                                    write_on_reg;

	dcache_offset_t                                          dcache_offset;

	assign pending_requests                                       = ~writeback_fifo_empty;

	rr_arbiter #(
		.NUM_REQUESTERS( `NUM_MAX_EX_PIPE )
	) rr_arbiter (
		.clk        ( clk                                                             ),
		.reset      ( reset                                                           ),
		.request    ( { {(`NUM_MAX_EX_PIPE - `NUM_EX_PIPE){1'b0}} ,pending_requests } ),
		.update_lru ( 1'b1                                                            ),
		.grant_oh   ( selected_request_oh                                             )
	);

	oh_to_idx #(
		.NUM_SIGNALS( `NUM_MAX_EX_PIPE ),
		.DIRECTION  ( "LSB0"           )
	) oh_to_idx (
		.one_hot( selected_request_oh ),
		.index  ( selected_pipe_max   )
	);

	assign selected_pipe                                          = selected_pipe_max;

	always_comb begin
		write_on_reg  = output_wb_request[selected_pipe].has_destination;
		dcache_offset = output_wb_request[PIPE_MEM_ID].result_address.offset[`DCACHE_OFFSET_LENGTH - 1 : 2];
	end

	genvar                                                   j;
	generate
		for ( j = 0; j < `HW_LANE; j++ ) begin : LANE_DATA_COMPOSER
			assign word_data_mem[j]   = output_wb_request[PIPE_MEM_ID].writeback_result[j][31 : 0];
			assign byte_data_mem[j]   = {{( `REGISTER_SIZE - 8 ){1'b0}}, word_data_mem[j][7 : 0]};
			assign half_data_mem[j]   = {{( `REGISTER_SIZE - 16 ){1'b0}}, word_data_mem[j][15 : 0]};
			assign byte_data_mem_s[j] = {{( `REGISTER_SIZE - 8 ){word_data_mem[j][7]} }, word_data_mem[j][7 : 0]};
			assign half_data_mem_s[j] = {{( `REGISTER_SIZE - 16 ){word_data_mem[j][15]}}, word_data_mem[j][15 : 0]};

			assign word_data_spm[j]   = output_wb_request[PIPE_SPM_ID].writeback_result[j][31 : 0];
			assign byte_data_spm[j]   = {{( `REGISTER_SIZE - 8 ){1'b0}}, word_data_spm[j][7 : 0]};
			assign half_data_spm[j]   = {{( `REGISTER_SIZE - 16 ){1'b0}}, word_data_spm[j][15 : 0]};
			assign byte_data_spm_s[j] = {{( `REGISTER_SIZE - 8 ){word_data_spm[j][7]} }, word_data_spm[j][7 : 0]};
			assign half_data_spm_s[j] = {{( `REGISTER_SIZE - 16 ){word_data_spm[j][15]}}, word_data_spm[j][15 : 0]};
		end
	endgenerate

	// 4 = BYTE_PER_REGISTER
	always_comb begin : BYTE_EN_SPM_MEM
		case ( output_wb_request[PIPE_SPM_ID].op_code ) //Era sbagliato, deve essere la costante PIPE_SPM_ID

			// Scalar
			LOAD_8,
			LOAD_8_U,
			LOAD_V_8,
			LOAD_V_8_U : mem_byte_en  = 4'b0001;
			LOOKUP_8  : mem_byte_en = 4'b0001 << output_wb_request[PIPE_SPM_ID].byte_en_shift;

			LOAD_16,
			LOAD_16_U,
			LOAD_V_16,
			LOAD_V_16_U : mem_byte_en = 4'b0011;

			LOAD_32_U,
			LOAD_32,
			LOAD_64,
			LOAD_V_32,
			LOAD_V_32_U,
			LOAD_V_64 : mem_byte_en   = 4'b1111;


			default : mem_byte_en     = 4'b1111; // per la scratchpad
		endcase
	end

	always_comb begin : RESULT_COMPOSER_MEM
		case ( output_wb_request[PIPE_MEM_ID].op_code )

			// Scalar
			LOAD_8 : result_data_mem      = vec_reg_size_t'( byte_data_mem_s[dcache_offset] );
			LOAD_16 : result_data_mem     = vec_reg_size_t'( half_data_mem_s[dcache_offset] );
			LOAD_32,
			LOAD_64 : result_data_mem     = vec_reg_size_t'( word_data_mem[dcache_offset] );

			LOAD_8_U : result_data_mem    = vec_reg_size_t'( byte_data_mem[dcache_offset] );
			LOAD_16_U : result_data_mem   = vec_reg_size_t'( half_data_mem[dcache_offset] );
			LOAD_32_U : result_data_mem   = vec_reg_size_t'( word_data_mem[dcache_offset] );

			// Vector
			LOAD_V_8 : result_data_mem    = byte_data_mem_s;
			LOAD_V_16 : result_data_mem   = half_data_mem_s;
			LOAD_V_32,
			LOAD_V_64 : result_data_mem   = word_data_mem;

			LOAD_V_8_U : result_data_mem  = word_data_mem;
			LOAD_V_16_U : result_data_mem = half_data_mem;
			LOAD_V_32_U : result_data_mem = word_data_mem;

			default : result_data_mem     = 0;
		endcase
	end

	always_comb begin : RESULT_COMPOSER_SPM // FIXME: PROVARE CON MEMORIA!!
		case ( output_wb_request[PIPE_SPM_ID].op_code )

			// Scalar
			LOAD_8 : result_data_spm      = vec_reg_size_t'( byte_data_spm_s[0] );
			LOAD_16 : result_data_spm     = vec_reg_size_t'( half_data_spm_s[0] );
			LOAD_32,
			LOAD_64 : result_data_spm     = vec_reg_size_t'( word_data_spm[0] );

			LOAD_8_U : result_data_spm    = vec_reg_size_t'( byte_data_spm[0] );
			LOAD_16_U : result_data_spm   = vec_reg_size_t'( half_data_spm[0] );
			LOAD_32_U : result_data_spm   = vec_reg_size_t'( word_data_spm[0] );

			// Vector
			LOAD_V_8 : result_data_spm    = byte_data_spm_s;
			LOAD_V_16 : result_data_spm   = half_data_spm_s;
			LOAD_V_32,
			LOAD_V_64 : result_data_spm   = word_data_spm;

			LOAD_V_8_U : result_data_spm  = word_data_spm;
			LOAD_V_16_U : result_data_spm = half_data_spm;
			LOAD_V_32_U : result_data_spm = word_data_spm;
			
			LOOKUP_8    : result_data_spm = word_data_spm;

			default : result_data_spm     = 0;
		endcase
	end

	always_comb begin : RESULT_COMPOSER_MOVE
		case ( output_wb_request[PIPE_INT_ID].op_code )
			//Move Immediate low writes the whole register, because the other half
			//undefined register part interferes with arithmetic operations.
			MOVEI : reg_byte_en   = 4'b1111;
			MOVEI_L : reg_byte_en = 4'b0011;
			MOVEI_H : reg_byte_en = 4'b1100;
			default : reg_byte_en = 4'b0000;
		endcase
	end

	always_comb begin
	    wb_next.has_2_results = 1'b0;
		case ( output_wb_request[selected_pipe].pipe_sel )
			PIPE_MEM : wb_next.wb_result_data = result_data_mem;
			PIPE_SPM : wb_next.wb_result_data = result_data_spm;
			PIPE_MAC,
			PIPE_INT : begin
                           wb_next.wb_result_data = output_wb_request[selected_pipe].writeback_result;
                           wb_next.wb_result_data1 = output_wb_request[selected_pipe].writeback_result1;
                           wb_next.has_2_results = output_wb_request[selected_pipe].has_2_results;
                       end 
			PIPE_CR,
			PIPE_FP,
			PIPE_SFU : wb_next.wb_result_data  = output_wb_request[selected_pipe].writeback_result;
			default : wb_next.wb_result_data  = 0;
		endcase
	end

	always_comb begin
		wb_thread_id_next              = output_wb_request[selected_pipe].thread_id;
		wb_next.wb_result_pc           = output_wb_request[selected_pipe].pc;
		wb_next.wb_result_register     = output_wb_request[selected_pipe].destination;
		wb_next.wb_result_is_scalar    = !output_wb_request[selected_pipe].is_destination_vectorial;
		wb_next.wb_result_hw_lane_mask = output_wb_request[selected_pipe].writeback_hw_lane_mask;
		wb_next.no_scoreboard          = output_wb_request[selected_pipe].no_scoreboard;

		if( output_wb_request[selected_pipe].is_movei )
			wb_next.wb_result_write_byte_enable = reg_byte_en;
		else if( output_wb_request[selected_pipe].is_memory_access )
			wb_next.wb_result_write_byte_enable = mem_byte_en;
		else
			wb_next.wb_result_write_byte_enable = {`BYTE_PER_REGISTER{1'b1}};
	end

//	always_ff @ ( posedge clk, posedge reset ) begin
//		if ( reset )
//			wb_valid <= 1'b0;
//		else
//			if ( selected_request_oh != 0 && write_on_reg ) begin
//				wb_valid <= output_wb_request[selected_pipe].writeback_valid;
//			end else
//				wb_valid <= 1'b0;
//	end
//
//	always_ff @ ( posedge clk ) begin
//		if ( selected_request_oh != 0 && write_on_reg ) begin
//			wb_result    <= wb_next;
//			wb_thread_id <= wb_thread_id_next;
//		end
//	end

	always_ff @ ( posedge clk, posedge reset ) begin
		if ( reset )
			wb_valid <= 1'b0;
		else if ( enable ) begin
			if ( selected_request_oh != 0 && write_on_reg ) 
				wb_valid <= output_wb_request[selected_pipe].writeback_valid;
			else
				wb_valid <= 1'b0;
		end 
	end

	always_ff @ ( posedge clk ) begin
		if ( enable ) begin
			if ( selected_request_oh != 0 && write_on_reg) begin
				wb_result    <= wb_next;
				wb_thread_id <= wb_thread_id_next;
			end
		end 
	end
	
endmodule
