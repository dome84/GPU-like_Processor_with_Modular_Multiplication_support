module ram_1r1w
    #(
	parameter DATA_WIDTH = 32,
    parameter SIZE = 32)
	    
    (
	input                             clk,
	    
    input [ADDR_WIDTH - 1:0]         addra,
    input [ADDR_WIDTH - 1:0]         addrb,
    input                            ena,
    input                            enb,
    input                            wena,
    input                            wenb,
 
    input [DATA_WIDTH - 1:0]         wdataa,
    input [DATA_WIDTH - 1:0]         wdatab,
    
    output logic[DATA_WIDTH - 1:0]   rdataa,
    output logic[DATA_WIDTH - 1:0]   rdatab
    );

    localparam ADDR_WIDTH = $clog2(SIZE);
    parameter RAM_PERFORMANCE = "LOW_LATENCY";
	reg [DATA_WIDTH - 1:0] ram1 [SIZE];

    logic[DATA_WIDTH - 1:0] data_from_ram1;
    logic[DATA_WIDTH - 1:0] data_from_ram2;

    always_ff @(posedge clk)
    begin
        
            if (wena) begin
                ram1[addra] <= wdataa;
                data_from_ram1 <= wdataa;
            end
            else
                data_from_ram1 <= ram1[addra];
 
    end
    
    always_ff @(posedge clk)
        begin
            
                if (wenb) begin
                    ram1[addrb] <= wdatab;
                    data_from_ram2 <= wdatab;
                end
                else
                    data_from_ram2 <= ram1[addrb];
     
        end

  
    assign rdataa = data_from_ram1;
    assign rdatab = data_from_ram2;

endmodule
