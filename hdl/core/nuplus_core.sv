`include "user_define.sv"
`include "nuplus_define.sv"
`include "load_store_unit_defines.sv"
`include "system_defines.sv"
`include "synchronization_defines.sv"

`ifndef SINGLE_CORE
`include "cache_controller_defines.sv"
`endif

module nuplus_core # (
		parameter TILE_ID = 0,
		parameter CORE_ID = 0 )
	(
		input                        clk,
		input                        reset,
		input                        ext_freeze,
		input                        resume,
		input [`THREAD_NUMB - 1 : 0] thread_en,

		//For debug not for DSU
		output address_t program_counter,

		// Host Interface
		input logic       hi_job_valid,
		input address_t   hi_job_pc,
		input thread_id_t hi_job_thread_id,

		//From Host Debug
		input logic                                   dsu_enable,
		input logic                                   dsu_single_step,
		input address_t   [7 : 0]                     dsu_breakpoint,
		input logic       [7 : 0]                     dsu_breakpoint_enable,
		input logic                                   dsu_thread_selection,
		input thread_id_t                             dsu_thread_id,
		input logic                                   dsu_en_vector,
		input logic                                   dsu_en_scalar,
		input logic                                   dsu_load_shift_reg,
		input logic                                   dsu_start_shift,
		input logic       [`REGISTER_ADDRESS - 1 : 0] dsu_reg_addr,

		input logic dsu_write_scalar,
		input logic dsu_write_vector,
		input logic dsu_serial_reg_in,

		//To Host Debug
		output address_t   [`THREAD_NUMB - 1 : 0] dsu_bp_instruction,
		output thread_id_t                        dsu_bp_thread_id,
		output logic                              dsu_serial_reg_out,
		output logic                              dsu_stop_shift,
		output logic                              dsu_hit_breakpoint,


`ifdef SINGLE_CORE
		output address_t     n2m_request_address,
		output dcache_line_t n2m_request_data,
		output logic         n2m_request_read,
		output logic         n2m_request_write,
		output logic         mc_avail_o,

		input logic         m2n_request_available,
		input logic         m2n_response_valid,
		input address_t     m2n_response_address,
		input dcache_line_t m2n_response_data,
`else
		// Network Interface - Instruction Cache
		input mem_instr_request_available,

		output logic     tc_instr_request_valid,
		output address_t tc_instr_request_address,


		// From Network Interface - Data Cache
		input logic                         ni_request_network_available,
		input logic                         ni_response_network_available,
		input coherence_forwarded_message_t ni_forwarded_request,
		input logic                         ni_forwarded_request_valid,
		input coherence_response_message_t  ni_response,
		input logic                         ni_response_valid,

		// To Network Interface - Data Cache
		output logic l1d_forwarded_request_consumed,
		output logic l1d_response_consumed,

		output logic                               l1d_request_valid,
		output coherence_request_message_t         l1d_request,
		output logic                               l1d_request_has_data,
		output tile_address_t              [1 : 0] l1d_request_destinations,
		output logic                       [1 : 0] l1d_request_destinations_valid,

		output logic                                l1d_response_valid,
		output coherence_response_message_t         l1d_response,
		output logic                                l1d_response_has_data,
		output tile_address_t               [1 : 0] l1d_response_destinations,
		output logic                        [1 : 0] l1d_response_destinations_valid,
`endif

		//Synchronize Barrier
		//From/To Virtual Network
		output                       bc2n_account_valid,
		output service_c2n_message_t bc2n_account_message,
		`ifndef SINGLE_CORE
		output dest_valid_t bc2n_account_destination_valid,
		`endif
		input  logic                 n2bc_network_available,
		//From Network Interface
		input  service_c2n_message_t n2bc_release_message,
		input                        n2bc_release_valid,
		output logic                 n2bc_mes_service_consumed
	);

	//  -----------------------------------------------------------------------
	//  -- Signals
	//  -----------------------------------------------------------------------
	// Thread Control Stage - Signals
	thread_id_t   tc_job_thread_id;
	address_t     tc_job_pc;
	logic         tc_job_valid;
	icache_lane_t tc_if_data_out;
	address_t     tc_if_addr_update_cache;
	logic         tc_if_valid_update_cache;
	thread_mask_t tc_if_thread_en;
	icache_lane_t mem_instr_request_data_in;
	logic         mem_instr_request_valid;

	// IF Stage - Signals
	logic           if_valid;
	thread_id_t     if_thread_selected_id;
	scal_reg_size_t if_pc_scheduled;
	instruction_t   if_inst_scheduled;
	logic           if_cache_miss;
	thread_mask_t   if_thread_miss;
	address_t       if_address_miss;

	// Decode Stage - Signals
	logic                 dec_valid;
	instruction_decoded_t dec_instr;

	// Instruction Buffer Stage - Signal
	thread_mask_t                                ib_fifo_full;
	thread_mask_t                                ib_instructions_valid;
	instruction_decoded_t [`THREAD_NUMB - 1 : 0] ib_instructions;

	// Instruction Scheduler Stage - Signal
	logic                 is_instruction_valid;
	thread_id_t           is_thread_id;
	instruction_decoded_t is_instruction;
	thread_mask_t         is_thread_scheduled_mask;
	scoreboard_t          is_destination_mask;

	// Operand Fetch Stage - Signals
	logic                 opf_valid;
	instruction_decoded_t opf_inst_scheduled;
	vec_reg_size_t        opf_fetched_op0;
	vec_reg_size_t        opf_fetched_op1;
	scal_reg_size_t       opf_fetched_op2;
	hw_lane_mask_t        opf_hw_lane_mask;
	scoreboard_t          opf_destination_bitmap;
	
	logic                 fast_valid;
    instruction_decoded_t fast_inst_scheduled;
    vec_reg_size_t        fast_fetched_op0;
    vec_reg_size_t        fast_fetched_op1;
    hw_lane_mask_t        fast_hw_lane_mask;
    scal_reg_size_t       fast_fetched_op2;

	// INT Pipe Stage - Signals
	logic                 int_valid;
	instruction_decoded_t int_inst_scheduled;
	vec_reg_size_t        int_result;
	hw_lane_mask_t        int_hw_lane_mask;
	scal_reg_size_t                              int_2nd_result;
    vec_reg_size_t                               int_forward;
    thread_id_t                                  int_thread_forward;
	
	// SFU Pipe Stage - Signals
    logic                                        sfu_valid;
    instruction_decoded_t                        sfu_inst_scheduled;
    vec_reg_size_t                               sfu_result;
    hw_lane_mask_t                               sfu_hw_lane_mask;
    vec_reg_size_t                               sfu_forward;
    thread_id_t                                  sfu_thread_forward;
    logic                                        sfu_valid_forward;
    
    // MAC Pipe Stage - Signals
    logic                                        mac_valid;
    instruction_decoded_t                        mac_inst_scheduled;
    vec_reg_size_t                               mac_result;
    hw_lane_mask_t                               mac_hw_lane_mask;
    scal_reg_size_t                              mac_2nd_result;

	// CRP Pipe Stage - Signals
	logic                 crp_valid;
	instruction_decoded_t crp_inst_scheduled;
	vec_reg_size_t        crp_result;
	hw_lane_mask_t        crp_hw_lane_mask;

	// Branch Control Stage - Signals
	logic        bc_rollback_enable;
	logic        bc_rollback_valid;
	address_t    bc_rollback_pc;
	thread_id_t  bc_rollback_thread_id;
	scoreboard_t bc_scoreboard;

	// FP Pipe Stage - Signals
	logic                 fpu_valid;
	instruction_decoded_t fpu_inst_scheduled;
	hw_lane_mask_t        fpu_fetched_mask;
	vec_reg_size_t        fpu_result_sp;


	// LDST Pipe Stage - Signals
	logic                                        l1d_valid;
	instruction_decoded_t                        l1d_instruction;
	dcache_line_t                                l1d_result;
	hw_lane_mask_t                               l1d_hw_lane_mask;
	dcache_address_t                             l1d_address;
	thread_mask_t                                l1d_almost_full;
	logic                 [`THREAD_NUMB - 1 : 0] no_load_store_pending;

	// Scratchpad Memory Stage - Signals
	logic                 spm_valid;
	instruction_decoded_t spm_inst_scheduled;
	vec_reg_size_t        spm_result;
	hw_lane_mask_t        spm_hw_lane_mask;

	// Writeback Stage - Signals
	logic        [`THREAD_NUMB - 1 : 0] rollback_valid;
	address_t    [`THREAD_NUMB - 1 : 0] rollback_pc_value;
	scoreboard_t [`THREAD_NUMB - 1 : 0] rollback_clear_bitmap;
	logic                               wb_valid;
	thread_id_t                         wb_thread_id;
	wb_result_t                         wb_result;
	logic        [`NUM_EX_PIPE - 1 : 0] wb_fifo_full;

	logic [`THREAD_NUMB - 1 : 0] scoreboard_empty;
	logic [`THREAD_NUMB - 1 : 0] release_val;

	// DSU - Signals
	logic                        freeze;
	logic                        nfreeze;
	logic [`THREAD_NUMB - 1 : 0] dsu_stop_issue;

	assign nfreeze = ~freeze;

	//For debug not for DSU
	assign program_counter = is_instruction.pc;


`ifdef SINGLE_CORE
	logic ldst_miss;
	logic ldst_flush;
	logic ldst_evict;

	logic               cc_update_ldst_valid;
	dcache_way_idx_t    cc_update_ldst_way;
	dcache_address_t    cc_update_ldst_address;
	dcache_privileges_t cc_update_ldst_privileges;
	dcache_line_t       cc_update_ldst_store_value;
	cc_command_t        cc_update_ldst_command;

	logic            cc_snoop_data_valid;
	dcache_set_t     cc_snoop_data_set;
	dcache_way_idx_t cc_snoop_data_way;
	dcache_line_t    ldst_snoop_data;

	logic       cc_wakeup;
	thread_id_t cc_wakeup_thread_id;

	logic        cc_snoop_tag_valid;
	dcache_set_t cc_snoop_tag_set;

	dcache_privileges_t [`DCACHE_WAY - 1 : 0] ldst_snoop_privileges;
	dcache_tag_t        [`DCACHE_WAY - 1 : 0] ldst_snoop_tag ;

	// Memory interface
	logic mem_instr_request_available;

	logic     tc_instr_request_valid;
	address_t tc_instr_request_address;
`endif

`ifdef PERFORMANCE_SYNC
	logic [`THREAD_NUMB - 1 : 0]        counter_sync_send;
	logic [`THREAD_NUMB - 1 : 0]        counter_sync_detect;
	logic [1 : 0][`THREAD_NUMB - 1 : 0] perf_events;
`endif
//-------------------------------------------------------------
//  -- Thread Controller
//  -----------------------------------------------------------------------


	thread_controller thread_controller (
		.clk                         ( clk                         ),
		.reset                       ( reset                       ),
		.enable                      ( nfreeze                     ),
		// Host Interface
		.hi_job_valid                ( hi_job_valid                ),
		.hi_job_pc                   ( hi_job_pc                   ),
		.hi_job_thread_id            ( hi_job_thread_id            ),
		//Instruction fetch stage
		.if_cache_miss               ( if_cache_miss               ),
		.if_thread_miss              ( if_thread_miss              ),
		.if_address_miss             ( if_address_miss             ),
		.tc_if_data_out              ( tc_if_data_out              ),
		.tc_if_addr_update_cache     ( tc_if_addr_update_cache     ),
		.tc_if_valid_update_cache    ( tc_if_valid_update_cache    ),
		.tc_if_thread_en             ( tc_if_thread_en             ),
		.thread_en                   ( thread_en                   ), //external signal from user
		.ib_fifo_full                ( ib_fifo_full                ), //from instruction buffer
		.tc_job_pc                   ( tc_job_pc                   ),
		.tc_job_thread_id            ( tc_job_thread_id            ),
		.tc_job_valid                ( tc_job_valid                ),
		.dsu_stop_issue              ( dsu_stop_issue              ),
		//Memory interface
		.mem_instr_request_available ( mem_instr_request_available ),
		.mem_instr_request_data_in   ( mem_instr_request_data_in   ),
		.mem_instr_request_valid     ( mem_instr_request_valid     ),
		.tc_instr_request_address    ( tc_instr_request_address    ),
		.tc_instr_request_valid      ( tc_instr_request_valid      )

	);

	//  -----------------------------------------------------------------------
	//  -- Instruction Fetch
	//  -----------------------------------------------------------------------

	instruction_fetch_stage instruction_fetch_stage1 (
		.clk    ( clk     ),
		.reset  ( reset   ),
		.enable ( nfreeze ),

		//Rollback stage interface
		.rollback_valid    ( rollback_valid    ),
		.rollback_pc_value ( rollback_pc_value ),

		//Instruction fetch stage interface
		.if_valid              ( if_valid                 ),
		.if_thread_selected_id ( if_thread_selected_id    ),
		.if_pc_scheduled       ( if_pc_scheduled          ),
		.if_inst_scheduled     ( if_inst_scheduled        ),
		//Thread controller stage interface
		.tc_data_out           ( tc_if_data_out           ),
		.tc_addr_update_cache  ( tc_if_addr_update_cache  ),
		.tc_valid_update_cache ( tc_if_valid_update_cache ),
		.tc_thread_en          ( tc_if_thread_en          ),
		.if_cache_miss         ( if_cache_miss            ),
		.if_thread_miss        ( if_thread_miss           ),
		.if_address_miss       ( if_address_miss          ),
		.tc_job_pc             ( tc_job_pc                ),
		.tc_job_thread_id      ( tc_job_thread_id         ),
		.tc_job_valid          ( tc_job_valid             )
	);

	//  -----------------------------------------------------------------------
	//  -- Decode
	//  -----------------------------------------------------------------------

	decode decode (
		.clk                   ( clk                   ),
		.reset                 ( reset                 ),
		.enable                ( nfreeze               ),
		.if_valid              ( if_valid              ),
		.if_thread_selected_id ( if_thread_selected_id ),
		.if_pc_scheduled       ( if_pc_scheduled       ),
		.if_inst_scheduled     ( if_inst_scheduled     ),
		.rollback_valid        ( rollback_valid        ),
		.dec_valid             ( dec_valid             ),
		.dec_instr             ( dec_instr             )
	);

	//  -----------------------------------------------------------------------
	//  -- Instruction Buffer
	//  -----------------------------------------------------------------------

	instruction_buffer # (
		.THREAD_FIFO_LENGTH ( `INSTRUCTION_FIFO_SIZE )
	)
	instruction_buffer (
		.clk                      ( clk                      ),
		.reset                    ( reset                    ),
		.enable                   ( nfreeze                  ),
		.dec_valid                ( dec_valid                ),
		.dec_instr                ( dec_instr                ),
		.l1d_full                 ( l1d_almost_full          ),
		.is_thread_scheduled_mask ( is_thread_scheduled_mask ),
		.rb_valid                 ( rollback_valid           ),
		.ib_fifo_full             ( ib_fifo_full             ),
		.ib_instructions_valid    ( ib_instructions_valid    ),
		.ib_instructions          ( ib_instructions          )
	);

	//  -----------------------------------------------------------------------
	//  -- Instruction Scheduler
	//  -----------------------------------------------------------------------

	instruction_scheduler instruction_scheduler (
		.clk                   ( clk                   ) ,
		.reset                 ( reset                 ) ,
		.enable                ( nfreeze               ) ,
		.ib_instructions_valid ( ib_instructions_valid ) ,
		.ib_instructions       ( ib_instructions       ) ,
		.wb_valid              ( wb_valid              ) ,
		.wb_thread_id          ( wb_thread_id          ) ,
		.wb_result             ( wb_result             ) ,
		.wb_fifo_full          ( wb_fifo_full          ) ,
		.rb_valid              ( rollback_valid        ) ,
		.rb_destination_mask   ( rollback_clear_bitmap ) ,
		.dsu_stop_issue        ( dsu_stop_issue        ) ,
		.is_instruction_valid  ( is_instruction_valid  ) ,
		.is_thread_id          ( is_thread_id          ) ,
		.is_instruction        ( is_instruction        ) ,
		.is_destination_mask   ( is_destination_mask   ) ,
		.scoreboard_empty      ( scoreboard_empty      ) ,
		.release_val           ( release_val           ) ,
		`ifdef PERFORMANCE_SYNC
		.counter_sync_detect ( counter_sync_detect ) ,
		`endif
		.is_thread_scheduled_mask ( is_thread_scheduled_mask )
	) ;

	//  -----------------------------------------------------------------------
	//  -- Operand Fetch
	//  -----------------------------------------------------------------------

	operand_fetch operand_fetch1 (
		.clk                      ( clk                    ),
		.reset                    ( reset                  ),
		.enable                   ( nfreeze                ),
		.issue_valid              ( is_instruction_valid   ),
		.issue_thread_id          ( is_thread_id           ),
		.issue_inst_scheduled     ( is_instruction         ),
		.issue_destination_bitmap ( is_destination_mask    ),
		.rollback_valid           ( rollback_valid         ),
		.wb_valid                 ( wb_valid               ),
		.wb_thread_id             ( wb_thread_id           ),
		.wb_result                ( wb_result              ),
		.int_forward              ( sfu_forward            ),
        .int_thread_forward       ( sfu_thread_forward     ),
        .int_valid_forward        ( sfu_valid_forward      ),
                
		//Intergace with DSU
		.dsu_en_scalar            ( dsu_en_scalar          ),
		.dsu_en_vector            ( dsu_en_vector          ),
		.dsu_reg_addr             ( dsu_reg_addr           ),
		.dsu_write_scalar         ( dsu_write_scalar       ),
		.dsu_write_vector         ( dsu_write_vector       ),
		.dsu_serial_reg_in        ( dsu_serial_reg_in      ),
		.dsu_serial_reg_out       ( dsu_serial_reg_out     ),
		.dsu_load_shift_reg       ( dsu_load_shift_reg     ),
		.dsu_start_shift          ( dsu_start_shift        ),
		.dsu_stop_shift           ( dsu_stop_shift         ),
		//To Ex Pipes
		.opf_valid                ( opf_valid              ),
		.opf_inst_scheduled       ( opf_inst_scheduled     ),
		.opf_fetched_op0          ( opf_fetched_op0        ),
		.opf_fetched_op1          ( opf_fetched_op1        ),
		.opf_fetched_op2          ( opf_fetched_op2        ),
		.opf_hw_lane_mask         ( opf_hw_lane_mask       ),
		.opf_destination_bitmap   ( opf_destination_bitmap ),
		
		// Fast pipe signals
        .fast_valid               ( fast_valid             ),
        .fast_inst_scheduled       ( fast_inst_scheduled     ),
        .fast_fetched_op0          ( fast_fetched_op0        ),
        .fast_fetched_op1          ( fast_fetched_op1        ),
        .fast_hw_lane_mask         ( fast_hw_lane_mask       ),
        .fast_fetched_op2          ( fast_fetched_op2        )
	);

	//  -----------------------------------------------------------------------
	//  -- Ex Pipes
	//  -----------------------------------------------------------------------

	//  -----------------------------------------------------------------------
	//  -- INT Pipe
	//  -----------------------------------------------------------------------

	int_pipe #(
		.TILE_ID( TILE_ID ),
		.CORE_ID( CORE_ID ) )
	int_pipe (
		.clk                ( clk                ),
		.reset              ( reset              ),
		.enable             ( nfreeze            ),
		//From Operand Fetch
		.opf_valid          ( opf_valid          ),
		.opf_inst_scheduled ( opf_inst_scheduled ),
		.opf_fetched_op0    ( opf_fetched_op0    ),
		.opf_fetched_op1    ( opf_fetched_op1    ),
		.opf_hw_lane_mask   ( opf_hw_lane_mask   ),
		.opf_fetched_op2    ( opf_fetched_op2    ),

		//To Writeback
		.int_valid          ( int_valid          ),
		.int_inst_scheduled ( int_inst_scheduled ),
		.int_result         ( int_result         ),
		.int_hw_lane_mask   ( int_hw_lane_mask   ),
		.int_2nd_result     ( int_2nd_result     ),
        .int_forward        ( int_forward        ),
        .int_thread_forward ( int_thread_forward )
	);

    //  -----------------------------------------------------------------------
    //  -- MAC Pipe
    //  -----------------------------------------------------------------------
   `ifdef MONTGOMERY_SUPPORT 
   
        mac_pipe #(
            .TILE_ID( TILE_ID ),
            .CORE_ID( CORE_ID ) )
        mac_pipe1 (
            .clk                ( clk                ),
            .reset              ( reset              ),
    
            //From Operand Fetch
            .opf_valid          ( opf_valid          ),
            .opf_inst_scheduled ( opf_inst_scheduled ),
            .opf_fetched_op0    ( opf_fetched_op0    ),
            .opf_fetched_op1    ( opf_fetched_op1    ),
            .opf_fetched_op2    ( opf_fetched_op2    ),
            .opf_hw_lane_mask   ( opf_hw_lane_mask   ),
    
            //To Writeback
            .mac_valid          ( mac_valid          ),
            .mac_inst_scheduled ( mac_inst_scheduled ),
            .mac_result         ( mac_result         ),
            .mac_hw_lane_mask   ( mac_hw_lane_mask   ),
            .mac_2nd_result     ( mac_2nd_result     )
        );
      
      `else        
        
        mac_pipe_old #(
            .TILE_ID( TILE_ID ),
            .CORE_ID( CORE_ID ) )
        mac_pipe1 (
            .clk                ( clk                ),
            .reset              ( reset              ),
    
            //From Operand Fetch
            .opf_valid          ( opf_valid          ),
            .opf_inst_scheduled ( opf_inst_scheduled ),
            .opf_fetched_op0    ( opf_fetched_op0    ),
            .opf_fetched_op1    ( opf_fetched_op1    ),
            .opf_hw_lane_mask   ( opf_hw_lane_mask   ),
    
            //To Writeback
            .mac_valid          ( mac_valid          ),
            .mac_inst_scheduled ( mac_inst_scheduled ),
            .mac_result         ( mac_result         ),
            .mac_hw_lane_mask   ( mac_hw_lane_mask   )
        );
        
      `endif
      
	//  -----------------------------------------------------------------------
	//  -- Branch Control Pipe
	//  -----------------------------------------------------------------------

	branch_control branch_control (
		//From Operand Fetch
		.opf_valid              ( opf_valid              ),
		.opf_inst_scheduled     ( opf_inst_scheduled     ),
		.opf_fetched_op0        ( opf_fetched_op0        ),
		.opf_fetched_op1        ( opf_fetched_op1        ),
		.opf_destination_bitmap ( opf_destination_bitmap ),

		//To Rollback Handler
		.bc_rollback_enable    ( bc_rollback_enable    ),
		.bc_rollback_valid     ( bc_rollback_valid     ),
		.bc_rollback_pc        ( bc_rollback_pc        ),
		.bc_rollback_thread_id ( bc_rollback_thread_id ),
		.bc_scoreboard         ( bc_scoreboard         )
	);

//  -----------------------------------------------------------------------
//  -- FPU Pipe
//  -----------------------------------------------------------------------

//  fp_pipe u_fp_pipe (
//      .clk               (clk               ),
//      .reset             (reset             ),
//      .enable            (nfreeze           ),
//      //To Writeback
//      .fpu_fetched_mask  (fpu_fetched_mask  ),
//      .fpu_inst_scheduled(fpu_inst_scheduled),
//      .fpu_result_sp     (fpu_result_sp     ),
//      .fpu_valid         (fpu_valid         ),
//      //From Operand Fetch
//      .opf_fetched_mask  (opf_hw_lane_mask  ),
//      .opf_fetched_op0   (opf_fetched_op0   ),
//      .opf_fetched_op1   (opf_fetched_op1   ),
//      .opf_inst_scheduled(opf_inst_scheduled),
//      .opf_valid         (opf_valid         )
//  );

//  -----------------------------------------------------------------------
//  -- SFU Pipe
//  -----------------------------------------------------------------------
	sfu_pipe #(
    .TILE_ID( TILE_ID ),
    .CORE_ID( CORE_ID ) )
        sfu_pipe1 (
            .clk                ( clk                ),
            .reset              ( reset              ),
        
            //From Operand Fetch
            .opf_valid          ( opf_valid          ),
            .opf_inst_scheduled ( opf_inst_scheduled ),
            .opf_fetched_op0    ( opf_fetched_op0    ),
            .opf_fetched_op1    ( opf_fetched_op1    ),
            .opf_hw_lane_mask   ( opf_hw_lane_mask   ),
        
            //To Writeback
            .sfu_valid          ( sfu_valid          ),
            .sfu_inst_scheduled ( sfu_inst_scheduled ),
            .sfu_result         ( sfu_result         ),
            .sfu_hw_lane_mask   ( sfu_hw_lane_mask   ),
            .sfu_forward              ( sfu_forward            ),
            .sfu_thread_forward       ( sfu_thread_forward     ),
            .sfu_valid_forward        ( sfu_valid_forward      )
        );


//  -----------------------------------------------------------------------
//  -- L1 Data Cache Memory
//  -----------------------------------------------------------------------

`ifdef SINGLE_CORE

	uncoherent_controller u_uncoherent_controller (
		.clk                        ( clk                         ),
		.reset                      ( reset                       ),
		//Load Store Unit
		.ldst_instruction           ( l1d_instruction             ),
		.ldst_address               ( l1d_address                 ),
		.ldst_miss                  ( ldst_miss                   ),
		.ldst_cache_line            ( l1d_result                  ),
		.ldst_flush                 ( ldst_flush                  ),
		.ldst_evict                 ( ldst_evict                  ),
		.cc_update_ldst_valid       ( cc_update_ldst_valid        ),
		.cc_update_ldst_way         ( cc_update_ldst_way          ),
		.cc_update_ldst_address     ( cc_update_ldst_address      ),
		.cc_update_ldst_privileges  ( cc_update_ldst_privileges   ),
		.cc_update_ldst_store_value ( cc_update_ldst_store_value  ),
		.cc_update_ldst_command     ( cc_update_ldst_command      ),
		.cc_wakeup                  ( cc_wakeup                   ),
		.cc_wakeup_thread_id        ( cc_wakeup_thread_id         ),
		.cc_snoop_tag_valid         ( cc_snoop_tag_valid          ),
		.cc_snoop_tag_set           ( cc_snoop_tag_set            ),
		.cc_snoop_data_valid        ( cc_snoop_data_valid         ),
		.cc_snoop_data_set          ( cc_snoop_data_set           ),
		.cc_snoop_data_way          ( cc_snoop_data_way           ),
		.ldst_snoop_data            ( ldst_snoop_data             ),
		.ldst_snoop_privileges      ( ldst_snoop_privileges       ),
		.ldst_snoop_tag             ( ldst_snoop_tag              ),
		//Memory controller
		.n2m_request_address        ( n2m_request_address         ),
		.n2m_request_data           ( n2m_request_data            ),
		.n2m_request_read           ( n2m_request_read            ),
		.n2m_request_write          ( n2m_request_write           ),
		.mc_avail_o                 ( mc_avail_o                  ),
		.m2n_request_available      ( m2n_request_available       ),
		.m2n_response_valid         ( m2n_response_valid          ),
		.m2n_response_address       ( m2n_response_address        ),
		.m2n_response_data          ( m2n_response_data           ),
		//Thread Controller - Instruction cache interface
		.mem_instr_request_available( mem_instr_request_available ),
		.mem_instr_request_data_in  ( mem_instr_request_data_in   ),
		.mem_instr_request_valid    ( mem_instr_request_valid     ),
		.tc_instr_request_valid     ( tc_instr_request_valid      ),
		.tc_instr_request_address   ( tc_instr_request_address    )
	);


	load_store_unit load_store_unit (
		.clk                        ( clk                        ),
		.reset                      ( reset                      ),
		//Operand Fetch
		.opf_valid                  ( opf_valid                  ),
		.opf_inst_scheduled         ( opf_inst_scheduled         ),
		.opf_fetched_op0            ( opf_fetched_op0            ),
		.opf_fetched_op1            ( opf_fetched_op1            ),
		.opf_hw_lane_mask           ( opf_hw_lane_mask           ),
		.no_load_store_pending      ( no_load_store_pending      ),
		//Writeback and Cache Controller
		.ldst_valid                 ( l1d_valid                  ),
		.ldst_instruction           ( l1d_instruction            ),
		.ldst_cache_line            ( l1d_result                 ),
		.ldst_hw_lane_mask          ( l1d_hw_lane_mask           ),
		.ldst_store_mask            (                            ),
		.ldst_address               ( l1d_address                ),
		.ldst_miss                  ( ldst_miss                  ),
		.ldst_evict                 ( ldst_evict                 ),
		.ldst_flush                 ( ldst_flush                 ),
		//Instruction Scheduler
		.ldst_almost_full           ( l1d_almost_full            ),
		//Cache Controller - Thread weakup
		.cc_wakeup                  ( cc_wakeup                  ),
		.cc_wakeup_thread_id        ( cc_wakeup_thread_id        ),
		//Cache Controller - Update Bus
		.cc_update_ldst_valid       ( cc_update_ldst_valid       ),
		.cc_update_ldst_command     ( cc_update_ldst_command     ),
		.cc_update_ldst_way         ( cc_update_ldst_way         ),
		.cc_update_ldst_address     ( cc_update_ldst_address     ),
		.cc_update_ldst_privileges  ( cc_update_ldst_privileges  ),
		.cc_update_ldst_store_value ( cc_update_ldst_store_value ),
		//Cache Controller - Tag Snoop Bus
		.cc_snoop_tag_valid         ( cc_snoop_tag_valid         ),
		.cc_snoop_tag_set           ( cc_snoop_tag_set           ),
		.ldst_snoop_privileges      ( ldst_snoop_privileges      ),
		.ldst_snoop_tag             ( ldst_snoop_tag             ),
		//Cache Controller - Data Snoop Bus
		.cc_snoop_data_valid        ( cc_snoop_data_valid        ),
		.cc_snoop_data_set          ( cc_snoop_data_set          ),
		.cc_snoop_data_way          ( cc_snoop_data_way          ),
		.ldst_snoop_data            ( ldst_snoop_data            ),
		//Cache Controller - LRU Access Bus
		//.ldst_lru_access_en         (                            ),
		.ldst_lru_update_set        (                            ),
		//Cache Controller Stage 2 - LRU Update Bus
		.ldst_lru_update_en         (                            ),
		.ldst_lru_update_way        (                            ),
		//Rollback Handler
		.rollback_valid             ( rollback_valid             ),
		.ldst_rollback_en           (                            ),
		.ldst_rollback_pc           (                            ),
		.ldst_rollback_thread_id    (                            )
	);

`else

	l1d_cache #(
		.TILE_ID( TILE_ID ),
		.CORE_ID( CORE_ID ) )
	l1d_cache (
		.clk                             ( clk                             ),
		.reset                           ( reset                           ),
		//From Operand Fetch
		.opf_valid                       ( opf_valid                       ),
		.opf_inst_scheduled              ( opf_inst_scheduled              ),
		.opf_fetched_op0                 ( opf_fetched_op0                 ),
		.opf_fetched_op1                 ( opf_fetched_op1                 ),
		.opf_hw_lane_mask                ( opf_hw_lane_mask                ),
		//From Network Interface
		.ni_request_network_available    ( ni_request_network_available    ),
		.ni_response_network_available   ( ni_response_network_available   ),
		.ni_forwarded_request            ( ni_forwarded_request            ),
		.ni_forwarded_request_valid      ( ni_forwarded_request_valid      ),
		.ni_response                     ( ni_response                     ),
		.ni_response_valid               ( ni_response_valid               ),
		//From Rollback Handler
		.rollback_valid                  ( rollback_valid                  ),
		//To Network Interface
		.l1d_forwarded_request_consumed  ( l1d_forwarded_request_consumed  ),
		.l1d_response_consumed           ( l1d_response_consumed           ),
		.l1d_request_valid               ( l1d_request_valid               ),
		.l1d_request                     ( l1d_request                     ),
		.l1d_request_has_data            ( l1d_request_has_data            ),
		.l1d_request_destinations        ( l1d_request_destinations        ),
		.l1d_request_destinations_valid  ( l1d_request_destinations_valid  ),
		.l1d_response_valid              ( l1d_response_valid              ),
		.l1d_response                    ( l1d_response                    ),
		.l1d_response_has_data           ( l1d_response_has_data           ),
		.l1d_response_destinations       ( l1d_response_destinations       ),
		.l1d_response_destinations_valid ( l1d_response_destinations_valid ),
		//To Writeback
		.l1d_valid                       ( l1d_valid                       ),
		.l1d_instruction                 ( l1d_instruction                 ),
		.l1d_result                      ( l1d_result                      ),
		.l1d_hw_lane_mask                ( l1d_hw_lane_mask                ),
		.l1d_store_mask                  ( l1d_store_mask                  ),
		.l1d_address                     ( l1d_address                     ),
		//To Instruction Scheduler
		.l1d_almost_full                 ( l1d_almost_full                 ),
		//To Sync Core
		.no_load_store_pending           ( no_load_store_pending           ),
		//To Rollback Handler
		.l1d_rollback_en                 ( l1d_rollback_en                 ),
		.l1d_rollback_pc                 ( l1d_rollback_pc                 ),
		.l1d_rollback_thread_id          ( l1d_rollback_thread_id          ),
		.mem_instr_request_data_in       ( mem_instr_request_data_in       ),
		.mem_instr_request_valid         ( mem_instr_request_valid         )
	);

	`endif

	//  -----------------------------------------------------------------------
	//  -- Scratchpad Memory
	//  -----------------------------------------------------------------------
	logic spm_rollback_en;
	logic spm_rollback_pc;
	logic spm_rollback_thread_id;

	scratchpad_memory_pipe scratchpad_memory_pipe1 (
		.clk   ( clk   ),
		.reset ( reset ),

		//From Operand Fetch
		.opf_valid          ( opf_valid          ),
		.opf_inst_scheduled ( opf_inst_scheduled ),
		.opf_fetched_op0    ( opf_fetched_op0    ),
		.opf_fetched_op1    ( opf_fetched_op1    ),
		.opf_hw_lane_mask   ( opf_hw_lane_mask   ),

		//To Writeback
		.spm_valid          ( spm_valid          ),
		.spm_inst_scheduled ( spm_inst_scheduled ),
		.spm_result         ( spm_result         ),
		.spm_hw_lane_mask   ( spm_hw_lane_mask   ),

		//To Dynamic Scheduler
		.spm_can_issue ( ),

		//To RollbackController - Questi segnali si devono attivare in modo combinatoriale nel primo ciclo di clock. Questo è vero per tutte le pipe d'esecuzione.
		.spm_rollback_en        ( spm_rollback_en        ),
		.spm_rollback_pc        ( spm_rollback_pc        ),
		.spm_rollback_thread_id ( spm_rollback_thread_id )
	);

	//  -----------------------------------------------------------------------
	//  -- Rollback Handler
	//  -----------------------------------------------------------------------

	rollback_handler rollback_handler (
		.clk                   ( clk                   ),
		.reset                 ( reset                 ),
		.enable                ( 1'b1                  ), //( nfreeze                ),
		.is_instruction_valid  ( is_instruction_valid  ),
		.is_thread_id          ( is_thread_id          ),
		.is_destination_mask   ( is_destination_mask   ), //TODO: caso SMT devono essere `THREAD_NUMB
		.bc_scoreboard         ( bc_scoreboard         ),
		.bc_rollback_enable    ( bc_rollback_enable    ),
		.bc_rollback_valid     ( bc_rollback_valid     ),
		.bc_rollback_pc        ( bc_rollback_pc        ),
		.bc_rollback_thread_id ( bc_rollback_thread_id ),
		//.l1d_rollback_en (l1d_rollback_en),
		//.l1d_rollback_pc (l1d_rollback_pc),
		//.l1d_rollback_thread_id (l1d_rollback_thread_id), // TODO. secondo fabio non servono a niente
		.rollback_pc_value     ( rollback_pc_value     ),
		.rollback_valid        ( rollback_valid        ),
		.rollback_clear_bitmap ( rollback_clear_bitmap )
	);

	//  -----------------------------------------------------------------------
	//  -- WriteBack
	//  -----------------------------------------------------------------------

	writeback writeback (
		.clk               ( clk                ),
		.reset             ( reset              ),
		.enable            ( 1'b1               ),//( nfreeze              ),
		//From FP Ex Pipe
		.fp_valid          ( fpu_valid          ),
		.fp_inst_scheduled ( fpu_inst_scheduled ),
		.fp_result         ( fpu_result_sp      ),
		.fp_mask_reg       ( fpu_fetched_mask   ),

		//From INT Ex Pipe
		.int_valid          ( int_valid          ),
		.int_inst_scheduled ( int_inst_scheduled ),
		.int_result         ( int_result         ),
		.int_hw_lane_mask   ( int_hw_lane_mask   ),
		.int_2nd_result      ( int_2nd_result    ),
		
		//From MAC Ex Pipe
        .mac_valid           ( mac_valid          ),
        .mac_inst_scheduled  ( mac_inst_scheduled ),
        .mac_result          ( mac_result         ),
        .mac_hw_lane_mask    ( mac_hw_lane_mask   ),
        .mac_2nd_result      ( mac_2nd_result     ),
		
		//From SFU pipe
        .sfu_valid           ( sfu_valid          ),
        .sfu_inst_scheduled  ( sfu_inst_scheduled ),
        .sfu_result          ( sfu_result         ),
        .sfu_hw_lane_mask    ( sfu_hw_lane_mask   ),

		//From Scrathpad Memory Pipe
		.spm_valid          ( spm_valid          ),
		.spm_inst_scheduled ( spm_inst_scheduled ),
		.spm_result         ( spm_result         ),
		.spm_hw_lane_mask   ( spm_hw_lane_mask   ),

		//From Cache L1 Pipe
		.ldst_valid          ( l1d_valid        ) ,
		.ldst_inst_scheduled ( l1d_instruction  ) ,
		.ldst_result         ( l1d_result       ) ,
		.ldst_hw_lane_mask   ( l1d_hw_lane_mask ) ,
		.ldst_address        ( l1d_address      ) ,
		//To Operand Fetch
		.wb_valid            ( wb_valid         ) ,
		.wb_thread_id        ( wb_thread_id     ) ,
		.wb_result           ( wb_result        ) ,

		//TO Dynamic Scheduler
		.wb_fifo_full ( wb_fifo_full )
	) ;

	//  -----------------------------------------------------------------------
	//  -- Debug Support Unit
	//  -----------------------------------------------------------------------

	debug_controller debug_controller (
		.clk        ( clk        ) ,
		.reset      ( reset      ) ,
		.resume     ( resume     ) ,
		.ext_freeze ( ext_freeze ) ,

		.dsu_enable            ( dsu_enable            ) ,
		.dsu_single_step       ( dsu_single_step       ) ,
		.dsu_breakpoint        ( dsu_breakpoint        ) ,
		.dsu_breakpoint_enable ( dsu_breakpoint_enable ) ,
		.dsu_thread_selection  ( dsu_thread_selection  ) ,
		.dsu_thread_id         ( dsu_thread_id         ) ,

		//From Instruction Scheduler
		.is_instruction_valid ( is_instruction_valid ) ,
		.is_instruction       ( is_instruction       ) ,
		.is_thread_id         ( is_thread_id         ) ,
		.scoreboard_empty     ( scoreboard_empty     ) ,

		//From LDST
		.no_load_store_pending ( no_load_store_pending ) ,

		//From Rollback Handler
		.rollback_valid ( rollback_valid ) ,

		.dsu_bp_instruction ( dsu_bp_instruction ) ,
		.dsu_bp_thread_id   ( dsu_bp_thread_id   ) ,
		.dsu_hit_breakpoint ( dsu_hit_breakpoint ) ,
		//From SX stage
		.dsu_stop_issue     ( dsu_stop_issue     ) ,
		.freeze             ( freeze             )
	) ;


	barrier_core #(
		.TILE_ID( TILE_ID )
	)
	u_barrier_core (
		.clk                ( clk                ),
		.reset              ( reset              ),
		//Operand Fetch
		.opf_valid          ( opf_valid          ), //controllare il bit dell'istruzione
		.opf_inst_scheduled ( opf_inst_scheduled ),
		//To ThreadScheduler
		.release_val        ( release_val        ),
		.scoreboard_empty   ( scoreboard_empty   ),
		//Id_Barrier
		.opf_fetched_op0    ( opf_fetched_op0    ),
		//Destination Barrier
		.opf_fetched_op1    ( opf_fetched_op1    ),
		`ifdef PERFORMANCE_SYNC
		.counter_sync_send ( counter_sync_send ),
		`endif
		//to Network Interface
		`ifndef SINGLE_CORE
		.c2n_account_destination_valid ( bc2n_account_destination_valid ),
		`endif
		.c2n_account_valid        ( bc2n_account_valid        ),
		.c2n_account_message      ( bc2n_account_message      ),
		.network_available        ( n2bc_network_available    ),
		//From Network Interface
		.n2c_release_message      ( n2bc_release_message      ),
		.n2c_release_valid        ( n2bc_release_valid        ),
		.n2c_mes_service_consumed ( n2bc_mes_service_consumed ),

		//Load Store Unit
		.no_load_store_pending ( no_load_store_pending )
	);

`ifdef PERFORMANCE_SYNC
	assign perf_events[0] = counter_sync_detect;
	assign perf_events[1] = counter_sync_send;

	performance_counter u_performance_counter (
		.clk            ( clk             ),
		.opf_fetched_op0( opf_fetched_op0 ),
		.perf_events    ( perf_events     ),
		.reset          ( reset           )
	);
`endif

endmodule