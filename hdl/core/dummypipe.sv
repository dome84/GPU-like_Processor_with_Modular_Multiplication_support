/* verilator lint_off UNUSED */
module dummypipe 

#(
parameter WIDTH = 8,
parameter DEPTH = 1
)

(
clk,
en,
datain,
q);

input clk, en;
input [WIDTH-1:0] datain;
output reg [WIDTH-1:0] q;

reg [WIDTH-1:0] tmp[DEPTH-1:0]  /* verilator public */;
integer i;

always @(posedge clk) begin
    if(en == 1) begin
        for(i = DEPTH-1; i>0; i = i - 1) begin
            tmp[i] <= tmp[i-1];
        end
        tmp[0] <= datain;
    end
end

assign q = tmp[DEPTH-1];
endmodule