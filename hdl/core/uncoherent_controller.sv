// TODO DA TESTARE!

`include "user_define.sv"
`include "nuplus_define.sv"
`include "load_store_unit_defines.sv"
`include "system_defines.sv"

/*
 * THIS MODULE CAN BE USED ONLY IN SINGLE-CORE MODE.
 * THIS MODULE WORKS ONLY IF THE THREADS DON'T SHARE THE SAME VARIABLE.
 *
 * This module is the L1 cache controller inside the nu+ core.
 * The main task is to house the requests from the core (load/store miss, instruction miss, flush, evict) and
 * to execute it one per time. The request are scheduled with a prefixed priority.
 *
 * An FSM manage the response from the memory:
 *  - for an instruction miss, a read was performed, so nothing has to be done
 * 	- for a load/store miss, a read was performed, so the response data has to be posed in the proper cache set/way;
 * 		if all the way are occupied for that set, a replace command is sent
 * 	- for a flush/evict, a store is performed, but the flush operation has to retrieve the data from the cache;
 * 		an hit is ever expected because only one operation per time is performed
 * 
 * Note that the hit check has to be done for a load/store miss because the read request are not merged, so a response 
 * could arrive after a cache miss for the same address 
 */

module uncoherent_controller (

		input                                              clk,
		input                                              reset,

		// Load Store Unit

		input  instruction_decoded_t                       ldst_instruction,
		input  dcache_address_t                            ldst_address,
		input  logic                                       ldst_miss,
		input  dcache_line_t                               ldst_cache_line,
		input  logic                                       ldst_flush,
		input  logic                                       ldst_evict,

		output logic                                       cc_update_ldst_valid,
		output dcache_way_idx_t                            cc_update_ldst_way,
		output dcache_address_t                            cc_update_ldst_address,
		output dcache_privileges_t                         cc_update_ldst_privileges,
		output dcache_line_t                               cc_update_ldst_store_value,
		output cc_command_t                                cc_update_ldst_command,

		output logic                                       cc_wakeup,
		output thread_id_t                                 cc_wakeup_thread_id,

		output logic                                       cc_snoop_tag_valid,
		output dcache_set_t                                cc_snoop_tag_set,
		output logic                                       cc_snoop_data_valid,
		output dcache_set_t                                cc_snoop_data_set,
		output dcache_way_idx_t                            cc_snoop_data_way,

		input  dcache_line_t                               ldst_snoop_data,
		input  dcache_privileges_t   [`DCACHE_WAY - 1 : 0] ldst_snoop_privileges,
		input  dcache_tag_t          [`DCACHE_WAY - 1 : 0] ldst_snoop_tag ,


		// Memory controller

		output address_t                                   n2m_request_address,
		output dcache_line_t                               n2m_request_data,
		output logic                                       n2m_request_read,
		output logic                                       n2m_request_write,
		output logic                                       mc_avail_o,

		input  logic                                       m2n_request_available,
		input  logic                                       m2n_response_valid,
		input  address_t                                   m2n_response_address,
		input  dcache_line_t                               m2n_response_data,

		// Thread Controller - Instruction cache interface

		output logic                                       mem_instr_request_available,
		output icache_lane_t                               mem_instr_request_data_in,
		output logic                                       mem_instr_request_valid,

		input  logic                                       tc_instr_request_valid,
		input  address_t                                   tc_instr_request_address

	);

	dcache_line_t     m2n_response_data_swap;
	dcache_line_t     out_data_swap;

	//-----------
	typedef struct packed {
		dcache_address_t address;
		thread_id_t thread_id;
		dcache_line_t cache_line;
		logic is_flush;
	} fifo_t;

	logic             pending_ldst, dequeue_ldst, empty_ldst;
	logic             pending_evict, empty_evict, dequeue_evict;
	logic             pending_inst, empty_inst, dequeue_inst;

	fifo_t            value_i, value_o, value_evict_o;
	address_t         value_inst_o;
	//-----------

	typedef enum logic [2 : 0] {IDLE, WAITING_MEM, WAITING_DEQUEUE, WAITING_INSTR_MEM, WAITING_REPLACEMENT, WAITING_SNOOP, WAITING_SNOOP_DATA} state_t;
	state_t           state;

	dcache_address_t  cc_update_ldst_address_temp, m2n_response_address_temp;

	dcache_way_mask_t way_matched_oh, way_valid_oh, flush_way_matched_oh;
	dcache_way_idx_t  way_matched_idx, way_matched_reg, flush_way_matched_idx, lru_out;
	logic             is_hit, is_all_valid, update_lru;

	//  -----------------------------------------------------------------------
	//  -- Memory swap Interface
	//  -----------------------------------------------------------------------
	// Endian swap vector data
	genvar            swap_word;
	generate
		for ( swap_word = 0; swap_word < 16 /*`HW_LANE*/; swap_word++ ) begin : swap_word_gen // Non so se � il parametro giusto, ma bisognerebbe usare un parametro non una costante esplicita
			assign m2n_response_data_swap[swap_word * 32 +: 8]      = m2n_response_data[swap_word * 32 + 24 +: 8];
			assign m2n_response_data_swap[swap_word * 32 + 8 +: 8]  = m2n_response_data[swap_word * 32 + 16 +: 8];
			assign m2n_response_data_swap[swap_word * 32 + 16 +: 8] = m2n_response_data[swap_word * 32 + 8 +: 8];
			assign m2n_response_data_swap[swap_word * 32 + 24 +: 8] = m2n_response_data[swap_word * 32 +: 8];

			assign n2m_request_data[swap_word * 32 +: 8]            = out_data_swap[swap_word * 32 + 24 +: 8];
			assign n2m_request_data[swap_word * 32 + 8 +: 8]        = out_data_swap[swap_word * 32 + 16 +: 8];
			assign n2m_request_data[swap_word * 32 + 16 +: 8]       = out_data_swap[swap_word * 32 + 8 +: 8];
			assign n2m_request_data[swap_word * 32 + 24 +: 8]       = out_data_swap[swap_word * 32 +: 8];
		end
	endgenerate

	//  -----------------------------------------------------------------------
	//  -- Core Interface - Input requests buffering
	//  -----------------------------------------------------------------------
	assign value_i.address                    = ldst_address,
		value_i.thread_id                     = ldst_instruction.thread_id,
		value_i.cache_line                    = ldst_cache_line,
		value_i.is_flush                      = ldst_flush;

	assign pending_ldst                       = !empty_ldst;
	assign pending_evict                      = !empty_evict;
	assign pending_inst                       = !empty_inst,
		mem_instr_request_available           = empty_inst;

	// 2* c'� perch� posso avere un cache miss istruzione prima che un data miss sia stato evaso
	sync_fifo #(
		.WIDTH ( $bits( fifo_t )  ),
		.SIZE  ( 2 * `THREAD_NUMB )
	)
	load_store_flush_fifo (
		.clk         ( clk          ),
		.reset       ( reset        ),
		.flush_en    ( 1'b0         ), //flush is synchronous, unlike reset
		.full        (              ),
		.almost_full (              ),
		.enqueue_en  ( ldst_miss    ),
		.value_i     ( value_i      ),
		.empty       ( empty_ldst   ),
		.almost_empty(              ),
		.dequeue_en  ( dequeue_ldst ),
		.value_o     ( value_o      )
	);

	sync_fifo #(
		.WIDTH                 ( $bits( fifo_t ) ),
		.SIZE                  ( `THREAD_NUMB    ),
		.ALMOST_FULL_THRESHOLD ( `THREAD_NUMB    )
	)
	evict_flush_fifo (
		.clk         ( clk                     ),
		.reset       ( reset                   ),
		.flush_en    ( 1'b0                    ), //flush is synchronous, unlike reset
		.full        (                         ),
		.almost_full (                         ),
		.enqueue_en  ( ldst_evict | ldst_flush ),
		.value_i     ( value_i                 ),
		.empty       ( empty_evict             ),
		.almost_empty(                         ),
		.dequeue_en  ( dequeue_evict           ),
		.value_o     ( value_evict_o           )
	);

	sync_fifo #(
		.WIDTH ( $bits( tc_instr_request_address ) ),
		.SIZE  ( 2                                 )
	)
	inst_flush_fifo (
		.clk         ( clk                      ),
		.reset       ( reset                    ),
		.flush_en    ( 1'b0                     ), //flush is synchronous, unlike reset
		.full        (                          ),
		.almost_full (                          ),
		.enqueue_en  ( tc_instr_request_valid   ),
		.value_i     ( tc_instr_request_address ),
		.empty       ( empty_inst               ),
		.almost_empty(                          ),
		.dequeue_en  ( dequeue_inst             ),
		.value_o     ( value_inst_o             )
	);

	//  -----------------------------------------------------------------------
	//  -- Snoop Tag and Data cache
	//  -----------------------------------------------------------------------

	always_comb begin
		cc_update_ldst_address_temp = dcache_address_t'( cc_update_ldst_address );
		m2n_response_address_temp   = dcache_address_t'( m2n_response_address );
	end
	assign cc_snoop_tag_set                   = ( state == IDLE && value_evict_o.is_flush && pending_evict )? value_evict_o.address.index : m2n_response_address_temp.index,
		cc_snoop_tag_valid                    = (state == IDLE && pending_evict) || (state == WAITING_MEM && m2n_response_valid); // xxx controllare

	assign cc_snoop_data_valid                = state == WAITING_SNOOP, // xxx controllare
		cc_snoop_data_set                     = value_evict_o.address.index,
		cc_snoop_data_way                     = flush_way_matched_idx;

	assign cc_update_ldst_privileges.can_read = 1'b1 ,
		cc_update_ldst_privileges.can_write   = 1'b1; // � come se fosse valid/invalid, basterebbe solo un bit

	//  -----------------------------------------------------------------------
	//  -- Snooping match hit
	//  -----------------------------------------------------------------------
	genvar            dcache_way;
	generate
		for ( dcache_way = 0; dcache_way < `DCACHE_WAY; dcache_way++ ) begin
			assign way_matched_oh[dcache_way]       = ( ( ldst_snoop_tag[dcache_way] == cc_update_ldst_address_temp.tag ) && ( ldst_snoop_privileges[dcache_way] == dcache_privileges_t'( 2'b11 ) ) );
			assign way_valid_oh [dcache_way]        = ldst_snoop_privileges[dcache_way] == dcache_privileges_t'( 2'b11 ) ;
			assign flush_way_matched_oh[dcache_way] = ( ( ldst_snoop_tag[dcache_way] == value_evict_o.address.tag ) && ( ldst_snoop_privileges[dcache_way] == dcache_privileges_t'( 2'b11 ) ) );
		end
	endgenerate

	assign is_hit                             = |way_matched_oh,
		is_all_valid                          = |way_valid_oh;

	oh_to_idx
	#(
		.NUM_SIGNALS( `DCACHE_WAY ),
		.DIRECTION  ( "LSB0"      )
	)
	u_oh_to_idx
	(
		.index  ( way_matched_idx ),
		.one_hot( way_matched_oh  )
	);

	oh_to_idx
	#(
		.NUM_SIGNALS( `DCACHE_WAY ),
		.DIRECTION  ( "LSB0"      )
	)
	u_oh_to_idx_2
	(
		.index  ( flush_way_matched_idx ),
		.one_hot( flush_way_matched_oh  )
	);


	//  -----------------------------------------------------------------------
	//  -- FSM controller
	//  -----------------------------------------------------------------------
	always_ff @( posedge clk, posedge reset ) begin
		if ( reset ) begin
			state                     <= IDLE;
		end else begin
			cc_update_ldst_valid      <= 1'b0;
			cc_wakeup                 <= 1'b0;
			dequeue_ldst              <= 1'b0;
			mc_avail_o                <= 1'b0;
			n2m_request_address       <= '{default : '0};
			mem_instr_request_data_in <= '{default : '0};
			n2m_request_read          <= 1'b0;
			n2m_request_write         <= 1'b0;
			out_data_swap             <= '{default : '0};
			dequeue_inst              <= 1'b0;
			dequeue_evict             <= 1'b0;
			update_lru                <= 1'b0;
			mem_instr_request_valid   <=1'b0;

			case( state )

				IDLE : begin
					if ( m2n_request_available ) begin
						if ( pending_inst ) begin
							state               <= WAITING_INSTR_MEM;
							n2m_request_address <= address_t'( value_inst_o );
							n2m_request_read    <= 1'b1;
							n2m_request_write   <= 1'b0;
							out_data_swap       <= '{default : '0};

						end else if ( pending_evict ) begin
							if ( value_evict_o.is_flush ) begin
								state               <= WAITING_SNOOP;
							end else begin
								n2m_request_address <= address_t'( value_evict_o.address );
								n2m_request_read    <= 1'b0;
								n2m_request_write   <= 1'b1;
								out_data_swap       <= value_evict_o.cache_line;
								dequeue_evict       <= 1'b1;
								state               <= WAITING_DEQUEUE;
							end
						end else if ( pending_ldst ) begin
							state               <= WAITING_MEM;
							n2m_request_address <= address_t'( value_o.address );
							n2m_request_read    <= 1'b1;
							n2m_request_write   <= 1'b0;
							out_data_swap       <= '{default : '0};
						end else
							state               <= IDLE;
					end else
						state                      <= IDLE;
				end

				WAITING_INSTR_MEM : begin
					n2m_request_read       <= 1'b0;
					n2m_request_write      <= 1'b0;
					mc_avail_o             <= 1'b1;
					if ( m2n_response_valid ) begin
						dequeue_inst               <= 1'b1;
						mem_instr_request_valid    <= 1'b1;
						mem_instr_request_data_in  <= icache_lane_t'( m2n_response_data_swap );
						state                      <= WAITING_DEQUEUE;
					end else
						state                      <= WAITING_INSTR_MEM;
				end

				WAITING_MEM : begin
					n2m_request_read       <= 1'b0;
					n2m_request_write      <= 1'b0;
					mc_avail_o             <= 1'b1;
					if ( m2n_response_valid ) begin
						// dopo bisogna vedere se c'� un replace
						state                      <= WAITING_REPLACEMENT;
						cc_update_ldst_address     <= m2n_response_address;
						cc_update_ldst_store_value <= m2n_response_data_swap;
						cc_wakeup_thread_id        <= value_o.thread_id; // senza accorpamenti load e store
						dequeue_ldst               <= 1'b1;

					end else
						state                      <= WAITING_MEM;
				end

				WAITING_REPLACEMENT : begin
					cc_update_ldst_command <= ( !is_hit & is_all_valid )? CC_REPLACEMENT: CC_UPDATE_INFO_DATA ;// ( is_hit | !is_valid )? CC_UPDATE_INFO_DATA : CC_REPLACEMENT ;
					cc_update_ldst_valid   <= 1'b1;
					update_lru             <= 1'b1; // deve essere aggiornato quando si legge cc_update_ldst_way in modo da leggere quello ancora non aggiornato
					cc_wakeup              <= 1'b1; // deve andare con l'update valid altrimenti i privilegi ed il tag non sono aggiornati in tempo per il recycle buffer
					way_matched_reg        <= way_matched_idx;
					state                  <= IDLE;
				end

				WAITING_DEQUEUE : begin
					// si aggiunge uno stato intermedio, altrimenti la coda non riesce a svuotarsi in tempo e viene generata una richiesta non utile
					state                  <= IDLE;
				end

				WAITING_SNOOP : begin
					// sto facendo il confronto tra le way per capire a quale chiedere il dato
					state                  <= WAITING_SNOOP_DATA;
				end

				WAITING_SNOOP_DATA : begin
					n2m_request_address    <= address_t'( value_evict_o.address );
					n2m_request_read       <= 1'b0;
					n2m_request_write      <= 1'b1;
					out_data_swap          <= ldst_snoop_data;
					dequeue_evict          <= 1'b1;
					state                  <= WAITING_DEQUEUE;
				end

			endcase
		end
	end

	//  -----------------------------------------------------------------------
	//  -- Data LRU
	//  -----------------------------------------------------------------------

	cache_lru_if #(
		.NUM_WAYS (`DCACHE_WAY ),
		.NUM_SET  (`DCACHE_SET )
	)
	u_cache_lru_if (
		.clk           (clk                          ),
		.reset         (reset                        ),
		//[1] Used to move a way to the MRU position when it has been accessed.
		.en_hit        ('0                            ),
		.set_hit       ('0                            ),
		.way_hit       ('0                            ),
		//[2] Used to request LRU to replace when filling.
		.en_update     (update_lru                   ),
		.set_update    (cc_update_ldst_address.index ),
		.way_update_lru(lru_out                      )
	);

	assign cc_update_ldst_way                 = ( is_hit )? way_matched_reg : lru_out;

endmodule