module priority_encoder_spm #(
        parameter INPUT_WIDTH   = 4,
        parameter MAX_PRIORITY  = "LSB"
    )(
        input   logic   [INPUT_WIDTH            - 1 : 0]    decode,
        output  logic   [$clog2(INPUT_WIDTH)    - 1 : 0]    encode,
        output  logic                                       valid
    );

//FIXME: Verilator non supporta la ricorsione, quindi il codice sottostante è stato inserito solo a scopo di
//simulazione. Si osservi che tale codice è ineficiente poichè l'indice i dei cicli è codificato su 32 bit in
//quanto è necessario avere un intero con segno.
//Inserire una compilazione condizionale che permette di complilare il codice commentato nel caso di sintesi
//con Vivado

    generate
        always_comb begin
            encode = 0;
            if (MAX_PRIORITY == "LSB") begin
                for (int i = INPUT_WIDTH - 1; i >= 0; i--)
                    if (decode[i] == 1)
                        encode = i[$clog2(INPUT_WIDTH)  - 1 : 0];
            end else begin
                for (int i = 0; i < INPUT_WIDTH; i++)
                    if (decode[i] == 1)
                        encode = i[$clog2(INPUT_WIDTH)  - 1 : 0];
            end
        end
    endgenerate

    assign valid = |decode;




// Ricorsione non supportata da verilator!
/*generate
 if (MAX_PRIORITY == "LSB")
 begin
 if (INPUT_WIDTH == 2)
 begin
 assign valid = |decode;
 assign encode = !decode[0];
 end
 else if (INPUT_WIDTH & (INPUT_WIDTH-1))
 priority_encoder #(1<<$clog2(INPUT_WIDTH), "LSB") priority_encoder ({1<<$clog2(INPUT_WIDTH) {1'b0}} | decode, encode,valid);
 else
 begin
 wire [$clog2(INPUT_WIDTH)-2:0] encode_low;
 wire [$clog2(INPUT_WIDTH)-2:0] encode_high;
 wire valid_low, valid_high;
 priority_encoder #(INPUT_WIDTH>>1, "LSB") low(decode[INPUT_WIDTH-1:INPUT_WIDTH>>1],encode_low,valid_low);
 priority_encoder #(INPUT_WIDTH>>1, "LSB") high(decode[(INPUT_WIDTH>>1)-1:0],encode_high,valid_high);

 assign valid = valid_low | valid_high;
 assign encode = valid_high ? {1'b0,encode_high} : {1'b1,encode_low};
 end
 end
 else
 begin
 if (INPUT_WIDTH == 2)
 begin
 assign valid = |decode;
 assign encode = decode[1];
 end
 else if (INPUT_WIDTH & (INPUT_WIDTH-1))
 priority_encoder #(1<<$clog2(INPUT_WIDTH), "MSB") priority_encoder ({1<<$clog2(INPUT_WIDTH) {1'b0}} | decode, encode,valid);
 else
 begin
 wire [$clog2(INPUT_WIDTH)-2:0] encode_low;
 wire [$clog2(INPUT_WIDTH)-2:0] encode_high;
 wire valid_low, valid_high;
 priority_encoder #(INPUT_WIDTH>>1, "MSB") low(decode[(INPUT_WIDTH>>1)-1:0],encode_low,valid_low);
 priority_encoder #(INPUT_WIDTH>>1, "MSB") high(decode[INPUT_WIDTH-1:INPUT_WIDTH>>1],encode_high,valid_high);
 assign valid = valid_low | valid_high;
 assign encode = valid_high ? {1'b1,encode_high} : {1'b0,encode_low};
 end
 end
 endgenerate*/

endmodule
