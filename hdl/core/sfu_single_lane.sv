`include "nuplus_define.sv"

module sfu_single_lane (
    	input  scal_reg_size_t				 op1,
    	input  scal_reg_size_t				 op2,
    	input  opcode_t	     				 op_code,
		output scal_reg_size_t				 result
	);

	scal_reg_size_t                    			 sbox_result;

	genvar i;
	generate
	   for(i = 0; i < `BYTE_PER_REGISTER; i++) begin
	       aesbox sbox(
	           .addr(  op1[8*i+:8] ),
	           .rdata( sbox_result[8*i+:8])
	       );
	   end
	endgenerate

	always_comb begin
        
		case ( op_code )
			
			SBOX, AESENC, AESENCLAST    : result = sbox_result;
			
			default : result = op1;
		endcase
	end

endmodule