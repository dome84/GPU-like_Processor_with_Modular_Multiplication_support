`include "nuplus_define.sv"

`define SCALAR_GAP     (`REGISTER_SIZE / `HW_LANE)


module mac_pipe_old #(
		parameter TILE_ID = 0,
		parameter CORE_ID = 0 )
	(
		input                        clk,
		input                        reset,

		// From Operand Fetch
		input                        opf_valid,
		input  instruction_decoded_t opf_inst_scheduled,
		input  vec_reg_size_t        opf_fetched_op0,
		input  vec_reg_size_t        opf_fetched_op1,
		input  hw_lane_mask_t        opf_hw_lane_mask,

		// To Writeback
		output logic                 mac_valid,
		output instruction_decoded_t mac_inst_scheduled,
		output vec_reg_size_t        mac_result,
		output hw_lane_mask_t        mac_hw_lane_mask
	);


	logic           is_mac_instr;
	vec_reg_size_t  vec_result;
	
	logic           tmp_valid;
	instruction_decoded_t tmp_inst_scheduled;
    hw_lane_mask_t        tmp_hw_lane_mask;
	

	assign is_mac_instr = opf_inst_scheduled.pipe_sel == PIPE_MAC;
	

	genvar          i;
	generate
		for ( i = 0; i < `HW_LANE; i ++ ) begin
		    logic [2*`REGISTER_SIZE - 1:0] tmp_result;
		    logic ce, multiply, loadacc;
		    
		    always_comb begin
		      case(opf_inst_scheduled.op_code)
		          GETACCL,
		              GETACCH: ce = 1'b0;
		          default: ce = opf_valid &  is_mac_instr;
		      endcase
		    end
		    
		    assign multiply = (opf_inst_scheduled.op_code == MAC)? 1'b0 : 1'b1; 
		    assign loadacc =  (opf_inst_scheduled.op_code == LOADACC)? 1'b1 : 1'b0;
		    
            mac mac_lane(
                .clk( clk                   ),
                .reset( reset               ),
                .ce ( ce                    ),
                .multiply   (multiply       ),
                .loadacc    ( loadacc       ),
                .a  ( opf_fetched_op0[i]    ),
                .b  ( opf_fetched_op1[i]    ),
                .q  ( tmp_result            ),
                .index  ( opf_inst_scheduled.thread_id)
            );
            
            assign vec_result[i] = (tmp_inst_scheduled.op_code == GETACCH || tmp_inst_scheduled.op_code == MULHI)? 
                                tmp_result[2*`REGISTER_SIZE - 1: `REGISTER_SIZE] : tmp_result[`REGISTER_SIZE-1:0];
		    
		end

	endgenerate

	dummypipe 
    #(.WIDTH(1), .DEPTH(5))
        valid_pipe
    (.clk(clk), .en(1'b1), .datain(opf_valid &  is_mac_instr), .q(tmp_valid));

	always_ff @ ( posedge clk) begin
		if ( reset )
			mac_valid <= 1'b0;
		else
			mac_valid <= tmp_valid;
	end
	
	
	dummypipe 
        #(.WIDTH($bits(opf_inst_scheduled)), .DEPTH(5))
            inst_pipe
        (.clk(clk), .en(1'b1), .datain(opf_inst_scheduled), .q(tmp_inst_scheduled));
        
   	dummypipe 
        #(.WIDTH($bits(opf_hw_lane_mask)), .DEPTH(5))
            mask_pipe
        (.clk(clk), .en(1'b1), .datain(opf_hw_lane_mask), .q(tmp_hw_lane_mask));

	always_ff @ ( posedge clk ) begin
		mac_inst_scheduled <= tmp_inst_scheduled;
		mac_hw_lane_mask   <= tmp_hw_lane_mask;

	    mac_result <= vec_result;
	end

endmodule