`include "nuplus_define.sv"
`include "user_define.sv"

/*
 * Rollback Handler restores PCs and scoreboards of the threads that issued a rollback.
 * In case of jump or trap, the Brach module in the Execution pipeline issues a rollback
 * request to this stage, and passes to the Rollback Handler the thread ID that issued
 * the rollback, the old scoreboard and the PC to restore. Furthermore, the Rollback Handler
 * flushes all issued requests from the thread still in the pipeline.
 */

module rollback_handler(
		input                                       clk,
		input                                       reset,
		input 										enable,

		input                                       is_instruction_valid,
		input  thread_id_t                          is_thread_id,
		input  scoreboard_t                         is_destination_mask,  // TODO: caso SMT devono essere `THREAD_NUMB

		input  scoreboard_t                         bc_scoreboard,
		input  logic                                bc_rollback_enable,
		input  logic                                bc_rollback_valid,
		input  address_t                            bc_rollback_pc,
		input  thread_id_t                          bc_rollback_thread_id,
		
		output address_t     [`THREAD_NUMB - 1 : 0] rollback_pc_value,
		output thread_mask_t                        rollback_valid,
		output scoreboard_t  [`THREAD_NUMB - 1 : 0] rollback_clear_bitmap
	);


	scoreboard_t [`THREAD_NUMB - 1 : 0] clear_bitmap;

	genvar                              thread_id;
	generate
		for ( thread_id = 0; thread_id < `THREAD_NUMB; thread_id++ ) begin : GEN_HANDLER
			scoreboard_t scoreboard_clear_int;
			scoreboard_t scoreboard_set_issue;
			scoreboard_t scoreboard_temp;

			assign scoreboard_clear_int             = ( bc_rollback_valid && bc_rollback_thread_id == thread_id ) ? bc_scoreboard : {$bits( scoreboard_t ){1'b1}};
			assign scoreboard_set_issue             = ( is_instruction_valid && is_thread_id == thread_id ) ? is_destination_mask : scoreboard_t'( 0 );
			assign scoreboard_temp                  = clear_bitmap[thread_id] & ~( scoreboard_clear_int & {`SCOREBOARD_LENGTH{bc_rollback_valid}} )
				| ( scoreboard_set_issue & {`SCOREBOARD_LENGTH{is_instruction_valid}} );
			assign rollback_clear_bitmap[thread_id] = scoreboard_temp;

			always_comb begin
				if ( bc_rollback_thread_id == thread_id ) begin
					rollback_valid[thread_id]    = bc_rollback_enable;
					rollback_pc_value[thread_id] = bc_rollback_pc;
				end else begin
					rollback_valid[thread_id]    = 1'b0;
					rollback_pc_value[thread_id] = 0;
				end
			end


//			always_ff @ ( posedge clk, posedge reset )
//				if ( reset )
//					clear_bitmap[thread_id] <= scoreboard_t'( 1'b0 );
//				else
//					if ( rollback_valid[thread_id] )
//						clear_bitmap[thread_id] <= scoreboard_t'( 1'b0 );
//					else
//						clear_bitmap[thread_id] <= scoreboard_temp;

			always_ff @ ( posedge clk, posedge reset )
				if ( reset )
					clear_bitmap[thread_id] <= scoreboard_t'( 1'b0 );
				else if ( enable ) begin
				 	if ( rollback_valid[thread_id] )
						clear_bitmap[thread_id] <= scoreboard_t'( 1'b0 );
					else
						clear_bitmap[thread_id] <= scoreboard_temp;
				 end
		end
	endgenerate

endmodule