`include "nuplus_define.sv"
`include "user_define.sv"

/*
 * Decoder module decodes fetched instruction from instruction_fetch_stage and fills all
 * instruction_decoded_t field. This module produces the control signals for the datapath
 * directly from the instruction bits. Output dec_instr helps execution and control module
 * to manage the issued instruction.
 *
 * Instruction type are:
 *
 *      - RR (Register to Register) has a destination register and two source registers.
 *      - RI (Register Immediate) has a destination register and one source registers and an immediate
 *          encoded in the instruction word.
 *      - MVI (Move Immediate) has a destination register and a 16 bit instruction encoded immediate.
 *      - MEM (Memory Instruction) has a destination/source field, in case of load the first register
 *          asses the destination register, otherwise in case of store the first register contains the
 *          store value. Next in both cases there is the base address and the immediate. The sum of
 *          base address and immediate will give the effective memory address.
 *      - JBA (Jump Base Address)
 *      - JRA (Jump Relative Address)
 */

module decode (
		input                                               clk,
		input                                               reset,
		input												enable,

		input                                               if_valid,
		input  thread_id_t                                  if_thread_selected_id,
		input  scal_reg_size_t                              if_pc_scheduled,
		input  instruction_t                                if_inst_scheduled,

		input                        [`THREAD_NUMB - 1 : 0] rollback_valid,

		output logic                                        dec_valid,
		output instruction_decoded_t                        dec_instr
	) ;

	instruction_decoded_t  instruction_decoded_next;

	logic                  is_rollback;

	assign is_rollback = rollback_valid[if_thread_selected_id];

	RR_instruction_body_t  RR_instruction_body ;
	RI_instruction_body_t  RI_instruction_body ;
	MVI_instruction_body_t MVI_instruction_body;
	MEM_instruction_body_t MEM_instruction_body;
	JBA_instruction_body_t JBA_instruction_body;
	JRA_instruction_body_t JRA_instruction_body;
	CTR_instruction_body_t CTR_instruction_body;

	always_comb begin
		RR_instruction_body                = if_inst_scheduled.body.RR_body;
		RI_instruction_body                = if_inst_scheduled.body.RI_body;
		MVI_instruction_body               = if_inst_scheduled.body.MVI_body;
		MEM_instruction_body               = if_inst_scheduled.body.MEM_body;
		JBA_instruction_body               = if_inst_scheduled.body.JBA_body;
		JRA_instruction_body               = if_inst_scheduled.body.JRA_body;
		CTR_instruction_body               = if_inst_scheduled.body.CTR_body;

		instruction_decoded_next           = instruction_decoded_t' ( 1'b0 ) ;
		instruction_decoded_next.pc        = if_pc_scheduled;
		instruction_decoded_next.thread_id = if_thread_selected_id;
		instruction_decoded_next.is_valid  = 1'b0;
		instruction_decoded_next.is_lookup = 0;
        instruction_decoded_next.is_op0_forward = 1'b0;
        instruction_decoded_next.is_op1_forward = 1'b0;
        instruction_decoded_next.has_2_results = 1'b0;
        instruction_decoded_next.has_3_sources = 1'b0;

		casez ( if_inst_scheduled.opcode )
			// RR
			8'b00_?????? : begin
				instruction_decoded_next.mask_enable              = RR_instruction_body.mask;
				instruction_decoded_next.is_valid                 = 1'b1;
				instruction_decoded_next.op_code                  = alu_op_t' ( if_inst_scheduled.opcode[5 : 0] ) ;

				instruction_decoded_next.source0                  = RR_instruction_body.source0;
				instruction_decoded_next.source1                  = RR_instruction_body.source1;
				instruction_decoded_next.destination              = RR_instruction_body.destination;
				
				case(instruction_decoded_next.op_code)
                    STOH1,
                    STOH2: begin
                            instruction_decoded_next.has_source0     = 1'b0; // Sempre alti, la discriminazione la facciamo all'interno (NO, perch� cos� abbiamo sempre dipendenze)
                            instruction_decoded_next.has_source1     = 1'b0; // della pipe intera che ignorer�  l'operando che non serve
                            end
                    default: begin
                                instruction_decoded_next.has_source0     = 1'b1; // Sempre alti, la discriminazione la facciamo all'interno (NO, perch� cos� abbiamo sempre dipendenze)
                                instruction_decoded_next.has_source1     = 1'b1; // della pipe intera che ignorer�  l'operando che non serve
                            end
                endcase
                
                case(instruction_decoded_next.op_code)
                    LOADACC,
                    LOADM,
                    //LOADB,
                    LOADMP,
                    LOADH,
                    INCBIND: instruction_decoded_next.has_destination = 1'b0;
                    default: instruction_decoded_next.has_destination = 1'b1;
                endcase
                                
				//instruction_decoded_next.has_source0              = 1'b1; // Sempre alti, la discriminazione la facciamo all'interno
				//instruction_decoded_next.has_source1              = 1'b1; // della pipe intera che ignorer� l'operando che non serve
				//instruction_decoded_next.has_destination          = 1'b1;

				instruction_decoded_next.is_source0_vectorial     = RR_instruction_body.register_selection[0]; // 
				instruction_decoded_next.is_source1_vectorial     = RR_instruction_body.register_selection[1]; // Nel caso della specifica istruzioni questi bit possono essere ignorati
				instruction_decoded_next.is_destination_vectorial = RR_instruction_body.register_selection[2];

				instruction_decoded_next.immediate                = 0;
				instruction_decoded_next.is_source1_immediate     = 1'b0;



				instruction_decoded_next.is_memory_access         = 1'b0;

				if ( if_inst_scheduled.opcode.alu_opcode < DEILVL && if_inst_scheduled.opcode.alu_opcode >= MULHI ) begin
					instruction_decoded_next.pipe_sel             = PIPE_MAC;
					instruction_decoded_next.is_int               = 1'b1;
					instruction_decoded_next.is_fp                = 1'b0;
				end else if ( if_inst_scheduled.opcode.alu_opcode <= SEXT32) begin
					instruction_decoded_next.pipe_sel             = PIPE_INT;
					instruction_decoded_next.is_int               = 1'b1;
					instruction_decoded_next.is_fp                = 1'b0;
				end else begin
					instruction_decoded_next.pipe_sel             = PIPE_FP;
					instruction_decoded_next.is_int               = 1'b0;
					instruction_decoded_next.is_fp                = 1'b1;
				end

                instruction_decoded_next.has_2_results          = (RR_instruction_body.unused[0] & (instruction_decoded_next.pipe_sel == PIPE_INT)) ||
                                                            (instruction_decoded_next.op_code == STOH2);
                instruction_decoded_next.has_3_sources          = (instruction_decoded_next.op_code == ADDC || instruction_decoded_next.op_code == SUBB ||
                                        instruction_decoded_next.op_code == MONT1 || instruction_decoded_next.op_code == MONT2 || instruction_decoded_next.op_code == LOADH);
                              
				instruction_decoded_next.is_load                  = 1'b0;
				instruction_decoded_next.is_movei                 = 1'b0;
				if ( if_inst_scheduled.body.RR_body.destination == `PC_REG )
					instruction_decoded_next.is_branch            = 1'b1;
				else
					instruction_decoded_next.is_branch            = 1'b0;
				instruction_decoded_next.is_conditional           = 1'b0;
				instruction_decoded_next.is_control               = 1'b0;

                case(instruction_decoded_next.op_code) //(E se la pipe non � intera?
                    SHUFFLE,
                    INTLVH,
                    INTLVL,
                    DEILVL,
                    DEILVH : instruction_decoded_next.is_shuffle = 1'b1;
                    default : instruction_decoded_next.is_shuffle = 1'b0;
                endcase 

			end

			// RI
			8'b010_????? : begin
				instruction_decoded_next.mask_enable              = RI_instruction_body.mask;
				instruction_decoded_next.is_valid                 = 1'b1;
				instruction_decoded_next.op_code                  = alu_op_t' ( if_inst_scheduled.opcode[4 : 0] ) ;

				instruction_decoded_next.source0                  = RI_instruction_body.source0;
				instruction_decoded_next.source1                  = 0;
				instruction_decoded_next.destination              = RI_instruction_body.destination;

				instruction_decoded_next.has_source0              = 1'b1;
				instruction_decoded_next.has_source1              = 1'b0;
				instruction_decoded_next.has_destination          = ~ (instruction_decoded_next.op_code == LOADACC || instruction_decoded_next.op_code == MONT1);

				instruction_decoded_next.is_destination_vectorial = RI_instruction_body.register_selection[1];
				instruction_decoded_next.is_source0_vectorial     = RI_instruction_body.register_selection[0];
				instruction_decoded_next.is_source1_vectorial     = 1'b0;
	
                instruction_decoded_next.has_2_results          = 0;
                instruction_decoded_next.has_3_sources          = (instruction_decoded_next.op_code == ADDC || instruction_decoded_next.op_code ==SUBB);

				instruction_decoded_next.immediate                = {{23{RI_instruction_body.immediate[8]}}, RI_instruction_body.immediate};
				instruction_decoded_next.is_source1_immediate     = 1'b1;

				instruction_decoded_next.pipe_sel 				  = ((instruction_decoded_next.op_code>=MULHI)?PIPE_MAC:PIPE_INT); // !!! DA CONTROLLARE

				instruction_decoded_next.is_memory_access         = 1'b0;
				instruction_decoded_next.is_int                   = 1'b1;
				instruction_decoded_next.is_fp                    = 1'b0;
				instruction_decoded_next.is_load                  = 1'b0;
				instruction_decoded_next.is_movei                 = 1'b0;
				instruction_decoded_next.is_branch                = 1'b0;
				instruction_decoded_next.is_conditional           = 1'b0;
				instruction_decoded_next.is_control               = 1'b0;
				
                case(instruction_decoded_next.op_code)
                    SHUFFLE,
                    INTLVH,
                    INTLVL,
                    DEILVL,
                    DEILVH : instruction_decoded_next.is_shuffle = 1'b1;
                    default : instruction_decoded_next.is_shuffle = 1'b0;
                endcase 

			end

			//MVI
			8'b01100_??? : begin
				instruction_decoded_next.mask_enable              = MVI_instruction_body.mask;
				instruction_decoded_next.is_valid                 = 1'b1;
				instruction_decoded_next.op_code                  = alu_op_t' ( if_inst_scheduled.opcode[2 : 0] ) ;

				instruction_decoded_next.source0                  = 0;
				instruction_decoded_next.source1                  = 0;
				instruction_decoded_next.destination              = MVI_instruction_body.destination;

				instruction_decoded_next.has_source0              = 1'b0;
				instruction_decoded_next.has_source1              = 1'b0;
				instruction_decoded_next.has_destination          = 1'b1;

				instruction_decoded_next.is_destination_vectorial = MVI_instruction_body.register_selection;
				instruction_decoded_next.is_source0_vectorial     = 1'b0;
				instruction_decoded_next.is_source1_vectorial     = 1'b0;
				
                instruction_decoded_next.has_2_results          = 0;
                instruction_decoded_next.has_3_sources          = 0;

				instruction_decoded_next.immediate                = {{16{MVI_instruction_body.immediate[15]}}, MVI_instruction_body.immediate};
				instruction_decoded_next.is_source1_immediate     = 1'b1;

				instruction_decoded_next.pipe_sel                 = PIPE_INT;

				instruction_decoded_next.is_memory_access         = 1'b0;
				instruction_decoded_next.is_int                   = 1'b0;
				instruction_decoded_next.is_fp                    = 1'b0;
				instruction_decoded_next.is_load                  = 1'b0;
				instruction_decoded_next.is_movei                 = 1'b1;
				instruction_decoded_next.is_branch                = 1'b0;
				instruction_decoded_next.is_conditional           = 1'b0;
				instruction_decoded_next.is_control               = 1'b0;

			end

			//MEM
			8'b10_?????? : begin
				instruction_decoded_next.mask_enable              = MEM_instruction_body.mask;
				instruction_decoded_next.is_valid                 = 1'b1;
				instruction_decoded_next.op_code                  = memory_op_t' ( if_inst_scheduled.opcode[5 : 0] ) ;

				instruction_decoded_next.source0                  = MEM_instruction_body.base_register;
				instruction_decoded_next.source1                  = MEM_instruction_body.src_dest_register;
				instruction_decoded_next.destination              = MEM_instruction_body.src_dest_register;

				instruction_decoded_next.has_source0              = 1'b1;
				instruction_decoded_next.has_source1              = 1'b1;
				instruction_decoded_next.has_destination          = !if_inst_scheduled.opcode[5];
				
				instruction_decoded_next.has_2_results          = 0;
                instruction_decoded_next.has_3_sources          = 0; 

				instruction_decoded_next.is_source0_vectorial     = 1'b0; // todo da settare in caso di gather/scatter operation
				instruction_decoded_next.is_source1_vectorial
																  = ( ( if_inst_scheduled.opcode[5 : 0] >= LOAD_V_8 & if_inst_scheduled.opcode[5 : 0] <= LOAD_V_32_U )
					| ( if_inst_scheduled.opcode[5 : 0] >= STORE_V_8 & if_inst_scheduled.opcode[5 : 0] <= STORE_V_64 ) ) ; // TODO AGGIORNARE VALORI
				instruction_decoded_next.is_destination_vectorial
																  = ( ( if_inst_scheduled.opcode[5 : 0] >= LOAD_V_8 & if_inst_scheduled.opcode[5 : 0] <= LOAD_V_32_U )
					| ( if_inst_scheduled.opcode[5 : 0] >= STORE_V_8 & if_inst_scheduled.opcode[5 : 0] <= STORE_V_64 ) ) ; // TODO AGGIRONARE VALORI

				instruction_decoded_next.immediate                = {{23{MEM_instruction_body.offset[8]}}, MEM_instruction_body.offset}; //immediate_size_t'(signed'(MEM_instruction_body.offset));
				// If a store occurs is_source1_immediate is not set, otherwise opf_fecthed_op1 holds the immediate value
				// and not the store value has should be.

				if ( if_inst_scheduled.opcode == 8'b10_010101 ) begin
					instruction_decoded_next.pipe_sel             = PIPE_CR;
					instruction_decoded_next.is_memory_access     = 1'b0;
					instruction_decoded_next.is_int               = 1'b1;
					instruction_decoded_next.is_source1_immediate = 1'b1;
				end else if ( MEM_instruction_body.shared ) begin
					instruction_decoded_next.pipe_sel             = PIPE_SPM;
					instruction_decoded_next.is_memory_access     = 1'b1;
					instruction_decoded_next.is_int               = 1'b0;
					instruction_decoded_next.is_source1_immediate = 1'b0;
                    if(if_inst_scheduled.opcode == 8'b10_011000 ) begin
                      instruction_decoded_next.is_lookup = 1;
                      instruction_decoded_next.is_source0_vectorial = 1'b1;
                      instruction_decoded_next.is_destination_vectorial = 1'b1;
                    end
                                      
				end else begin
					instruction_decoded_next.pipe_sel             = PIPE_MEM;
					instruction_decoded_next.is_memory_access     = 1'b1;
					instruction_decoded_next.is_int               = 1'b0;
					instruction_decoded_next.is_source1_immediate = 1'b0;
				end

				instruction_decoded_next.is_fp                    = 1'b0;
				instruction_decoded_next.is_load                  = !if_inst_scheduled.opcode[5];
				instruction_decoded_next.is_movei                 = 1'b0;
				instruction_decoded_next.is_branch                = 1'b0;
				instruction_decoded_next.is_conditional           = 1'b0;
				instruction_decoded_next.is_control               = 1'b0;

			end

			//JBA
			8'b01110_??? : begin
				instruction_decoded_next.mask_enable              = 1'b0;
				instruction_decoded_next.is_valid                 = 1'b1;
				instruction_decoded_next.op_code                  = `OP_CODE_WIDTH' ( if_inst_scheduled.opcode[2 : 0] ) ;

				// In case of conditional branch, source0 stores the conditional value to satisfies,
				// in case of unconditional branch it contains the jump base address
				instruction_decoded_next.source0                  = JBA_instruction_body.dest;

				instruction_decoded_next.has_source0              = 1'b1;
				instruction_decoded_next.has_source1              = 1'b1;


				if ( if_inst_scheduled.opcode[2 : 0] == JMPSR ) begin
					instruction_decoded_next.has_destination      = 1'b1;
					instruction_decoded_next.is_int               = 1'b1;
					instruction_decoded_next.is_source1_immediate = 1'b0;
					instruction_decoded_next.destination          = `RA_REG;
					instruction_decoded_next.source1              = `PC_REG;
				end else if ( if_inst_scheduled.opcode[2 : 0] == JRET ) begin
					instruction_decoded_next.has_destination      = 1'b0;
					instruction_decoded_next.has_source0          = 1'b1;
					instruction_decoded_next.has_source1          = 1'b0;
					instruction_decoded_next.is_int               = 1'b0;
					instruction_decoded_next.is_source1_immediate = 1'b0;
					instruction_decoded_next.destination          = 0;
					instruction_decoded_next.source0              = `RA_REG;
				end else begin
					instruction_decoded_next.has_destination      = 1'b0;
					instruction_decoded_next.is_int               = 1'b0;
					instruction_decoded_next.is_source1_immediate = 1'b1;
					instruction_decoded_next.destination          = 0;
					instruction_decoded_next.source1              = JBA_instruction_body.dest;

				end

				instruction_decoded_next.is_source0_vectorial     = 1'b0;
				instruction_decoded_next.is_source1_vectorial     = 1'b0;
				instruction_decoded_next.is_destination_vectorial = 1'b0;
				
                instruction_decoded_next.has_2_results          = 0;
                instruction_decoded_next.has_3_sources          = 0;

				instruction_decoded_next.immediate                = {{14{JBA_instruction_body.immediate[17]}}, JBA_instruction_body.immediate};

				instruction_decoded_next.pipe_sel                 = PIPE_BRANCH;

				instruction_decoded_next.is_memory_access         = 1'b0;
				instruction_decoded_next.is_fp                    = 1'b0;
				instruction_decoded_next.is_load                  = 1'b0;
				instruction_decoded_next.is_movei                 = 1'b0;
				instruction_decoded_next.is_branch                = 1'b1;
				instruction_decoded_next.is_conditional           = if_inst_scheduled.opcode[2];
				instruction_decoded_next.is_control               = 1'b0;
				instruction_decoded_next.branch_type              = JBA;

			end

			//JRA
			8'b01111_??? : begin
				instruction_decoded_next.mask_enable              = 1'b0;
				instruction_decoded_next.is_valid                 = 1'b1;
				instruction_decoded_next.op_code                  = `OP_CODE_WIDTH' ( if_inst_scheduled.opcode[2 : 0] ) ;

				instruction_decoded_next.source1                  = 0;
				instruction_decoded_next.destination              = 0;

				if ( if_inst_scheduled.opcode[2 : 0] == JMPSR ) begin
					instruction_decoded_next.has_destination      = 1'b1;
					instruction_decoded_next.has_source0          = 1'b1;
					instruction_decoded_next.has_source1          = 1'b0;
					instruction_decoded_next.is_int               = 1'b1;
					instruction_decoded_next.is_source1_immediate = 1'b1;
					instruction_decoded_next.destination          = `RA_REG;
					instruction_decoded_next.source0              = `PC_REG;
				end else if ( if_inst_scheduled.opcode[2 : 0] == JRET ) begin
					instruction_decoded_next.has_destination      = 1'b0;
					instruction_decoded_next.has_source0          = 1'b1;
					instruction_decoded_next.has_source1          = 1'b0;
					instruction_decoded_next.is_int               = 1'b0;
					instruction_decoded_next.is_source1_immediate = 1'b0;
					instruction_decoded_next.destination          = 0;
					instruction_decoded_next.source0              = `RA_REG;
				end else begin
					instruction_decoded_next.has_destination      = 1'b0;
					instruction_decoded_next.has_source0          = 1'b0;
					instruction_decoded_next.has_source1          = 1'b0;
					instruction_decoded_next.is_int               = 1'b0;
					instruction_decoded_next.is_source1_immediate = 1'b1;
					instruction_decoded_next.destination          = 0;
					instruction_decoded_next.source0              = JBA_instruction_body.dest;

				end



				instruction_decoded_next.is_source0_vectorial     = 1'b0;
				instruction_decoded_next.is_source1_vectorial     = 1'b0;
				instruction_decoded_next.is_destination_vectorial = 1'b0;
				
                instruction_decoded_next.has_2_results          = 0;
                instruction_decoded_next.has_3_sources          = 0;

				instruction_decoded_next.immediate                = {{14{JRA_instruction_body.immediate[17]}}, JRA_instruction_body.immediate[17 : 0]};
				instruction_decoded_next.is_source1_immediate     = 1'b1;

				instruction_decoded_next.pipe_sel                 = PIPE_BRANCH;

				instruction_decoded_next.is_memory_access         = 1'b0;
				instruction_decoded_next.is_movei                 = 1'b0;
				instruction_decoded_next.is_fp                    = 1'b0;
				instruction_decoded_next.is_load                  = 1'b0;
				instruction_decoded_next.is_branch                = 1'b1;
				instruction_decoded_next.is_conditional           = 1'b0;
				instruction_decoded_next.is_control               = 1'b0;
				instruction_decoded_next.branch_type              = JRA;

			end

			//Control
			8'b01101_??? : begin
				instruction_decoded_next.mask_enable              = 1'b0;
				instruction_decoded_next.is_valid                 = 1'b1;
				instruction_decoded_next.op_code                  = `OP_CODE_WIDTH' ( if_inst_scheduled.opcode[2 : 0] ) ;

				instruction_decoded_next.source0                  = CTR_instruction_body.source0;
				instruction_decoded_next.source1                  = CTR_instruction_body.source1;
				instruction_decoded_next.destination              = CTR_instruction_body.source0;

				instruction_decoded_next.has_source0              = 1'b1;
				instruction_decoded_next.has_destination          = 1'b0;

				instruction_decoded_next.is_source0_vectorial     = 1'b0;
				instruction_decoded_next.is_source1_vectorial     = 1'b0;
				instruction_decoded_next.is_destination_vectorial = 1'b0;

				instruction_decoded_next.immediate                = 0;
				instruction_decoded_next.is_source1_immediate     = 1'b0;
				instruction_decoded_next.is_memory_access         = 1'b0;

				if ( if_inst_scheduled.opcode.contr_opcode[2 : 0] == FLUSH ) begin
					instruction_decoded_next.pipe_sel             = PIPE_MEM;
					instruction_decoded_next.has_source1          = 1'b0;
				end else begin
					instruction_decoded_next.pipe_sel             = PIPE_SYNC;
					instruction_decoded_next.has_source1          = 1'b1;
				end

				instruction_decoded_next.is_int                   = 1'b0;
				instruction_decoded_next.is_fp                    = 1'b0;
				instruction_decoded_next.is_load                  = 1'b0;
				instruction_decoded_next.is_movei                 = 1'b0;
				instruction_decoded_next.is_branch                = 1'b0;
				instruction_decoded_next.is_branch                = 1'b0;
				instruction_decoded_next.is_conditional           = 1'b0;
				instruction_decoded_next.is_control               = 1'b1;
			
			end
				
            // SFU
             8'b1110_???? : begin
                instruction_decoded_next.mask_enable = RR_instruction_body.mask;
                instruction_decoded_next.is_valid    = 1'b1;
                instruction_decoded_next.op_code     = sfu_op_t'( if_inst_scheduled.opcode[3 : 0] );

                instruction_decoded_next.source0     = RR_instruction_body.source0;
                instruction_decoded_next.source1     = RR_instruction_body.source1;
                instruction_decoded_next.destination = RR_instruction_body.destination;

                instruction_decoded_next.has_source0     = 1'b1; // Sempre alti, la discriminazione la facciamo all'interno
                instruction_decoded_next.has_source1     = 1'b1; // della pipe intera che ignorer�  l'operando che non serve
                instruction_decoded_next.has_destination = 1'b1;

                instruction_decoded_next.is_source0_vectorial     = RR_instruction_body.register_selection[0];
                instruction_decoded_next.is_source1_vectorial     = RR_instruction_body.register_selection[1]; // Nel caso della specifica istruzioni questi bit possono essere ignorati
                instruction_decoded_next.is_destination_vectorial = RR_instruction_body.register_selection[2];
                
                instruction_decoded_next.has_2_results          = 1'b0;
                instruction_decoded_next.has_3_sources          = 1'b0;

                instruction_decoded_next.immediate            = 0;
                instruction_decoded_next.is_source1_immediate = 1'b0;

                instruction_decoded_next.pipe_sel = PIPE_SFU;

                instruction_decoded_next.is_memory_access = 1'b0;
                instruction_decoded_next.is_int           = 1'b0;
                instruction_decoded_next.is_fp            = 1'b0;
                instruction_decoded_next.is_load          = 1'b0;
                instruction_decoded_next.is_movei         = 1'b0;
                instruction_decoded_next.is_branch        = 1'b0;
                instruction_decoded_next.is_conditional   = 1'b0;
                instruction_decoded_next.is_control       = 1'b0;
                instruction_decoded_next.is_shuffle       = 1'b0;
                instruction_decoded_next.is_sfu           = 1'b1;
                
                instruction_decoded_next.is_op0_forward   = (RR_instruction_body.source0 == {`REGISTER_INDEX_LENGTH{1'b1}});
 
			end


			default : begin

			end

		endcase
	end

//	always_ff @ ( posedge clk, posedge reset ) begin
//		if ( reset ) begin
//			dec_valid <= 1'b0;
//		end else begin
//			dec_instr <= instruction_decoded_next;
//			dec_valid <= if_valid & ~is_rollback & instruction_decoded_next.is_valid;
//		end
//	end

	always_ff @ ( posedge clk, posedge reset ) begin
		if ( reset ) begin
			dec_valid <= 1'b0;
		end else if ( enable ) begin
			dec_instr <= instruction_decoded_next;
			dec_valid <= if_valid & ~is_rollback & instruction_decoded_next.is_valid;// & ~dsu_pipe_flush[if_thread_selected_id];
		end
	end


`ifdef SIMULATION
	always_ff @ ( posedge clk )
		if ( dec_valid ) begin
			assert( dec_instr.pipe_sel != PIPE_FP )
			else $error( "[Decode]: floating point instruction decoded! Time: %t  PC: %h", $time( ), dec_instr.pc );
		end
`endif

endmodule
