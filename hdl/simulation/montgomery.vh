`include "nuplus_define.sv"
`include "user_define.sv"

`define MULT_ITER(reg0,lane) {8'h40|SHUFFLE, 6'd0, reg0, lane,       2'b01, 1'b0},/*GETLANE*/ \
                              {LOADACC, 6'd0, 6'd4, 6'd5,  2'b00, 3'b111, 1'b0}, \
                              {MAC,     6'd0, 6'd2, 6'd0, 2'b00, 3'b101, 1'b0}, \
                              {GETACCL, 6'd4, 6'd0, 6'd0 , 2'b00, 3'b111, 1'b0}, \
                              {GETACCH, 6'd5, 6'd0, 6'd0 , 2'b00, 3'b111, 1'b0}, \
                              {LOADACC, 6'd0, 6'd6, 6'd7,  2'b00, 3'b111, 1'b0}, \
                              {MAC,     6'd0, 6'd3, 6'd0, 2'b00, 3'b101, 1'b0}, \
                              {GETACCL, 6'd6, 6'd0, 6'd0 , 2'b00, 3'b111, 1'b0}, \
                              {GETACCH, 6'd7, 6'd0, 6'd0 , 2'b00, 3'b111, 1'b0}, \
                              \
                              {MULLO,   6'd2, 6'd4, 6'd14,  2'b00, 3'b001, 1'b0}, /*calc. quoziente*/  \
                              {ADDC,     6'd6, 6'd6, 6'd11,  2'b01, 3'b111, 1'b0}, \
                              {8'h40|ROTL, 6'd59, 6'd59, 9'd8,       2'b00, 1'b0}, \
                              {SHUFFLE, 6'd7, 6'd7, 6'd15, 2'b00, 3'b111, 1'b0},  \
                              {8'h40|AND, 6'd12, 6'd59, 9'd1,       2'b00, 1'b0},   \
                              {ADD,     6'd13, 6'd12, 6'd13, 2'b00, 3'b000, 1'b0}, \
                              {8'h40|AND, 6'd59, 6'd59, 9'h1FE,       2'b00, 1'b0},  \
                              {8'h40|XOR, 6'd60, 6'd60, 9'h1FF,       2'b00, 1'b0}, \
                              {ADDC,     6'd4, 6'd4, 6'd7,  2'b01, 3'b111, 1'b1}, \
                              {8'h40|XOR, 6'd60, 6'd60, 9'h1FF,       2'b00, 1'b0}, \
                              {ADDC,     6'd6, 6'd6, 6'd5,  2'b01, 3'b111, 1'b0}, \
                              {8'h40|AND, 6'd12, 6'd59, 9'h1FF,       2'b00, 1'b0},/*MOVE*/  \
                              {ADD,      6'd7, 6'd13, 6'd7,  2'b01, 3'b110, 1'b1},  \
                              {8'h40|SHUFFLE, 6'd4, 6'd7, 9'd0,       2'b01, 1'b0},/*GETLANE*/ \
                              {8'h40|AND, 6'd13, 6'd59, 9'd1,       2'b00, 1'b0}, \
                              {8'h40|AND, 6'd59, 6'd12, 9'h1FF,       2'b00, 1'b0},/*MOVE*/ \
                              {XOR,      6'd5, 6'd5, 6'd5,  2'b00, 3'b111, 1'b0}, \
                              {XOR,      6'd7, 6'd7, 6'd7,  2'b00, 3'b111, 1'b0}, \
                              {LOADACC, 6'd0, 6'd4, 6'd5,  2'b00, 3'b111, 1'b0}, \
                              {MAC,     6'd0, 6'd12, 6'd2, 2'b00, 3'b101, 1'b0}, \
                              {GETACCL, 6'd4, 6'd0, 6'd0 , 2'b00, 3'b111, 1'b0}, \
                              {GETACCH, 6'd5, 6'd0, 6'd0 , 2'b00, 3'b111, 1'b0}, \
                              {LOADACC, 6'd0, 6'd6, 6'd7,  2'b00, 3'b111, 1'b0}, \
                              {MAC,     6'd0, 6'd13, 6'd2, 2'b00, 3'b101, 1'b0}, \
                              {GETACCL, 6'd6, 6'd0, 6'd0 , 2'b00, 3'b111, 1'b0}, \
                              {GETACCH, 6'd7, 6'd0, 6'd0 , 2'b00, 3'b111, 1'b0}, \
                              {8'h40|ROTL, 6'd59, 6'd59, 9'd8,       2'b00, 1'b0}, \
                              {SHUFFLE, 6'd7, 6'd7, 6'd15, 2'b00, 3'b111, 1'b0}, \
                              {8'h40|AND, 6'd4, 6'd4, 9'h1FF,       2'b10, 1'b1},/*MOVE*/ \
                              {ADDC,     6'd4, 6'd4, 6'd7,  2'b01, 3'b111, 1'b0}, \
                              {8'h40|AND, 6'd12, 6'd59, 9'd1,       2'b00, 1'b0},   \
                              {ADD,     6'd13, 6'd12, 6'd13, 2'b00, 3'b000, 1'b0}, \
                              {8'h40|AND, 6'd59, 6'd59, 9'h1FE,       2'b00, 1'b0},  \
                              {ADDC,     6'd6, 6'd6, 6'd5,  2'b01, 3'b111, 1'b0}, \
                              {SHUFFLE, 6'd5, 6'd4, 6'd14, 2'b00, 3'b111, 1'b0},  \
                              {8'h40|AND, 6'd4, 6'd6, 9'h1FF,       2'b11, 1'b0},/*MOVE VEC*/ \
                              {8'h40|AND, 6'd6, 6'd5, 9'h1FF,       2'b11, 1'b0},/*MOVE VEC*/ \
                              {XOR,      6'd5, 6'd5, 6'd5,  2'b00, 3'b111, 1'b0}, \
                              {XOR,      6'd7, 6'd7, 6'd7,  2'b00, 3'b111, 1'b0}
                              
                                 
 instruction_t       instructions[0:480]  = {  {XOR    , 6'd4, 6'd4, 6'd4, 2'b00, 3'b111, 1'b0},
                                              {XOR    , 6'd5, 6'd5, 6'd5, 2'b00, 3'b111, 1'b0},
                                              {XOR    , 6'd6, 6'd6, 6'd6, 2'b00, 3'b111, 1'b0},
                                              {XOR    , 6'd7, 6'd7, 6'd7, 2'b00, 3'b111, 1'b0},
                                              {XOR    , 6'd8, 6'd8, 6'd8, 2'b00, 3'b111, 1'b0},
                                              {XOR    , 6'd9, 6'd9, 6'd9, 2'b00, 3'b111, 1'b0},
                                              {XOR    , 6'd10, 6'd10, 6'd10, 2'b00, 3'b111, 1'b0},
                                              {XOR    , 6'd11, 6'd11, 6'd11, 2'b00, 3'b111, 1'b0},
                                              {XOR    , 6'd59, 6'd59, 6'd59, 2'b00, 3'b000, 1'b0},
                                              {XOR    , 6'd13, 6'd13, 6'd13, 2'b00, 3'b000, 1'b0},
                                              {XOR    , 6'd12, 6'd12, 6'd12, 2'b00, 3'b000, 1'b0},
 
                                              {INTLVL, 6'd4, 6'd3, 6'd2, 2'b00, 3'b111, 1'b0},
                                              {INTLVH,  6'd3, 6'd3, 6'd2, 2'b00, 3'b111, 1'b0},
                                              {8'h40|ADD, 6'd2, 6'd4, 9'd0,       2'b11, 1'b0},
                                              
                                              {INTLVL, 6'd4, 6'd13, 6'd12, 2'b00, 3'b111, 1'b0},
                                              {INTLVH,  6'd13, 6'd13, 6'd12, 2'b00, 3'b111, 1'b0},
                                              {8'h40|ADD, 6'd12, 6'd4, 9'd0,       2'b11, 1'b0},
                                              
                                              {XOR    , 6'd4, 6'd4, 6'd4, 2'b00, 3'b111, 1'b0},
                                              
                                              {8'h60|MOVEI, 6'd60, 16'd1,       1'b0, 1'b0},
                                              
                                              {8'h60|MOVEI, 6'd15, 16'd3,       1'b1, 1'b1},
                                              {8'h60|MOVEI, 6'd14, 16'd1,       1'b1, 1'b1},
                                              {8'h40|SHL, 6'd60, 6'd60, 9'd1,    2'b00, 1'b0},
                                              {8'h60|MOVEI, 6'd15, 16'd0,       1'b1, 1'b1},
                                              {8'h60|MOVEI, 6'd14, 16'd2,       1'b1, 1'b1},
                                              {8'h40|SHL, 6'd60, 6'd60, 9'd1,    2'b00, 1'b0},
                                              {8'h60|MOVEI, 6'd15, 16'd1,       1'b1, 1'b1},
                                              {8'h60|MOVEI, 6'd14, 16'd3,       1'b1, 1'b1},
                                              {8'h40|SHL, 6'd60, 6'd60, 9'd1,    2'b00, 1'b0},
                                              {8'h60|MOVEI, 6'd15, 16'd2,       1'b1, 1'b1},
                                              {8'h60|MOVEI, 6'd14, 16'd0,       1'b1, 1'b1},
                                              {8'h40|SHR, 6'd60, 6'd60, 9'd3,    2'b00, 1'b0},
                                              
                                              //Multipliction: 1st iteration
                                              `MULT_ITER(6'd1,9'd0),
                                              `MULT_ITER(6'd1,9'd1),
                                              `MULT_ITER(6'd1,9'd2),
                                              `MULT_ITER(6'd1,9'd3),
                                              `MULT_ITER(6'd0,9'd0),
                                              `MULT_ITER(6'd0,9'd1),
                                              `MULT_ITER(6'd0,9'd2),
                                              `MULT_ITER(6'd0,9'd3),
                                              
                                              {XOR    , 6'd8, 6'd8, 6'd8, 2'b00, 3'b111, 1'b0},
                                              {XOR    , 6'd9, 6'd9, 6'd9, 2'b00, 3'b111, 1'b0},
                                              /* Carry recovery */
                                              {XOR    , 6'd0, 6'd0, 6'd0, 2'b00, 3'b000, 1'b0},
                                              //{8'h40|SHUFFLE, 6'd1, 6'd4, 9'd0,       2'b01, 1'b0},/*GETLANE*/ 
                                              {8'h40|AND, 6'd8, 6'd4, 9'h1FF,       2'b11, 1'b1},/*MOVE*/
                                              {SHUFFLE, 6'd8, 6'd8, 6'd14, 2'b00, 3'b111, 1'b0},
                                              
                                              {ADDC,     6'd6, 6'd6, 6'd5,  2'b01, 3'b111, 1'b0},
                                              //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                              //8'h40|SHUFFLE, 6'd1, 6'd6, 9'd0,       2'b01, 1'b0},/*GETLANE*/
                                              {8'h40|AND, 6'd8, 6'd6, 9'h1FF,       2'b11, 1'b1},/*MOVE*/
                                              {SHUFFLE, 6'd8, 6'd8, 6'd14, 2'b00, 3'b111, 1'b0},
                                              
                                              {XOR    , 6'd4, 6'd4, 6'd4, 2'b00, 3'b111, 1'b1},
                                              {SHUFFLE, 6'd4, 6'd4, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {ADDC,     6'd4, 6'd4, 6'd5,  2'b01, 3'b111, 1'b0},
                                              //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                              //{8'h40|SHUFFLE, 6'd1, 6'd4, 9'd0,       2'b01, 1'b0},/*GETLANE*/
                                              {8'h40|AND, 6'd8, 6'd4, 9'h1FF,       2'b11, 1'b1},/*MOVE*/
                                              {SHUFFLE, 6'd8, 6'd8, 6'd14, 2'b00, 3'b111, 1'b0},
                                              
                                              {XOR    , 6'd6, 6'd6, 6'd6, 2'b00, 3'b111, 1'b1},
                                              {SHUFFLE, 6'd6, 6'd6, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {ADDC,     6'd6, 6'd6, 6'd5,  2'b01, 3'b111, 1'b0},
                                              //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                              //{8'h40|SHUFFLE, 6'd1, 6'd6, 9'd0,       2'b01, 1'b0},/*GETLANE*/
                                              {8'h40|AND, 6'd8, 6'd6, 9'h1FF,       2'b11, 1'b1},/*MOVE*/
                                              {SHUFFLE, 6'd8, 6'd8, 6'd14, 2'b00, 3'b111, 1'b0},
                                              
                                              {XOR    , 6'd4, 6'd4, 6'd4, 2'b00, 3'b111, 1'b1},
                                              {SHUFFLE, 6'd4, 6'd4, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {ADDC,     6'd4, 6'd4, 6'd5,  2'b01, 3'b111, 1'b0},
                                              //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                              //{8'h40|SHUFFLE, 6'd1, 6'd4, 9'd0,       2'b01, 1'b0},/*GETLANE*/
                                              {8'h40|AND, 6'd9, 6'd4, 9'h1FF,       2'b11, 1'b1},/*MOVE*/
                                              {SHUFFLE, 6'd9, 6'd9, 6'd14, 2'b00, 3'b111, 1'b0},
                                              
                                              {XOR    , 6'd6, 6'd6, 6'd6, 2'b00, 3'b111, 1'b1},
                                              {SHUFFLE, 6'd6, 6'd6, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {ADDC,     6'd6, 6'd6, 6'd5,  2'b01, 3'b111, 1'b0},
                                              //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                              //{8'h40|SHUFFLE, 6'd1, 6'd6, 9'd0,       2'b01, 1'b0},/*GETLANE*/
                                              {8'h40|AND, 6'd9, 6'd6, 9'h1FF,       2'b11, 1'b1},/*MOVE*/
                                              {SHUFFLE, 6'd9, 6'd9, 6'd14, 2'b00, 3'b111, 1'b0},
                                              
                                              {XOR    , 6'd4, 6'd4, 6'd4, 2'b00, 3'b111, 1'b1},
                                              {SHUFFLE, 6'd4, 6'd4, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {ADDC,     6'd4, 6'd4, 6'd5,  2'b01, 3'b111, 1'b0},
                                              //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                              //{8'h40|SHUFFLE, 6'd1, 6'd4, 9'd0,       2'b01, 1'b0},/*GETLANE*/
                                              {8'h40|AND, 6'd9, 6'd4, 9'h1FF,       2'b11, 1'b1},/*MOVE*/
                                              {SHUFFLE, 6'd9, 6'd9, 6'd14, 2'b00, 3'b111, 1'b0},
                                                
                                              {XOR    , 6'd6, 6'd6, 6'd6, 2'b00, 3'b111, 1'b1},
                                              {SHUFFLE, 6'd6, 6'd6, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {ADDC,     6'd6, 6'd6, 6'd5,  2'b01, 3'b111, 1'b0},
                                              //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                              //{8'h40|SHUFFLE, 6'd1, 6'd6, 9'd0,       2'b01, 1'b0},/*GETLANE*/
                                              {8'h40|AND, 6'd9, 6'd6, 9'h1FF,       2'b11, 1'b1},/*MOVE*/
                                              {SHUFFLE, 6'd9, 6'd9, 6'd14, 2'b00, 3'b111, 1'b0},
                                              
                                              {SHUFFLE, 6'd4, 6'd4, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {ADD,     6'd4, 6'd13, 6'd4,  2'b00, 3'b110, 1'b1},
                                              
                                              {8'h40|AND, 6'd10, 6'd8, 9'h1FF,       2'b11, 1'b0},/*MOVE*/
                                              {8'h40|AND, 6'd11, 6'd9, 9'h1FF,       2'b11, 1'b0},/*MOVE*/
                                              
                                              /*Subtraction*/
                                              {XOR    , 6'd59, 6'd59, 6'd59, 2'b00, 3'b000, 1'b0},
                                              
                                              {SUBB,     6'd8, 6'd8, 6'd12,  2'b01, 3'b111, 1'b1},
                                              //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                              //{8'h40|SHUFFLE, 6'd1, 6'd8, 9'd0,       2'b01, 1'b0},/*GETLANE*/
                                              
                                              {SHUFFLE, 6'd8, 6'd8, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {SUBB,     6'd8, 6'd8, 6'd13,  2'b01, 3'b111, 1'b1},
                                              //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                              //{8'h40|SHUFFLE, 6'd1, 6'd8, 9'd0,       2'b01, 1'b0}, /*GETLANE*/
                                              
                                              {SHUFFLE, 6'd8, 6'd8, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {SHUFFLE, 6'd12, 6'd12, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {SUBB,     6'd8, 6'd8, 6'd12,  2'b01, 3'b111, 1'b1},
                                             //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                              //{8'h40|SHUFFLE, 6'd1, 6'd8, 9'd0,       2'b01, 1'b0}, /*GETLANE*/
                                              
                                              {SHUFFLE, 6'd8, 6'd8, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {SHUFFLE, 6'd13, 6'd13, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {SUBB,     6'd8, 6'd8, 6'd13,  2'b01, 3'b111, 1'b1},
                                              //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                              //{8'h40|SHUFFLE, 6'd1, 6'd8, 9'd0,       2'b01, 1'b0}, /*GETLANE*/
                                              {SHUFFLE, 6'd8, 6'd8, 6'd14, 2'b00, 3'b111, 1'b0},
                                              
                                    
                                              {SHUFFLE, 6'd12, 6'd12, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {SUBB,     6'd9, 6'd9, 6'd12,  2'b01, 3'b111, 1'b1},
                                              //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                              //{8'h40|SHUFFLE, 6'd1, 6'd9, 9'd0,       2'b01, 1'b0}, /*GETLANE*/
                                            
                                              {SHUFFLE, 6'd9, 6'd9, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {SHUFFLE, 6'd13, 6'd13, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {SUBB,     6'd9, 6'd9, 6'd13,  2'b01, 3'b111, 1'b1},
                                              //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                              //{8'h40|SHUFFLE, 6'd1, 6'd9, 9'd0,       2'b01, 1'b0}, /*GETLANE*/
                                              
                                              {SHUFFLE, 6'd9, 6'd9, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {SHUFFLE, 6'd12, 6'd12, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {SUBB,     6'd9, 6'd9, 6'd12,  2'b01, 3'b111, 1'b1},
                                              //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                              //{8'h40|SHUFFLE, 6'd1, 6'd9, 9'd0,       2'b01, 1'b0}, /*GETLANE*/
                                              
                                              {SHUFFLE, 6'd9, 6'd9, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {SHUFFLE, 6'd13, 6'd13, 6'd14, 2'b00, 3'b111, 1'b0},
                                              {SUBB,     6'd9, 6'd9, 6'd13,  2'b01, 3'b111, 1'b1},
                                              //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                              //{8'h40|SHUFFLE, 6'd1, 6'd9, 9'd0,       2'b01, 1'b0}, /*GETLANE*/
                                              
                                              {SUBB,     6'd4, 6'd4, 6'd5,  2'b01, 3'b111, 1'b1},
                                              
                                              {SHUFFLE, 6'd9, 6'd9, 6'd14, 2'b00, 3'b111, 1'b0}//,
                                              //{8'h60|MOVEI, 6'd4, 16'hFFFF,       1'b1, 1'b1},
                                              //{8'h60|MOVEI_H, 6'd4, 16'hFFFF,       1'b1, 1'b1},
                                              //{8'h40|CMPEQ, 6'd0, 6'd4, 9'h1FF,       2'b01, 1'b1}
                                            };
                                              
 
 instruction_t       instructions2[0:12]  = {{LOADACC, 6'd0, 6'd2, 6'd2, 2'b00, 3'b111, 1'b0},
                                               {MAC,     6'd0, 6'd1, 6'd0, 2'b00, 3'b111, 1'b0},
                                               {GETACCL, 6'd4, 6'd0, 6'd0, 2'b00, 3'b111, 1'b0},
                                               {GETACCH, 6'd5, 6'd0, 6'd0, 2'b00, 3'b111, 1'b0},
                                               {SHUFFLE, 6'd2, 6'd14, 6'd15, 2'b00, 3'b111, 1'b0},
                                               {INTLVL,  6'd6, 6'd14, 6'd15, 2'b00, 3'b111, 1'b0},
                                               {INTLVH,  6'd7, 6'd14, 6'd15, 2'b00, 3'b111, 1'b0},
                                               {ADD,     6'd10, 6'd8, 6'd9, 2'b01, 3'b111, 1'b0},
                                               {ADDC,    6'd10, 6'd14, 6'd15, 2'b01, 3'b111, 1'b0},
                                               {ASHR,    6'd11, 6'd8, 6'd9, 2'b00, 3'b111, 1'b0}, 
                                               {ROTR,    6'd12, 6'd8, 6'd9, 2'b00, 3'b111, 1'b0},
                                               {ROTL,    6'd13, 6'd8, 6'd9, 2'b00, 3'b111, 1'b0},
                                               {8'hE0 | SBOX,    6'd13, 6'd14, 6'd14, 2'b00, 3'b111, 1'b0}  };
                                               
 function  automatic [`V_REGISTER_SIZE*6-1:0] GCDExtended;
 
 input   [`V_REGISTER_SIZE*2 - 1 : 0]                a;
 input   [`V_REGISTER_SIZE*2 - 1 : 0]                b;
 logic   [`V_REGISTER_SIZE*2 - 1 : 0]                x;
 logic   [`V_REGISTER_SIZE*2 - 1 : 0]                y;
 
 logic   [`V_REGISTER_SIZE*2 - 1 : 0]                b1;
 logic   [`V_REGISTER_SIZE*2 - 1 : 0]                x1;
 logic   [`V_REGISTER_SIZE*2 - 1 : 0]                y1;
 
     begin                                              
 // Base Case
   if (a === {`V_REGISTER_SIZE*2{1'b0}})
     begin
       x = {`V_REGISTER_SIZE*2{1'b0}};
       y = {{(`V_REGISTER_SIZE*2-1){1'b0}}, 1'b1};
       GCDExtended = {y,x,b};
     end
   else begin
       {y1,x1,b1} = GCDExtended(b%a, a);
    
       // Update x and y using results of recursive
       // call
       x = y1 - (b/a) * x1;
       y = x1;
    
       GCDExtended = {y,x,b1}; 
       end
   end
   endfunction 
   
   
   
  function automatic [`V_REGISTER_SIZE*2 - 1 : 0] modInverse;
  
  input   [`V_REGISTER_SIZE*2 - 1 : 0]                a;
  input   [`V_REGISTER_SIZE*2 - 1 : 0]                m;
  logic   [`V_REGISTER_SIZE*2 - 1 : 0]                x;
  logic   [`V_REGISTER_SIZE*2 - 1 : 0]                y;
  logic   [`V_REGISTER_SIZE*2 - 1 : 0]                g; 
  logic   [`V_REGISTER_SIZE*2 - 1 : 0]                res;
   
  begin

       {y,x,g} = GCDExtended(a, m);
       
       if (g != {{(`V_REGISTER_SIZE*2-1){1'b0}}, 1'b1})
           $display("Inverse doesn't exist %h", g);
       else begin
           // m is added to handle negative x
           res = (x%m + m) % m;
           modInverse = res;
       end
  end
  endfunction