`include "nuplus_define.sv"
`include "user_define.sv"
                                              
instruction_t       instructions[0:171]  = {  {INTLVL, 6'd4, 6'd3, 6'd2, 2'b00, 3'b111, 1'b0},
                                                {INTLVH,  6'd3, 6'd3, 6'd2, 2'b00, 3'b111, 1'b0},
                                                {8'h40|ADD, 6'd2, 6'd4, 9'd0,       2'b11, 1'b0},
                                                
                                                {INTLVL, 6'd4, 6'd13, 6'd12, 2'b00, 3'b111, 1'b0},
                                                {INTLVH,  6'd13, 6'd13, 6'd12, 2'b00, 3'b111, 1'b0},
                                                {8'h40|ADD, 6'd12, 6'd4, 9'd0,       2'b11, 1'b0},
                                                
                                                {XOR    , 6'd4, 6'd4, 6'd4, 2'b00, 3'b111, 1'b0},
                                                {XOR    , 6'd5, 6'd4, 6'd4, 2'b00, 3'b111, 1'b0},
                                                {LOADH,   6'd0, 6'd20, 6'd21, 2'b00, 3'b111, 1'b0}, 
                                               {8'h60|MOVEI, 6'd60, 16'd1,       1'b0, 1'b0},
                                                
                                                {8'h60|MOVEI, 6'd15, 16'd3,       1'b1, 1'b1},
                                                {8'h60|MOVEI, 6'd14, 16'd1,       1'b1, 1'b1},
                                                {8'h40|SHL, 6'd60, 6'd60, 9'd1,    2'b00, 1'b0},
                                                {8'h60|MOVEI, 6'd15, 16'd0,       1'b1, 1'b1},
                                                {8'h60|MOVEI, 6'd14, 16'd2,       1'b1, 1'b1},
                                                {8'h40|SHL, 6'd60, 6'd60, 9'd1,    2'b00, 1'b0},
                                                {8'h60|MOVEI, 6'd15, 16'd1,       1'b1, 1'b1},
                                                {8'h60|MOVEI, 6'd14, 16'd3,       1'b1, 1'b1},
                                                {8'h40|SHL, 6'd60, 6'd60, 9'd1,    2'b00, 1'b0},
                                                {8'h60|MOVEI, 6'd15, 16'd2,       1'b1, 1'b1},
                                                //{8'h60|MOVEI, 6'd14, 16'hFFFF,       1'b1, 1'b1},
                                                {8'h40|ADD, 6'd49, 6'd14, 9'd0,       2'b11, 1'b0},
                                                {8'h40|ADD, 6'd14, 6'd14, 9'h1FF,       2'b11, 1'b1},
                                                {8'h40|SHR, 6'd60, 6'd60, 9'd3,    2'b00, 1'b0},
                                                  {LOADM, 6'd0, 6'd12, 6'd13,  2'b00, 3'b111, 1'b0},
                                                  //{LOADB, 6'd0, 6'd1, 6'd0,  2'b00, 3'b111, 1'b0},
                                                  {8'h40|SHUFFLE, 6'd59, 6'd1, 9'd0,       2'b01, 1'b0}, /*GETLANE*/
                                                  {LOADMP, 6'd0, 6'd3, 6'd14,  2'b00, 3'b101, 1'b0},
                                                  {MONT1,     6'd4, 6'd2, 6'd4, 2'b00, 3'b111, 1'b0}, //Iter. 1
                                                  {LOADM, 6'd0, 6'd13, 6'd13,  2'b00, 3'b111, 1'b0},
                                                  {MONT2,     6'd5, 6'd3, 6'd5, 2'b00, 3'b111, 1'b0},
                                                  {STOH2,   6'd19, 6'd0, 6'd0, 2'b00, 3'b111, 1'b0},
                                                  {STOH1,   6'd18, 6'd0, 6'd0, 2'b00, 3'b111, 1'b0},
                                                  {LOADM, 6'd0, 6'd12, 6'd13,  2'b00, 3'b111, 1'b0},
                                                  {8'h40|ADD, 6'd57, 6'd59, 9'd0,       2'b00, 1'b0},
                                                  
                                                  {8'h40|ADD, 6'd59, 6'd58, 9'd0,       2'b00, 1'b0},
                                                  {LOADH,   6'd0, 6'd20, 6'd21, 2'b00, 3'b111, 1'b0}, 
                                                  {8'h40|SHUFFLE, 6'd59, 6'd1, 9'd1,       2'b01, 1'b0}, /*GETLANE*/                  
                                                  {SHUFFLE, 6'd4, 6'd4, 6'd14, 2'b00, 3'b111, 1'b0},
                                                  {MONT1,     6'd5, 6'd2, 6'd5, 2'b00, 3'b111, 1'b0}, //Iter. 2
                                                  {LOADM, 6'd0, 6'd13, 6'd13,  2'b00, 3'b111, 1'b0},
                                                  {MONT2,     6'd4, 6'd3, 6'd4, 2'b00, 3'b111, 1'b0},
                                                  {STOH2,   6'd21, 6'd0, 6'd0, 2'b00, 3'b111, 1'b0},
                                                  {STOH1,   6'd20, 6'd0, 6'd0, 2'b00, 3'b111, 1'b0},
                                                  {LOADM, 6'd0, 6'd12, 6'd13,  2'b00, 3'b111, 1'b0},
                                                 {8'h40|ADD, 6'd58, 6'd59, 9'd0,       2'b00, 1'b0},
                                                 
                                                   {8'h40|ADD, 6'd59, 6'd57, 9'd0,       2'b00, 1'b0},
                                                 {LOADH,   6'd0, 6'd18, 6'd19, 2'b00, 3'b111, 1'b0}, 
                                                 {8'h40|SHUFFLE, 6'd59, 6'd1, 9'd2,       2'b01, 1'b0}, /*GETLANE*/                  
                                                 {SHUFFLE, 6'd5, 6'd5, 6'd14, 2'b00, 3'b111, 1'b0},
                                                 {MONT1,     6'd4, 6'd2, 6'd4, 2'b00, 3'b111, 1'b0}, //Iter. 3 
                                                 {LOADM, 6'd0, 6'd13, 6'd13,  2'b00, 3'b111, 1'b0},    
                                                 {MONT2,     6'd5, 6'd3, 6'd5, 2'b00, 3'b111, 1'b0},
                                                 {STOH2,   6'd19, 6'd0, 6'd0, 2'b00, 3'b111, 1'b0},
                                                 {STOH1,   6'd18, 6'd0, 6'd0, 2'b00, 3'b111, 1'b0},
                                                 {LOADM, 6'd0, 6'd12, 6'd13,  2'b00, 3'b111, 1'b0},
                                                {8'h40|ADD, 6'd57, 6'd59, 9'd0,       2'b00, 1'b0},
                                                
                                                {8'h40|ADD, 6'd59, 6'd58, 9'd0,       2'b00, 1'b0},
                                             {LOADH,   6'd0, 6'd20, 6'd21, 2'b00, 3'b111, 1'b0}, 
                                             {8'h40|SHUFFLE, 6'd59, 6'd1, 9'd3,       2'b01, 1'b0}, /*GETLANE*/                  
                                             {SHUFFLE, 6'd4, 6'd4, 6'd14, 2'b00, 3'b111, 1'b0},
                                             {MONT1,     6'd5, 6'd2, 6'd5, 2'b00, 3'b111, 1'b0}, //Iter. 4
                                             {LOADM, 6'd0, 6'd13, 6'd13,  2'b00, 3'b111, 1'b0},    
                                             {MONT2,     6'd4, 6'd3, 6'd4, 2'b00, 3'b111, 1'b0},
                                             {STOH2,   6'd21, 6'd0, 6'd0, 2'b00, 3'b111, 1'b0},
                                             {STOH1,   6'd20, 6'd0, 6'd0, 2'b00, 3'b111, 1'b0},
                                             {LOADM, 6'd0, 6'd12, 6'd13,  2'b00, 3'b111, 1'b0},
                                            {8'h40|ADD, 6'd58, 6'd59, 9'd0,       2'b00, 1'b0},
                                            
                                           {8'h40|ADD, 6'd59, 6'd57, 9'd0,       2'b00, 1'b0},
                                          {LOADH,   6'd0, 6'd18, 6'd19, 2'b00, 3'b111, 1'b0}, 
                                          {8'h40|SHUFFLE, 6'd59, 6'd0, 9'd0,       2'b01, 1'b0}, /*GETLANE*/                  
                                          {SHUFFLE, 6'd5, 6'd5, 6'd14, 2'b00, 3'b111, 1'b0},
                                          {MONT1,     6'd4, 6'd2, 6'd4, 2'b00, 3'b111, 1'b0}, //Iter. 5
                                          {LOADM, 6'd0, 6'd13, 6'd13,  2'b00, 3'b111, 1'b0},     
                                          {MONT2,     6'd5, 6'd3, 6'd5, 2'b00, 3'b111, 1'b0},
                                          {STOH2,   6'd19, 6'd0, 6'd0, 2'b00, 3'b111, 1'b0},
                                          {STOH1,   6'd18, 6'd0, 6'd0, 2'b00, 3'b111, 1'b0},
                                          {LOADM, 6'd0, 6'd12, 6'd13,  2'b00, 3'b111, 1'b0},
                                         {8'h40|ADD, 6'd57, 6'd59, 9'd0,       2'b00, 1'b0},
                                         
                                         {8'h40|ADD, 6'd59, 6'd58, 9'd0,       2'b00, 1'b0},
                                          {LOADH,   6'd0, 6'd20, 6'd21, 2'b00, 3'b111, 1'b0}, 
                                          {8'h40|SHUFFLE, 6'd59, 6'd0, 9'd1,       2'b01, 1'b0}, /*GETLANE*/                  
                                          {SHUFFLE, 6'd4, 6'd4, 6'd14, 2'b00, 3'b111, 1'b0},
                                          {MONT1,     6'd5, 6'd2, 6'd5, 2'b00, 3'b111, 1'b0}, //Iter. 6 
                                          {LOADM, 6'd0, 6'd13, 6'd13,  2'b00, 3'b111, 1'b0},    
                                          {MONT2,     6'd4, 6'd3, 6'd4, 2'b00, 3'b111, 1'b0},
                                          {STOH2,   6'd21, 6'd0, 6'd0, 2'b00, 3'b111, 1'b0},
                                          {STOH1,   6'd20, 6'd0, 6'd0, 2'b00, 3'b111, 1'b0},
                                          {LOADM, 6'd0, 6'd12, 6'd13,  2'b00, 3'b111, 1'b0},
                                         {8'h40|ADD, 6'd58, 6'd59, 9'd0,       2'b00, 1'b0},
                                         
                                           {8'h40|ADD, 6'd59, 6'd57, 9'd0,       2'b00, 1'b0},
                                         {LOADH,   6'd0, 6'd18, 6'd19, 2'b00, 3'b111, 1'b0}, 
                                         {8'h40|SHUFFLE, 6'd59, 6'd0, 9'd2,       2'b01, 1'b0}, /*GETLANE*/                  
                                         {SHUFFLE, 6'd5, 6'd5, 6'd14, 2'b00, 3'b111, 1'b0},
                                         {MONT1,     6'd4, 6'd2, 6'd4, 2'b00, 3'b111, 1'b0}, //Iter. 7 
                                         {LOADM, 6'd0, 6'd13, 6'd13,  2'b00, 3'b111, 1'b0},    
                                         {MONT2,     6'd5, 6'd3, 6'd5, 2'b00, 3'b111, 1'b0},
                                         {STOH2,   6'd19, 6'd0, 6'd0, 2'b00, 3'b111, 1'b0},
                                         {STOH1,   6'd18, 6'd0, 6'd0, 2'b00, 3'b111, 1'b0},
                                         {LOADM, 6'd0, 6'd12, 6'd13,  2'b00, 3'b111, 1'b0},
                                        {8'h40|ADD, 6'd57, 6'd59, 9'd0,       2'b00, 1'b0},
                                        
                                        {8'h40|ADD, 6'd59, 6'd58, 9'd0,       2'b00, 1'b0},
                                         {LOADH,   6'd0, 6'd20, 6'd21, 2'b00, 3'b111, 1'b0}, 
                                         {8'h40|SHUFFLE, 6'd59, 6'd0, 9'd3,       2'b01, 1'b0}, /*GETLANE*/                  
                                         {SHUFFLE, 6'd4, 6'd4, 6'd14, 2'b00, 3'b111, 1'b0},
                                         {MONT1,     6'd5, 6'd2, 6'd5, 2'b00, 3'b111, 1'b0}, //Iter. 8
                                         {LOADM, 6'd0, 6'd13, 6'd13,  2'b00, 3'b111, 1'b0},     
                                         {MONT2,     6'd4, 6'd3, 6'd4, 2'b00, 3'b111, 1'b0},
                                         {STOH2,   6'd21, 6'd0, 6'd0, 2'b00, 3'b111, 1'b0},
                                         {STOH1,   6'd20, 6'd0, 6'd0, 2'b00, 3'b111, 1'b0},
                                        {8'h40|ADD, 6'd58, 6'd59, 9'd0,       2'b00, 1'b0},
                                        
                                        {SHUFFLE, 6'd5, 6'd5, 6'd14, 2'b00, 3'b111, 1'b0},
                                        //    Carry recovery
                                        {8'h40|ADD, 6'd59, 6'd57, 9'd0,       2'b00, 1'b0},
                                        {ADDC,     6'd18, 6'd18, 6'd19,  2'b01, 3'b111, 1'b0},
                                        {8'h40|ADD, 6'd57, 6'd59, 9'd0,       2'b00, 1'b0},
                                        {ADD,      6'd8, 6'd18, 6'd4,  2'b01, 3'b111, 1'b0},
                                        
                                        {ADDC,     6'd20, 6'd20, 6'd21,  2'b01, 3'b111, 1'b0},
                                        {ADDC,      6'd55, 6'd55, 6'd55, 2'b00, 3'b111, 1'b0},
                                        {8'h40|ADD, 6'd59, 6'd58, 9'd0,       2'b00, 1'b0},
                                        {ADDC,     6'd9, 6'd20, 6'd5,  2'b01, 3'b111, 1'b0},
                                        {ADDC,      6'd55, 6'd56, 6'd55, 2'b00,3'b111, 1'b0},
                                        {8'h40|ADD, 6'd59, 6'd57, 9'd0,       2'b00, 1'b0},
                                        {ADDC,      6'd9, 6'd9, 6'd37,   2'b01,3'b111, 1'b0},
                                        {ADDC,      6'd55, 6'd56, 6'd55, 2'b00,3'b111, 1'b0},
                                        
                                        {8'h60|MOVEI, 6'd60, 16'h07,       1'b0, 1'b0},
                                        {8'h40|ADD, 6'd5, 6'd8, 9'd0,       2'b11, 1'b1},
                                        {SHUFFLE, 6'd8, 6'd8, 6'd14, 2'b00, 3'b111, 1'b0},
                                        {SHUFFLE, 6'd9, 6'd9, 6'd49, 2'b00, 3'b111, 1'b0},

                                        {ADD,      6'd8, 6'd8, 6'd55,  2'b01, 3'b111, 1'b0},
                                        {ADDC,     6'd9, 6'd9, 6'd37,  2'b01, 3'b111, 1'b1},
                                       {SHUFFLE, 6'd8, 6'd8, 6'd49, 2'b00, 3'b111, 1'b0},
                                       {8'h40|SHR, 6'd60, 6'd60, 9'd1,    2'b00, 1'b0},
                                       {SHUFFLE, 6'd9, 6'd9, 6'd49, 2'b00, 3'b111, 1'b0},
                                       
                                       {ADDC,     6'd8, 6'd8, 6'd37,  2'b01, 3'b111, 1'b1},
                                       {ADDC,     6'd9, 6'd9, 6'd37,  2'b01, 3'b111, 1'b1},
                                      {SHUFFLE, 6'd8, 6'd8, 6'd49, 2'b00, 3'b111, 1'b0},
                                      {8'h40|SHR, 6'd60, 6'd60, 9'd1,    2'b00, 1'b0},
                                      {SHUFFLE, 6'd9, 6'd9, 6'd49, 2'b00, 3'b111, 1'b0},
                                      
                                      {ADDC,     6'd8, 6'd8, 6'd37,  2'b01, 3'b111, 1'b1},
                                     {ADDC,     6'd9, 6'd9, 6'd37,  2'b01, 3'b111, 1'b1},
                                    {SHUFFLE, 6'd8, 6'd8, 6'd49, 2'b00, 3'b111, 1'b0},
                                    {SHUFFLE, 6'd9, 6'd9, 6'd49, 2'b00, 3'b111, 1'b0},
                              
                                   
                                    {ADDC,     6'd4, 6'd8, 6'd37,  2'b00, 3'b111, 1'b1},
                                    {8'h40|ADD, 6'd8, 6'd5, 9'd0,       2'b11, 1'b1},
                                     
                                    {DEILVL, 6'd5, 6'd8, 6'd9, 2'b00, 3'b111, 1'b0},
                                    {DEILVH, 6'd9, 6'd8, 6'd9, 2'b00, 3'b111, 1'b0},
                                    {8'h40|ADD, 6'd8, 6'd5, 9'd0,       2'b11, 1'b0},
                                    
                                    //subtraction
                                    {ADD,     6'd10, 6'd37, 6'd8, 2'b00, 3'b111, 1'b0},
                                    {ADD,     6'd11, 6'd37, 6'd9, 2'b00, 3'b111, 1'b0},
                                                                                       
                                    {XOR    , 6'd59, 6'd59, 6'd59, 2'b00, 3'b000, 1'b0},
                                                                                     
                                     {SUBB,     6'd8, 6'd8, 6'd12,  2'b01, 3'b111, 1'b1},
                                     //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                    // {8'h40|SHUFFLE, 6'd1, 6'd8, 9'd0,       2'b01, 1'b0},/*GETLANE*/
                                     
                                     {SHUFFLE, 6'd8, 6'd8, 6'd49, 2'b00, 3'b111, 1'b0},
                                     {SUBB,     6'd8, 6'd8, 6'd13,  2'b01, 3'b111, 1'b1},
                                     //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                    // {8'h40|SHUFFLE, 6'd1, 6'd8, 9'd0,       2'b01, 1'b0}, /*GETLANE*/
                                     
                                     {SHUFFLE, 6'd8, 6'd8, 6'd49, 2'b00, 3'b111, 1'b0},
                                     {SHUFFLE, 6'd12, 6'd12, 6'd49, 2'b00, 3'b111, 1'b0},
                                     {SUBB,     6'd8, 6'd8, 6'd12,  2'b01, 3'b111, 1'b1},
                                     //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                    // {8'h40|SHUFFLE, 6'd1, 6'd8, 9'd0,       2'b01, 1'b0}, /*GETLANE*/
                                     
                                     {SHUFFLE, 6'd8, 6'd8, 6'd49, 2'b00, 3'b111, 1'b0},
                                     {SHUFFLE, 6'd13, 6'd13, 6'd49, 2'b00, 3'b111, 1'b0},
                                     {SUBB,     6'd8, 6'd8, 6'd13,  2'b01, 3'b111, 1'b1},
                                     //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                     //{8'h40|SHUFFLE, 6'd1, 6'd8, 9'd0,       2'b01, 1'b0}, /*GETLANE*/
                                     {SHUFFLE, 6'd8, 6'd8, 6'd49, 2'b00, 3'b111, 1'b0},
                                     
                           
                                     {SHUFFLE, 6'd12, 6'd12, 6'd49, 2'b00, 3'b111, 1'b0},
                                     {SUBB,     6'd9, 6'd9, 6'd12,  2'b01, 3'b111, 1'b1},
                                     //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                    // {8'h40|SHUFFLE, 6'd1, 6'd9, 9'd0,       2'b01, 1'b0}, /*GETLANE*/
                                   
                                     {SHUFFLE, 6'd9, 6'd9, 6'd49, 2'b00, 3'b111, 1'b0},
                                     {SHUFFLE, 6'd13, 6'd13, 6'd49, 2'b00, 3'b111, 1'b0},
                                     {SUBB,     6'd9, 6'd9, 6'd13,  2'b01, 3'b111, 1'b1},
                                    //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                    // {8'h40|SHUFFLE, 6'd1, 6'd9, 9'd0,       2'b01, 1'b0}, /*GETLANE*/
                                     
                                     {SHUFFLE, 6'd9, 6'd9, 6'd49, 2'b00, 3'b111, 1'b0},
                                     {SHUFFLE, 6'd12, 6'd12, 6'd49, 2'b00, 3'b111, 1'b0},
                                     {SUBB,     6'd9, 6'd9, 6'd12,  2'b01, 3'b111, 1'b1},
                                     //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                     //{8'h40|SHUFFLE, 6'd1, 6'd9, 9'd0,       2'b01, 1'b0}, /*GETLANE*/
                                     
                                     {SHUFFLE, 6'd9, 6'd9, 6'd49, 2'b00, 3'b111, 1'b0},
                                     {SHUFFLE, 6'd13, 6'd13, 6'd49, 2'b00, 3'b111, 1'b0},
                                     {SUBB,     6'd9, 6'd9, 6'd13,  2'b01, 3'b111, 1'b1},
                                     //{8'h40|ADD, 6'd0, 6'd0, 9'd1,       2'b00, 1'b0},
                                    //{8'h40|SHUFFLE, 6'd1, 6'd9, 9'd0,       2'b01, 1'b0}, /*GETLANE*/
                                     
                                     {SUBB,     6'd4, 6'd4, 6'd6,  2'b01, 3'b111, 1'b1},
                                     
                                     {SHUFFLE, 6'd9, 6'd9, 6'd49, 2'b00, 3'b111, 1'b0},
                                     
                                     {8'h78, 24'd0}
                                  
                                     
                                                };                                              
