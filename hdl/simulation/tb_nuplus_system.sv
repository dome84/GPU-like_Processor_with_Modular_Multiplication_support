`include "nuplus_define.sv"
`include "user_define.sv"
`include "system_defines.sv"
`include "load_store_unit_defines.sv"

module tb_nuplus_system( );

	logic clk            = 1'b0;
	logic reset          = 1'b1;
//  boot_message_t message_in       = '{default : '0};
//  logic          message_in_valid = 1'b0;
	logic job_terminated;

	logic [1 : 0] mux_selection;

	logic             uart_tx;
	logic             uart_rx;
	logic [16 - 1 : 0]synch_tmp;
    localparam                     CLK_RATE              = 50000000;
    localparam                     CLK_PERIOD_NS         = 20;
    localparam                     DEFAULT_UART_BAUD     = 921600;
    localparam                     CLK_PER_BIT           = ((CLK_RATE / DEFAULT_UART_BAUD) - 1);
    localparam                     BIT_PERIOD            = CLK_PER_BIT * CLK_PERIOD_NS;

	nuplus_system nuplus_system (
		.clk           ( clk            ),
		.reset         ( reset          ),
		.end_of_kernel ( job_terminated ),
		.mux_selection ( mux_selection  ),
		.uart_tx       ( uart_tx        ),
		.uart_rx       ( uart_rx        )
	);

	// Takes in input byte and serializes it
	task UART_WRITE_BYTE;
		input [7 : 0] data_in;
		integer i;
		begin
			// Send Start Bit
			uart_rx <= 1'b0;
			#( BIT_PERIOD );
			// Send Data Byte
			for ( i = 0; i < 8; i = i + 1 )
			begin
				uart_rx <= data_in[i];
				#( BIT_PERIOD );
			end
			// Send Stop Bit
			uart_rx <= 1'b1;
			#( BIT_PERIOD );
		end
	endtask // UART_WRITE_BYTE


	always #( CLK_PERIOD_NS/2 ) clk = ~clk;

	logic rx = 1'b0;
	initial begin

		#100
		reset = 0;

		@ ( posedge nuplus_system.uart_control_unit.divisor_set );

		UART_WRITE_BYTE( `BARRIER_COMMAND );
		synch_tmp={7'b0,5'b0,3'b01,1'b1};
		#100 rx  = ~rx;
		UART_WRITE_BYTE( synch_tmp[7 : 0] );
		#100 rx = ~rx;
		UART_WRITE_BYTE( synch_tmp[15 : 8] );
		#100 rx = ~rx;
		UART_WRITE_BYTE( `BOOT_CORE_COMMAND );
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Destination
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Thread_id
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Program counter
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Program counter
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Program counter
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Program counter
		#100 rx = ~rx;
		UART_WRITE_BYTE( `BOOT_CORE_COMMAND );
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Destination
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h01 ); //Thread_id
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Program counter
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Program counter
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Program counter
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Program counter
		#100 rx = ~rx;

//`define DSU
`ifdef DSU
		UART_WRITE_BYTE( `DSU_ENABLE_COMMAND );
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Destination
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Destination
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Destination
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Destination
		//#100 rx = ~rx;
		wait ( nuplus_system.uart_control_unit.tx_char_in == `DSU_ENABLE_ACK )

		UART_WRITE_BYTE( `DSU_SET_BP_COMMAND );
		//Send 4 byte for breakpoint
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Bp
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Bp
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h01 ); //Bp
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Bp
		//#100 rx = ~rx;
		wait ( nuplus_system.uart_control_unit.tx_char_in == `DSU_SET_BP_ACK )

		UART_WRITE_BYTE( `DSU_SET_BP_MASK_COMMAND );
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'b00000001 ); //Bp Mask
		//#100 rx = ~rx;
		wait ( nuplus_system.uart_control_unit.tx_char_in == `DSU_SET_BP_MASK_ACK )

//      UART_WRITE_BYTE( `DSU_ENABLE_SELECTION_TH_COMMAND );
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h01 ); //Thread id
//      //#100 rx = ~rx;
//      wait ( nuplus_system.uart_control_unit.tx_char_in == `DSU_ENABLE_SELECTION_TH_ACK)
`endif
		//#100 rx = ~rx;
		UART_WRITE_BYTE( `ENABLE_THREAD_COMMAND );
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Destination
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'b0000_0001 ); //Thread mask
		#100 rx = ~rx;
//      UART_WRITE_BYTE( `ENABLE_THREAD_COMMAND );
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h00 ); //Destination
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'b0000_0010 ); //Thread mask
//      #1000 rx = ~rx;
`ifdef DSU

		UART_WRITE_BYTE( `GET_INTERRUPTION_COMMAND );

		wait ( nuplus_system.uart_control_unit.tx_char_in == `BREAKPOINT_CATCHED )

		UART_WRITE_BYTE( `DSU_DISABLE_SELECTION_TH_COMMAND );
		//#100 rx = ~rx;
		wait ( nuplus_system.uart_control_unit.tx_char_in == `DSU_DISABLE_SELECTION_TH_ACK )

		UART_WRITE_BYTE( `DSU_READ_V_REG_COMMAND );
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //reg address
		//#100 rx = ~rx;
		wait ( nuplus_system.uart_control_unit.tx_char_in == `DSU_READ_V_REG_ACK )

		UART_WRITE_BYTE( `DSU_READ_S_REG_COMMAND );
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h3d ); //reg address
		//#100 rx = ~rx;
		wait ( nuplus_system.uart_control_unit.tx_char_in == `DSU_READ_S_REG_ACK )

		UART_WRITE_BYTE( `DSU_READ_BP_INFO_COMMAND );
		wait ( nuplus_system.uart_control_unit.tx_char_in == `DSU_READ_BP_INFO_ACK )

		UART_WRITE_BYTE( `DSU_RESUME_COMMAND );
		//#100 rx = ~rx;

		wait ( nuplus_system.uart_control_unit.tx_char_in == `DSU_RESUME_ACK )
`endif

		UART_WRITE_BYTE( `GET_INTERRUPTION_COMMAND );

		wait ( nuplus_system.uart_control_unit.tx_char_in == `KERNEL_ENDED )
		//@ ( posedge job_terminated )
		#1000

`ifdef DSU
		UART_WRITE_BYTE( `DSU_DISABLE_COMMAND );
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Destination
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Destination
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Destination
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 ); //Destination

		wait ( nuplus_system.uart_control_unit.tx_char_in == `DSU_DISABLE_ACK )
`endif

		UART_WRITE_BYTE( `READ_MEMORY_COMMAND );
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h02 );
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 );
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 );
		#100 rx = ~rx;
		UART_WRITE_BYTE( 8'h00 );

//      UART_WRITE_BYTE( `WRITE_MEMORY_COMMAND );
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h00 );   //Address
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h00 );   //Address
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h00 );   //Address
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h00 );   //Address
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h08 );   //Byte Number
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h01 );   //1 byte
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h02 );   //2 byte
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h03 );   //3 byte
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h04 );   //4 byte
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h05 );   //5 byte
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h06 );   //6 byte
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h07 );   //7 byte
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h08 );   //8 byte
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( `READ_MEMORY_COMMAND );
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h00 );    //Address
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h00 );    //Address
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h00 );    //Address
//      #100 rx = ~rx;
//      UART_WRITE_BYTE( 8'h00 );    //Address



		#1000000 $finish( );
	end


endmodule
