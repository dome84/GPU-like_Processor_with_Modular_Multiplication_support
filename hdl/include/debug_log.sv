`ifndef __DEBUG_LOG
`define __DEGUB_LOG

`include "user_define.sv"

`ifdef DISPLAY_CORE
task print_core_issue;
	input int tile_id;
	input instruction_decoded_t is_instruction;
	$fdisplay( `DISPLAY_CORE_VAR, "=======================" );
	$fdisplay( `DISPLAY_CORE_VAR, "Instruction Issue - [Time %.16d] [TILE %.2h] [THREAD %.2h] [PC %h]", $time( ), tile_id, is_instruction.thread_id, is_instruction.pc );
	//$fdisplay( file, "PC: %h", is_instruction.pc );
	if ( is_instruction.is_memory_access ) begin
		$fdisplay( `DISPLAY_CORE_VAR, "op_code: %s   Dest: %d,  Src0: %d,  Src1: %d", is_instruction.op_code.mem_opcode.name( ), is_instruction.destination, is_instruction.source0, is_instruction.source1 );
	end else if ( is_instruction.is_branch )
		$fdisplay( `DISPLAY_CORE_VAR, "op_code: %s", is_instruction.op_code.j_opcode.name( ) );
	else if ( is_instruction.is_int | is_instruction.is_fp )
		$fdisplay( `DISPLAY_CORE_VAR, "op_code: %s", is_instruction.op_code.alu_opcode.name( ) );
	else if ( is_instruction.is_movei )
		$fdisplay( `DISPLAY_CORE_VAR, "op_code: %s", is_instruction.op_code.movei_opcode.name( ) );
endtask

`endif

`endif