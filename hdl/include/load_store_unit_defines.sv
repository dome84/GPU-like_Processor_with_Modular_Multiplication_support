`ifndef __LOAD_STORE_UNIT_DEFINES
`define __LOAD_STORE_UNIT_DEFINES

`include "nuplus_define.sv"

`define DCACHE_SIZE               (`DCACHE_WAY*`DCACHE_SET)

typedef logic [`DCACHE_TAG_LENGTH    - 1 : 0] dcache_tag_t;
typedef logic [`DCACHE_SET_LENGTH    - 1 : 0] dcache_set_t;
typedef logic [`DCACHE_OFFSET_LENGTH - 1 : 0] dcache_offset_t;
typedef logic [`DCACHE_WIDTH         - 1 : 0] dcache_line_t;
typedef logic [`DCACHE_WIDTH/8       - 1 : 0] dcache_store_mask_t;
typedef logic [`DCACHE_WAY           - 1 : 0] dcache_way_mask_t;
typedef logic [$clog2(`DCACHE_WAY)   - 1 : 0] dcache_way_idx_t;

typedef struct packed {
    dcache_tag_t    tag;
    dcache_set_t    index;
    dcache_offset_t offset;
} dcache_address_t;

typedef struct packed {
    instruction_decoded_t instruction;
    dcache_address_t      address;
    dcache_line_t         store_value;
    dcache_store_mask_t   store_mask;
    hw_lane_mask_t        hw_lane_mask;
} dcache_request_t;

typedef struct packed{
    logic can_read;
    logic can_write;
} dcache_privileges_t;

typedef struct packed {
	instruction_decoded_t inst_scheduled;
	address_t address;
	logic [`NUM_BYTE_LANE - 1 : 0] fecthed_mask;
	hw_lane_mask_t lane_mask;
	vec_reg_size_t value;
	thread_mask_t thread_bitmap;
	logic [`DCACHE_OFFSET_LENGTH - 1 : 0]scalar_offset;
} ldst_request_t;

`endif