`ifndef __NUPLUS_DEFINES_SV
`define __NUPLUS_DEFINES_SV

`include "user_define.sv"

//  -----------------------------------------------------------------------
//  -- DSU Defines
//  -----------------------------------------------------------------------
typedef enum {
	IDLE_MODE,
	RUN_NORMAL_MODE,
	RUN_DEBUG_MODE,
	HALT_MODE,
	WAIT_INSTRUCTION_END,
	WAIT_LOAD_STORE,
	STOP_MODE
}core_state_t;

//  -----------------------------------------------------------------------
//  -- Core Defines
//  -----------------------------------------------------------------------

`define ADDRESS_SIZE 32
`define REGISTER_NUMBER 64
`define REGISTER_SIZE 32
`define REGISTER_ADDRESS  $clog2 ( `REGISTER_NUMBER * `THREAD_NUMB ) 
`define BYTE_PER_REGISTER ( `REGISTER_SIZE / 8 )
`define REGISTER_INDEX_LENGTH $clog2( `REGISTER_NUMBER )
`define V_REGISTER_SIZE ( `HW_LANE * `REGISTER_SIZE )
`define NUM_BYTE_LINE ( `V_REGISTER_SIZE/8 )
`define IMMEDIATE_SIZE 32
`define OP_CODE_WIDTH 8
`define NUM_EX_PIPE 6
`define NUM_MAX_EX_PIPE 16
`define NUM_INTERNAL_PIPE_WIDTH 4                             // inside an exec. pipe there could be other sub-pipes
`define INSTRUCTION_FIFO_SIZE 8
`define SCOREBOARD_LENGTH ( `REGISTER_NUMBER*2 )
`define CACHE_LINE_WIDTH ( `V_REGISTER_SIZE * `K_FACTOR )

//  -----------------------------------------------------------------------
//  -- Register Defines
//  -----------------------------------------------------------------------

`define PC_REG ( `REGISTER_NUMBER - 1 )
`define RA_REG ( `REGISTER_NUMBER - 2 )
`define SP_REG ( `REGISTER_NUMBER - 3 )
//`define FP_REG ( `REGISTER_NUMBER - 4 )
`define MASK_REG ( `REGISTER_NUMBER - 4 )
`define CARRY_REG ( `REGISTER_NUMBER - 5 )

//  -----------------------------------------------------------------------
//  -- L1 Cache Defines
//  -----------------------------------------------------------------------

`define INSTRUCTION_LENGTH 32
`define ICACHE_WIDTH `CACHE_LINE_WIDTH
`define ICACHE_SET 32
`define ICACHE_SET_LENGTH $clog2( `ICACHE_SET )
`define ICACHE_WAY 4
`define ICACHE_WAY_LENGTH $clog2( `ICACHE_WAY )
`define ICACHE_OFFSET_LENGTH $clog2( `ICACHE_WIDTH/8 )
`define ICACHE_TAG_LENGTH ( `ADDRESS_SIZE - `ICACHE_SET_LENGTH - `ICACHE_OFFSET_LENGTH )
//`define ICACHE_SET_SUBFIELD      `ICACHE_SET_LENGTH + `ICACHE_OFFSET_LENGTH - 1 : `ICACHE_OFFSET_LENGTH
//`define ICACHE_TAG_SUBFIELD      `ICACHE_TAG_LENGTH + `ICACHE_OFFSET_LENGTH + `ICACHE_SET_LENGTH - 1 : `ICACHE_SET_LENGTH + `ICACHE_OFFSET_LENGTH

`define DCACHE_WIDTH `CACHE_LINE_WIDTH
`define DCACHE_SET 32
`define DCACHE_SET_LENGTH $clog2( `DCACHE_SET )
`define DCACHE_OFFSET_LENGTH $clog2( `DCACHE_WIDTH/8 )
`define DCACHE_TAG_LENGTH ( `ADDRESS_SIZE - `DCACHE_SET_LENGTH - `DCACHE_OFFSET_LENGTH )
`define DCACHE_WAY 4
`define NUM_BYTE_LANE ( `V_REGISTER_SIZE/8 )
//`define DCACHE_SET_SUBFIELD      `DCACHE_SET_LENGTH + `DCACHE_OFFSET_LENGTH - 1 : `DCACHE_OFFSET_LENGTH
//`define DCACHE_TAG_SUBFIELD      `ADDRESS_SIZE - 1 : `DCACHE_SET_LENGTH + `DCACHE_OFFSET_LENGTH


//  -----------------------------------------------------------------------
//  -- Typedef
//  -----------------------------------------------------------------------

typedef logic [`ADDRESS_SIZE - 1 : 0] address_t;
typedef logic [`REGISTER_INDEX_LENGTH - 1 : 0] reg_addr_t;
typedef logic [`REGISTER_SIZE - 1 : 0] scal_reg_size_t;
typedef scal_reg_size_t [`HW_LANE - 1 : 0] vec_reg_size_t;    // x[i][j], i: hw_lane, j: reg_size
typedef logic [`HW_LANE - 1 : 0] hw_lane_mask_t;
typedef logic [$clog2( `THREAD_NUMB ) - 1 : 0] thread_id_t;
typedef logic [`ICACHE_WIDTH - 1 : 0] icache_lane_t;
typedef logic [2*`REGISTER_NUMBER - 1 : 0] scoreboard_t;
typedef logic [`THREAD_NUMB - 1 : 0] thread_mask_t;
typedef logic [`NUM_EX_PIPE - 1 : 0] ex_pipe_mask_t;
typedef logic [`BYTE_PER_REGISTER - 1 : 0] reg_byte_enable_t; //

typedef struct packed {
	logic [`ICACHE_TAG_LENGTH - 1 : 0] tag;
	logic [`ICACHE_SET_LENGTH - 1 : 0] index;
	logic [`ICACHE_OFFSET_LENGTH - 1 : 0] offset;
} icache_address_t;

//  -----------------------------------------------------------------------
//  -- Operation type definitions
//  -----------------------------------------------------------------------

// ALU operation type - Integer and FP
typedef enum logic [`OP_CODE_WIDTH - 1 : 0]{
	SHUFFLE      = `OP_CODE_WIDTH'b000000,
	OR       = `OP_CODE_WIDTH'b000001,
	AND      = `OP_CODE_WIDTH'b000010,
	XOR      = `OP_CODE_WIDTH'b000011,

	ADD      = `OP_CODE_WIDTH'b000100,
	SUB      = `OP_CODE_WIDTH'b000101,
	ADDC     = `OP_CODE_WIDTH'b000110,
    SUBB     = `OP_CODE_WIDTH'b000111,

    ASHR     = `OP_CODE_WIDTH'b001000,
    SHR      = `OP_CODE_WIDTH'b001001,
    SHL      = `OP_CODE_WIDTH'b001010,
    ROTR     = `OP_CODE_WIDTH'b001011,
    ROTL     = `OP_CODE_WIDTH'b001100,
    CLZ      = `OP_CODE_WIDTH'b001101,

	CMPEQ    = `OP_CODE_WIDTH'b001110,
	CMPNE    = `OP_CODE_WIDTH'b001111,
	CMPGT    = `OP_CODE_WIDTH'b010000,
	CMPGE    = `OP_CODE_WIDTH'b010001,
	CMPLT    = `OP_CODE_WIDTH'b010010,
	CMPLE    = `OP_CODE_WIDTH'b010011,
	CMPGT_U  = `OP_CODE_WIDTH'b010100,
	CMPGE_U  = `OP_CODE_WIDTH'b010101,
	CMPLT_U  = `OP_CODE_WIDTH'b010110,
	CMPLE_U  = `OP_CODE_WIDTH'b010111,

	
    MULHI    = `OP_CODE_WIDTH'b011000,
    MULLO    = `OP_CODE_WIDTH'b011001,
    MONT1    = `OP_CODE_WIDTH'b011010,
    LOADACC  = `OP_CODE_WIDTH'b011011,
    GETACCL  = `OP_CODE_WIDTH'b011100,
    GETACCH  = `OP_CODE_WIDTH'b011101,
    LOADM    = `OP_CODE_WIDTH'b011110,
    LOADMP   = `OP_CODE_WIDTH'b011111,
    INCBIND  = `OP_CODE_WIDTH'b100000,
    MONT2    = `OP_CODE_WIDTH'b100001,
    MAC      = `OP_CODE_WIDTH'b100010,
    STOH1    = `OP_CODE_WIDTH'b100011,
    STOH2    = `OP_CODE_WIDTH'b100100,
    LOADH    = `OP_CODE_WIDTH'b100101,
    LOADH3   = `OP_CODE_WIDTH'b100110,

    DEILVL   = `OP_CODE_WIDTH'b100111,
    DEILVH   = `OP_CODE_WIDTH'b101000,
    CTZ      = `OP_CODE_WIDTH'b101001,
    MOVE     = `OP_CODE_WIDTH'b101010,
    
    INTLVL   = `OP_CODE_WIDTH'b101011,
    INTLVH   = `OP_CODE_WIDTH'b101100,
    SEXT8    = `OP_CODE_WIDTH'b101101,
    SEXT16   = `OP_CODE_WIDTH'b101110,
    SEXT32   = `OP_CODE_WIDTH'b101111,

    ADD_FP   = `OP_CODE_WIDTH'b110000,
    SUB_FP   = `OP_CODE_WIDTH'b110001,
    MUL_FP   = `OP_CODE_WIDTH'b110010,
    DIV_FP   = `OP_CODE_WIDTH'b110011,
    CMPGT_FP = `OP_CODE_WIDTH'b110100,
    CMPLT_FP = `OP_CODE_WIDTH'b110101,
    CMPGE_FP = `OP_CODE_WIDTH'b110110,
    CMPLE_FP = `OP_CODE_WIDTH'b110111,
    CMPEQ_FP = `OP_CODE_WIDTH'b111000,
    CMPNE_FP = `OP_CODE_WIDTH'b111001,
    CRP_V_32 = `OP_CODE_WIDTH'b111111
}alu_op_t;

// Memory operation types
typedef enum logic[`OP_CODE_WIDTH - 1 : 0] {
	LOAD_8      = `OP_CODE_WIDTH'b000000,
	LOAD_16     = `OP_CODE_WIDTH'b000001,
	LOAD_32     = `OP_CODE_WIDTH'b000010,
	LOAD_64     = `OP_CODE_WIDTH'b000011,
	LOAD_8_U    = `OP_CODE_WIDTH'b000100,
	LOAD_16_U   = `OP_CODE_WIDTH'b000101,
	LOAD_32_U   = `OP_CODE_WIDTH'b000110,
	LOAD_V_8    = `OP_CODE_WIDTH'b000111,
	LOAD_V_16   = `OP_CODE_WIDTH'b001000,
	LOAD_V_32   = `OP_CODE_WIDTH'b001001,
	LOAD_V_64   = `OP_CODE_WIDTH'b001010,
	LOAD_V_8_U  = `OP_CODE_WIDTH'b001011,
	LOAD_V_16_U = `OP_CODE_WIDTH'b001100,
	LOAD_V_32_U = `OP_CODE_WIDTH'b001101,
	LOAD_G_8    = `OP_CODE_WIDTH'b001110,
	LOAD_G_16   = `OP_CODE_WIDTH'b001111,
	LOAD_G_32   = `OP_CODE_WIDTH'b010000,
	LOAD_G_64   = `OP_CODE_WIDTH'b010001,
	LOAD_G_8_U  = `OP_CODE_WIDTH'b010010,
	LOAD_G_16_U = `OP_CODE_WIDTH'b010011,
	LOAD_G_32_U = `OP_CODE_WIDTH'b010100,
	LOAD_CR     = `OP_CODE_WIDTH'b010101,
	LOOKUP_8    = `OP_CODE_WIDTH'b011000,
	STORE_8     = `OP_CODE_WIDTH'b100000,
	STORE_16    = `OP_CODE_WIDTH'b100001,
	STORE_32    = `OP_CODE_WIDTH'b100010,
	STORE_64    = `OP_CODE_WIDTH'b100011,
	STORE_V_8   = `OP_CODE_WIDTH'b100100,
	STORE_V_16  = `OP_CODE_WIDTH'b100101,
	STORE_V_32  = `OP_CODE_WIDTH'b100110,
	STORE_V_64  = `OP_CODE_WIDTH'b100111,
	STORE_S_8   = `OP_CODE_WIDTH'b101000,
	STORE_S_16  = `OP_CODE_WIDTH'b101001,
	STORE_S_32  = `OP_CODE_WIDTH'b101010,
	STORE_S_64  = `OP_CODE_WIDTH'b101011,
	STORE_CR    = `OP_CODE_WIDTH'b101100
} memory_op_t;

typedef enum logic[`OP_CODE_WIDTH - 1 : 0] {
	MOVEI   = `OP_CODE_WIDTH'b000010,
	MOVEI_L = `OP_CODE_WIDTH'b000000,
	MOVEI_H = `OP_CODE_WIDTH'b000001
} movei_t;

typedef enum logic[`OP_CODE_WIDTH - 1 : 0] {
	JMP        = `OP_CODE_WIDTH'b000000,
	JMPSR      = `OP_CODE_WIDTH'b000001,
	JSYS       = `OP_CODE_WIDTH'b000010,
	JRET       = `OP_CODE_WIDTH'b000011,
	JERET      = `OP_CODE_WIDTH'b000100,
	BRANCH_EQZ = `OP_CODE_WIDTH'b000101,
	BRANCH_NEZ = `OP_CODE_WIDTH'b000110
} j_op_t;

// Control Operation - Cache and other compiler control mechanism - TODO: define others control operation
typedef enum logic[`OP_CODE_WIDTH - 1 : 0] {
	BARRIER_CORE   = `OP_CODE_WIDTH'b000000,
	BARRIER_THREAD = `OP_CODE_WIDTH'b000001,
	FLUSH          = `OP_CODE_WIDTH'b000010
} control_op_t;

typedef enum logic[`OP_CODE_WIDTH - 1 : 0] {
	SBOX      = `OP_CODE_WIDTH'b000000,
	MIXCOL    = `OP_CODE_WIDTH'b000010,
	MIXCOLLAST= `OP_CODE_WIDTH'b000011,
	AESENC    = `OP_CODE_WIDTH'b000100,
	AESENCLAST= `OP_CODE_WIDTH'b000101,
	ENCXOR    = `OP_CODE_WIDTH'b000110
} sfu_op_t;

typedef enum logic [`NUM_INTERNAL_PIPE_WIDTH - 1 : 0] {
	PIPE_MEM,
	PIPE_INT,
	PIPE_CR,
	PIPE_BRANCH,
	PIPE_FP,
	PIPE_SPM,
	PIPE_SFU,
	PIPE_SYNC,
	PIPE_CRP,
    PIPE_MAC
} pipeline_disp_t;

typedef enum logic [0 : 0] {
	JBA,
	JRA
} branch_type_t;

//  -----------------------------------------------------------------------
//  -- Instruction Format definitions
//  -----------------------------------------------------------------------

//Instruction bodies
typedef struct packed {
	reg_addr_t destination;
	reg_addr_t source0;
	reg_addr_t source1;
	logic [1 : 0] unused; // TODO: inserire bit L
	logic [2 : 0] register_selection;
	logic mask;
} RR_instruction_body_t;

typedef struct packed {
	reg_addr_t destination;
	reg_addr_t source0;
	logic [8 : 0] immediate;
	logic [1 : 0] register_selection;
	logic mask;
} RI_instruction_body_t;

typedef struct packed {
	reg_addr_t destination;
	logic [15 : 0] immediate;
	logic register_selection;
	logic mask;
} MVI_instruction_body_t;

/* verilator lint_off SYMRSVDWORD */
typedef struct packed {
	reg_addr_t src_dest_register;
	reg_addr_t base_register;
	logic [8 : 0] offset;
	logic long;
	logic shared;
	logic mask;
} MEM_instruction_body_t;
/* verilator lint_on SYMRSVDWORD */

typedef struct packed {
	logic [23 : 0] boh;
} MPOLI_instruction_body_t;

typedef struct packed {
	reg_addr_t dest;
	logic [17 : 0] immediate;
} JBA_instruction_body_t;

typedef struct packed {
	logic [23 : 0] immediate;
} JRA_instruction_body_t;

typedef struct packed {
	reg_addr_t source0;
	reg_addr_t source1;
	logic [8 : 0] immediate;
	logic [2 : 0] unused;
} CTR_instruction_body_t;

//=====================================================================================================

typedef union packed {
	RR_instruction_body_t RR_body;
	RI_instruction_body_t RI_body;
	MVI_instruction_body_t MVI_body;
	MEM_instruction_body_t MEM_body;
	MPOLI_instruction_body_t MPOLI_body;
	JBA_instruction_body_t JBA_body;
	JRA_instruction_body_t JRA_body;
	CTR_instruction_body_t CTR_body;
} instruction_body_t;

typedef union packed {
	alu_op_t alu_opcode;
	memory_op_t mem_opcode;
	movei_t movei_opcode;
	j_op_t j_opcode;
	control_op_t contr_opcode;
	sfu_op_t   sfu_opcode;
} opcode_t;

typedef struct packed {
	//instruction_type_t  instruction_type;
	opcode_t opcode;
	instruction_body_t body;
} instruction_t;

//  -----------------------------------------------------------------------
//  -- Decode definitions
//  -----------------------------------------------------------------------

typedef struct packed {
	address_t pc;
	thread_id_t thread_id;
	logic mask_enable;
	logic is_valid;

	// Operand Register Fields
	reg_addr_t source0;
	reg_addr_t source1;
	reg_addr_t destination;
	logic has_source0;
	logic has_source1;
	logic has_destination;
	logic is_source0_vectorial;
	logic is_source1_vectorial;
	logic is_destination_vectorial;
	logic signed [`IMMEDIATE_SIZE - 1 : 0] immediate;
	logic is_source1_immediate;

    logic has_2_results;
	logic has_3_sources;
	
	logic is_op0_forward;
    logic is_op1_forward;
    
    logic [2:0] byte_shift;
    logic is_lookup;
    
	// Ex Pipes Fields
	pipeline_disp_t pipe_sel;
	opcode_t op_code;
	logic is_memory_access;
	logic is_int;
	logic is_fp;
	logic is_load;
	logic is_movei;
	logic is_branch;
	logic is_conditional;
	logic is_control;
	logic is_sfu;
    logic is_shuffle;

	branch_type_t branch_type;
} instruction_decoded_t;

//  -----------------------------------------------------------------------
//  -- Control Registers
//  -----------------------------------------------------------------------

typedef enum logic [1 : 0]{
	READ_TILE_ID,
	READ_CORE_ID,
	READ_THREAD_ID,
	READ_GLOBAL_ID
} control_register_index_t;

typedef struct packed {
	logic interrupt_enable;
	logic [15 : 0] interrupt_mask;
	logic supervisor;
	logic interrupt_pending;
	logic interrupt_trigger_mode;
	address_t isr_handler;
} control_register_t;

//  -----------------------------------------------------------------------
//  -- Execution Floating Point type definition
//  -----------------------------------------------------------------------
`define IEEE754_SP_EXP_WIDTH 8
`define IEEE754_SP_MAN_WIDTH 23
`define IEEE754_DP_EXP_WIDTH 11
`define IEEE754_DP_MAN_WIDTH 52

`define FP_ADD_LATENCY 10
`define FP_MULT_LATENCY 3
`define FP_DIV_LATENCY 17

typedef logic [`IEEE754_SP_MAN_WIDTH - 1 : 0] mantissa_sp_t;
typedef logic [`IEEE754_SP_EXP_WIDTH - 1 : 0] exponent_sp_t;
typedef logic [`IEEE754_DP_MAN_WIDTH - 1 : 0] mantissa_dp_t;
typedef logic [`IEEE754_DP_EXP_WIDTH - 1 : 0] exponent_dp_t;

typedef struct packed {
	logic sign;
	exponent_sp_t exp;
	mantissa_sp_t frac;
} ieee754_sp_t;

typedef struct packed {
	logic sign;
	exponent_dp_t exponent;
	mantissa_dp_t mantissa;
} ieee754_dp_t;

typedef struct packed {
	ieee754_sp_t fpnum;
	logic is_nan;
	logic is_inf;
} sp_float_t;

//  -----------------------------------------------------------------------
//  -- WB definitions
//  -----------------------------------------------------------------------

`define WB_FIFO_SIZE 8

typedef struct packed {
	scal_reg_size_t wb_result_pc;
	vec_reg_size_t wb_result_data;
	scal_reg_size_t wb_result_data1;
    logic      has_2_results;
	reg_addr_t wb_result_register;
	hw_lane_mask_t wb_result_hw_lane_mask;
	reg_byte_enable_t wb_result_write_byte_enable;
	logic wb_result_is_scalar;
	logic no_scoreboard;
} wb_result_t;

`endif