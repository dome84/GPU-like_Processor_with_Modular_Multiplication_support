`ifndef __USER_DEFINES_SV
`define __USER_DEFINES_SV

/*
 * Per simulare � predisposto il testbench tb_nuplus_system.sv.
 * 
 * Prima di far partire la simulazione, posizionarsi nel path del progetto e
 * creare una cartella in ./simulation/message_log con il nome del file del kernel.
 * 
 * Inoltre bisogna cambiare i parametri FILE_NAME e PROJECT_PATH che puntano al path del progetto
 * ed al nome del file del kernel.
 *
 */

//--------------------------------
// -- ARCHITECTURE PARAMETERS
//--------------------------------

// Core user defines
`define THREAD_NUMB       4 // MUST BE A POWER OF 2
`define HW_LANE           4 // MUST BE A POWER OF 2, NON TOCCARE   //Modificato per test
`define K_FACTOR          4 // QUANTE VOLTE LA LINEA DI CACHE E' PIU' GRANDE DI UNA VLANE, NON TOCCARE

`define SINGLE_CORE
`define MONTGOMERY_SUPPORT
//--------------------------------
// -- SIMULATION PARAMETERS
//--------------------------------

`define SIMULATION

`ifdef SIMULATION

	// TODO CAMBIARE I PERCORSI
	`define FILE_NAME         "mont512_code.bin"
	`define PROJECT_PATH      "D:/nuplus_repository/"
	`define MEMORY_BIN        {`PROJECT_PATH, "kernel/mont/", `FILE_NAME}

	//`define DISPLAY_CORE      // usato quando si vuole visualizzare le istruzioni del core
	//`define DISPLAY_MEMORY    // usato nel mango_dummy per fare il print della memoria finale

	`ifdef DISPLAY_CORE
		`define DISPLAY_CORE_VAR  nuplus_system.core_file
		`define DISPLAY_CORE_FILE {`PROJECT_PATH, "simulation_log/",`FILE_NAME,"/display_core.txt"}
	`endif

	`ifdef DISPLAY_MEMORY
		`define DISPLAY_MEMORY_VAR  nuplus_system.memory_file
		`define DISPLAY_MEMORY_FILE {"/home/luigi/nuplussinglecore/simulation_log/",`FILE_NAME,"/display_memory.txt"}
	`endif

`endif

`endif