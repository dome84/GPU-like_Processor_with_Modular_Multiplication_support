`ifndef __SYSTEM_DEFINES_SV
`define __SYSTEM_DEFINES_SV

`include "nuplus_define.sv"

`define AXI_DATA_WIDTH        32
`define AXI_DATA_BYTE_WIDTH   `AXI_DATA_WIDTH/8
`define UART_HM_WIDTH         32
`define BUS_MASTER            2
`define CLOCK_RATE            100000000 //50000000
`define UART_BAUD_RATE        5000000//921600
`define ID_DEVICE_LENGTH      32
`define ASYNC_FIFO_IP

//  -----------------------------------------------------------------------
//  -- Loader Defines
//  -----------------------------------------------------------------------

`define WRITE_MEMORY_COMMAND   				8'h01
`define READ_MEMORY_COMMAND    				8'h02
`define BOOT_CORE_COMMAND      				8'h03
`define ENABLE_THREAD_COMMAND 			 	8'h04
`define READ_ID_COMMAND        				8'h05
`define BARRIER_COMMAND        				8'h06
`define GET_INTERRUPTION_COMMAND			8'h07

`define WRITE_MEMORY_ACK      				8'h11
`define READ_MEMORY_ACK       				8'h12
`define BOOT_CORE_ACK        		 		8'h13
`define ENABLE_THREAD_ACK     				8'h14
`define READ_ID_ACK           				8'h15
`define BARRIER_ACK           				8'h16
`define GET_INTERRUPTION_ACK				8'h17

`define DSU_ENABLE_COMMAND	   				8'h21
`define	DSU_DISABLE_COMMAND	   				8'h22
`define	DSU_SET_BP_COMMAND	  			 	8'h23
`define DSU_SET_BP_MASK_COMMAND				8'h24
`define	DSU_ENABLE_SS_COMMAND  				8'h25
`define	DSU_DISABLE_SS_COMMAND 				8'h26
`define	DSU_ENABLE_SELECTION_TH_COMMAND		8'h27
`define	DSU_DISABLE_SELECTION_TH_COMMAND	8'h28
`define	DSU_READ_V_REG_COMMAND				8'h29
`define	DSU_READ_S_REG_COMMAND				8'h2a
`define	DSU_READ_BP_INFO_COMMAND			8'h2b			//capire se implementare, dipende da lldb se mantiene traccia di queste informazioni a livello software
`define	DSU_RESUME_COMMAND					8'h2c

//TODO: Luigi - da implementare il comando dall'uart
//`define	DSU_WRITE_V_REG_COMMAND				8'h2d
//`define	DSU_WRITE_S_REG_COMMAND				8'h2e

`define DSU_ENABLE_ACK	   					8'h31
`define	DSU_DISABLE_ACK	   					8'h32
`define	DSU_SET_BP_ACK	  			 		8'h33
`define DSU_SET_BP_MASK_ACK					8'h34
`define	DSU_ENABLE_SS_ACK  					8'h35
`define	DSU_DISABLE_SS_ACK 					8'h36
`define	DSU_ENABLE_SELECTION_TH_ACK			8'h37
`define	DSU_DISABLE_SELECTION_TH_ACK		8'h38
`define	DSU_READ_V_REG_ACK					8'h39
`define	DSU_READ_S_REG_ACK					8'h3a
`define	DSU_READ_BP_INFO_ACK				8'h3b
`define	DSU_RESUME_ACK						8'h3c

//TODO: Luigi - da implementare il comando dall'uart
//`define	DSU_WRITE_V_REG_ACK					8'h3d  
//`define	DSU_WRITE_S_REG_ACK					8'h3e

`define KERNEL_ENDED						8'h41
`define BREAKPOINT_CATCHED					8'h42

//  -----------------------------------------------------------------------
//  -- HOST and UART Defines
//  -----------------------------------------------------------------------

`ifdef SINGLE_CORE

`define HOST_COMMAND_NUM          32
`define HOST_COMMAND_WIDTH        ( $clog2( `HOST_COMMAND_NUM ) )

typedef enum logic [`HOST_COMMAND_WIDTH - 1 : 0] {
	//COMMAND FOR BOOT
	BOOT_COMMAND,
	ENABLE_THREAD,
	GET_CORE_STATUS,
	GET_END_OF_KERNEL,

	//COMMAND FOR SYNC
	SYNC_COMMAND,
	
	//RESPONSE FROM BOOT
	BOOT_ACK,
	THREAD_ENABLED,
	END_OF_KERNEL,
	CORE_STATUS,
	
	//COMMAND FOR DEBUGGER
	DSU_ENABLE_CMD,
	DSU_DISABLE_CMD,
	DSU_SET_BP_CMD,					
	DSU_SET_BP_MASK_CMD,
	DSU_ENABLE_BP_CMD,
	DSU_DISABLE_BP_CMD,
	DSU_ENABLE_SS_CMD,	//SS = Single Step 
	DSU_DISABLE_SS_CMD,
	DSU_ENABLE_SELECTION_TH_CMD,
	DSU_DISABLE_SELECTION_TH_CMD,
	DSU_READ_V_REG_CMD,				
	DSU_READ_S_REG_CMD,			
	DSU_WRITE_V_REG_CMD,				
	DSU_WRITE_S_REG_CMD,	
	DSU_READ_BP_INFO_CMD,			
	DSU_RESUME_CMD,				
	
	//RESPONSE FROM DEBUGGER
	DSU_BREAKPOINT_CATCHED,
	DSU_REG_VALUE_RSP,
	DSU_BP_VALUE_RSP,
	DSU_ACK_RSP
} host_messages_t;

typedef enum {
	STATE_WAIT_COMMAND_UART,
	STATE_RECEIVE_UART_DATA,
	//Boot
	STATE_MAKE_BOOT_CORE,
	STATE_MAKE_ENABLE_THREAD,
	STATE_BARRIER_COUNTER,
	STATE_CATCH_INTERRUPT,
	//Dsu
	STATE_DSU_TO_UART_ACK,
	STATE_ENABLE_THREAD_DSU,
	STATE_SET_BP_DSU,
	STATE_SET_BP_MASK_DSU,
	STATE_CATCH_BP_INFO_DSU,
	STATE_SEND_DATA_HOST,
	STATE_READ_REG_DSU,
	STATE_LOAD_SHIFT_REGISTER,
	STATE_WAIT_DSU_REGISTER
}h2c_cu_state_t;

typedef struct packed {
	logic [`ADDRESS_SIZE - 1 : 0] bp_address;
	logic [$clog2 (`THREAD_NUMB) - 1 : 0] thread_id;
}breakpoint_info_t;

`endif

typedef enum {

	STATE_WAIT_COMMAND,
	STATE_WAIT_HOST_ACK,
	STATE_RECEIVE_DATA_COMMAND,
	STATE_WAIT_BUFFER_DEQUEUE,
	STATE_SEND_COMMAND,
	STATE_SEND_DATA,
	STATE_WAIT_TX_UART,
	STATE_FINAL_ACK,
	
	//HRM Uart state
	STATE_CATCH_STOP,
	
	//DSU Uart state
	STATE_WAIT_DSU_ACK, 
	STATE_PULL_DSU_DATA
	//STATE_CATCH_BP

} uart_state_t;

typedef enum {

	STATE_WAIT_MEMORY_COMMAND,
	STATE_READ_MEMORY,
	STATE_WAIT_DATA,
	STATE_WRITE_DATA

} uart_memory_t;

typedef enum {

	STATE_WAIT_DSU_COMMAND

} uart_dsu_t;

`define CC_COMMAND_LENGTH                2
typedef logic [`CC_COMMAND_LENGTH - 1 : 0 ] cc_command_t;

typedef enum cc_command_t {
	CC_REPLACEMENT,
	CC_UPDATE_INFO,
	CC_UPDATE_INFO_DATA
} cc_commands_enum_t;

//-----------

typedef enum {
	START_BOOT,
	IDLE_BOOT,
	NOTIFY_BOOT,
	NOTIFY_THREAD_EN,
	NOTIFY_STATUS
}boot_state_t;

typedef struct packed {                        
	address_t hi_job_pc;                       //32
	logic hi_job_valid;                        //1
	thread_id_t hi_job_thread_id;              //2
	logic [`THREAD_NUMB - 1 : 0] hi_thread_en; //4
	host_messages_t message;                   //8
} boot_message_t;


//  -----------------------------------------------------------------------
//  -- AMBA AXI Defines
//  -----------------------------------------------------------------------

// AMBA AXI and ACE Protocol Specification, rev E, Table A3-3
typedef enum logic[1:0] {
	AXI_BURST_FIXED = 2'b00,
	AXI_BURST_INCR  = 2'b01,
	AXI_BURST_WRAP  = 2'b10
} axi_burst_type_t;


// AMBA AXI-4 bus interface
interface axi4_interface;
    // Write address channel (Table A2-2)
    logic [31:0] m_awaddr;
    logic [7:0] m_awlen;
    logic [2:0] m_awsize;
    axi_burst_type_t m_awburst;
    logic [3:0] m_awcache;
    logic m_awvalid;
    logic s_awready;

    // Write data channel (Table A2-3)
    logic [`AXI_DATA_WIDTH - 1:0] m_wdata;
    logic [`AXI_DATA_WIDTH / 8 - 1:0] m_wstrb;
    logic m_wlast;
    logic m_wvalid;
    logic s_wready;

    // Write response channel (Table A2-4)
    logic s_bvalid;
    logic m_bready;

    // Read address channel (Table A2-5)
    logic [31:0] m_araddr;
    logic [7:0] m_arlen;
    logic [2:0] m_arsize;
    axi_burst_type_t m_arburst;
    logic [3:0] m_arcache;
    logic m_arvalid;
    logic s_arready;

    // Read data channel (Table A2-6)
    logic [`AXI_DATA_WIDTH - 1:0] s_rdata;
    logic s_rvalid;
    logic m_rready;

    modport master(input s_awready, s_wready, s_bvalid, s_arready, s_rvalid, s_rdata,
        output m_awaddr, m_awlen, m_awvalid, m_wdata, m_wlast, m_wvalid, m_bready, m_araddr, m_arlen,
        m_arvalid, m_rready, m_awsize, m_awburst, m_wstrb, m_arsize, m_arburst, m_awcache, m_arcache);
    modport slave(input m_awaddr, m_awlen, m_awvalid, m_wdata, m_wlast, m_wvalid, m_bready, m_araddr,
        m_arlen, m_arvalid, m_rready, m_awsize, m_awburst, m_wstrb, m_arsize, m_arburst,
        m_awcache, m_arcache,
        output s_awready, s_wready, s_bvalid, s_arready, s_rvalid, s_rdata);
endinterface

`endif