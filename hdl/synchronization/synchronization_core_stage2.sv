`include "synchronization_defines.sv"
module synchronization_core_stage2 # (
		`ifdef DISPLAY_SYNC
	parameter TILE_ID = 0
	`endif
	)
	(
		input clk,
		input reset,

		//SYNC_STAGE1
		output barrier_t ss2_id_barrier_pending,

		input sync_info_t ss1_barrier,
		input logic       ss1_valid_mess,

		//SYNC_STAGE3
		output logic       ss2_barrier_valid,
		output sync_info_t ss2_barrier,
		input  sync_info_t ss3_barrier,
		input  logic       ss3_valid_barrier,
		output logic ss2_valid_mem,
		output barrier_data_t ss2_data_barrier_read,
		input  barrier_data_t ss3_data_barrier_write

	);
	sync_info_t                        barrier_info_tmp;
	logic                              ss2_barrier_valid_tmp;
	logic          [`BARRIER_NUMB_FOR_TILE-1:0] valid_mem;

	always_ff @ ( posedge clk) begin
		ss2_valid_mem <= valid_mem[ss1_barrier.id_barrier[$clog2(`BARRIER_NUMB_FOR_TILE)-1:0]];
			
	end

always_ff @ ( posedge clk, posedge reset) begin
	if (reset) 
			valid_mem             <= 0;
	else 
		if (ss3_valid_barrier)
			valid_mem[ss3_barrier.id_barrier[$clog2(`BARRIER_NUMB_FOR_TILE)-1:0]] <= 1;
	end

	always_ff @ ( posedge clk, posedge reset ) begin
		if ( reset ) begin
			barrier_info_tmp <= 0;
			ss2_barrier_valid_tmp <= 0;
		
		end else begin
			barrier_info_tmp<=ss1_barrier;
			ss2_barrier_valid_tmp<=ss1_valid_mess;
			end
	end

	assign ss2_barrier            = barrier_info_tmp;
	assign ss2_barrier_valid      = ss2_barrier_valid_tmp;
	assign ss2_id_barrier_pending = barrier_info_tmp.id_barrier;//DA Rivedere, non so se va prima o dopo il banco di Flip-Flop
	//BRAM
	memory_bank_2r1w #
	(
		.COL_WIDTH    ( $bits( barrier_data_t ) ),
		.NB_COL       ( 1                       ),
		.SIZE         ( `BARRIER_NUMB_FOR_TILE  ),
		.WRITE_FIRST1 ( "TRUE"                  ),
		.WRITE_FIRST2 ( "FALSE"                 )
	)
	tag_sram
	(
		.clock        ( clk                    ),
		.read1_address( ss1_barrier.id_barrier[$clog2(`BARRIER_NUMB_FOR_TILE)-1:0] ),
		.read1_data   ( ss2_data_barrier_read  ),
		.read1_enable ( ss1_valid_mess         ),
		.read2_address( 0                      ),
		.read2_data   (                        ),
		.read2_enable ( 0                      ),
		.write_address( ss3_barrier.id_barrier[$clog2(`BARRIER_NUMB_FOR_TILE)-1:0] ),
		.write_data   ( ss3_data_barrier_write ),
		.write_enable ( ss3_valid_barrier      )
		);
	
`ifdef DISPLAY_SYNC
	always_ff @(posedge clk) begin
		if(ss1_valid_mess || ss3_valid_barrier || ss2_barrier_valid)begin
			$fdisplay ( `DISPLAY_SYNC_VAR, "=======================" ) ;
			$fdisplay ( `DISPLAY_SYNC_VAR, "Sync_Core-Stage2 - [Tile - %.16d ] [Time %.16d]",TILE_ID,  $time ( ) ) ;
			if(ss1_valid_mess) begin
				$fdisplay ( `DISPLAY_SYNC_VAR, "=========" ) ;
				$fdisplay ( `DISPLAY_SYNC_VAR, "From Stage 1 Before Read" ) ;
				if(ss1_barrier.barrier_type==SETUP) begin
					$fdisplay ( `DISPLAY_SYNC_VAR, "Setup - [Id %.16d]  [cnt %.16d]", ss1_barrier.id_barrier, ss1_barrier.cnt_setup ) ;
				end else if (ss1_barrier.barrier_type==ACCOUNT)
					$fdisplay ( `DISPLAY_SYNC_VAR, "Account - [Id %.16d]  [Tile_src %.16d]", ss1_barrier.id_barrier, ss1_barrier.tile_id_source ) ;				
			end
			if(ss3_valid_barrier) begin
				$fdisplay ( `DISPLAY_SYNC_VAR, "=========" ) ;
				$fdisplay ( `DISPLAY_SYNC_VAR, "From Stage 3" ) ;
				if(ss3_barrier.barrier_type==SETUP)
					$fdisplay ( `DISPLAY_SYNC_VAR, "Setup - [Id %.16d]  [cnt %.16d]", ss3_barrier.id_barrier, ss3_barrier.cnt_setup ) ;
				else if (ss3_barrier.barrier_type==ACCOUNT)
					$fdisplay ( `DISPLAY_SYNC_VAR, "Account - [Id %.16d]  [Tile_src %.16d]", ss3_barrier.id_barrier, ss3_barrier.tile_id_source ) ;
				else if (ss3_barrier.barrier_type==RELEASE)
					$fdisplay ( `DISPLAY_SYNC_VAR, "Release - [Id %.16d]  [Tile_src %.16d]", ss3_barrier.id_barrier, ss3_barrier.tile_id_source ) ;
				
				$fdisplay ( `DISPLAY_SYNC_VAR, "DataWrite - [cnt: %.16d] [mask_slave: %.16d]  [is_setup: %.16d] ", ss3_data_barrier_write.cnt,ss3_data_barrier_write.mask_slave , ss3_data_barrier_write.is_setup) ;
				
			end
			if(ss2_barrier_valid) begin
				$fdisplay ( `DISPLAY_SYNC_VAR, "=========" ) ;
				$fdisplay ( `DISPLAY_SYNC_VAR, "To Stage 3 After Read" ) ;
				if(ss2_barrier.barrier_type==SETUP) begin
					$fdisplay ( `DISPLAY_SYNC_VAR, "Setup - [Id %.16d]  [cnt %.16d]", ss2_barrier.id_barrier, ss2_barrier.cnt_setup ) ;
				end else if (ss2_barrier.barrier_type==ACCOUNT)
					$fdisplay ( `DISPLAY_SYNC_VAR, "Account - [Id %.16d]  [Tile_src %.16d]", ss2_barrier.id_barrier, ss2_barrier.tile_id_source ) ;				
				if(ss2_valid_mem)
					$fdisplay ( `DISPLAY_SYNC_VAR, "DataRead - [cnt: %.16d] [mask_slave: %.16d]  [is_setup: %.16d] ", ss2_data_barrier_read.cnt,ss2_data_barrier_read.mask_slave , ss2_data_barrier_read.is_setup) ;
				else
					$fdisplay ( `DISPLAY_SYNC_VAR, "DataRead - NOT VALID(first_read)");		
				
			end
		end
		
		end
`endif
endmodule