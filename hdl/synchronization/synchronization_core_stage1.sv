`include "synchronization_defines.sv"
`include "user_define.sv"
module synchronization_core_stage1 # (
	`ifdef DISPLAY_SYNC
	parameter TILE_ID = 0
	`endif
	)
	(
		input                 clk,
		input                 reset,
		// NETWORK INTERFACE
		//Setup
		input  sync_setup_t   ni_setup_mess,
		input  logic          ni_setup_mess_valid,
		output logic          ss1_setup_consumed,
		//Account
		input  sync_account_t ni_account_mess,
		input  logic          ni_account_mess_valid,
		output logic          ss1_account_consumed,
		//Release
		input  logic          ni_release_almost_full,


		//SYNC_STAGE2
		input  logic 	   ss2_valid_pending,
		input  barrier_t   ss2_id_barrier_pending,
		output sync_info_t ss1_barrier,
		output logic       ss1_valid_mess,

		//SYNC_STAGE3
		input barrier_t ss3_id_barrier,
		input logic 	ss3_valid_id_barrier


	);
	barrier_t                              output_id_barrier;
	synch_type                             output_sync_type;
	logic                                  output_barrier_valid;
	logic                                  can_issue_setup;
	logic                                  can_issue_account;
	logic                                  release_fifo_full;
	logic      [$clog2( `TILE_COUNT *`THREAD_NUMB)-1:0] output_cnt_max;
	logic      [$clog2( `TILE_COUNT )-1:0] output_id_tile_source;
	logic                                  update_consumed_account, update_consumed_setup;
always_comb begin
	 can_issue_setup <= ni_setup_mess_valid & (~( (ss2_id_barrier_pending && ni_setup_mess.id_barrier) | ss2_valid_pending )) & (~( (ss1_barrier.id_barrier && ni_setup_mess.id_barrier) | ss1_valid_mess)) & (~( (ss3_id_barrier && ni_setup_mess.id_barrier) | ss3_valid_id_barrier)) & ~ni_release_almost_full;
	 can_issue_account <= ni_account_mess_valid & (~( (ss2_id_barrier_pending && ni_setup_mess.id_barrier) | ss2_valid_pending )) & ( ~( (ss1_barrier.id_barrier && ni_setup_mess.id_barrier) | ss1_valid_mess)) & (~( (ss3_id_barrier && ni_setup_mess.id_barrier) | ss3_valid_id_barrier)) & ~ni_release_almost_full;
end
//	assign can_issue_account = ni_account_mess_valid & !( ss2_id_barrier_pending==ni_account_mess.id_barrier ) & !( ss1_barrier.id_barrier==ni_account_mess.id_barrier ) & !( ss3_id_barrier==ni_account_mess.id_barrier ) & !ni_release_almost_full;

	always_comb begin: arbiter

		output_id_barrier <= 0;
		output_sync_type  <= ZERO;
		update_consumed_setup   = 1'b0;
		update_consumed_account = 1'b0;
		//ss1_setup_consumed =1'b0;
		//ss1_account_consumed =1'b0;
		output_barrier_valid    = 1'b0;
		output_cnt_max        <= 0;
		output_id_tile_source <= 0;

		if ( can_issue_setup )begin
			output_barrier_valid = 1'b1;
			output_id_barrier <= ni_setup_mess.id_barrier;
			output_cnt_max    <= ni_setup_mess.cnt_setup;
			output_sync_type  <= SETUP;
			update_consumed_setup = 1'b1;
		//ss1_setup_consumed =1'b1;

		end else if ( can_issue_account ) begin
			output_barrier_valid = 1'b1;
			output_id_barrier     <= ni_account_mess.id_barrier;
			output_id_tile_source <= ni_account_mess.tile_id_source;
			output_sync_type      <= ACCOUNT;
			update_consumed_account = 1'b1;
		//ss1_account_consumed =1'b1;

		end
	end

	always_ff @( posedge clk ) begin
		ss1_barrier.id_barrier     <= output_id_barrier;
		ss1_barrier.barrier_type   <= output_sync_type;
		ss1_barrier.cnt_setup      <= output_cnt_max;
		ss1_barrier.tile_id_source <= output_id_tile_source;

	end


	always_ff @( posedge clk, posedge reset )
		if ( reset ) begin
			ss1_valid_mess <= 1'b0;
		//ss1_setup_consumed<=0;
		//ss1_account_consumed<=0;
		end else begin
			ss1_valid_mess <= output_barrier_valid;
		//ss1_account_consumed <=update_consumed_account;
		//ss1_setup_consumed <=update_consumed_setup;

		//if(update_consumed_account) begin
		//  update_consumed_account=1'b0;
		//  ss1_account_consumed =1'b1;
		//end if(update_consumed_setup) begin
		//  update_consumed_setup=1'b0;
		//  ss1_setup_consumed =1'b1;
		//end


		end
	assign ss1_account_consumed = update_consumed_account;
	assign ss1_setup_consumed   = update_consumed_setup;

`ifdef DISPLAY_SYNC
	always_ff @(posedge clk) begin
		if(ni_setup_mess_valid || ni_account_mess_valid)begin
			$fdisplay ( `DISPLAY_SYNC_VAR, "=======================" ) ;
			$fdisplay ( `DISPLAY_SYNC_VAR, "Sync_Core-Stage1 - [Tile - %.16d]  [Time %.16d]",TILE_ID, $time ( ) ) ;
			if(ni_setup_mess_valid)
				$fdisplay ( `DISPLAY_SYNC_VAR, "Setup Request - [Id %.16d]  [cnt %.16d]  [is_master %.16d]", ni_setup_mess.id_barrier, ni_setup_mess.cnt_setup, ni_setup_mess.is_master ) ;
			if(ni_account_mess_valid)
				$fdisplay ( `DISPLAY_SYNC_VAR, "Account Request - [Id %.16d]  [tile_src %.16d]", ni_account_mess.id_barrier, ni_account_mess.tile_id_source ) ;
			if(can_issue_setup)
				$fdisplay ( `DISPLAY_SYNC_VAR, "Setup Schedule - [Id %.16d]  [cnt %.16d]  [is_master: TODO]", output_id_barrier,output_cnt_max ) ;
			else if(can_issue_account)
				$fdisplay ( `DISPLAY_SYNC_VAR, "Account Schedule - [Id %.16d]  [tile_src %.16d]", output_id_barrier, output_id_tile_source ) ;	
			
		end
	end
`endif
endmodule
