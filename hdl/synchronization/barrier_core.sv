`include "nuplus_define.sv"
`include "user_define.sv"
`include "synchronization_defines.sv"

/*
 * Da fabio: questo modulo è fatto a 3 stadi di pipe:
 * 	1- aspetta che una richiesta di sync ACCOUNT dal core arriva, e si mette in attesa del release
 * 	1.a - un thread può sollevare una richiesta di account se non ci sono load/store pendenti e la scoreboard non è vuota
 * 	2 - viene scelto un thread tra quelli che vogliono fare ACCOUNT ed eliminato tra quelli eleggibili
 * 	2.a- sulla rete viene inviato un messaggio di ACCOUNT con i dati della richiesta del core precedentemente salvata
 * 	3 - quando arriva un messaggio dalla rete di RELEASE della barrier, si elimina l'attesa
 */

module barrier_core # (
		parameter TILE_ID = 0
	)
	(
		input                                                reset,
		input                                                clk,
		//Operand Fetch
		input                                                opf_valid,                    // controllare il bit dell'istruzione
		input  instruction_decoded_t                         opf_inst_scheduled,
		// To Core
		output logic                 [ `THREAD_NUMB - 1 : 0] release_val,
		//Id_Barrier
		input  vec_reg_size_t                                opf_fetched_op0,
		//Destination Barrier
		input  vec_reg_size_t                                opf_fetched_op1,
		//Load Store Unit
		input  logic                 [ `THREAD_NUMB - 1 : 0] no_load_store_pending,
		input  logic                 [ `THREAD_NUMB - 1 : 0] scoreboard_empty,
		`ifdef PERFORMANCE_SYNC
		output logic                 [`THREAD_NUMB - 1 : 0]  counter_sync_send,
		`endif

		//to Network Interface
		output                                               c2n_account_valid,
		output service_c2n_message_t                         c2n_account_message,
		`ifndef SINGLE_CORE
		output dest_valid_t                                  c2n_account_destination_valid,
		`endif

		input  logic                                         network_available,
		//From Network Interface
		input  service_c2n_message_t                         n2c_release_message,
		input                                                n2c_release_valid,
		output logic                                         n2c_mes_service_consumed

	);
	//
	//Stage 1 Barrier Core
	//

	// Account Generate
	barrier_t                                              barrier_release;
	logic                 [$clog2( `THREAD_NUMB ) - 1 : 0] account_scheduled_id;
	logic                 [`THREAD_NUMB - 1 : 0]           is_valid, mask_release, release_bit_attend;
	service_c2n_message_t [`THREAD_NUMB - 1 : 0]           c2n_account_message_tmp;
	logic                 [`THREAD_NUMB - 1 : 0]           can_account, valid_tmp, release_val_tmp;
	logic                 [`THREAD_NUMB - 1 : 0]           is_thread_account_mask;
	`ifndef SINGLE_CORE
	tile_address_t 		  [`THREAD_NUMB - 1 : 0]		   destination; //1
	dest_valid_t           [`THREAD_NUMB - 1 : 0]                                one_hot_destination_valid;
	`endif

	generate
		genvar thread_id;
		for ( thread_id = 0; thread_id < `THREAD_NUMB; thread_id++ ) begin : barrier_entries

			assign is_valid[thread_id]     = opf_valid & opf_inst_scheduled.is_control & opf_inst_scheduled.thread_id==thread_id & opf_inst_scheduled.pipe_sel==PIPE_SYNC;
			assign can_account[thread_id]  = valid_tmp[thread_id] & no_load_store_pending[thread_id] & ~scoreboard_empty[thread_id] & network_available;


			always_ff @( posedge clk, posedge reset )begin
				if ( reset ) begin
					valid_tmp[thread_id]               <= 1'b0;
					release_bit_attend[thread_id]      <= 0;
					release_val[thread_id]             <= 1;
					c2n_account_message_tmp[thread_id] <= 0;
				end
				else begin
					release_val[thread_id]             <=release_val_tmp[thread_id];
					if ( is_valid[thread_id] ) begin
						`ifdef DIRECTORY_BARRIER
						destination[thread_id] <= opf_fetched_op0[0][$bits( barrier_t )-1:$clog2(`BARRIER_NUMB_FOR_TILE)];
						`else
						`ifndef SINGLE_CORE
						destination[thread_id] <= opf_fetched_op1[0];
						`endif
						`endif
						c2n_account_message_tmp[thread_id].message_type                                                             <= SYNC;
						c2n_account_message_tmp[thread_id].data[1:0]                                                                <= ACCOUNT;
						c2n_account_message_tmp[thread_id].data[$clog2( `TILE_COUNT )+1:2]                                          <= TILE_ID;
						`ifndef SINGLE_CORE
						c2n_account_message_tmp[thread_id].data[$bits( barrier_t )+$clog2( `TILE_COUNT )+1:$clog2( `TILE_COUNT )+2] <= opf_fetched_op0[0][$bits( barrier_t )-1:0];
						`else 
						c2n_account_message_tmp[thread_id].data[$bits( barrier_t )+$clog2( `TILE_COUNT ):$clog2( `TILE_COUNT )+1] <= opf_fetched_op0[0][$bits( barrier_t )-1:0];
						`endif
						valid_tmp[thread_id]                                                                                        <= 1'b1;
						release_bit_attend[thread_id]                                                                               <= 1'b1;

					end else if ( is_thread_account_mask[thread_id] )begin
						valid_tmp[thread_id]                                                                                        <= 0;
						c2n_account_message_tmp[thread_id]                                                                          <= c2n_account_message_tmp[thread_id];
						release_bit_attend[thread_id]                                                                               <= 1'b1;

					end else if ( release_val_tmp[thread_id] ) begin
						c2n_account_message_tmp[thread_id]                                                                          <= c2n_account_message_tmp[thread_id];
						release_bit_attend[thread_id]                                                                               <= 1'b0;
					end else begin
						c2n_account_message_tmp[thread_id]                                                                          <= c2n_account_message_tmp[thread_id];
						valid_tmp[thread_id]                                                                                        <= valid_tmp[thread_id];
						release_bit_attend[thread_id]                                                                               <= release_bit_attend[thread_id];

					end

				end

			end


			`ifndef SINGLE_CORE
			assign mask_release[thread_id] = ( barrier_release == c2n_account_message_tmp[thread_id].data[$bits( barrier_t )+$clog2( `TILE_COUNT )+1:$clog2( `TILE_COUNT )+2] ) & n2c_release_valid && release_bit_attend[thread_id];
			`else 
			assign mask_release[thread_id] = ( barrier_release == c2n_account_message_tmp[thread_id].data[$bits( barrier_t )+$clog2( `TILE_COUNT ):$clog2( `TILE_COUNT )+1] ) & n2c_release_valid && release_bit_attend[thread_id];
			`endif
			always_comb begin
				release_val_tmp[thread_id] = 1;

				if ( release_bit_attend[thread_id] && ~mask_release[thread_id] )
					release_val_tmp[thread_id] = 0;
			end

`ifndef SINGLE_CORE
		idx_to_oh #(
		.NUM_SIGNALS( $bits( dest_valid_t ) ),
		.DIRECTION  ( "LSB0"                ),
		.INDEX_WIDTH( $clog2( `TILE_COUNT ) )
	)
	u_idx_to_oh (
		.index  ( destination[thread_id] ),
		.one_hot( one_hot_destination_valid[thread_id])
		);
`endif

		end

	endgenerate


	//Release Receive
	always_comb begin
		n2c_mes_service_consumed = 1'b0;
		if ( n2c_release_valid ) begin
			barrier_release          = n2c_release_message.data[$bits( barrier_t )+1 : 2];
			n2c_mes_service_consumed = 1'b1;
		end else begin
			n2c_mes_service_consumed = 1'b0;
			barrier_release          = 0;
		end
	end


	//
	// Stage 2 Barrier Core
	//
	//Schedule Account message between Thread. Round Robin Scheduler

	rr_arbiter #(
		.NUM_REQUESTERS( `THREAD_NUMB )
	)
	rr_arbiter (
		.clk       ( clk                    ),
		.reset     ( reset                  ),
		.request   ( can_account            ),
		.update_lru( |can_account           ),
		.grant_oh  ( is_thread_account_mask )
	);

	oh_to_idx #(
		.NUM_SIGNALS( `THREAD_NUMB         ),
		.DIRECTION  ( "LSB0"               ),
		.INDEX_WIDTH( $bits( thread_id_t ) )
	)
	oh_to_idx (
		.one_hot( is_thread_account_mask ),
		.index  ( account_scheduled_id   )
	);

//	dest_valid_t                                           one_hot_destination_valid;


//	idx_to_oh #(
//		.NUM_SIGNALS( $bits( dest_valid_t ) ),
//		.DIRECTION  ( "LSB0"                ),
//		.INDEX_WIDTH( $clog2( `TILE_COUNT ) )
//	)
//	u_idx_to_oh (
//		.index  ( destination ),
//		.one_hot( one_hot_destination_valid                                 )
//	);
	//
	// Stage 3 Barrier Core
	//
	//Send Account Message
	assign c2n_account_valid             = can_account[account_scheduled_id];
	assign c2n_account_message           = c2n_account_message_tmp[account_scheduled_id];
	`ifndef SINGLE_CORE
	assign c2n_account_destination_valid = one_hot_destination_valid[account_scheduled_id];
	`endif
	`ifdef PERFORMANCE_SYNC
	assign counter_sync_send             = release_bit_attend;
	`endif

`ifdef DISPLAY_SYNC

	always_ff @(posedge clk) begin
		//  for ( int thread_idx = 0; thread_idx < `THREAD_NUMB; thread_idx++ ) begin
		if(is_valid[0])begin
			$fdisplay ( `DISPLAY_BARRIER_VAR, "=======================" ) ;
			$fdisplay ( `DISPLAY_BARRIER_VAR, "Barrier_Core [Tile_id %.16d ] - [Time %.16d]", TILE_ID,$time ( ) ) ;
			`ifdef DIRECTORY_BARRIER
			$fdisplay ( `DISPLAY_BARRIER_VAR, "Barrier Arrive [Thread: 0]  [id_barrier %.16d]  [dest %.16d]", opf_fetched_op0[0][$bits( barrier_t )-1:0] , opf_fetched_op0[0][$bits( barrier_t )-1:$clog2(`BARRIER_NUMB_FOR_TILE)]);
			`else
			$fdisplay ( `DISPLAY_BARRIER_VAR, "Barrier Arrive [Thread: 0]  [id_barrier %.16d]  [dest %.16d]", opf_fetched_op0[0][$bits( barrier_t )-1:0] , opf_fetched_op1[0][$clog2( `TILE_COUNT )-1:0]);
			`endif
			$fdisplay ( `DISPLAY_BARRIER_VAR, " [Can_Account: %.16d ]", can_account[0]);
		end
		if(is_valid[1])begin
			$fdisplay ( `DISPLAY_BARRIER_VAR, "=======================" ) ;
			$fdisplay ( `DISPLAY_BARRIER_VAR, "Barrier_Core [Tile_id %.16d ] - [Time %.16d]", TILE_ID,$time ( ) ) ;
			$fdisplay ( `DISPLAY_BARRIER_VAR, "Barrier Arrive [Thread: 1]  [id_barrier %.16d]  [dest %.16d]", c2n_account_message_tmp[1].data[$bits( barrier_t )+$clog2( `TILE_COUNT )+1:$clog2( `TILE_COUNT )+2] ,destination[1]);
			$fdisplay ( `DISPLAY_BARRIER_VAR, " [Can_Account: %.16d ]", can_account[1]);
		end
		if(is_valid[2])begin
			$fdisplay ( `DISPLAY_BARRIER_VAR, "=======================" ) ;
			$fdisplay ( `DISPLAY_BARRIER_VAR, "Barrier_Core [Tile_id %.16d ] - [Time %.16d]", TILE_ID,$time ( ) ) ;
			$fdisplay ( `DISPLAY_BARRIER_VAR, "Barrier Arrive [Thread: 2]  [id_barrier %.16d]  [dest %.16d]", c2n_account_message_tmp[2].data[$bits( barrier_t )+$clog2( `TILE_COUNT )+1:$clog2( `TILE_COUNT )+2] ,destination[2]);
			$fdisplay ( `DISPLAY_BARRIER_VAR, " [Can_Account: %.16d ]", can_account[2]);
		end
		if(is_valid[3])begin
			$fdisplay ( `DISPLAY_BARRIER_VAR, "=======================" ) ;
			$fdisplay ( `DISPLAY_BARRIER_VAR, "Barrier_Core [Tile_id %.16d ] - [Time %.16d]", TILE_ID,$time ( ) ) ;
			$fdisplay ( `DISPLAY_BARRIER_VAR, "Barrier Arrive [Thread: 3]  [id_barrier %.16d]  [dest %.16d]", c2n_account_message_tmp[3].data[$bits( barrier_t )+$clog2( `TILE_COUNT )+1:$clog2( `TILE_COUNT )+2] ,destination[3]);
			$fdisplay ( `DISPLAY_BARRIER_VAR, " [Can_Account: %.16d ]", can_account[3]);
		end
		if(can_account[account_scheduled_id]) begin
			$fdisplay ( `DISPLAY_BARRIER_VAR, "=======================" ) ;
			$fdisplay ( `DISPLAY_BARRIER_VAR, "Barrier_Core [Tile_id %.16d ] - [Time %.16d]", TILE_ID,$time ( ) ) ;
			$fdisplay ( `DISPLAY_BARRIER_VAR, "Account Send [Thread: %.16d ]  [id_barrier %.16d]  [dest %.16d]",account_scheduled_id, c2n_account_message.data[$bits( barrier_t )+$clog2( `TILE_COUNT )+1:$clog2( `TILE_COUNT )+2] ,destination);
		end
		if(n2c_release_valid) begin
			$fdisplay ( `DISPLAY_BARRIER_VAR, "=======================" ) ;
			$fdisplay ( `DISPLAY_BARRIER_VAR, "Barrier_Core [Tile_id %.16d ] - [Time %.16d]", TILE_ID,$time ( ) ) ;
			$fdisplay ( `DISPLAY_BARRIER_VAR, "Release Arrive [Thread: %.16d ]  [id_barrier %.16d]  ",account_scheduled_id, n2c_release_message.data[$bits( barrier_t )+1 : 2] );
		end


	end
`endif

endmodule