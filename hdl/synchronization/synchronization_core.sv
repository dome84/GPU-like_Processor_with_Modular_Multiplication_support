`include "synchronization_defines.sv"

module synchronization_core # (
	`ifdef DISPLAY_SYNC
	parameter TILE_ID = 0
	`endif
	)
	(
		input                 clk,
		input                 reset,
		// NETWORK INTERFACE
		`ifdef SINGLE_CORE
		output logic 	 	  job_terminated,
		output logic		  account_available,
		output logic		  setup_available,
		`endif
		//Setup
		input  sync_setup_t   ni_setup_mess,
		input  logic          ni_setup_mess_valid,
		output logic          ss1_setup_consumed,
		//Account
		input  sync_account_t ni_account_mess,
		input  logic          ni_account_mess_valid,
		output logic          ss1_account_consumed,
		//Release
		output sync_release_t ss3_release_mess,
		output logic          ss3_release_valid,
		input  logic          ni_available                  //controllare questo segnale con MIRKO
	);


	//signal Stage1
	barrier_t   ss2_id_barrier_pending;
	sync_info_t ss1_barrier;
	logic       ss1_valid_mess;
	barrier_t   ss3_id_barrier;

	//signal Stage2
	logic          ss2_barrier_valid;
	sync_info_t    ss2_barrier;
	sync_info_t    ss3_barrier;
	logic          ss3_valid_barrier;
	logic          ss2_valid_mem;
	barrier_data_t ss2_data_barrier_read;
	barrier_data_t ss3_data_barrier_write;


//  -----------------------------------------------------------------------
//  -- Synchronization Core- FIFO Setup/Account/Release
//  -----------------------------------------------------------------------

	logic        ni_setup_mess_valid_fifo,empty_setup, almost_full_setup, ss1_setup_consumed_fifo, setup_consumed_fifo;
	sync_setup_t ni_setup_mess_fifo;

	sync_fifo #(
		.WIDTH                 ( $bits( sync_setup_t ) ),
		.SIZE                  ( 8                     ),
		.ALMOST_FULL_THRESHOLD ( 7                     ),
		.ALMOST_EMPTY_THRESHOLD( 1                     )
	)
	setup_sync_fifo (
		.almost_empty(                     ),
		.almost_full ( almost_full_setup   ),
		.clk         ( clk                 ),
		.dequeue_en  ( setup_consumed_fifo ), //TO DO
		.empty       ( empty_setup         ),
		.enqueue_en  ( ni_setup_mess_valid & (~almost_full_setup)),
		.flush_en    ( 0                   ), //flush is synchronous, unlike reset
		.full        (                     ),
		.reset       ( reset               ),
		.value_i     ( ni_setup_mess       ),
		.value_o     ( ni_setup_mess_fifo  )
	);
	assign ni_setup_mess_valid_fifo = !empty_setup;
	assign ss1_setup_consumed       = (~almost_full_setup) & ni_setup_mess_valid;
	assign setup_consumed_fifo      = ss1_setup_consumed_fifo & (~empty_setup);
	assign setup_available = ~almost_full_setup;





	logic          ni_account_mess_valid_fifo, empty_account, almost_full_account, ss1_account_consumed_fifo, account_consumed_fifo;
	sync_account_t ni_account_mess_fifo;

	sync_fifo #(
		.WIDTH                 ( $bits( sync_account_t ) ),
		.SIZE                  ( 8                       ),
		.ALMOST_FULL_THRESHOLD ( 7                       ),
		.ALMOST_EMPTY_THRESHOLD( 1                       )
	)
	account_sync_fifo (
		.almost_empty(                       ),
		.almost_full ( almost_full_account   ),
		.clk         ( clk                   ),
		.dequeue_en  ( account_consumed_fifo ), //TO DO
		.empty       ( empty_account         ),
		.enqueue_en  ( ni_account_mess_valid &(~almost_full_account) ),
		.flush_en    ( 0                     ), //flush is synchronous, unlike reset
		.full        (                       ),
		.reset       ( reset                 ),
		.value_i     ( ni_account_mess       ),
		.value_o     ( ni_account_mess_fifo  )
	);
	assign ni_account_mess_valid_fifo = !empty_account;
	assign ss1_account_consumed       = (~almost_full_account) & ni_account_mess_valid;
	assign account_consumed_fifo      = ss1_account_consumed_fifo;
	`ifdef SINGLE_CORE
	assign account_available= ~almost_full_account;
	`endif


	logic          ni_release_mess_valid_fifo, empty_release, almost_full_release,deq_rel;
	sync_release_t sc_release_mess_fifo;

	sync_fifo #(
		.WIDTH                 ( $bits( sync_release_t ) ),
		.SIZE                  ( 8                       ),
		.ALMOST_FULL_THRESHOLD ( 4                       ),
		.ALMOST_EMPTY_THRESHOLD( 1                       )
	)
	release_sync_fifo (
		.almost_empty(                            ),
		.almost_full ( almost_full_release        ),
		.clk         ( clk                        ),
		.dequeue_en  ( deq_rel                    ),
		.empty       ( empty_release              ),
		.enqueue_en  ( ni_release_mess_valid_fifo ),
		.flush_en    ( 0                          ), //flush is synchronous, unlike reset
		.full        (                            ),
		.reset       ( reset                      ),
		.value_i     ( sc_release_mess_fifo       ),
		.value_o     ( ss3_release_mess           )
	);
	assign ss3_release_valid             = !empty_release;
	assign deq_rel = !empty_release & ni_available;
//  -----------------------------------------------------------------------
//  -- Synchronization Core - Stage1
//  -----------------------------------------------------------------------

	synchronization_core_stage1 # ( 
		`ifdef DISPLAY_SYNC
		.TILE_ID(TILE_ID)
		`endif
		)
	Stage1(
		.clk                   ( clk                        ),
		.reset                 ( reset                      ),
		//Network Interface
		.ni_setup_mess         ( ni_setup_mess_fifo         ),
		.ni_setup_mess_valid   ( ni_setup_mess_valid_fifo   ),
		.ss1_setup_consumed    ( ss1_setup_consumed_fifo    ),
		.ni_account_mess       ( ni_account_mess_fifo       ),
		.ni_account_mess_valid ( ni_account_mess_valid_fifo ),
		.ss1_account_consumed  ( ss1_account_consumed_fifo  ),
		.ni_release_almost_full( almost_full_release        ),
		//Sync_Stage2
		.ss2_id_barrier_pending( ss2_id_barrier_pending     ),
		.ss2_valid_pending	   ( ss2_barrier_valid			),
		.ss1_barrier           ( ss1_barrier                ),
		.ss1_valid_mess        ( ss1_valid_mess             ),
		//Sync_Stage3
		.ss3_id_barrier        ( ss3_id_barrier             ),
		.ss3_valid_id_barrier  ( ss3_valid_barrier			)
	);

//  -----------------------------------------------------------------------
//  -- Synchronization Core - Stage2
//  -----------------------------------------------------------------------

	synchronization_core_stage2 # (
`ifdef DISPLAY_SYNC
		.TILE_ID(TILE_ID)
		`endif		
)
	Stage2(
		.clk                   ( clk                    ),
		.reset                 ( reset                  ),
		//Sync_Stage1
		.ss2_id_barrier_pending( ss2_id_barrier_pending ),
		.ss1_barrier           ( ss1_barrier            ),
		.ss1_valid_mess        ( ss1_valid_mess         ),
		//Sync_Stage3
		.ss2_barrier_valid     ( ss2_barrier_valid      ),
		.ss2_barrier           ( ss2_barrier            ),
		.ss3_barrier           ( ss3_barrier            ),
		.ss3_valid_barrier     ( ss3_valid_barrier      ),
		.ss2_valid_mem         ( ss2_valid_mem          ),
		.ss2_data_barrier_read ( ss2_data_barrier_read  ),
		.ss3_data_barrier_write( ss3_data_barrier_write )
	);

//  -----------------------------------------------------------------------
//  -- Synchronization Core - Stage3
//  -----------------------------------------------------------------------

	synchronization_core_stage3 Stage3(
		.clk                   ( clk                        ),
		.reset                 ( reset                      ),
		`ifdef SINGLE_CORE
		.job_terminated 	   ( job_terminated				),
		`endif
		//Sync_Stage1
		.ss3_id_barrier        ( ss3_id_barrier             ),
		//Sync_Stage2
		.ss2_barrier_valid     ( ss2_barrier_valid          ),
		.ss2_barrier           ( ss2_barrier                ),
		.ss3_barrier           ( ss3_barrier                ),
		.ss3_valid_barrier     ( ss3_valid_barrier          ),
		.ss2_valid_mem         ( ss2_valid_mem              ),
		.ss2_data_barrier_read ( ss2_data_barrier_read      ),
		.ss3_data_barrier_write( ss3_data_barrier_write     ),
		.ss3_release_mess      ( sc_release_mess_fifo       ),
		.ss3_release_valid     ( ni_release_mess_valid_fifo )

	);


endmodule