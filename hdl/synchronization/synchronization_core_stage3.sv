`include "synchronization_defines.sv"

module synchronization_core_stage3 
	(
		input clk,
		input reset,

		//SYNC_STAGE1
		output barrier_t ss3_id_barrier,

		//SYNC_STAGE2
		input  logic       ss2_barrier_valid,
		input  sync_info_t ss2_barrier,
		output sync_info_t ss3_barrier,
		output logic       ss3_valid_barrier,
		input  logic       ss2_valid_mem,

		`ifdef SINGLE_CORE
		output logic 	   job_terminated,
		`endif
		input  barrier_data_t ss2_data_barrier_read,
		output barrier_data_t ss3_data_barrier_write,

		//VIRTUAL NETWORK
		output sync_release_t ss3_release_mess,
		output logic          ss3_release_valid
	);
	barrier_data_t ss3_data_barrier_tmp;
	logic          can_issue_setup;
	logic          can_issue_account;

	assign can_issue_setup   = ( ss2_barrier.barrier_type== SETUP ) && ss2_barrier_valid;
	assign can_issue_account = ( ss2_barrier.barrier_type== ACCOUNT ) && ss2_barrier_valid;

	always_comb begin
		ss3_data_barrier_tmp=0;

		if( can_issue_setup ) begin
			if( ss2_valid_mem )begin
				ss3_data_barrier_tmp.cnt <= ss2_data_barrier_read.cnt + ss2_barrier.cnt_setup;
								ss3_data_barrier_tmp.mask_slave                 = ss2_data_barrier_read.mask_slave;
				
			end else begin
				ss3_data_barrier_tmp.cnt <= ss2_barrier.cnt_setup;
				ss3_data_barrier_tmp.mask_slave =0;
			end
		`ifndef SINGLE_CORE	
			if(ss2_barrier.id_barrier==0)begin
				ss3_data_barrier_tmp.mask_slave[`TILE_H2C_ID]= 1;
			end
		`endif
			ss3_data_barrier_tmp.is_setup  <=1'b1;
		end else if ( can_issue_account ) begin
			if ( ss2_valid_mem )begin
				ss3_data_barrier_tmp.cnt = ss2_data_barrier_read.cnt - 1'b1;
				ss3_data_barrier_tmp.mask_slave                 = ss2_data_barrier_read.mask_slave;
			end else begin
				ss3_data_barrier_tmp.cnt = - 1'b1;
				ss3_data_barrier_tmp.mask_slave   =0;
			end
			ss3_data_barrier_tmp.mask_slave[ss2_barrier.tile_id_source]= 1;
			ss3_data_barrier_tmp.is_setup                              = ss2_data_barrier_read.is_setup;
		end
	end
	always_ff @( posedge clk, posedge reset )begin
		if ( reset ) begin
			ss3_release_valid <= 1'b0;
			job_terminated <= 1'b0;
		end else begin

			if ( ss3_data_barrier_tmp.cnt==0 && ss3_data_barrier_tmp.is_setup==1'b1 )begin
					ss3_release_valid <= 1'b1;
					`ifdef SINGLE_CORE
						if(ss2_barrier.id_barrier == 0)
							job_terminated <= 1'b1;
					`endif
			end else begin
				ss3_release_valid <= 1'b0;
				job_terminated <= 1'b0;
			end
		end
	end

	always_ff @( posedge clk )begin

		if ( ss3_data_barrier_tmp.cnt==0 && ss3_data_barrier_tmp.is_setup==1'b1 )begin
			ss3_data_barrier_write      <= 0;
			ss3_valid_barrier           <= ss2_barrier_valid;
			ss3_barrier                 <= ss2_barrier;
			//Release
			ss3_release_mess.id_barrier <= ss2_barrier.id_barrier;
			ss3_release_mess.mask_slave <= ss3_data_barrier_tmp.mask_slave;
			//ss3_release_valid           <= 1'b1;
		end else begin
			ss3_data_barrier_write <= ss3_data_barrier_tmp;
			ss3_valid_barrier      <= ss2_barrier_valid;
			ss3_barrier            <= ss2_barrier;
			//No Release
			//ss3_release_valid      <=1'b0;

		end
	end


	assign ss3_id_barrier = ss3_barrier.id_barrier;
endmodule