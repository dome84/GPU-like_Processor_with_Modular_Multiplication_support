`ifndef __SYNCHRONIZATION_DEFINES
`define __SYNCHRONIZATION_DEFINES

`include "user_define.sv"
//`include "message_service_defines.sv"

`ifdef DIRECTORY_BARRIER

`define BARRIER_NUMB          (`TILE_COUNT) * 4 * `THREAD_NUMB
`define BARRIER_NUMB_FOR_TILE `THREAD_NUMB * 4

`else

`define BARRIER_NUMB          (`TILE_COUNT) * 4 * `THREAD_NUMB
`define BARRIER_NUMB_FOR_TILE (`TILE_COUNT) * 4 * `THREAD_NUMB

`endif

`ifdef SINGLE_CORE
		`define TILE_COUNT 2
`endif

typedef logic[$clog2(`BARRIER_NUMB) - 1 : 0] barrier_t;

`define BARRIER_SIZE_MEM          $clog2(`TILE_COUNT*`THREAD_NUMB) + (`TILE_COUNT-1) + 1

typedef struct packed{
	barrier_t id_barrier;
	logic [`TILE_COUNT - 1 : 0] mask_slave;
}sync_release_t;

`ifdef SINGLE_CORE
	//`define HOST_MESSAGE_LENGTH       ( $bits( boot_message_t ) )
`define HOST_MESSAGE_LENGTH       ( $bits( sync_release_t ) )
typedef logic [`HOST_MESSAGE_LENGTH - 1 : 0] message_data_t;

`define MESSAGE_TYPE_LENGTH       2

typedef enum logic [`MESSAGE_TYPE_LENGTH - 1 : 0] {
	BOOT = 0,
	SYNC = 1,
	DEBUG = 2
} message_type_t;

typedef struct packed {
	message_type_t message_type;
	message_data_t data;
} service_c2n_message_t;
`endif

typedef enum{
	START_SETUP,
	IDLE_SETUP,
	SETUP_SERVICE,
	WAIT_NET_SETUP,
	SEND_SETUP
}setup_interface_state_t;

typedef enum{
	START_RELEASE,
	IDLE_RELEASE,
	RELEASE_SERVICE,
	WAIT_HOST_RELEASE
}release_interface_state_t;

typedef enum logic [ 1 : 0 ]{
	SETUP   = 2'b00,
	ACCOUNT = 2'b01,
	RELEASE = 2'b10,
	ZERO    = 2'b11
} synch_type;

typedef struct packed{
	logic [$clog2(`TILE_COUNT*`THREAD_NUMB) - 1 : 0] cnt;
	logic [`TILE_COUNT - 1 : 0] mask_slave;
	logic is_setup;
}barrier_data_t;

typedef struct packed{
	barrier_t id_barrier;
	logic [$clog2(`TILE_COUNT*`THREAD_NUMB) - 1 : 0] cnt_setup;
	logic is_master;
}sync_setup_t;

typedef struct packed{
	barrier_t id_barrier;
	logic [$clog2(`TILE_COUNT) - 1 : 0] tile_id_source;
}sync_account_t;

typedef struct packed{
	barrier_t id_barrier;
	logic [$clog2(`TILE_COUNT) - 1 : 0] tile_id_source;
	logic [$clog2(`TILE_COUNT*`THREAD_NUMB) - 1 : 0] cnt_setup;
	synch_type barrier_type;
}sync_info_t;

`endif