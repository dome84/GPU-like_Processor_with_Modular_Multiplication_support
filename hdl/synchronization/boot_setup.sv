`include "user_define.sv"
`include "nuplus_define.sv"
`include "message_service_defines.sv"
`include "router_defines.sv"
`include "synchronization_defines.sv"

/*
 * Scritto da Fabio: dovrebbe fuzionare così, da quel che ho capito.
 * - se è valido l'ingresso dall'host, se è di sync va bene
 * - al prossimo dato in ingresso valido, si salvano i dati dall'host per formare il messaggio di setup
 * - quando la rete è pronta, allora si manda il messaggio di setup alla rete
 */

module boot_setup #(
		parameter ITEM_w = 32 // Input and output item width (control interface)
	)
	(
		input                         clk,
		input                         reset,
		//Host interface
		output logic                  sm_item_valid_o,
		output logic                  sm_item_avail_o,
		input  logic [ITEM_w - 1 : 0] sm_item_data_i,
		input  logic                  sm_item_valid_i,
		input  logic                  sm_item_avail_i,
		
		
		output logic [ITEM_w - 1 : 0] sm_item_data_o,
		output logic                  wait_sync,

		//Virtual Network
		output logic                 valid_arbiter,
		output service_c2n_message_t c2n_mes_setup,
		output logic                 c2n_mes_valid,
		output logic                 c2n_has_data,
		input  logic                 ni_network_available,
		output dest_valid_t          c2n_mes_setup_destinations_valid
	);

	setup_interface_state_t state_setup, next_state_setup;

	sync_setup_t    setup_message_to_net;
`ifndef SINGLE_CORE
	tile_address_t  destination, dest_tmp;
`endif
	host_messages_t message_host_in,item_data_o;
	logic           update;
	logic           has_data;
	logic   		c2n_mes_valid_next;
	logic           item_valid_o;
	logic 			update_wait_sync, reset_wait_sync;

`ifndef SINGLE_CORE
	dest_valid_t one_hot_destination;
`endif
	
	assign sm_item_data_o = 0;
	assign item_valid_o = 0;
	assign message_host_in                                                                        = host_messages_t'( sm_item_data_i );

	

//  -----------------------------------------------------------------------
//  -- Control Unit - Next State sequential
//  -----------------------------------------------------------------------
	always_ff @ ( posedge clk, posedge reset ) begin
		if ( reset )
			state_setup <= START_SETUP;
		else
			state_setup <= next_state_setup; // default is to stay in current state
	end

	always_comb begin
		update                               = 1'b0;
		update_wait_sync					 = 1'b0;
		reset_wait_sync						 = 1'b0;
		//wait_sync                            = 1'b1;
		valid_arbiter                        = 1'b1;
		c2n_mes_valid_next                   = 1'b0;
		sm_item_avail_o                      = 1'b0;
		next_state_setup                     = state_setup;
		unique case ( state_setup )
			START_SETUP : begin
				reset_wait_sync	 = 1'b0;
				//wait_sync        = 1'b0;
				valid_arbiter    = 1'b0;
				next_state_setup = IDLE_SETUP;
			end

			IDLE_SETUP : begin

				sm_item_avail_o = 1'b1;
				
				if ( sm_item_valid_i ) begin
					if(message_host_in == SYNC_COMMAND ) begin
						update_wait_sync = 1'b1;
						//wait_sync        = 1'b1;
						valid_arbiter    = 1'b1;
						next_state_setup =  SETUP_SERVICE ;
					end
				end else begin
					reset_wait_sync	 = 1'b1;
					//wait_sync        = 1'b0;
					valid_arbiter    = 1'b0;
					next_state_setup = IDLE_SETUP;
				end
			end

			SETUP_SERVICE : begin
				if ( sm_item_valid_i ) begin
					update = 1'b1;
					if( ni_network_available ) begin
						sm_item_avail_o                     = 1'b0;
						c2n_mes_valid_next                  = 1'b1;
						next_state_setup                    = SEND_SETUP;
					end else begin
						sm_item_avail_o  = 1'b0;
						next_state_setup = WAIT_NET_SETUP;
					end
				end else begin
					sm_item_avail_o  = 1'b1;
					next_state_setup = SETUP_SERVICE;
				end
			end
			WAIT_NET_SETUP : begin
				if( ni_network_available ) begin
					c2n_mes_valid_next                   = 1'b1;
					sm_item_avail_o                      = 1'b0;
					next_state_setup                     = SEND_SETUP;
				end else begin
					sm_item_avail_o  = 1'b0;
					next_state_setup = WAIT_NET_SETUP;
				end
			end
			SEND_SETUP : begin
				sm_item_avail_o  = 1'b0;
				next_state_setup = IDLE_SETUP;
			end
		endcase

	end

	always_ff @ ( posedge clk, posedge reset ) begin
		if( reset ) begin
			sm_item_valid_o <= 0;
			c2n_mes_valid   <= 0;
		end else begin
			sm_item_valid_o                  <= item_valid_o;
			c2n_mes_valid                    <= c2n_mes_valid_next;
		end
	end
	
//  -----------------------------------------------------------------------
//  -- Setup message composer
//  -----------------------------------------------------------------------

	// solo quando al secondo colpo di clock il dato in ingresso è valido aggiorno i dati nel messaggio di setup
	`ifdef DIRECTORY_BARRIER
	assign dest_tmp = sm_item_data_i[ $bits( tile_address_t )+$bits( tile_address_t )+$clog2( `TILE_COUNT*`THREAD_NUMB )+$clog2( `BARRIER_NUMB_FOR_TILE ): $bits( tile_address_t )+$clog2( `TILE_COUNT*`THREAD_NUMB )+$clog2( `BARRIER_NUMB_FOR_TILE )+1]; //TODO estrapoilare solo la parte Tile
	`else 
	assign dest_tmp = sm_item_data_i[$bits( tile_address_t )+$clog2( `TILE_COUNT ) : $clog2( `TILE_COUNT )+1]; // XXX posso usare SOLO Tile potenza di 2
	`endif
	always_ff @( posedge clk ) begin
		if( update ) begin
			setup_message_to_net.is_master  <= sm_item_data_i[0];
			setup_message_to_net.cnt_setup  <= sm_item_data_i[$clog2( `TILE_COUNT*`THREAD_NUMB ):1];
			destination                     <= dest_tmp;//sm_item_data_i[$bits( tile_address_t )+$clog2( `TILE_COUNT ):$clog2( `TILE_COUNT )+1]; // XXX posso usare SOLO Tile potenza di 2
			setup_message_to_net.id_barrier <= sm_item_data_i[$clog2( `BARRIER_NUMB )+$bits( tile_address_t )+$clog2( `TILE_COUNT*`THREAD_NUMB ): $bits( tile_address_t )+$clog2( `TILE_COUNT*`THREAD_NUMB )+1];
		end 
		
		if( update_wait_sync ) begin
			wait_sync <= 1'b1;
		end else if(reset_wait_sync) begin
			wait_sync <= 1'b0;
		end
	end

	idx_to_oh #(
		.NUM_SIGNALS( `TILE_COUNT ),
		.DIRECTION  ( "LSB0"   ),
		.INDEX_WIDTH( $clog2( `TILE_COUNT ) )
	)
	u_idx_to_oh (
		.one_hot( one_hot_destination ),
		.index  ( destination   )
	);
	
	assign c2n_mes_setup.message_type                                                             = SYNC;
	assign c2n_mes_setup.data[1:0]                                                                = SETUP ;
	assign c2n_mes_setup.data[$clog2( `BARRIER_NUMB )+1:2]                                        = setup_message_to_net.id_barrier ;
	assign c2n_mes_setup.data[`SETUP_IS_MASTER]                                                   = setup_message_to_net.is_master ;
	assign c2n_mes_setup.data[`SETUP_IS_MASTER+$clog2( `TILE_COUNT*`THREAD_NUMB):`SETUP_IS_MASTER+1]          = setup_message_to_net.cnt_setup;
	//assign c2n_mes_setup.data[$bits( message_data_t )-1:`SETUP_IS_MASTER+$clog2( `TILE_COUNT*`THREAD_NUMB )+1] = 0;
	
	assign c2n_mes_setup_destinations_valid                                                       = one_hot_destination;
	assign c2n_has_data                                                                           = has_data;

	//Utile per settare il bit has_data nella network per inviare un pacchetto più grande di 1 flit (64 bit)
	generate
		if ( $bits( service_c2n_message_t ) <= `PAYLOAD_W )
			assign has_data = 1'b0;
		else
			assign has_data = 1'b1;
	endgenerate
	


endmodule
